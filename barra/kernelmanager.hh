#ifndef KERNELLIST_HH
#define KERNELLIST_HH

#include <unisim/kernel/service/service.hh>
#include <unisim/service/interfaces/memory.hh>
#include <unisim/service/interfaces/resetable.hh>
#include <unisim/service/interfaces/runnable.hh>
#include <unisim/service/interfaces/typed_registers.hh>
#include <unisim/service/interfaces/scheduler.hh>
#include <unisim/service/interfaces/instruction_stats.hh>
#include <unisim/component/cxx/processor/tesla/interfaces.hh>
#include <unisim/component/cxx/processor/tesla/stats.hh>    // Argh
#include "gdev_interface.h"
#include "gdev_kernel.hh"
#include <unisim/component/cxx/scheduler/cuda_scheduler/cuda_scheduler.hh>


using unisim::service::interfaces::TypedRegisters;
using unisim::service::interfaces::Resetable;
using unisim::service::interfaces::Runnable;
using unisim::service::interfaces::Memory;
using unisim::service::interfaces::Scheduler;
using unisim::service::interfaces::InstructionStats;
using unisim::kernel::service::Client;
using unisim::kernel::service::Service;
using unisim::kernel::service::ServiceImport;
using unisim::kernel::service::ServiceExport;
using unisim::kernel::service::Object;
using unisim::component::cxx::processor::tesla::GPRID;
using unisim::component::cxx::processor::tesla::SMAddress;
using unisim::component::cxx::processor::tesla::ConfigurationRegisterID;
using unisim::component::cxx::processor::tesla::ActiveRegisterID;
using unisim::component::cxx::processor::tesla::ConstBaseRegisterID;
using unisim::component::cxx::processor::tesla::SamplerBase;
using unisim::component::cxx::processor::tesla::SamplerIndex;
using unisim::component::cxx::processor::tesla::Stats;
using unisim::kernel::service::Parameter;
using unisim::component::cxx::scheduler::cuda_scheduler::CUDAGrid;

template<class CONFIG>
struct KernelManager
{

  KernelManager();
  ~KernelManager();

  GdevKernel<CONFIG>* getKernel(struct gdev_kernel *k);
  void removeKernel(struct gdev_kernel *k);
  void removeAllKernels();

  std::map<uint64_t, GdevKernel<CONFIG>*>& getKernelsMap();


  private:
    std::map<uint64_t, GdevKernel<CONFIG>*> kernels;


};




template<class CONFIG>
KernelManager<CONFIG>::KernelManager(){

}

template<class CONFIG>
KernelManager<CONFIG>::~KernelManager(){

}

template<class CONFIG>
GdevKernel<CONFIG>*  KernelManager<CONFIG>::getKernel(struct gdev_kernel* kernel){
  uint64_t addr = (uint64_t)kernel;
  GdevKernel<CONFIG>* k = kernels[addr];
  if( k == NULL){
      k = new GdevKernel<CONFIG>(*kernel);
      kernels[addr] = k;
  }
  std::cout << "Insert kernel addr="<<addr << "    - "<< std::hex << k << std::endl;

  return k;
}

template<class CONFIG>
std::map<uint64_t, GdevKernel<CONFIG>*>& KernelManager<CONFIG>::getKernelsMap(){
    return kernels;
}


template<class CONFIG>
void KernelManager<CONFIG>::removeKernel(struct gdev_kernel* kernel){
  uint64_t addr = kernel;
  kernels.erase(addr);
}

template<class CONFIG>
void KernelManager<CONFIG>::removeAllKernels(){
  kernels.clear();
}

#endif
