/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_COMMAND_HH
#define UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_COMMAND_HH

#include <boost/shared_ptr.hpp>
#include "cuda.h"
#include "exception.hh"
#include <unisim/kernel/service/service.hh>
#include <unisim/service/interfaces/memory.hh>
#include <unisim/service/interfaces/resetable.hh>
#include <unisim/service/interfaces/runnable.hh>
#include <unisim/service/interfaces/typed_registers.hh>
#include <unisim/service/interfaces/scheduler.hh>
#include <unisim/service/interfaces/instruction_stats.hh>
#include <unisim/component/cxx/processor/tesla/interfaces.hh>
#include <unisim/component/cxx/processor/tesla/stats.hh>    // Argh
#include <unisim/component/cxx/scheduler/cuda_scheduler/cuda_scheduler.hh>
#include <unisim/component/cxx/memory/ram/memory.hh>

using boost::shared_ptr;
using unisim::service::interfaces::TypedRegisters;
using unisim::service::interfaces::Resetable;
using unisim::service::interfaces::Runnable;
using unisim::service::interfaces::Memory;
using unisim::service::interfaces::Scheduler;
using unisim::service::interfaces::InstructionStats;
using unisim::kernel::service::Client;
using unisim::kernel::service::Service;
using unisim::kernel::service::ServiceImport;
using unisim::kernel::service::ServiceExport;
using unisim::kernel::service::Object;
using unisim::component::cxx::processor::tesla::GPRID;
using unisim::component::cxx::processor::tesla::SMAddress;
using unisim::component::cxx::processor::tesla::ConfigurationRegisterID;
using unisim::component::cxx::processor::tesla::ActiveRegisterID;
using unisim::component::cxx::processor::tesla::ConstBaseRegisterID;
using unisim::component::cxx::processor::tesla::SamplerBase;
using unisim::component::cxx::processor::tesla::SamplerIndex;
using unisim::component::cxx::processor::tesla::Stats;
using unisim::kernel::service::Parameter;
using unisim::component::cxx::scheduler::cuda_scheduler::CUDAScheduler;
using unisim::component::cxx::scheduler::cuda_scheduler::CUDAGrid;

namespace unisim {
namespace component {
namespace cxx {
namespace command_processor {
namespace tesla {

template<class CONFIG>
struct Command
{
	virtual ~Command() {};
	virtual void execute() = 0;
	virtual std::string dump() =0;

};


template<class CONFIG>
struct SyncCommand : Command<CONFIG>
{
	SyncCommand(uint32_t* seqarray, uint32_t seq);
	virtual void execute();
	virtual std::string dump();

	private:
		uint32_t* sequences_array;
		uint32_t sequence;

};

template<class CONFIG>
struct LaunchGridCommand : Command<CONFIG>
{
  LaunchGridCommand(CUDAGrid<CONFIG>& grid, CUDAScheduler<CONFIG> * scheduler);
	virtual void execute();
	virtual std::string dump();

  private:
  	CUDAGrid<CONFIG>& grid;
  	CUDAScheduler<CONFIG> * scheduler;
};


template< class CONFIG>
struct MemCopyCommand : Command<CONFIG>
{
	MemCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, uint32_t size);
	virtual void execute() = 0;
	virtual std::string dump() =0;

	protected:
		Memory<typename CONFIG::address_t> * memory;
		struct gdev_ctx * context;
		uint32_t size;
};

template<class CONFIG>
struct H2DCopyCommand: public MemCopyCommand<CONFIG>
{
	H2DCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, typename CONFIG::address_t  dst_addr, void const* src_addr, uint32_t size);
	virtual void execute();
	virtual std::string dump();

private:
	typename CONFIG::address_t dst;
	void const* src;
};

template<class CONFIG>
struct D2DCopyCommand: public MemCopyCommand<CONFIG>
{
	D2DCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, typename CONFIG::address_t dst_addr, typename CONFIG::address_t src_addr, uint32_t size);
	virtual void execute();
	virtual std::string dump();

private:
	typename CONFIG::address_t dst;
	typename CONFIG::address_t src;
};

template<class CONFIG>
struct D2HCopyCommand: public MemCopyCommand<CONFIG>
{
	D2HCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, void * dst_addr, typename CONFIG::address_t src_addr, uint32_t size);
	virtual void execute();
	virtual std::string dump();

private:
	typename CONFIG::address_t src;
	void * dst;


};





} // end of namespace tesla
} // end of namespace command_processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
