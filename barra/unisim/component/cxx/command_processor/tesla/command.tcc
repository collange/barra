/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_COMMAND_TCC
#define UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_COMMAND_TCC

#include "command.hh"

namespace unisim {
namespace component {
namespace cxx {
namespace command_processor {
namespace tesla {



template<class CONFIG>
LaunchGridCommand<CONFIG>::LaunchGridCommand(CUDAGrid<CONFIG>& _grid, CUDAScheduler<CONFIG> * scheduler):
  grid(_grid), scheduler(scheduler)
{

}

template<class CONFIG>
void LaunchGridCommand<CONFIG>::execute(){
  scheduler->Schedule(grid);
}

template<class CONFIG>
std::string LaunchGridCommand<CONFIG>::dump(){
  return "Launch kernel";
}


template<class CONFIG>
SyncCommand<CONFIG>::SyncCommand(uint32_t* seqarray, uint32_t seq): sequences_array(seqarray), sequence(seq){

}

template<class CONFIG>
void SyncCommand<CONFIG>::execute(){
  sequences_array[sequence] = sequence;
}

template<class CONFIG>
std::string SyncCommand<CONFIG>::dump(){
  return "Synchronize";
}


///////////////////////////////////////////////
// MemCopyCommand

template<class CONFIG>
MemCopyCommand<CONFIG>::MemCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, uint32_t size)
  :memory(memory), context(ctx), size(size)
{}

//
////////////////////////////////////////////////


///////////////////////////////////////////////
// D2DCopyCommand

template<class CONFIG>
D2DCopyCommand<CONFIG>::D2DCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, typename CONFIG::address_t dst_addr, typename CONFIG::address_t src_addr, uint32_t size)
  : MemCopyCommand<CONFIG>(memory, ctx, size), dst(dst_addr), src(src_addr)
{}

template<class CONFIG>
void D2DCopyCommand<CONFIG>::execute(){
  std::vector<uint8_t> buffer(this->size);
  if(!this->memory->ReadMemory(this->src, &buffer[0], this->size)) {
    throw CudaException(CUDA_ERROR_INVALID_VALUE);
  }
  if(!this->memory->WriteMemory(this->dst, &buffer[0], this->size)) {
    throw CudaException(CUDA_ERROR_INVALID_VALUE);
  }
}

template<class CONFIG>
std::string D2DCopyCommand<CONFIG>::dump(){
  return "Copy Dev to Dev";
}

//
///////////////////////////////////////////////


///////////////////////////////////////////////
// D2HCopyCommand

template<class CONFIG>
D2HCopyCommand<CONFIG>::D2HCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, void * dst_addr, typename CONFIG::address_t src_addr, uint32_t size)
  : MemCopyCommand<CONFIG>(memory, ctx,size), dst(dst_addr), src(src_addr)
{}

template<class CONFIG>
void D2HCopyCommand<CONFIG>::execute(){
  if(!this->memory->ReadMemory(this->src, this->dst, this->size)) {
    throw CudaException(CUDA_ERROR_INVALID_VALUE);
  }
}

template<class CONFIG>
std::string D2HCopyCommand<CONFIG>::dump(){
    std::string str = "Copy Dev to Host";
    return str;
}

//
///////////////////////////////////////////////


///////////////////////////////////////////////
// H2DCopyCommand

template<class CONFIG>
H2DCopyCommand<CONFIG>::H2DCopyCommand(Memory<typename CONFIG::address_t> * memory, struct gdev_ctx *ctx, typename CONFIG::address_t dst_addr, void const* src_addr, uint32_t size)
  : MemCopyCommand<CONFIG>(memory, ctx, size), dst(dst_addr), src(src_addr)
{}

template<class CONFIG>
void H2DCopyCommand<CONFIG>::execute(){
  if(!this->memory->WriteMemory(this->dst, this->src, this->size)) {
    throw CudaException(CUDA_ERROR_INVALID_VALUE);
  }
}

template<class CONFIG>
std::string H2DCopyCommand<CONFIG>::dump(){
  return "Copy Host to Dev";
}
//
///////////////////////////////////////////////



} // end of namespace tesla
} // end of namespace command_processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
