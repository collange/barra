/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_STREAM_TCC
#define UNISIM_COMPONENT_CXX_COMMAND_PROCESSOR_TESLA_STREAM_TCC


#include "stream.hh"


namespace unisim {
namespace component {
namespace cxx {
namespace command_processor {
namespace tesla {

  template<class CONFIG>
  Stream<CONFIG>::Stream(CUDAScheduler<CONFIG> * scheduler, bool verbose):
  scheduler(scheduler), verbose(verbose)
  {
    worker = new boost::thread(boost::bind( &Stream::run, this ));
  }
  template<class CONFIG>
  Stream<CONFIG>::~Stream(){
    std::cout<< "Ending worker thread"<< std::endl;
    worker->interrupt();
  }


#if 0
  template<class CONFIG>
  Event* Stream<CONFIG>::CreateEvent(){
    return NULL;
  }

  template<class CONFIG>
  void Stream<CONFIG>::pushEvent(Event & event){

  }
#endif

  template<class CONFIG>
  void Stream<CONFIG>::pushLaunchGrid(CUDAGrid<CONFIG> & grid){
    LaunchGridCommand<CONFIG>* c = new LaunchGridCommand<CONFIG>(grid, scheduler);
    pushCommand(c);
  }

  template<class CONFIG>
  void Stream<CONFIG>::pushSync(uint32_t * seqarray, uint32_t seq){
    SyncCommand<CONFIG>* c = new SyncCommand<CONFIG>(seqarray, seq);
    pushCommand(c);
  }

//TODO : Remove context parameter (NULL)
  template<class CONFIG>
  void Stream<CONFIG>::pushD2DCopy(Memory<typename CONFIG::address_t> * memory, typename CONFIG::address_t  dst, typename CONFIG::address_t  src, uint32_t size){
    D2DCopyCommand<CONFIG>* c = new D2DCopyCommand<CONFIG>(memory, NULL, dst, src, size);
    pushCommand(c);
  }

  template<class CONFIG>
  void Stream<CONFIG>::pushH2DCopy(Memory<typename CONFIG::address_t> * memory, typename CONFIG::address_t  dst, void const* src, uint32_t size){
    H2DCopyCommand<CONFIG>* c = new H2DCopyCommand<CONFIG>(memory, NULL, dst, src, size);
    pushCommand(c);
  }

  template<class CONFIG>
  void Stream<CONFIG>::pushD2HCopy(Memory<typename CONFIG::address_t> * memory, void* dst, typename CONFIG::address_t  src, uint32_t size){
    D2HCopyCommand<CONFIG>* c = new D2HCopyCommand<CONFIG>(memory, NULL, dst, src, size);
    pushCommand(c);
  }

  template<class CONFIG>
  bool Stream<CONFIG>::isEmpty(){
    return cmd_queue.empty();
  }


  template<class CONFIG>
  void Stream<CONFIG>::pushCommand(Command<CONFIG>* c){
    boost::mutex::scoped_lock lock(mutex);
    cmd_queue.push(c);
    condition.notify_one();
  }

  template<class CONFIG>
  void Stream<CONFIG>::run(){
    while(1){
        Command<CONFIG>* c;
        {
          boost::mutex::scoped_lock lock(mutex);
          while(cmd_queue.empty())
            condition.wait(lock);
          c = cmd_queue.front();
          cmd_queue.pop();
       }
      if(verbose)
        std::cout<<"Processing Command : "<<c->dump()<<std::endl ;
      c->execute();
      delete(c);
    }
  }




} // end of namespace tesla
} // end of namespace command_processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim
#endif
