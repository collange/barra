/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_OPERATION_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_OPERATION_HH

//#include <unisim/component/cxx/processor/tesla/tesla_opcode.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src1.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src2.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src3.hh>
#include <unisim/component/cxx/processor/tesla/tesla_dest.hh>
#include <unisim/component/cxx/processor/tesla/tesla_control.hh>
#include <unisim/component/cxx/processor/tesla/liveness.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

enum Operand
{
	OpDest,
	OpSrc1,
	OpSrc2,
	OpSrc3
};

struct OperandID {
    Domain domain;
    int id;    // register number
    
    OperandID() : domain(DomainNone) {}
    OperandID(Domain d, int id) :
    	domain(d), id(id) {}
};

// Not execution units.
enum InstructionClass {
	ClassNOP,
	ClassMOV,
	ClassALU,	// IADD, LOP
	ClassAGU,	// ISCAD
	ClassXLU,	// IMAD, IMUL
	ClassFMAD,
	ClassFMUL,
	ClassSFU,
	ClassDFMA,
	ClassBR,
	ClassMEM,
	ClassSCI,	// Scalar instructions
	
	InstructionClassMax,
};

inline std::ostream & operator<<(std::ostream & st, OperandID const & op)
{
	switch(op.domain) {
	case DomainNone:
		st << "?";
		break;
	case DomainGPR:
		st << "r";
		break;
	case DomainAR:
		st << "a";
		break;
	case DomainPred:
		st << "p";
		break;
	case DomainConst:
		st << "c";
		break;
	case DomainShared:
		st << "s";
		break;
	case DomainGlobal:
		st << "g";
		break;
	case DomainLocal:
		st << "l";
		break;
	default:
		assert(false);
	}
	if(op.id >= 0) {
		st << op.id;
	}
	return st;
}

// TeslaOperation: base of all opcodes
// Only one instance of Operation per machine instruction in memory
// Unmutable
// Controls subfields decoding
template<class CONFIG>
struct TeslaOperation
{
	TeslaOperation(typename CONFIG::address_t addr, typename CONFIG::insn_t iw);
	~TeslaOperation();

	virtual void disasm(CPU<CONFIG> * cpu, TeslaInstruction<CONFIG> const * insn,
		ostream& buffer) = 0;
	virtual void classify(typename CONFIG::operationstats_t * stats) = 0;
	void initStats(typename CONFIG::operationstats_t * stats);
	virtual void enumerateOperands(unisim::component::cxx::processor::tesla::TeslaOperation<CONFIG> & op) = 0;

	bool OutputsPred();

	typename CONFIG::address_t address() const;
	
	//typedef typename tesla_isa::opcode::Operation<CONFIG> OpCode;
	typedef tesla_isa::dest::Operation<CONFIG> OperDest;
	typedef tesla_isa::src1::Operation<CONFIG> OperSrc1;
	typedef tesla_isa::src2::Operation<CONFIG> OperSrc2;
	typedef tesla_isa::src3::Operation<CONFIG> OperSrc3;
	typedef tesla_isa::control::Operation<CONFIG> OperControl;

	OperSrc1 * src1;
	OperSrc2 * src2;
	OperSrc3 * src3;
	OperDest * dest;
	OperControl * control;
	
	DataType op_type[4];
	InstructionClass iclass;
	
	static unsigned int const MaxInputs=6;	// FMA64 has 6 32-bit inputs
	static unsigned int const MaxOutputs=4;	// Load128 has 4 32-bit outputs
    static unsigned int const MaxLoads=1;
	OperandID inputs[MaxInputs];
	unsigned int inputCount;
	OperandID outputs[MaxOutputs];
	unsigned int outputCount;
	unsigned loads[MaxLoads];
	unsigned int loadCount;
	
	bool fallthrough;
	
	void AddInput(OperandID const & op);
	void AddGPRInput(RegType rt, unsigned int id);
	void AddOutput(OperandID const & op);
	void AddGPROutput(RegType rt, unsigned int id);
	void EnumerateOperands();
    void AddLoad(unsigned no);
    std::vector<Next<CONFIG> > findSuccessors (Context<CONFIG> context);

private:
	typename CONFIG::address_t addr;
	typename CONFIG::insn_t iw;
	
	static tesla_isa::src1::Decoder<CONFIG> src1_decoder;
	static tesla_isa::src2::Decoder<CONFIG> src2_decoder;
	static tesla_isa::src3::Decoder<CONFIG> src3_decoder;
	static tesla_isa::dest::Decoder<CONFIG> dest_decoder;
	static tesla_isa::control::Decoder<CONFIG> control_decoder;
	
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
