/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_DUAL_REGISTER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_DUAL_REGISTER_TCC

#include <unisim/component/cxx/processor/tesla/dual_register.hh>
#include <cassert>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


/***********
 * DualTag *
 ***********/

template<class CONFIG>
void DualTag<CONFIG>::SetScalar(bool s, mask_t mask)
{
	for (int i = 0; i != CONFIG::WARP_SIZE; ++i)
		if ( mask[i] )
			strided[i] = s;
}

template<class CONFIG>
void DualTag<CONFIG>::SetStrided(bool s, mask_t mask)
{
	for (int i = 0; i != CONFIG::WARP_SIZE; ++i)
		if ( mask[i] )
			strided[i] = s;
}

template<class CONFIG>
void DualTag<CONFIG>::SetStridedValue(uint32_t base, uint32_t stride)
{
	stridedComponent.Set(base, stride);
}

template<class CONFIG>
void DualTag<CONFIG>::SetStridedValue(DualTag<CONFIG>& other)
{
	stridedComponent.Set(other.stridedComponent);
}

template<class CONFIG>
void DualTag<CONFIG>::UpdateWrite(VectorRegister<CONFIG> regs, mask_t mask, bool b16, bool hilo)
{
	flattenNb = 0;
	bool isStrided;
	AffineMetaData<CONFIG> amd = regs.CheckStridedMMD(mask);
	DualTag<CONFIG> tmp(amd);

	if ( b16 ) 
		isStrided = tmp.IsStrided16(hilo, mask);
	else
		isStrided = amd.strided;

	if ( CONFIG::LAZY_FLATTEN ) {
		if ( (~mask).none() && isStrided ) {
			for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i) {
				stridedComponent[i].b = amd.recap[i].b;
				stridedComponent[i].s = amd.recap[i].s;
			}
			strided = mask;
		}
		else { //divergence
			for (int i = 0; i != CONFIG::WARP_SIZE; ++i) {
				flattenNb += !(strided[i] & amd.strided);
				strided[i] = strided[i] & amd.strided;
			}
		}
	}
	else {
		if ( CONFIG::VERTICAL_SIMT ) {
			if ( isStrided ) {
				flattenNb += (~strided & mask).count();
				strided = mask;
			}
			else {
				flattenNb += (strided & ~mask).count();
				strided &= ~mask;
			}
		}
		else {
			if ( (~mask & strided).none() && isStrided) {
				for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i) {
					stridedComponent[i].b = amd.recap[i].b;
					stridedComponent[i].s = amd.recap[i].s;
				}
				strided = mask;
			}
			else {
				for (int i = 0; i != CONFIG::WARP_SIZE; ++i)
					if ( mask[i] ) {
						flattenNb += !(strided[i] & amd.strided);
						strided[i] = strided[i] & amd.strided;
					}
			}
		}
	}
}

template<class CONFIG>
void DualTag<CONFIG>::UpdateRead(VectorRegister<CONFIG> regs, mask_t mask, bool b16, bool hilo)
{
	flattenNb = 0;
	bool isStrided;
	AffineMetaData<CONFIG> amd = regs.CheckStridedMMD(mask);

	if ( b16 ) {
		isStrided = IsStrided16(hilo, mask);
	}
	else  {
		isStrided = amd.strided;
	}

	if ( !CONFIG::VERTICAL_SIMT ) {
		if ( !isStrided || (~strided & mask).any()) {
			for (int i = 0; i != CONFIG::WARP_SIZE; ++i)
				if ( mask[i] ) {
					flattenNb += strided[i];
					strided[i] = 0;
				}
		}
	}
	else {
		if ( !isStrided ) {
			flattenNb += (strided & ~mask).count();
			strided &= ~mask;
		}
	}
}

template<class CONFIG>
bool DualTag<CONFIG>::MaskedEq(AffineMetaData<CONFIG> amd, mask_t mask) const
{
	bool ret = 1;
	for (int i = 0; i != CONFIG::WARP_SIZE; ++i) {
		if ( mask[i] )
			ret &= amd.recap[i/CONFIG::STRIDED_SEG_SIZE].s == stridedComponent[i/CONFIG::STRIDED_SEG_SIZE].s;
			ret &= amd.recap[i/CONFIG::STRIDED_SEG_SIZE].b == stridedComponent[i/CONFIG::STRIDED_SEG_SIZE].b;
	}
	return ret;
}

template<class CONFIG>
bool DualTag<CONFIG>::IsStrided16(bool const hi, mask_t const mask) const
{
	if ( (mask & ~strided).any() )
		return false;
	else {
		if (hi) {
			bool res = 1;
			for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
				res &= (stridedComponent[i].s & 0xffff0000) || !((((stridedComponent[i].b) & 0xffff) + (stridedComponent[i].s & 0xffff) * (CONFIG::STRIDED_SEG_SIZE - 1) ) & 0xffff0000);
			return res;
		}
		else {
			bool res = 1;
			for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
				res &= !(((stridedComponent[i].b & 0xffff) + (stridedComponent[i].s & 0xffff) * (CONFIG::STRIDED_SEG_SIZE - 1)) & 0xffff0000);
			return res;
		}
	}
}

template<class CONFIG>
bool DualTag<CONFIG>::IsScalar16(bool const hi, mask_t const mask) const
{
	if ( (mask & ~strided).any() )
		return false;
	else {
		if (hi) {
			bool res = 1;
			for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
				res &= !(stridedComponent[i].s & 0xffff0000) && (((stridedComponent[i].b & 0xffff) + (stridedComponent[i].s & 0xffff) * (CONFIG::STRIDED_SEG_SIZE - 1)) & 0xffff0000);
			return res;
		}
		else {
			bool res = 1;
			for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
				res &= !(stridedComponent[i].s & 0xffff);
			return res;
		}
	}
}

template<class CONFIG>
DualTag<CONFIG> DualTag<CONFIG>::Split(int hilo) const
{
	DualTag<CONFIG> res(*this);
	for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i) {
		if (hilo) {
			res.stridedComponent[i].b = stridedComponent[i].b >> 16;
			res.stridedComponent[i].s = stridedComponent[i].s >> 16;
		}
		else {
			res.stridedComponent[i].b = stridedComponent[i].b & 0xffff;
			res.stridedComponent[i].s = stridedComponent[i].s & 0xffff;
		}
	}
	res.strided = strided; // We will check it in an updateread/write to update the flatten counter
	return res;
}

template<class CONFIG>
bool DualTag<CONFIG>::Flatten(mask_t mask)
{
	bool ret = (mask & strided).any();
	flattenNb += (mask & strided).count();
	strided &= ~mask;
	return ret;
}

/*************************
 * DualTagVectorRegister *
 *************************/

template <class CONFIG, class Base>
DualTagVectorRegister<CONFIG, Base>::DualTagVectorRegister()
{
	SetScalar(false);
	SetStrided(false);
	SetScalar16(0, false);
	SetScalar16(1, false);
	SetStrided16(0, false);
	SetStrided16(1, false);
}

template <class CONFIG, class Base>
DualTagVectorRegister<CONFIG, Base>::DualTagVectorRegister(uint32_t val) :
	tag(val,0), Base(val)
{
	SetScalar(true);
	//SetStrided(true);
	//SetScalar16(0, true);
	//SetScalar16(1, true);
	//SetStrided16(0, true);
	//SetStrided16(1, true);
}

template <class CONFIG, class Base>
DualTagVectorRegister<CONFIG, Base>::DualTagVectorRegister(ThisType const & other) :
	tag(other.tag), Base(other)
{
	SetScalar(other.IsScalar());
	SetStrided(other.IsStrided());
	SetScalar16(false, other.IsScalar16(false));
	SetScalar16(true, other.IsScalar16(true));
	SetStrided16(false, other.IsStrided16(false));
	SetStrided16(true, other.IsStrided16(true));
}

template <class CONFIG, class Base>
DualTagVectorRegister<CONFIG, Base>::DualTagVectorRegister(Base const & other) :
	tag(0,0), Base(other)
{
	SetScalar(false);
	SetStrided(false);
	SetScalar16(0, false);
	SetScalar16(1, false);
	SetStrided16(0, false);
	SetStrided16(1, false);
}


template <class CONFIG, class Base>
template<class Base2>
DualTagVectorRegister<CONFIG, Base>::DualTagVectorRegister(DualTagVectorAddress<CONFIG, Base2> const & addr) :
	tag(addr.tag), Base(addr)
{
	SetScalar(addr.IsScalar());
	SetStrided(addr.IsStrided());
	SetScalar16(0, addr.IsScalar());
	SetScalar16(1, addr.IsScalar());
	SetStrided16(0, false);
	SetStrided16(1, false);
}

template <class CONFIG, class Base>
void DualTagVectorRegister<CONFIG, Base>::Write(ThisType const & vec, bitset<CONFIG::WARP_SIZE> mask)
{
	Base::Write(vec, mask);
	if(mask == ~bitset<CONFIG::WARP_SIZE>(0)) {
		SetScalar(vec.IsScalar());
		SetStrided(vec.IsStrided());
		SetScalar16(false, vec.IsScalar16(false));
		SetStrided16(false, vec.IsStrided16(false));
		SetScalar16(true, vec.IsScalar16(true));
		SetStrided16(true, vec.IsStrided16(true));
	}
	else if(mask != 0) {
		SetScalar(false);
		SetStrided(false);
		SetScalar16(false, false);
		SetStrided16(false, false);
		SetScalar16(true, false);
		SetStrided16(true, false);
	}
	tag = vec.tag;
}

template <class CONFIG, class Base>
void DualTagVectorRegister<CONFIG, Base>::Write16(ThisType const & vec,
	bitset<CONFIG::WARP_SIZE> mask, int hi)
{
	Base::Write16(vec, mask, hi);
	SetScalar(false);
	SetStrided(false);
	if(mask == ~bitset<CONFIG::WARP_SIZE>(0)) {
		SetScalar16(hi, vec.IsScalar16(false));
		SetStrided16(hi, vec.IsStrided16(false));
	}
	else if(mask != 0) {
		SetScalar16(hi, false);
		SetStrided16(hi, false);
	}
	tag = vec.tag;
	if ( hi ) {
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i) {
			tag.stridedComponent[i].b <<= 16;
			tag.stridedComponent[i].s <<= 16;
		}
	}
}

template <class CONFIG, class Base>
DualTagVectorRegister<CONFIG, Base> DualTagVectorRegister<CONFIG, Base>::Split(int hilo) const
{
	DualTagVectorRegister<CONFIG, Base> vr(Base::Split(hilo));
	vr.SetScalar(scalar16[hilo]);
	vr.SetScalar16(0, scalar16[hilo]);
	vr.SetStrided(strided16[hilo]);
	vr.SetStrided16(0, strided16[hilo]);
	vr.tag = tag.Split(hilo);
	return vr;
}

template <class CONFIG, class Base>
int DualTagVectorRegister<CONFIG, Base>::FlattenNb() const
{
	return tag.flattenNb;
}

template <class CONFIG, class Base>
std::ostream & operator << (std::ostream & os, DualTagVectorRegister<CONFIG, Base> const & r)
{
	os << (Base const &) r;
	if ( CONFIG::TRACE_STRIDED ) {
		os << std::endl;
		os << r.tag.strided << " ";
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i ) {
			os << std::hex << "b = " << r.tag.stridedComponent[i].b << ", s = " << r.tag.stridedComponent[i].s << "; " << std::dec ;
		}
	}
	return os;
}

/************************
 * DualTagVectorAddress *
 ************************/

template <class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base>::DualTagVectorAddress()
{
	SetScalar(false);
	SetStrided(false);
}

template <class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base>::DualTagVectorAddress(typename DualTagVectorAddress<CONFIG, Base>::address_t addr) :
	tag(addr,0), Base(addr)
{
	SetScalar(true);
	SetStrided(true);
}

template <class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base>::DualTagVectorAddress(Base const & other) :
	Base(other)
{
	SetScalar(false);
	SetStrided(false);
}

template <class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base>::DualTagVectorAddress(DualTagVectorAddress<CONFIG, Base> const & other) :
	tag(other.tag), Base(other)
{
	SetScalar(other.IsScalar());
	SetStrided(other.IsStrided());
}

template <class CONFIG, class Base>
template <class Base2>
DualTagVectorAddress<CONFIG, Base>::DualTagVectorAddress(DualTagVectorRegister<CONFIG, Base2> const & vr) :
	tag(vr.tag), Base(vr)
{
	SetScalar(vr.IsScalar());
	SetStrided(vr.IsStrided());
}

template <class CONFIG, class Base>
void DualTagVectorAddress<CONFIG, Base>::Write(ThisType const & vec, std::bitset<CONFIG::WARP_SIZE> mask)
{
	Base::Write(vec, mask);
	if(mask == ~bitset<CONFIG::WARP_SIZE>(0)) {
		SetScalar(vec.IsScalar());
		SetStrided(vec.IsStrided());
	}
	else if(mask != 0) {
		SetScalar(false);
		SetStrided(false);
	}
}

template <class CONFIG, class Base>
void DualTagVectorAddress<CONFIG, Base>::Reset()
{
	Base::Reset();
	SetScalar(false);
	SetStrided(false);
}

template <class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base> & DualTagVectorAddress<CONFIG, Base>::operator+=(ThisType const & other)
{
	Base::operator+=(other);
	SetScalar(IsScalar() && other.IsScalar());
	SetStrided(IsStrided() && other.IsStrided());
	return *this;
}

template<class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base> operator+(DualTagVectorAddress<CONFIG, Base> const & a, DualTagVectorAddress<CONFIG, Base> const & b)
{
	DualTagVectorAddress<CONFIG, Base> dest(operator+(Base(a), Base(b)));
	dest.SetScalar(a.IsScalar() && b.IsScalar());
	dest.SetStrided(a.IsStrided() && b.IsStrided());
	return dest;
}

template<class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base> operator*(unsigned int factor, DualTagVectorAddress<CONFIG, Base> const & addr)
{
	DualTagVectorAddress<CONFIG, Base> dest(operator*(factor, Base(addr)));
	dest.SetScalar(addr.IsScalar());
	dest.SetStrided(addr.IsStrided());
	return dest;
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
