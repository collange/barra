/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#include <unisim/component/cxx/processor/tesla/word_sectored_cache.hh>
#include <unisim/component/cxx/cache/cache.tcc>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


using namespace std;


template<class CONFIG, class WORD_SECTORED_CACHE>
std::bitset<CONFIG::WARP_SIZE> WordSectoredCache<CONFIG, WORD_SECTORED_CACHE>::LookupMasked(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats)
{
	typename WORD_SECTORED_CACHE::ADDRESS addr = access.addr;
	typename WORD_SECTORED_CACHE::ADDRESS line_base_addr;
	typename WORD_SECTORED_CACHE::ADDRESS block_base_addr;
	uint32_t index;
	uint32_t sector;
	uint32_t offset;
	uint32_t size_to_block_boundary;
	uint32_t way;
	CacheSet<WORD_SECTORED_CACHE> *set;
	CacheLine<WORD_SECTORED_CACHE> *line;
	CacheBlock<WORD_SECTORED_CACHE> *block;
	
	// Decode the address
	Cache::DecodeAddress(addr, line_base_addr, block_base_addr, index, sector, offset, size_to_block_boundary);

	access.line_base_addr = line_base_addr;
	access.block_base_addr = block_base_addr;
	access.index = index;
	access.sector = sector;
	access.offset = offset;
	access.size_to_block_boundary = size_to_block_boundary;
	access.set = set = &word_sectored_cache[index];
	access.line_hit = false;

	// Associative search
	for(way = 0; way < WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY; way++)
	{
		line = &(*set)[way];
		if(line->status.valid && line->GetBaseAddr() == line_base_addr)
		{
			// line hit: block may need a fill if not yet present in the line
			access.line_hit = true;
			access.line = line;
			access.line_to_evict = 0;
			access.way = way;
			block = &(*line)[sector];
			access.block = block;


			return access.block->status.vmask;
		}
	}

	// line miss
	access.line = 0;
	access.block = 0;
	mask_t result;
	result.reset();
	return result;
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::ChooseLineToEvict(CacheAccess<WORD_SECTORED_CACHE>& access)
{
	uint32_t way;
	uint32_t i;
	uint32_t n;
	uint32_t plru_bits = access.set->status.plru_bits;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
		
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
		
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(i = 0, way = 0, n = 0; n < WORD_SECTORED_CACHE::CACHE_LOG_ASSOCIATIVITY; n++)
	{
		uint32_t bi = (plru_bits >> i) & 1;
		way = (way << 1) | bi;
		i = (i << 1) + 1 + bi;
	}
	access.way = way;
	access.line_to_evict = &(*access.set)[way];
	assert(way < WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY);
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::EmuEvict(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats)
{
	if(access.line_to_evict->status.valid)
	{
		uint32_t sector;
		for(sector = 0; sector < CacheLine<WORD_SECTORED_CACHE>::BLOCKS_PER_LINE; sector++)
		{
			CacheBlock<WORD_SECTORED_CACHE>& block_to_evict = (*access.line_to_evict)[sector];
			if(block_to_evict.status.vmask.any() && block_to_evict.status.dirty)
			{
				stats.WBack();
				if(cpu.TraceCache())
				{
					cerr << dec << "    WordSectored WBack from way " << access.way << " from set " << access.index << endl;
				}
			}
			block_to_evict.status.vmask.reset();
			block_to_evict.status.dirty = false;
		}
	
		access.line_to_evict->status.valid = false;
	}
	access.line = access.line_to_evict;
	access.line_to_evict = 0;
}



template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::EmuFill(CacheAccess<WORD_SECTORED_CACHE>& access, mask_t mask, WordSectoredCacheStats<CONFIG> &stats)
{
	access.block = &(*access.line)[access.sector];
	access.line->status.valid = true;
	access.line->SetBaseAddr(access.line_base_addr);
	access.block->status.vmask = mask;
	access.block->status.dirty = false;
	stats.Fill();
}

template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::EmuWrite(CacheAccess<WORD_SECTORED_CACHE>& access, mask_t mask, WordSectoredCacheStats<CONFIG> &stats)
{
	access.block = &(*access.line)[access.sector];
	access.line->status.valid = true;
	access.line->SetBaseAddr(access.line_base_addr);
	access.block->status.vmask = mask;
	access.block->status.dirty = false;
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::UpdateReplacementPolicy(CacheAccess<WORD_SECTORED_CACHE>& access)
{
	uint32_t i;
	uint32_t n;
	uint32_t way = access.way;
	uint32_t plru_bits = access.set->status.plru_bits;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(n = WORD_SECTORED_CACHE::CACHE_LOG_ASSOCIATIVITY, i = 0; n != 0; n--)
	{
		uint32_t bi = (way >> (n - 1)) & 1;
		plru_bits = (plru_bits & ~(1 << i)) | ((1 ^ bi) << i);
		i = (i << 1) + 1 + bi;
	}

	access.set->status.plru_bits = plru_bits;
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::MRU(CacheAccess<WORD_SECTORED_CACHE>& access)
{
	uint32_t i;
	uint32_t n;
	uint32_t way = access.way;
	uint32_t plru_bits = access.set->status.plru_bits;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
	
	if(WORD_SECTORED_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(n = WORD_SECTORED_CACHE::CACHE_LOG_ASSOCIATIVITY, i = 0; n != 0; n--)
	{
		uint32_t bi = (way >> (n - 1)) & 1;
		plru_bits = (plru_bits & ~(1 << i)) | (bi << i);
		i = (i << 1) + 1 + bi;
	}

	access.set->status.plru_bits = plru_bits;
}



template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::EvictFill(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
	ChooseLineToEvict(access);
	EmuEvict(access, stats);
	EmuFill(access, mask);
	UpdateReplacementPolicy(access);
}



template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Invalidate()
{
	uint32_t index;
	for(index = 0; index < Cache::NUM_SETS; index++)
	{
		uint32_t way;
		CacheSet<WORD_SECTORED_CACHE>& set = word_sectored_cache[index];
		set.status.plru_bits = 0;
		for(way = 0; way < CacheSet<WORD_SECTORED_CACHE>::ASSOCIATIVITY; way++)
		{
			CacheLine<WORD_SECTORED_CACHE>& line = set[way];
			uint32_t sector;
			for(sector = 0; sector < CacheLine<WORD_SECTORED_CACHE>::BLOCKS_PER_LINE; sector++)
			{
				CacheBlock<WORD_SECTORED_CACHE>& block = line[sector];
				block.status.vmask.reset();
				block.status.dirty = false;
			}
			line.status.valid = false;
			line.SetBaseAddr(0);
		}
	}
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::PartialInvalidate(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
	access.block->status.vmask = access.block->status.vmask & ~mask;
    if (!access.block->status.vmask.any())
    {
        MRU(access);
        pressure--;
    }
}


template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Alloc(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
    if (!(access->vmask.any()))
    {
    	ChooseLineToEvict(access);
	    EmuEvict(access, stats);
        pressure++;
        stats.Miss();
        stats.WBack();
    }
    else if (!(access->vmask&~mask).any())
    {
        stats.PartialMiss();
    }
    else
    {
        stats.Hit();
    }
    
    stats.Pressure(pressure);
	UpdateReplacementPolicy(access);
}

template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Fill(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
    if (!(access->vmask.any()))
    {
        stats.Miss();
        stats.Fill();
    	ChooseLineToEvict(access);
	    EmuEvict(access, stats);
        pressure++;
    }
    else if (!(access->vmask&~mask).any())
    {
        stats.PartialMiss();
        stats.Fill();
    }
    else
    {
        stats.Hit();
    }
	EmuFill(access, mask);
    stats.Pressure(pressure);
	UpdateReplacementPolicy(access);
}

template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Read(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
    assert(!(~mask & access.vmask).any());
	UpdateReplacementPolicy(access);
}

template<class CONFIG, class WORD_SECTORED_CACHE>
void WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Write(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
    access.vmask |= mask;
	UpdateReplacementPolicy(access);
}

template<class CONFIG, class WORD_SECTORED_CACHE>
int WordSectoredCache<CONFIG,WORD_SECTORED_CACHE>::Pressure()
{
    return pressure;
}

template<class CONFIG, class WORD_SECTORED_CACHE>
std::bitset<CONFIG::WARP_SIZE> WordSectoredCache<CONFIG, WORD_SECTORED_CACHE>::Lookup(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask)
{
    return LookupMasked(access, stats);
}



} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim
