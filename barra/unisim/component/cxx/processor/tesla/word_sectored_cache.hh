/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_WORD_SECTORED_CACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_WORD_SECTORED_CACHE_HH

#include <unisim/component/cxx/cache/cache.hh>
#include <unisim/component/cxx/processor/tesla/local_cache.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
class WordSectoredCacheStats;


using unisim::component::cxx::cache::CacheBlock;
using unisim::component::cxx::cache::CacheLine;
using unisim::component::cxx::cache::CacheSet;


template<class CONFIG, class WORD_SECTORED_CACHE>
class WordSectoredCache
{
public:
	typedef	VectorRegister<CONFIG> VecReg;
	typedef typename WORD_SECTORED_CACHE::address_t address_t;
	typedef typename cxx::cache::Cache<WORD_SECTORED_CACHE> Cache;
	typedef std::bitset<CONFIG::WARP_SIZE> mask_t;

	void EvictFill(CacheAccess<WORD_SECTORED_CACHE>& affacess, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);

	mask_t LookupMasked(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats);

	void ChooseLineToEvict(CacheAccess<WORD_SECTORED_CACHE>& access);
	void EmuEvict(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats);
	void EmuFill(CacheAccess<WORD_SECTORED_CACHE>& access, mask_t mask, WordSectoredCacheStats<CONFIG> &stats);
	void EmuWrite(CacheAccess<WORD_SECTORED_CACHE>& access, mask_t mask, WordSectoredCacheStats<CONFIG> &stats);
	
	void UpdateReplacementPolicy(CacheAccess<WORD_SECTORED_CACHE>& access);
	void MRU(CacheAccess<WORD_SECTORED_CACHE>& access);

	void Invalidate();

	WordSectoredCache(CPU<CONFIG>& my_cpu) : cpu(my_cpu), pressure(0) {  };

	void PartialInvalidate(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);
    void Alloc(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask); //Lookup before Alloc

    void Fill(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);

    void Read(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);
    void Write(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);

    int Pressure();
    std::bitset<CONFIG::WARP_SIZE> Lookup(CacheAccess<WORD_SECTORED_CACHE>& access, WordSectoredCacheStats<CONFIG> &stats, mask_t mask);
private:
	Cache word_sectored_cache;
	CPU<CONFIG>& cpu;
    int pressure;
};









} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
