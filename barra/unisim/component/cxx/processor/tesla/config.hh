/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_CONFIG_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_CONFIG_HH

#include <unisim/component/cxx/processor/tesla/simfloat.hh>
#include <unisim/component/cxx/processor/tesla/hostfloat/hostfloat.hh>
#include <unisim/component/cxx/processor/tesla/stats.hh>
#include <unisim/component/cxx/processor/tesla/vectorfp32.hh>
#include <unisim/component/cxx/processor/tesla/tesla2_flow.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
struct ImplicitFlow;

template<class CONFIG>
struct NimtFlow;

template<class CONFIG>
struct AVCL0
{
	struct AFFINE
	{
		typedef uint32_t address_t;
		typedef uint32_t ADDRESS;


		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			std::bitset<CONFIG::WARP_SIZE> vmask;
			bool dirty;
		};

		static const bool ENABLED = true;
		static const uint32_t DECOMPRESSION_FACTOR = 8;
		static const uint32_t PHYSICAL_SIZE = 16;
		static const uint32_t CACHE_SIZE = DECOMPRESSION_FACTOR * PHYSICAL_SIZE * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 16 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY; // 4-way set associative
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 3; // 8 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 8 blocks per line
	};
	struct WORD_SECTORED
	{
		typedef uint32_t address_t;
		typedef uint32_t ADDRESS;


		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			std::bitset<CONFIG::WARP_SIZE> vmask;
            bool dirty;
		};

		static const uint32_t CACHE_SIZE = 32 * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 128 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY;
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 0; // 1 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 1 blocks per line
	};

};

template<class CONFIG>
struct AVCL1
{
	struct AFFINE
	{
		typedef uint32_t address_t;
		typedef uint32_t ADDRESS;

		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			std::bitset<CONFIG::WARP_SIZE> vmask;
			bool dirty;
		};

		static const bool ENABLED = true;
		static const uint32_t DECOMPRESSION_FACTOR = 8;
		static const uint32_t PHYSICAL_SIZE = 16;
		static const uint32_t CACHE_SIZE = DECOMPRESSION_FACTOR * PHYSICAL_SIZE * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 16 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY; // 4-way set associative
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 3; // 8 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 8 blocks per line
	};
	struct LOCAL
	{
		typedef uint32_t address_t;
		typedef uint32_t ADDRESS;

		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			bool valid;
			bool dirty;
		};

		static const uint32_t CACHE_SIZE = 32 * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 128 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY;
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 0; // 1 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 1 blocks per line
	};

};

struct BaseConfig
{
	typedef BaseConfig Self;

	typedef uint32_t address_t;             // 32-bit effective address
	typedef uint64_t virtual_address_t;     // only 32 bits (G80) or 40 bits (GT200) are used, all remaining bits *must* be set to zero
	typedef uint32_t physical_address_t;    // 32-bit physical address
	typedef uint32_t reg_t;                 // register type
	typedef uint64_t insn_t;				// instruction word
//	static const uint32_t MEMORY_PAGE_SIZE = 4096;
	
//	typedef FloatDAZFTZ<SoftFloatIEEE> float_t;
	typedef hostfloat::TeslaBinary32 float_t;
	typedef FloatDAZFTZ<SoftHalfIEEE> half_t;
	
	//typedef TeslaOperation<BaseConfig> operation_t;
	typedef VectorFP32<BaseConfig> vfp32;
	typedef Stats<BaseConfig> stats_t;
	typedef OperationStats<BaseConfig> operationstats_t;

	
//	typedef ImplicitFlow<BaseConfig> control_flow_t;
//	typedef TeslaFlow<BaseConfig> control_flow_t;
	typedef NimtFlow<BaseConfig> control_flow_t;

	static uint32_t const LOG_WARP_SIZE = 5;
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const LOG_MAX_WARPS = 10 - LOG_WARP_SIZE;	// 1024 threads
	static uint32_t const MAX_WARPS_PER_BLOCK = (16 * 32) / WARP_SIZE;

	static uint32_t const MAX_WARPS = 1 << LOG_MAX_WARPS; //(32 * 32) / WARP_SIZE;		// Fermi
	static uint32_t const MAX_VGPR = 512;
	static uint32_t const MAX_CTAS = 8;

	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const STACK_DEPTH = 100;	// Random value
	static uint32_t const MAX_ADDR_REGS = 4;
	static uint32_t const MAX_PRED_REGS = 4;
	static uint32_t const MAX_SAMPLERS = 16;
	
	static uint32_t const TRANSACTION_SIZE = 16;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	
	// Granularity of scalar/strided detection
	static uint32_t const STRIDED_SEG_SIZE = 16;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;

	
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;

	static uint32_t const NIMT_PHASES = 2;

	// Memory layout
	static address_t const CODE_START = 0x10000000;
	static address_t const CODE_END = 0x20000000;
	// 16 * 64 K
	static address_t const CONST_START = 0x20000000;	// -> 0x20100000
	static uint32_t const CONST_SEG_SIZE = 64 * 1024;
	static uint32_t const CONST_SEG_NUM = 16;

	static address_t const SHARED_START = 0x20100000;	// -> 0x20104000
	static uint32_t const SHARED_SIZE = 16 * 1024;
	
	static address_t const LOCAL_START = 0x30000000;
	static address_t const GLOBAL_START = 0x40000000;
	static address_t const GLOBAL_RELOC_START = 0x80000000;
	static uint32_t const LOCAL_WORDS_PER_THREAD = 0x00000100;	// 1KB ought to be enough for anyone

	static size_t const LOCAL_SIZE = GLOBAL_START - LOCAL_START;
	static size_t const GLOBAL_SIZE = GLOBAL_RELOC_START - GLOBAL_START;

	
	static int const COMPUTE_CAP_MAJOR = 1;
	static int const COMPUTE_CAP_MINOR = 1;
	static int const SHADER_CLOCK_KHZ = 1000000;
	static int const CORE_COUNT = 1;
	
	static bool const CAP_GLOBALATOMICS = false;
	static bool const CAP_SMLOCKS = false;
	static bool const CAP_DP = true;

	// Default values, can be overriden at run-time
	static bool const TRACE_INSN = false;
	static bool const TRACE_MASK = false;
	static bool const TRACE_REG = false;
	static bool const TRACE_REG_FLOAT = false;
	static bool const TRACE_LOADSTORE = false;
	static bool const TRACE_BRANCH = false;
	static bool const TRACE_SYNC = false;
	static bool const TRACE_RESET = false;
	static bool const TRACE_DEPS = true;
	static bool const TRACE_CACHE = true;
    static bool const TRACE_LIVENESS = false;
    static bool const TRACE_STRIDED = true;

	static bool const STAT_SCALAR_REG = true;
	static bool const STAT_STRIDED_REG = true;
	static bool const STAT_STRIDED_REG_LOCAL = true;
	static bool const STAT_DUAL_REG = true;
	static bool const STAT_SIMTIME = true;
	static bool const STAT_FPDOMAIN = true;
	static bool const STAT_TRANSACTIONWIDTH = true;
	static bool const STAT_NIMT = true;
	static bool const STAT_LIVENESS = true;
	static bool const STAT_REGISTER_CACHE = true;
	static bool const STAT_SIMILARITY = true;
	static bool const STAT_SCALAR_INSN = false;
	static bool const STAT_DEFUSEDISTANCE = true;

	static bool const USE_FLATTEN = false;
	static bool const LAZY_FLATTEN = false;
	static bool const VERTICAL_SIMT = false; // à tort ?
	static bool const MASKED_STRIDED_REGS = true;
	static bool const STRIDED_REG_LOCAL_ALIGNMENT = false;	// makes alignement mandatory for CheckStridedMasked(...)
															// it will return false if any of the bases in the warp isn't aligned
	static bool const STRIDED_REG_PAIRWISE = true;		// Stride estimation based on pair-wise comparisons

	static bool const CACHE_ENABLED = false;
	static bool const AFFINE_CACHE_ENABLED = false;
	
	static bool const DEBUG_NONAN = false;

	// Architectural registers per thread
    static unsigned const MAX_GPRS = 128;

	enum AddressMapping {
		AddressMappingPacked,
		AddressMappingSparse,
		AddressMappingSkewedSum,
		AddressMappingSkewedXorRev,
	};
	static AddressMapping const LOCAL_ADDRESS_MAPPING = AddressMappingSkewedXorRev; //AddressMappingSkewedXorRev;

	enum LaneMapping {
		LaneMappingLinear,
		LaneMappingReverseOdd,
		LaneMappingXorRev,
		LaneMappingReverseHalf,
		LaneMappingXor,
		LaneMappingXorm1,
	};
	static LaneMapping const LANE_MAPPING = LaneMappingLinear;
//	static LaneMapping const LANE_MAPPING = LaneMappingReverseOdd;
//	static LaneMapping const LANE_MAPPING = LaneMappingXorRev;
//	static LaneMapping const LANE_MAPPING = LaneMappingXor;
};


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
