/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_INSTRUCTION_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_INSTRUCTION_TCC

#include <unisim/component/cxx/processor/tesla/instruction.hh>
#include <unisim/component/cxx/processor/tesla/tesla_opcode.tcc>
#include <unisim/component/cxx/processor/tesla/tesla_dest.tcc>
#include <unisim/component/cxx/processor/tesla/tesla_src1.tcc>
#include <unisim/component/cxx/processor/tesla/tesla_src2.tcc>
#include <unisim/component/cxx/processor/tesla/tesla_src3.tcc>
#include <unisim/component/cxx/processor/tesla/tesla_control.tcc>

#include <unisim/component/cxx/processor/tesla/tesla_instruction.tcc>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


template <class CONFIG>
tesla_isa::opcode::Decoder<CONFIG> InstructionBase<CONFIG>::op_decoder;

template <class CONFIG>
InstructionBase<CONFIG>::InstructionBase(CPU<CONFIG> * cpu, typename CONFIG::address_t addr) :
	cpu(cpu), addr(addr), stats(0), scalarInsn(1)
{
}

template <class CONFIG>
InstructionBase<CONFIG>::InstructionBase() :
	cpu(0), addr(0), iw(0), stats(0), scalarInsn(1)
{
	// I hate containers
}

template <class CONFIG>
void InstructionBase<CONFIG>::SetCPU(CPU<CONFIG> * cpu)
{
	this->cpu = cpu;
}


template <class CONFIG>
VectorRegister<CONFIG> const & InstructionBase<CONFIG>::Temp(unsigned int i) const
{
	assert(i < TempCount);
	return temp[i];
}

template <class CONFIG>
VectorRegister<CONFIG> & InstructionBase<CONFIG>::Temp(unsigned int i)
{
	assert(i < TempCount);
	return temp[i];
}

template <class CONFIG>
VectorRegister<CONFIG> & InstructionBase<CONFIG>::GetSrc1()
{
	return temp[TempSrc1];
}

template <class CONFIG>
VectorRegister<CONFIG> & InstructionBase<CONFIG>::GetSrc2()
{
	return temp[TempSrc2];
}

template <class CONFIG>
VectorRegister<CONFIG> & InstructionBase<CONFIG>::GetSrc3()
{
	return temp[TempSrc3];
}

template <class CONFIG>
VectorRegister<CONFIG> & InstructionBase<CONFIG>::GetDest()
{
	return temp[TempDest];
}


// For load/store instructions, no subword access
template <class CONFIG>
void InstructionBase<CONFIG>::ReadBlock(int dest, DataType dt)
{
	switch(dt)
	{
	case DT_U8:
	case DT_S8:
	case DT_U16:
	case DT_S16:
	case DT_U32:
	case DT_S32:
	case DT_F32:
		Temp(0) = cpu->GetGPR(dest);
		break;
	case DT_U64:
		// Needs to be aligned
		assert(dest % 2 == 0);
		Temp(0) = cpu->GetGPR(dest);
		Temp(1) = cpu->GetGPR(dest + 1);
		break;
	case DT_U128:
		assert(dest % 4 == 0);
		Temp(0) = cpu->GetGPR(dest);
		Temp(1) = cpu->GetGPR(dest + 1);
		Temp(2) = cpu->GetGPR(dest + 2);
		Temp(3) = cpu->GetGPR(dest + 3);
		break;
	default:
		assert(false);
	}
	Temp(0).UpdateRead(Mask());
	stats->RegRead(&Temp(0), dt,Mask(),cpu,dest);
}

template <class CONFIG>
void InstructionBase<CONFIG>::WriteBlock(int reg, DataType dt)
{
	bitset<CONFIG::WARP_SIZE> mask = Mask();
	switch(dt)
	{
	case DT_U8:
	case DT_S8:
	case DT_U16:
	case DT_S16:
	case DT_U32:
	case DT_S32:
	case DT_F32:
		// Overwrite whole register
		cpu->GetGPR(reg).Write(Temp(0), mask);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(reg, cerr);
		}
		break;
	case DT_U64:
		// Needs to be aligned
		assert(reg % 2 == 0);
		cpu->GetGPR(reg).Write(Temp(0), mask);
		cpu->GetGPR(reg + 1).Write(Temp(1), mask);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(reg, cerr);
			cpu->DumpGPR(reg + 1, cerr);
		}
		break;
	case DT_U128:
		assert(reg % 4 == 0);
		cpu->GetGPR(reg).Write(Temp(0), mask);
		cpu->GetGPR(reg + 1).Write(Temp(1), mask);
		cpu->GetGPR(reg + 2).Write(Temp(2), mask);
		cpu->GetGPR(reg + 3).Write(Temp(3), mask);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(reg, cerr);
			cpu->DumpGPR(reg + 1, cerr);
			cpu->DumpGPR(reg + 2, cerr);
			cpu->DumpGPR(reg + 3, cerr);
		}
		break;
	default:
		assert(false);
	}
	stats->RegWrite(&Temp(0), dt, mask, cpu, reg);
    stats->RegKill(mask,cpu,addr);
}


// For GP instructions, loads from 16 to 64-bit regs
template <class CONFIG>
void InstructionBase<CONFIG>::ReadReg(int reg, int tempbase, RegType rt)
{
	switch(rt)
	{
	case RT_U16:
		{
		uint32_t rnum = (reg >> 1);
		uint32_t hilo = (reg & 1);
		cpu->GetGPR(rnum).UpdateRead(Mask(), 1, hilo);
		Temp(tempbase) = cpu->GetGPR(rnum).Split(hilo);
		Temp(tempbase).tag = cpu->GetGPR(rnum).Split(hilo).tag;
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		readRegisters.push_back(rnum);
		}
		break;
	case RT_U32:
		cpu->GetGPR(reg).UpdateRead(Mask());
		Temp(tempbase) = cpu->GetGPR(reg);
		Temp(tempbase).tag = cpu->GetGPR(reg).tag;
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		readRegisters.push_back(reg);
		break;
	case RT_U64:
		// Needs to be aligned
		assert(reg % 2 == 0);
		cpu->GetGPR(reg).UpdateRead(Mask());
		cpu->GetGPR(reg + 1).UpdateRead(Mask());
		Temp(tempbase) = cpu->GetGPR(reg);
		Temp(tempbase + 1) = cpu->GetGPR(reg + 1);
		Temp(tempbase).tag = cpu->GetGPR(reg).tag;
		Temp(tempbase + 1).tag = cpu->GetGPR(reg + 1).tag;
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		ScalarInsn() &= Temp(tempbase + 1).tag.IsStrided(Mask());
		readRegisters.push_back(reg);
		readRegisters.push_back(reg + 1);
		break;
	default:
		assert(false);
	}
	stats->RegRead(&Temp(tempbase), RegTypeToDataType(rt), Mask(), cpu, reg);
}

template <class CONFIG>
void InstructionBase<CONFIG>::WriteReg(int reg, int tempbase, DataType dt,
	bitset<CONFIG::WARP_SIZE> mask)
{
	Temp(tempbase).UpdateWrite(mask);
	switch(dt)
	{
	case DT_U16:
	case DT_S16:
		{
		uint32_t rnum = (reg >> 1);
		uint32_t hilo = (reg & 1);
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		if ( Class() == ClassSCI )
			Temp(tempbase).UpdateWrite(mask, 1);
		cpu->GetGPR(rnum).Write16(Temp(tempbase), mask, hilo);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(rnum, cerr);
		}
		}
		break;
	case DT_U32:
	case DT_S32:
	case DT_F32:
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		Temp(tempbase).tag.strided = ~std::bitset<CONFIG::WARP_SIZE>(0);
		if ( Class() == ClassSCI )
			Temp(tempbase).UpdateWrite(mask);
		cpu->GetGPR(reg).Write(Temp(tempbase), mask);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(reg, cerr);
		}
		break;
	case DT_U64:
		assert(reg % 2 == 0);
		ScalarInsn() &= Temp(tempbase).tag.IsStrided(Mask());
		if ( Class() == ClassSCI )
			Temp(tempbase).UpdateWrite(mask);
		cpu->GetGPR(reg).Write(Temp(tempbase), mask);
		cpu->GetGPR(reg + 1).Write(Temp(tempbase), mask);
		if(cpu->TraceReg()) {
			cpu->DumpGPR(reg, cerr);
			cpu->DumpGPR(reg + 1, cerr);
		}
		break;
	default:
		assert(false);
	}
	readRegisters.clear();
	stats->RegWrite(&Temp(tempbase), dt, mask, cpu, reg);
    stats->RegKill(mask,cpu,addr);
}

template <class CONFIG>
void InstructionBase<CONFIG>::SetDest(VectorRegister<CONFIG> const & value)
{
	Temp(TempDest) = value;
}

template <class CONFIG>
bitset<CONFIG::WARP_SIZE> InstructionBase<CONFIG>::Mask() const
{
	return mask;
}

template <class CONFIG>
typename CONFIG::operationstats_t * InstructionBase<CONFIG>::Stats()
{
	return stats;
}


template<class CONFIG>
bool InstructionBase<CONFIG>::Flatten()
{
	bool ret = 0;

	temp[TempSrc1].Flatten(this->Mask());
	temp[TempSrc2].Flatten(this->Mask());
	temp[TempSrc3].Flatten(this->Mask());

	for ( std::list<int>::iterator it = readRegisters.begin(); it != readRegisters.end(); ++it) {
		if ( cpu->GetGPR(*it).Flatten(Mask()) ) {
			ret = 1;
		}
	} 
	readRegisters.clear();

	return ret;
}

template <class CONFIG>
InstructionClass InstructionBase<CONFIG>::Class() const 
{
	if ( CONFIG::STAT_SCALAR_INSN ) {
		bool scalar;
		switch (operation->iclass) {
			case ClassMOV:
			case ClassALU:
			case ClassAGU:
			case ClassXLU:
				if (ScalarInsn()) {
					return ClassSCI;
				}
				break;
			default:
				break;
		}
	}

	return operation->iclass;
}


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
