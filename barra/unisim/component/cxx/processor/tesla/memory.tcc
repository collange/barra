/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_MEMORY_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_MEMORY_TCC

// Declarations in cpu.hh

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

// High-level functions

template <class CONFIG>
VectorAddress<CONFIG> CPU<CONFIG>::LocalAddress(VectorAddress<CONFIG> const & addr, unsigned int segment)
{
	// Slow case: scatter/gather
	assert(segment == 0);
	// Local memory interleaved.
	// TODO: check bounds
	VecAddr translated_addr;
	for(unsigned int i = 0; i != WARP_SIZE; ++i) {
		translated_addr[i] = LocalAddress(addr[i], segment) + 4 * i;
	}
	return translated_addr;
}

static uint32_t brev(uint32_t src, unsigned int bits)
{
	uint32_t dest = 0;
	for(unsigned int i = 0; i != bits; ++i)
	{
		dest = (dest << 1) | (src & 1);
		src >>= 1;
	}
	return dest;
}

template <class CONFIG>
typename CONFIG::address_t CPU<CONFIG>::LocalAddress(address_t addr, unsigned int segment)
{
	// Fast case: load/store
	assert(segment == 0);
	uint32_t byte_offset = addr & 3;
	addr >>= 2;	// Now in words

	assert(addr < CONFIG::LOCAL_WORDS_PER_THREAD);

	// Local memory interleaved.
	switch(CONFIG::LOCAL_ADDRESS_MAPPING)
	{
	case CONFIG::AddressMappingPacked:
	{
		// TODO: num_warp can de different for last block!
		// Across all threads in the GPU * 32-bit? (rounded to warp size)
		unsigned int chunk_size = core_count * num_warps * WARP_SIZE * 4;
		address_t chunk_offset_base = (coreid * num_warps + current_warpid) * WARP_SIZE * 4 + CONFIG::LOCAL_START;

		return chunk_offset_base + addr * chunk_size + byte_offset;
	}
	case CONFIG::AddressMappingSparse:
	{
		// | SMID |Local@| Warp|  TID| B|
		// |------|------|-----|-----|--|
		return 4 * WARP_SIZE * (current_warpid +
		                        MAX_WARPS * (addr +
		                                     CONFIG::LOCAL_WORDS_PER_THREAD * coreid)) + byte_offset + CONFIG::LOCAL_START;
	}
	case CONFIG::AddressMappingSkewedSum:
	{
		uint32_t skewed_wid = (current_warpid + addr) % MAX_WARPS;
		return 4 * WARP_SIZE * (skewed_wid +
		                        MAX_WARPS * (addr +
		                                     CONFIG::LOCAL_WORDS_PER_THREAD * coreid)) + byte_offset + CONFIG::LOCAL_START;
	}
	case CONFIG::AddressMappingSkewedXorRev:
	{
		uint32_t skewed_wid = current_warpid ^ brev(addr, CONFIG::LOG_MAX_WARPS);
		assert(skewed_wid < (1 << CONFIG::LOG_MAX_WARPS));
		return 4 * WARP_SIZE * (skewed_wid |
		                        ((addr + CONFIG::LOCAL_WORDS_PER_THREAD * coreid) << CONFIG::LOG_MAX_WARPS)) + byte_offset + CONFIG::LOCAL_START;

	}
	default:
		assert(false);
	}
}


template <class CONFIG>
VectorRegister<CONFIG> CPU<CONFIG>::ReadConstant(VectorRegister<CONFIG> const & addr, uint32_t seg)
{
	assert(seg < CONFIG::CONST_SEG_NUM);
	VecReg v;
	std::bitset<CONFIG::WARP_SIZE> mask;
	mask.set();

	// Gather(VecAddr(addr), v, mask, 2, 1,
	// 	CONFIG::CONST_START + seg * CONFIG::CONST_SEG_SIZE, DomainConst);
	Gather(VecAddr(addr), v, mask, 2, 1,
		const_base[seg], DomainConst);
	return v;

}

template <class CONFIG>
VectorRegister<CONFIG> CPU<CONFIG>::ReadConstant(unsigned int addr, uint32_t seg)
{
	assert(seg < CONFIG::CONST_SEG_NUM);
	VecReg v;
	// Broadcast(addr, v, 2, 1,
	// 	CONFIG::CONST_START + seg * CONFIG::CONST_SEG_SIZE, DomainConst);
	Broadcast(addr, v, 2, 1,
		const_base[seg], DomainConst);
	return v;
}



template <class CONFIG>
VectorAddress<CONFIG> CPU<CONFIG>::EffectiveAddress(uint32_t reg, uint32_t addr_lo, uint32_t addr_hi,
	bool addr_imm, uint32_t shift)
{
	uint32_t addr_reg = (addr_hi << 2) | addr_lo;
	VecAddr offset;
	// [$a#addr_reg + dest]
	if(addr_imm) {
		// Immediate in words
		offset = VecAddr(reg << shift);
	}
	else {
		VecReg vroffset = GetGPR(reg);
		offset = VecAddr(vroffset);
		if(export_stats) {
			GetOpStats().RegRead(&vroffset, DT_U32, GetCurrentMask(), this, reg);
		}
	}

	if(addr_reg != 0) {
		offset += GetAddr(addr_reg);
	}
	return offset;
}

template <class CONFIG>
void CPU<CONFIG>::GatherShared(VectorRegister<CONFIG> & output, uint32_t src, uint32_t addr_lo, uint32_t addr_hi, bool addr_imm, std::bitset<CONFIG::WARP_SIZE> mask, SMType type)
{
	uint32_t shift = 0;
	if(type == SM_U16 || type == SM_S16) shift = 1;
	else if(type == SM_U32) shift = 2;

	if(((addr_hi << 2) | addr_lo) == 0 && addr_imm) {
		address_t addr = src;
		if(mask.any()) {
			ReadShared(addr, output, type);
		}
		output.SetScalar();
	}
	else {
		VecAddr offset = EffectiveAddress(src, addr_lo, addr_hi, addr_imm, shift);
		GatherShared(offset, output, mask, type);
	}
}

// For current block
template <class CONFIG>
void CPU<CONFIG>::GatherShared(VectorAddress<CONFIG> const & addr,
	VectorRegister<CONFIG> & data, std::bitset<CONFIG::WARP_SIZE> mask, SMType t)
{
	for(unsigned int i = 0; i != WARP_SIZE; ++i) {
		assert(!mask[i] || addr[i] < sm_size);
	}
	unsigned int logsize = SMTypeLogSize(t);
	Gather(addr, data, mask, logsize, 1,
		CurrentWarp().GetSMAddress(), DomainShared);
}

template <class CONFIG>
void CPU<CONFIG>::ScatterShared(VectorRegister<CONFIG> const & output, uint32_t dest,
	uint32_t addr_lo, uint32_t addr_hi, bool addr_imm,
	std::bitset<CONFIG::WARP_SIZE> mask, SMType type)
{

	uint32_t shift = 0;
	if(type == SM_U16 || type == SM_S16) shift = 1;
	else if(type == SM_U32) shift = 2;
	VecAddr offset = EffectiveAddress(dest, addr_lo, addr_hi, addr_imm, shift);

	for(unsigned int i = 0; i != WARP_SIZE; ++i) {
		assert(!mask[i] || offset[i] < sm_size);
	}

	unsigned int logsize = SMTypeLogSize(type);
	address_t base = CurrentWarp().GetSMAddress();
	Scatter(offset, output, mask, logsize, 1, base, DomainShared);
}

// For current block
template <class CONFIG>
void CPU<CONFIG>::ReadShared(typename CONFIG::address_t addr, VectorRegister<CONFIG> & data, SMType t)
{
	unsigned int logsize = SMTypeLogSize(t);
	unsigned int size = 1 << logsize;
	assert(size * addr < sm_size);
	Broadcast(addr, data, logsize, size,
		CurrentWarp().GetSMAddress(), DomainShared);
}

template <class CONFIG>
void CPU<CONFIG>::GatherConstant(VecReg & output, uint32_t src, uint32_t addr_lo,
	uint32_t addr_hi, bool addr_imm, uint32_t segment,
	std::bitset<CONFIG::WARP_SIZE> mask, SMType type)
{
	unsigned int logsize = SMTypeLogSize(type);
	VecAddr addr = EffectiveAddress(src, addr_lo, addr_hi, addr_imm, logsize);
	address_t base = CONFIG::CONST_START + segment * CONFIG::CONST_SEG_SIZE;
	Gather(addr, output, mask, logsize, 1, base, DomainShared);
}

template <class CONFIG>
void CPU<CONFIG>::Gather(VecAddr const & addr, VecReg data[],
	std::bitset<CONFIG::WARP_SIZE> mask, DataType dt, Domain domain)
{
	switch(dt)
	{
	case DT_U32:
	case DT_S32:
		Gather(addr, data[0], mask, 2, 1, 0, domain);
		break;
	case DT_U16:
	case DT_S16:
		Gather(addr, data[0], mask, 1, 1, 0, domain);
		break;
	case DT_U8:
	case DT_S8:
		Gather(addr, data[0], mask, 0, 1, 0, domain);
		break;
	case DT_U64:
		// Actual Gather64, not burst transfer?
		// TODO: check alignment
		// From little-endian mem to little-endian regs
		Gather(addr, data[0], mask, 2, 1, 0, domain);
		Gather(addr, data[1], mask, 2, 1, 4, domain);
		break;
	case DT_U128:
		// TODO: check alignment
		Gather(addr, data[0], mask, 2, 1, 0, domain);
		Gather(addr, data[1], mask, 2, 1, 4, domain);
		Gather(addr, data[2], mask, 2, 1, 8, domain);
		Gather(addr, data[3], mask, 2, 1, 12, domain);
		break;
	default:
		assert(false);
	}
}

template <class CONFIG>
void CPU<CONFIG>::Scatter(VecAddr const & addr, VecReg const data[],
	std::bitset<CONFIG::WARP_SIZE> mask, DataType dt, Domain domain)
{
	switch(dt)
	{
	case DT_U32:
	case DT_S32:
		Scatter(addr, data[0], mask, 2, 1, 0, domain);
		break;
	case DT_U16:
	case DT_S16:
		Scatter(addr, data[0], mask, 1, 1, 0, domain);
		break;
	case DT_U8:
	case DT_S8:
		Scatter(addr, data[0], mask, 0, 1, 0, domain);
		break;
	case DT_U64:
		Scatter(addr, data[0], mask, 2, 1, 0, domain);
		Scatter(addr, data[1], mask, 2, 1, 4, domain);
		break;
	case DT_U128:
		Scatter(addr, data[0], mask, 2, 1, 0, domain);
		Scatter(addr, data[1], mask, 2, 1, 4, domain);
		Scatter(addr, data[2], mask, 2, 1, 8, domain);
		Scatter(addr, data[3], mask, 2, 1, 12, domain);
		break;
	default:
		assert(false);
	}
}

template <class CONFIG>
void CPU<CONFIG>::ScatterGlobal(VecReg const output[], uint32_t dest, uint32_t addr_lo, uint32_t addr_hi, bool addr_imm, uint32_t segment, std::bitset<CONFIG::WARP_SIZE> mask, DataType dt)
{
	if(!mask.any()) return;

	unsigned int shift = 0;
	if(dt == DT_U16 || dt == DT_S16) shift = 1;	// 16-bit
	else if(dt == DT_U32 || dt == DT_S32) shift = 2;	// 32-bit
	else if(dt == DT_U64) shift = 3;
	else if(dt == DT_U128) shift = 4;

	// [seg][$a#addr_reg + dest]
	VecAddr offset = EffectiveAddress(dest, addr_lo, addr_hi, addr_imm, shift);

	// TODO: segment
	assert(segment == 14);
	Scatter(offset, output, mask, dt, DomainGlobal);

	if(export_stats)
		GetOpStats().ScatterGlobal(offset, shift, mask);
}

template <class CONFIG>
void CPU<CONFIG>::GatherGlobal(VecReg output[], uint32_t src, uint32_t addr_lo, uint32_t addr_hi, bool addr_imm, uint32_t segment, std::bitset<CONFIG::WARP_SIZE> mask, DataType dt)
{
	if(!mask.any()) return;

	unsigned int shift = 0;
	if(dt == DT_U16 || dt == DT_S16) shift = 1;	// 16-bit
	else if(dt == DT_U32 || dt == DT_S32) shift = 2;	// 32-bit
	else if(dt == DT_U64) shift = 3;
	else if(dt == DT_U128) shift = 4;

	// [seg][$a#addr_reg + dest]
	VecAddr offset = EffectiveAddress(src, addr_lo, addr_hi, addr_imm, shift);

	// TODO: segment??
	assert(segment == 14);
	Gather(offset, output, mask, dt, DomainGlobal);

	if(export_stats)
		GetOpStats().GatherGlobal(offset, shift, mask);
}

template <class CONFIG>
void CPU<CONFIG>::ScatterLocal(VecReg const output[], uint32_t dest, uint32_t addr_lo, uint32_t addr_hi, bool addr_imm, uint32_t segment, std::bitset<CONFIG::WARP_SIZE> mask, DataType dt)
{
	if(!mask.any()) return;

	// Local memory always byte-indexed
	unsigned int shift = 0;
	/*if(dt == DT_U16 || dt == DT_S16) shift = 1;	// 16-bit
	else if(dt == DT_U32 || dt == DT_S32) shift = 2;	// 32-bit
	else if(dt == DT_U64) shift = 3;
	else if(dt == DT_U128) shift = 4;*/

	assert(dt != DT_U64 && dt != DT_U128);
	unsigned int addr = (addr_hi << 2) | addr_lo;
	if(addr == 0 && addr_imm && dt == DT_U32) {
		assert(segment == 0);
		// Local memory is really always byte-indexed
		StoreLocal32(output[0], dest, mask);
		if(export_stats)
			GetOpStats().StoreLocal(2, output, mask);
	}
	else {
		// [seg][$a#addr_reg + dest]
		VecAddr offset = EffectiveAddress(dest, addr_lo, addr_hi, addr_imm, shift);

		offset = LocalAddress(offset, segment);

		Scatter(offset, output, mask, dt, DomainLocal);
		if(export_stats)
			GetOpStats().ScatterLocal(offset, DataTypeLogSize(dt), mask, output);
	}
}

template <class CONFIG>
void CPU<CONFIG>::GatherLocal(VecReg output[], uint32_t src, uint32_t addr_lo, uint32_t addr_hi, bool addr_imm, uint32_t segment, std::bitset<CONFIG::WARP_SIZE> mask, DataType dt)
{
	if(!mask.any()) return;

	// Local memory always byte-indexed
	unsigned int shift = 0;
	/*if(dt == DT_U16 || dt == DT_S16) shift = 1;	// 16-bit
	else if(dt == DT_U32 || dt == DT_S32) shift = 2;	// 32-bit
	else if(dt == DT_U64) shift = 3;
	else if(dt == DT_U128) shift = 4;*/

	assert(dt != DT_U64 && dt != DT_U128);
	unsigned int addr = (addr_hi << 2) | addr_lo;
	if(addr == 0 && addr_imm && dt == DT_U32) {
		assert(segment == 0);
		// Local memory is really always byte-indexed
		LoadLocal32(output[0], src, mask);
		if(export_stats)
			GetOpStats().LoadLocal(2, output, mask);
	}
	else {
		// [seg][$a#addr_reg + dest]
		VecAddr offset = EffectiveAddress(src, addr_lo, addr_hi, addr_imm, shift);

		offset = LocalAddress(offset, segment);

		Gather(offset, output, mask, dt, DomainLocal);
		if(export_stats)
			GetOpStats().GatherLocal(offset, DataTypeLogSize(dt), mask, output);
	}
}

template <class CONFIG>
void CPU<CONFIG>::LoadLocal32(VecReg & output, address_t addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(!mask.any()) return;

	// addr in bytes
	addr = LocalAddress(addr, 0);
	VectorLoad(addr, output, mask, 2, DomainLocal);

//	if (export_stats && CONFIG::CACHE_ENABLED)
//		meta_cache.Load(output, addr, mask, GetOpStats().L1());
}


template <class CONFIG>
void CPU<CONFIG>::StoreLocal32(VecReg const & output, address_t addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(!mask.any()) return;

	// addr in bytes
	addr = LocalAddress(addr, 0);
//	if (export_stats && CONFIG::CACHE_ENABLED)
//		meta_cache.Store(output, addr, mask, GetOpStats().L1());

	VectorStore(addr, output, mask, 2, DomainLocal);
}

template <class CONFIG>
void CPU<CONFIG>::Sample1DS32(unsigned int sampler,
	VectorRegister<CONFIG> dest[],
	VectorRegister<CONFIG> const src[],
	uint32_t destBitfield)
{
	assert(sampler < CONFIG::MAX_SAMPLERS);
	samplers[sampler].Sample1DS32(dest, src, destBitfield);
}


// Mid-level functions

template <class CONFIG>
void CPU<CONFIG>::Scatter(VecAddr const & addr, VecReg const & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize,
	uint32_t factor, address_t offset, Domain domain)
{
	// TODO: check overlap and warn
	for(unsigned int i = 0; i != WARP_SIZE; ++i)
	{
		if(mask[i]) {
			Write(addr[i], data[i], logsize, factor, offset);
		}
	}
}

template <class CONFIG>
void CPU<CONFIG>::Gather(VecAddr const & addr, VecReg & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize,
	uint32_t factor, address_t offset, Domain domain)
{
	for(unsigned int i = 0; i != WARP_SIZE; ++i)
	{
		if(mask[i]) {
			Read(addr[i], data[i], logsize, factor, offset);
		}
	}
}

// Overwrites all 32 bits of the output, even for smaller word sizes.
template <class CONFIG>
void CPU<CONFIG>::Broadcast(address_t addr, VecReg & data,
	uint32_t logsize,
	uint32_t factor, address_t offset, Domain domain)
{
	uint32_t val = 0;
	Read(addr, val, logsize, factor, offset);
	data = VecReg(val);
}



// Overwrites ALL register lanes, including masked lanes
template <class CONFIG>
void CPU<CONFIG>::VectorLoad(address_t addr, VecReg & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain)
{
	if(!ReadMemory(addr, &data.v[0], CONFIG::WARP_SIZE << logsize))
		throw MemoryAccessException<CONFIG>();
}


template <class CONFIG>
void CPU<CONFIG>::VectorStore(address_t addr, VecReg const & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain)
{
	if((~mask).any()) {
		// Partially-set mask: split store
		for(unsigned int i = 0; i != WARP_SIZE; ++i)
		{
			if(mask[i]) {
				Write(addr, data[i], logsize, 1, i << logsize);
			}
		}

	}
	else {
		if(!WriteMemory(addr, &data.v[0], CONFIG::WARP_SIZE << logsize))
			throw MemoryAccessException<CONFIG>();
	}
}


// Low-level functions

template <class CONFIG>
void CPU<CONFIG>::Read(address_t addr,
	uint32_t & data, uint32_t logsize,
	uint32_t factor, address_t offset)
{
	// logsize = 2 -> Read32
	// logsize = 1 -> Read16
	// logsize = 0 -> Read8
	unsigned int size = 1 << logsize;
	if(TraceLoadstore()) {
		cerr << " Read" << 8 * size << " @" << std::hex << offset << "+" << addr << "*" << factor;
	}
	assert(!((addr * factor + offset) & (size - 1)));	// Check alignment

	if(!ReadMemory(addr * factor + offset, &data, size)) {
		throw MemoryAccessException<CONFIG>();
	}

	if(TraceLoadstore()) {
		cerr  << ": " << data << std::dec << endl;
	}
}

template <class CONFIG>
void CPU<CONFIG>::Write(address_t addr,
	uint32_t data, uint32_t logsize,
	uint32_t factor, address_t offset)
{
	// logsize = 2 -> Write32
	// logsize = 1 -> Write16
	// logsize = 0 -> Write8
	unsigned int size = 1 << logsize;
	if(TraceLoadstore()) {
		cerr << " Write" << 8 * size << ": " << std::hex << data
			<< " @" << offset << "+" << addr << "*" << factor << std::dec << endl;
	}
	assert(!((addr * factor + offset) & (size - 1)));	// Check alignment
	if(!WriteMemory(addr * factor + offset, &data, size)) {
		throw MemoryAccessException<CONFIG>();
	}
}


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
