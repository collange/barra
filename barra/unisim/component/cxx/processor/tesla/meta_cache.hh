/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

// Deprecated, removed from build. See perfmodel/affine_vector_cache.hh

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_META_CACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_META_CACHE_HH

#include <unisim/component/cxx/processor/tesla/local_cache.hh>
#include <unisim/component/cxx/processor/tesla/affine_cache.hh>
#include <unisim/component/cxx/processor/tesla/register.hh>
#include <unisim/component/cxx/cache/cache.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
class MetaCacheStats;

template<class CONFIG>
class CPU;

template<class CONFIG, class L>
class MetaCache
{
	private:
		LocalCache<CONFIG, typename L::LOCAL> local_cache;
		AffineCache<CONFIG, typename L::AFFINE> affine_cache;
		CPU<CONFIG>& cpu;
		
	public:
		typedef	VectorRegister<CONFIG> VecReg;
		typedef typename CONFIG::address_t address_t;
		typedef typename cxx::cache::Cache<typename L::LOCAL> Cache;
		typedef std::bitset<CONFIG::WARP_SIZE> mask_t;


		MetaCache(CPU<CONFIG>& my_cpu) :	local_cache(my_cpu),
											affine_cache(my_cpu),
											cpu(my_cpu) {  }
		
		bool Load(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats);
		bool Store(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats);
		
		bool SplitLoad(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats);
		bool SplitStore(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats);
		
		bool All(mask_t mask) { return (mask.count() == CONFIG::WARP_SIZE); }
		void PartialInvalidate(address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats);
		void Invalidate();

};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
