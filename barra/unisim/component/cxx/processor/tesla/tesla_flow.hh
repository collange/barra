/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_FLOW_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_FLOW_HH

#include <unisim/component/cxx/processor/tesla/forward.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

// From US Patent 7353369 B1
// Note: it does not work. Do not use it.

template<class CONFIG>
struct TeslaFlow
{
	enum StackTokenType
	{
		ID_SYNC = 0,
		ID_DIVERGE = 1,
		ID_CALL,
		ID_BREAK,
		//ID_BOTTOM	// Not in patent
	};

	// Store addresses relative to code segment base
	struct StackToken
	{
		StackToken() {}
		StackToken(std::bitset<CONFIG::WARP_SIZE> mask, StackTokenType id,
			typename CONFIG::address_t address) :
			mask(mask), id(id), address(address) {}

		std::bitset<CONFIG::WARP_SIZE> mask;
		StackTokenType id;
		// *Absolute* address (NV patent uses addresses relative to base)
		typename CONFIG::address_t address;
	};

	typedef std::stack<StackToken> stack_t;
	typedef typename CONFIG::address_t address_t;
	typedef bitset<CONFIG::WARP_SIZE> mask_t;
	
	TeslaFlow(Warp<CONFIG> & warp);
	
	void Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
		typename CONFIG::address_t code_base);
	void Branch(address_t target, std::bitset<CONFIG::WARP_SIZE> mask);
	void Meet(address_t addr);
	bool Join();
	bool End();
	void Return(std::bitset<CONFIG::WARP_SIZE> mask);
	void Kill(std::bitset<CONFIG::WARP_SIZE> mask);
	std::bitset<CONFIG::WARP_SIZE> GetCurrentMask() const;
	void PreBreak(address_t addr);
	void Break(std::bitset<CONFIG::WARP_SIZE> mask);
	void Call(address_t addr);

	address_t GetPC() const { return pc; }
	address_t GetRPC() const { return pc - code_base; }	// Relative PC
	void SetNPC(address_t npc) { this->npc = npc; }
	bool BarrierStop();
	void BarrierResume();

	// List sub-warps: instructions to fetch next
	unsigned int AllPCs(SubWarp<CONFIG> list[], unsigned int size);

	// Start execution. Update current PC and current Mask.
	void Issue(SubWarp<CONFIG> const & sw);

	// Update PC for threads not replaying
	void Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask);

	// Used for barriers
	bool Ready(SubWarp<CONFIG> const & subwarp) const;


private:
	void SetPC(address_t pc) { this->pc = pc; }
	address_t GetNPC() const { return npc; }
	void SetCurrentMask(std::bitset<CONFIG::WARP_SIZE> mask);
	
	CPU<CONFIG> * cpu;
	Warp<CONFIG> & warp;

	mask_t replay_mask;
	mask_t current_mask;
	mask_t initial_mask;
	
	stack_t stack;
	address_t pc;
	address_t npc;
	address_t code_base;
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
