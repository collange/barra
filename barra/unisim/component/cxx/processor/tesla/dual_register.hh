/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_DUAL_REGISTER_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_DUAL_REGISTER_HH

#include <unisim/component/cxx/processor/tesla/register.hh>
#include <iostream>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template <class CONFIG>
struct VectorAddress;

template <class CONFIG, class Base>
struct DualTagVectorAddress;

template <class CONFIG, class Base>
struct DualTagVectorRegister;

template <class CONFIG>
struct AffineMetaData;

template <class CONFIG>
struct DualTag
{
	typedef std::bitset<CONFIG::WARP_SIZE> mask_t;
	struct st_values {
		int32_t b,s;

		st_values(int b = 0, int s = 0) : b(b), s(s) {}
		void Set(uint32_t base, uint32_t stride) { b = base; s = stride;}
		void Set(st_values const &other) { b = other.b; s = other.s;}
	};
	st_values stridedComponent[ CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE ];
	mask_t strided;

	DualTag<CONFIG>(int b = 0, int s = 0, mask_t mask = ~mask_t(0)) : flattenNb(0)
	{
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
			stridedComponent[i] = st_values(b,s);
		strided = mask;
	}

	DualTag<CONFIG>(DualTag<CONFIG> const& other) : flattenNb(other.flattenNb)
	{
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
			stridedComponent[i] = st_values(other.stridedComponent[i]);
		strided = other.strided;
	}

	DualTag<CONFIG>(AffineMetaData<CONFIG> const& amd, mask_t mask = ~mask_t(0)) : flattenNb(0)
	{
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i)
			stridedComponent[i] = st_values(amd.recap[i].b, amd.recap[i].s);
		SetStrided(amd.strided, mask);
	}

	unsigned int flattenNb;

	void SetStridedValue(uint32_t base, uint32_t stride);
	void SetStridedValue(DualTag<CONFIG>& other);

	void SetScalar(bool const s = true, mask_t const mask = ~mask_t(0));
	void SetStrided(bool const s = true, mask_t const mask = ~mask_t(0));

	bool IsStrided16(bool const hi, mask_t const mask = ~mask_t(0)) const;
	bool IsScalar16(bool const hi, mask_t const mask = ~mask_t(0)) const;

	bool IsStrided(mask_t const mask = ~mask_t(0)) const { return (~strided & mask).none(); }

	void UpdateWrite(VectorRegister<CONFIG> regs, mask_t mask = ~mask_t(0),  bool b16 = false,bool hilo = false);
	void UpdateWrite(VectorAddress<CONFIG> addr, mask_t mask = ~mask_t(0)) { UpdateWrite((VectorRegister<CONFIG> &) addr, mask); }

	void UpdateRead(VectorRegister<CONFIG> regs, mask_t mask = ~mask_t(0), bool b16 = false,bool hilo = false);
	void UpdateRead(VectorAddress<CONFIG> addr, mask_t mask = ~mask_t(0)) { UpdateRead((VectorRegister<CONFIG>&)addr, mask); }

	uint32_t Hits(mask_t const mask = ~mask_t(0)) const { return (mask & strided).count(); }
	uint32_t Hits16(mask_t const mask = ~mask_t(0), bool hilo = false) const { return IsStrided16(hilo, mask) * (strided & mask).count(); }
	uint32_t operator[] (unsigned int const lane) const;
	bool MaskedEq(AffineMetaData<CONFIG> amd, mask_t mask) const;

	bool Flatten(mask_t mask);

	DualTag<CONFIG> Split(int hilo) const;
};

template <class CONFIG, class Base>
struct DualTagVectorRegister : Base
{
	static unsigned int const WARP_SIZE = CONFIG::WARP_SIZE;
	typedef DualTagVectorRegister<CONFIG, Base> ThisType;
	typedef std::bitset<CONFIG::WARP_SIZE> mask_t;

	DualTagVectorRegister();
	DualTagVectorRegister(uint32_t val);
	DualTagVectorRegister(ThisType const & other);
	DualTagVectorRegister(Base const & other);
	
	template <class Base2>
	DualTagVectorRegister(DualTagVectorAddress<CONFIG, Base2> const & addr);
	void Write(ThisType const & vec, std::bitset<CONFIG::WARP_SIZE> mask);
	void Write16(ThisType const & vec, std::bitset<CONFIG::WARP_SIZE> mask, int hi);

	DualTagVectorRegister<CONFIG, Base> Split(int hilo) const;

	DualTag<CONFIG> tag;
	bool scalar;
	bool strided;
	bool scalar16[2];
	bool strided16[2];
	
	bool IsScalar() const { return scalar; }
	bool IsStrided() const { return strided; }
	bool IsScalar16(bool hi) const { return scalar16[hi]; } //return tag.IsScalar16(hi); }
	bool IsStrided16(bool hi) const { return strided16[hi]; } //return tag.IsStrided16(hi); }

	void SetScalar(bool s = true) {
		tag.SetStrided(s);
		scalar = s;
		if(s) {
			scalar16[0] = scalar16[1] = true;
			strided = strided16[0] = strided16[1] = true;
		}
	}
	void SetStrided(bool s = true) { tag.SetStrided(s); strided = s; }
	void SetScalar16(bool hi, bool s = true) { scalar16[hi] = s; }
	void SetStrided16(bool hi, bool s = true) { strided16[hi] = s; }

	int FlattenNb() const;

	bool Flatten(mask_t mask = ~mask_t(0)) { return tag.Flatten(mask); }

	void UpdateRead(mask_t mask = ~mask_t(0), bool b16 = false, bool hilo = false) { tag.UpdateRead(*this, mask, b16, hilo); }
	void UpdateWrite(mask_t mask = ~mask_t(0), bool b16 = false, bool hilo = false) { tag.UpdateWrite(*this, mask, b16, hilo); }
	void GetStridedValue(mask_t mask = ~mask_t(0)) 
	{
		AffineMetaData<CONFIG> amd = this->CheckStridedMMD(mask);
		for (int i = 0; i != CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE; ++i) {
			tag.stridedComponent[i].b = amd.recap[i].b;
			tag.stridedComponent[i].s = amd.recap[i].s;
		}
		//std::cerr << "Init " << *this << std::endl;
	}

	uint32_t Hits(mask_t const mask) const { return tag.Hits(mask); }
	uint32_t Hits16(mask_t const mask, bool hilo) const { return tag.Hits16(mask, hilo); }
};

template <class CONFIG, class Base>
std::ostream & operator << (std::ostream & os, DualTagVectorRegister<CONFIG, Base> const & r);

template <class CONFIG, class Base>
struct DualTagVectorAddress : Base
{
	static unsigned int const WARP_SIZE = CONFIG::WARP_SIZE;
	typedef typename CONFIG::address_t address_t;
	typedef DualTagVectorAddress<CONFIG, Base> ThisType;
	typedef std::bitset<CONFIG::WARP_SIZE> mask_t;
	
	DualTagVectorAddress();
	DualTagVectorAddress(address_t addr);
	DualTagVectorAddress(Base const & other);
	DualTagVectorAddress(ThisType const & other);

	template <class Base2>
	DualTagVectorAddress(DualTagVectorRegister<CONFIG, Base2> const & vr);

	DualTag<CONFIG> tag;

	void Write(ThisType const & vec, std::bitset<CONFIG::WARP_SIZE> mask);
	
	void Reset();
	
	ThisType & operator+=(ThisType const & other);
	
	bool scalar;
	bool strided;

	bool IsScalar() const { return scalar; }
	bool IsStrided() const { return strided; }
	void SetScalar(bool s = true) { tag.SetScalar(s); scalar = s; }
	void SetStrided(bool s = true) { tag.SetStrided(s); strided = s; }

	void UpdateRead(mask_t mask = ~mask_t(0)) { tag.UpdateRead(*this, mask); }
	void UpdateWrite(mask_t mask = ~mask_t(0)) { tag.UpdateWrite(*this, mask); }
};

template<class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base> operator+(DualTagVectorAddress<CONFIG, Base> const & a, DualTagVectorAddress<CONFIG, Base> const & b);

template<class CONFIG, class Base>
DualTagVectorAddress<CONFIG, Base> operator*(unsigned int factor, DualTagVectorAddress<CONFIG, Base> const & addr);

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
