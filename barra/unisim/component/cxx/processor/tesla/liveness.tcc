/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: 
 *     Elie Gédéon (elie.gedeon@ens-lyon.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LIVENESS_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LIVENESS_TCC

#include <unisim/component/cxx/processor/tesla/liveness.hh>
//#include <unisim/component/cxx/processor/tesla/memory.hh>
#include <unisim/component/cxx/processor/tesla/instruction.hh>

#include <sstream>
#include <stdexcept>
#include <cassert>
#include <iomanip>
#include <map>
#include <bitset>
#include <queue>
#include <algorithm>
#include <iostream>
#include <utility>
#include <limits>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

//using namespace std;
using boost::shared_ptr;


template <class CONFIG>
Liveness<CONFIG>::Liveness(CPU<CONFIG> *pcpu) : cpu(pcpu), ready(false), nb_registres(0)
{
}

// Compute def, uses
template <class CONFIG>
Liveness<CONFIG>::InOut::InOut(Instruction<CONFIG>&ins, vector<Next<CONFIG> > &succ)
{
    nb_registres = 0;
    ins.InitDependencies();
    for (unsigned i = 0; i < ins.InputCount(); i++)
    {
        OperandID id = ins.Input(i);
        if (id.domain == DomainGPR)
        {
            nb_registres = max(nb_registres, id.id);
            in[id.id] = true;
        }
    }

    for (unsigned i = 0; i < ins.OutputCount(); i++)
    {
        OperandID id = ins.Output(i);
        if (id.domain == DomainGPR)
        {
            nb_registres = max(nb_registres, id.id);
            out[id.id] = true;
            if(ins.Class() == ClassMEM) {
            	// Output register from memory op: assume load
            	load[id.id] = true;
            }
        }
    }

    for (unsigned i = 0; i < succ.size(); i++)
		next.insert(succ[i].addr);
}

template <class CONFIG>
void Liveness<CONFIG>::InOut::operator|=(Liveness<CONFIG>::InOut& io)
{
    in = io.in;
    out = io.out;
    load = io.load;
    nb_registres = io.nb_registres;
    typename set<typename CONFIG::address_t>::iterator it;
    for (it = io.next.begin(); it != io.next.end(); ++it)
    {
        next.insert(*it);
    }
}



template <class CONFIG>
Liveness<CONFIG>::InOut::InOut()
{
    ;
}

template <class CONFIG>
Liveness<CONFIG>::Load::Load()
{
    fill(lru, lru + CONFIG::MAX_GPRS, numeric_limits<int>::max());
    pending.reset();
}

template <class CONFIG>
int & Liveness<CONFIG>::Load::operator[](unsigned i)
{
    return lru[i];
}

template <class CONFIG>
void Liveness<CONFIG>::Load::operator|=(Load& other)
{
    pending |= other.pending;
    for (unsigned i = 0; i < CONFIG::MAX_GPRS; i++)
    {
        if (pending[i])
        {
            if (lru[i] > 1+other.lru[i]);
            {
                lru[i] = 1+other.lru[i];
            }
        }
        else
            lru[i] = numeric_limits<int>::max();
//            cerr<<dec<<(lru[i]!=other.lru[i])<<",";
    }
}

template <class CONFIG>
bool Liveness<CONFIG>::Load::operator!=(Load& other)
{
    return !equal(lru,lru+CONFIG::MAX_GPRS,other.lru) || pending!=other.pending;
}

template <class CONFIG>
void Liveness<CONFIG>::compute()
{
	using namespace std;
    ins.clear();
    outs.clear();
    loads.clear();

    inOuts.clear();
    nb_registres = 0;
    queue<pair<typename CONFIG::address_t, Context<CONFIG> > > worklist;
    set<pair<typename CONFIG::address_t, typename CONFIG::address_t> > succ;
    
    // Breadth-first graph traversal
    // Functions are parsed once for each call site
    worklist.push(make_pair(0,Context<CONFIG>(cpu->CodeBase())));
    while (!worklist.empty())
    {
        pair<typename CONFIG::address_t, Context<CONFIG> > pc = worklist.front();
        worklist.pop();
        
        // Edge not already traversed?
        if (succ.find(make_pair(pc.first, pc.second.CallSite())) == succ.end())
        {
        	// Mark edge as done
        	succ.insert(make_pair(pc.first, pc.second.CallSite()));
        	
		    Instruction<CONFIG> instruction(cpu, pc.first + cpu->CodeBase());
		    
		    // Compute list of successors in this context (call stack)
			vector<Next<CONFIG> > su = instruction.findNexts(pc.second);

			// Propagate register liveness data
            InOut inOut(instruction, su);
            inOuts[pc.first] |= inOut;
            
            // Hack: we recount registers here, as we do not have access
            // to regcount at this point.
            nb_registres = max(nb_registres, inOut.nb_registres);
            for (unsigned i = 0; i < su.size(); i++)
            {
                if (su[i].addr != (typename CONFIG::address_t)(-1)
                    && succ.find(make_pair(su[i].addr, su[i].context.CallSite())) == succ.end())
                {
                    worklist.push(make_pair(su[i].addr, su[i].context));
                }
            }
        }
    }

    inOuts[0].in[0] = true; //r0 = tid at the beginning

    for (typename map<typename CONFIG::address_t, InOut>::iterator it = inOuts.begin(); it != inOuts.end(); it++)
    {
        InOut inOut = it->second;
        typename CONFIG::address_t pc = it->first;
	    typename set<typename CONFIG::address_t>::iterator it2;
	    for (it2 = inOut.next.begin(); it2 != inOut.next.end(); ++it2)
            inOuts[*it2].previous.insert(pc);
        
    }
    
    int k = 0;
    bool end = false;
    while (!end)
    {
        k++;
        end = true;
        for (typename map<typename CONFIG::address_t, InOut>::iterator it = inOuts.begin(); it != inOuts.end(); it++)
        {
            InOut inOut = it->second;
            typename CONFIG::address_t pc = it->first;

            bitset <CONFIG::MAX_GPRS> newin;
            bitset <CONFIG::MAX_GPRS> newout;
            Load newload;

		    typename set<typename CONFIG::address_t>::iterator it2;

		    for (it2 = inOut.next.begin(); it2 != inOut.next.end(); ++it2)
                newout |= ins[*it2];

		    for (it2 = inOut.previous.begin(); it2 != inOut.previous.end(); ++it2)
                newload |= loads[*it2];
            newload.pending &= ~(inOut.in | inOut.out);
            newload.pending |= inOut.load;
            //newload.pending = inOut.load;


            for (unsigned i = 0; i < CONFIG::MAX_GPRS; i++)
                if (inOut.load[i])
                    newload.lru[i] = 0;

            newin = inOut.in | (outs[pc] & ~inOut.out);

            if (newin != ins[pc] || newout != outs[pc] || newload != loads[pc])
                end = false;

            ins[pc] = newin;
            outs[pc] = newout;
            loads[pc] = newload;
        }
    }
}

template <class CONFIG>
void Liveness<CONFIG>::BuildCFG()
{
}

template <class CONFIG>
void Liveness<CONFIG>::export_stats()
{
    compute();
    for (typename map<typename CONFIG::address_t, bitset<CONFIG::MAX_GPRS> >::iterator it = ins.begin(); it != ins.end(); it++)
    {
        typename CONFIG::address_t pc = it->first;
        (*cpu->GetStats())[pc].SetAliveIn(ins[pc].count());
        (*cpu->GetStats())[pc].SetAliveOut(outs[pc].count());
        (*cpu->GetStats())[pc].SetAliveLoad(loads[pc].pending.count());
    }
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
