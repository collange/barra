/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_INSTRUCTION_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_INSTRUCTION_HH

#include <unisim/component/cxx/processor/tesla/tesla_opcode.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src1.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src2.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src3.hh>
#include <unisim/component/cxx/processor/tesla/tesla_dest.hh>
#include <unisim/component/cxx/processor/tesla/tesla_control.hh>
#include <list>

#include <unisim/component/cxx/processor/tesla/tesla_instruction.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

#if 0
template<class CONFIG>
struct OpCodeDecoder : tesla_isa::opcode::Decoder<CONFIG>
{
private:
	CPU<CONFIG> & cpu;
public:
	OpCodeDecoder(CPU<CONFIG> & cpu) :
		cpu(cpu) {}
	virtual void Fetch(tesla_isa::opcode::CodeType& ct, typename CONFIG::address_t addr) {
		typename CONFIG::insn_t iw;
		cpu.Fetch(iw, addr);
		ct = CodeType(&iw, sizeof(typename CONFIG::insn_t));
	}
};
#endif

// Instruction:
// Instruction as passed from a pipeline stage to the next.
// Dynamic, allocated during decode stage, discarded on retirement.
// Multiple Instructions in flight for the same Operation are possible.
template<class CONFIG>
struct Instruction :
//	SelectType<CONFIG::ISA == ISATesla,
//	           TeslaInstruction<CONFIG>,
//	           FermiInstruction<CONFIG> >::t
	TeslaInstruction<CONFIG>
{
	typedef TeslaInstruction<CONFIG> Base;
	Instruction(CPU<CONFIG> * cpu, typename CONFIG::address_t addr) : Base(cpu, addr) {}
	Instruction() : Base() {}
};



// ISA-independent stuff
// ISA-specific Instruction types derive from this
template <class CONFIG>
struct InstructionBase
{
	InstructionBase(CPU<CONFIG> * cpu, typename CONFIG::address_t addr);
	virtual ~InstructionBase() {}
	InstructionBase();

	//Scalar instruction Set/Unset
	bool & ScalarInsn() { return scalarInsn; }
	bool ScalarInsn() const { return scalarInsn; }

	// Interface with Operation*
	VectorRegister<CONFIG> const & Temp(unsigned int i) const;
	VectorRegister<CONFIG> & Temp(unsigned int i);

	// Temp(0) = src1 and dest
	// Temp(1) = src1.hi and dest.hi (64-bit)
	// Temp(2) = src2  or src1.hilo (128-bit)
	// Temp(3) = src2.hi (64-bit) or src1.hihi (128-bit)
	// Temp(4) = src3
	// Temp(5) = src3.hi (64-bit)
	enum {
		TempCount = 6,
		TempSrc1 = 0,
		TempSrc2 = 2,
		TempSrc3 = 4,
		TempDest = 0
	};
	
	VectorRegister<CONFIG> & GetSrc1();
	VectorRegister<CONFIG> & GetSrc2();
	VectorRegister<CONFIG> & GetSrc3();
	VectorRegister<CONFIG> & GetDest();

	void ReadBlock(int reg, DataType dt);
	void WriteBlock(int reg, DataType dt);
	void ReadReg(int reg, int tempbase, RegType rt);
	void WriteReg(int reg, int tempbase, DataType dt,
		std::bitset<CONFIG::WARP_SIZE> mask);

	bitset<CONFIG::WARP_SIZE> Mask() const;

	void SetCPU(CPU<CONFIG> * cpu);

	typename CONFIG::operationstats_t * Stats();

	// Deprecated?
	void SetDest(VectorRegister<CONFIG> const & value);

	// TODO: more stuff here...

    typename CONFIG::insn_t InstructionWord() const { return iw; }

	bool Flatten();
	std::list<int> readRegisters;
    InstructionClass Class() const;

	typedef typename tesla_isa::opcode::Operation<CONFIG> OpCode;
//	void SetOperation(OpCode *operation);
//	OpCode * GetOperation() const { return operation; }
protected:
	CPU<CONFIG> * cpu;
	typename CONFIG::address_t addr;
	typename CONFIG::insn_t iw;
	OpCode * operation;
	static tesla_isa::opcode::Decoder<CONFIG> op_decoder;

	VectorRegister<CONFIG> temp[TempCount];

	std::bitset<CONFIG::WARP_SIZE> mask;
	typename CONFIG::operationstats_t * stats;
	bool scalarInsn;
	
};



} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
