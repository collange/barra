/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_IMPLICIT_FLOW_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_IMPLICIT_FLOW_TCC

#include <unisim/component/cxx/processor/tesla/implicit_flow.hh>
#include <unisim/component/cxx/processor/tesla/cpu.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
ImplicitFlow<CONFIG>::ImplicitFlow(Warp<CONFIG> & warp) :
	warp(warp),	breakaddr(0)

{
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
	typename CONFIG::address_t code_base)
{
	this->cpu = cpu;
	replay_mask = current_mask = mask;
	this->code_base = code_base;
	join_stack = std::stack<address_t>();
	mask_stack = MaskStack<CONFIG::WARP_SIZE, CONFIG::STACK_DEPTH>();
	loop_stack = std::stack<address_t>();
	breakaddr = 0;
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Branch(address_t target_addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert((target_addr & 3) == 0);	// 64-bit-aligned targets
	typename CONFIG::address_t adjpc = GetRPC();

	// Check mask
	// Mask == none -> nop
	// Mask == all  -> jump
	// Mask == other -> argh
	
	// Side effect: take other side of cond branch???
	typename CONFIG::address_t abs_target = target_addr + code_base;
	if(cpu->TraceBranch()) {
		cerr << " Branch mask = " << mask << endl;
	}
	
	assert((mask & ~(GetCurrentMask())).none());	// No zombies allowed

	if((mask).none())
	{
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, not taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniNotTaken();
		if(adjpc > target_addr)
		{
			// Not-taken backward loop
			// Re-enable threads
			// Compare PC with top of loop address stack
			// if ==, pop
			if(GetLoop() != 0 && GetLoop() == GetPC()) {
				if(cpu->TraceBranch()) {
					cerr << hex;
					cerr << " End of loop " << GetRPC()
						<< "  detected, restoring context\n";
					cerr << dec;
				}
				PopLoop();
				SetCurrentMask(PopMask());
				if(cpu->TraceBranch()) {
					cerr << "  Mask = " << GetCurrentMask() << endl;
				}
			}
		}
	}
	else if(mask == GetCurrentMask())	// Same number of enabled threads as before
	{
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniTaken();
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(abs_target);
	}
	else
	{
		(*cpu->stats)[GetRPC()].BranchDivergent();
		if(adjpc > target_addr)
		{
			// backward jump, loop
			// Compare PC with top of loop address stack
			// if !=, push PC and mask
			if(cpu->TraceBranch()) {
				cerr << " Conditional non-coherent backward jump from " << hex
					<< adjpc << " to " << target_addr << dec << endl;
			}
			(*cpu->stats)[GetRPC()].Jump();
			
			if(GetLoop() != GetPC()) {
				if(cpu->TraceBranch()) {
					cerr << hex;
					cerr << " Start of loop " << GetRPC() << " detected, saving context\n";
					cerr << "  Current mask = " << GetCurrentMask() << endl;
					cerr << dec;
				}
				PushLoop(GetPC());
				PushMask(GetCurrentMask());
			}
			SetCurrentMask(mask);
			SetNPC(abs_target);
			
		}
		else
		{
			// forward jump
			// Push lots of stuff in stacks
			// then don't take the branch.
			if(cpu->TraceBranch()) {
				cerr << hex;
				cerr << " Conditional non-coherent forward jump from "
					<< adjpc << " to " << target_addr << endl;
				cerr << dec;
			}
			std::bitset<CONFIG::WARP_SIZE> tos = GetCurrentMask();
			PushMask(tos);
			
			// TODO: CheckJoin *before* pushing new target??

			// Start by executing the 'not taken' branch: negate mask
			SetCurrentMask(~mask & tos);
			PushJoin(abs_target);
			// At the right time (PC >= tos?) go back and
			// take the branch.
			// Keep comparing PC to top of stack, go back (1st pass) or pop (2nd pass) if >=.
		}
		if(cpu->TraceBranch()) {
			cerr << "  New mask = " << GetCurrentMask() << endl;
		}
	}
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Meet(address_t addr)
{
	(*cpu->stats)[GetRPC()].Unexecute();
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::PreBreak(address_t addr)
{
	(*cpu->stats)[GetRPC()].Unexecute();
	breakaddr = addr;
}


template<class CONFIG>
bool ImplicitFlow<CONFIG>::Join()
{
	return true;
}

template<class CONFIG>
bool ImplicitFlow<CONFIG>::End()
{
	// Make sure stacks are now empty
	assert(mask_stack.empty());
	assert(join_stack.empty());
	assert(loop_stack.empty());
	return true;
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Return(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// Returns are like forward jumps
	if(call_stack.empty()) {
		// BOS reached, kill thread
		Kill(mask);
	}
	else if(mask == GetCurrentMask()) {
		(*cpu->stats)[GetRPC()].Jump();
		// Return
		address_t callsite = call_stack.top();
		assert(!join_stack.empty());
		address_t join_addr = join_stack.top();
		join_stack.pop();
		assert(!mask_stack.empty());
		std::bitset<CONFIG::WARP_SIZE> join_mask = mask_stack.top();
		mask_stack.pop();
		if(callsite == join_addr) {
			// Nothing to do in this function, return
			if(cpu->TraceBranch()) {
				cerr << " Return to " << callsite - code_base << endl;
			}
			SetNPC(callsite);
			SetCurrentMask(join_mask);
			call_stack.pop();
		}
		else {
			// Pop and switch to other branch
			if(cpu->TraceBranch()) {
				cerr << " Switch to " << join_addr - code_base << endl;
			}
			SetNPC(join_addr);
			SetCurrentMask(join_mask);
		}
	}
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Break(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// Treat like a regular branch instruction
	assert(breakaddr != 0);
	
	Branch(breakaddr, mask);
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Kill(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// mask : threads to kill
	// GetCurrentMask() & ~mask : threads alive
	std::bitset<CONFIG::WARP_SIZE> alive = GetCurrentMask() & ~mask;
	if(cpu->TraceBranch()) {
		cerr << " Kill, mask=" << mask << endl;
	}
	if(alive.none())
	{
		// Pop everything.
		while(!mask_stack.empty()) mask_stack.pop();
		while(!join_stack.empty()) join_stack.pop();
		while(!loop_stack.empty()) loop_stack.pop();
		warp.state = Warp<CONFIG>::Finished;
		if(cpu->TraceBranch()) {
			cerr << "  Warp killed" << endl;
		}
	}
	else if(!mask.none())
	{
		// Remove masked threads from whole stack
		mask_stack.dig(mask, 0);
		SetCurrentMask(alive);
		if(cpu->TraceBranch()) {
			cerr << "  Inactive threads killed. Mask="
				<< GetCurrentMask() << endl;
		}
	}
	else if(cpu->TraceBranch()) {
		cerr << "  No thread killed. Mask="
			<< GetCurrentMask() << endl;
	}
}

template <class CONFIG>
void ImplicitFlow<CONFIG>::CheckJoin()
{
	// TODO: if jumping outside the current loop, pop loop
	// Merge paths
	for(;InConditional() && GetNPC() == GetJoin()
		&& (call_stack.empty() || call_stack.top() != GetJoin());	// Ignore returns
	    (*cpu->stats)[GetRPC()].Execute(0, cpu, 0))
	{
		// Joined
		// Restore last mask and join
		SetCurrentMask(PopMask());
		PopJoin();
	}
	
	if(InConditional() && GetNPC() > GetJoin()
		&& (call_stack.empty() || call_stack.top() != GetJoin()))	// Ignore returns
	{
		// Jumped over join point
		// Go back following the other branch
		std::swap(join_stack.top(), npc);
		// Invert condition
		SetCurrentMask(~GetCurrentMask() & GetNextMask());
		(*cpu->stats)[GetRPC()].Jump();
	}

}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> ImplicitFlow<CONFIG>::GetCurrentMask() const
{
	//return current_mask;
	return replay_mask;
}

template <class CONFIG>
typename CONFIG::address_t ImplicitFlow<CONFIG>::GetLoop() const
{
	if(loop_stack.empty()) {
		return 0;
	}
	return loop_stack.top();
}

template <class CONFIG>
void ImplicitFlow<CONFIG>::PushLoop(address_t addr)
{
	loop_stack.push(addr);
}

template <class CONFIG>
void ImplicitFlow<CONFIG>::PopLoop()
{
	assert(!loop_stack.empty());
	loop_stack.pop();
}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> ImplicitFlow<CONFIG>::PopMask()
{
	std::bitset<CONFIG::WARP_SIZE> msk = mask_stack.top();
	mask_stack.pop();
	return msk;
}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> ImplicitFlow<CONFIG>::GetNextMask()
{
	return mask_stack.top();
}

template <class CONFIG>
void ImplicitFlow<CONFIG>::PushMask(std::bitset<CONFIG::WARP_SIZE> mask)
{
	mask_stack.push(mask);
}

template <class CONFIG>
bool ImplicitFlow<CONFIG>::InConditional() const
{
	return !join_stack.empty();
}

template <class CONFIG>
typename CONFIG::address_t ImplicitFlow<CONFIG>::GetJoin() const
{
	return join_stack.top();
}

template <class CONFIG>
void ImplicitFlow<CONFIG>::PushJoin(typename CONFIG::address_t addr)
{
	// TODO: raise exn if ovf
	assert(join_stack.size() < CONFIG::STACK_DEPTH);
	join_stack.push(addr);
}

template <class CONFIG>
typename CONFIG::address_t ImplicitFlow<CONFIG>::PopJoin()
{
	address_t addr = join_stack.top();
	join_stack.pop();
	return addr;
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Call(address_t target_addr)
{
	if(cpu->TraceBranch()) {
		cerr << " Call to " << target_addr << ", mask=" << GetCurrentMask() << endl;
	}
	if(GetCurrentMask().any()) {
		(*cpu->stats)[GetRPC()].Jump();
		address_t abs_target = target_addr + code_base;
		join_stack.push(npc);
		mask_stack.push(GetCurrentMask());
		call_stack.push(npc);
		SetNPC(abs_target);
	}
}

//template<class CONFIG>
//bool ImplicitFlow<CONFIG>::Step(int phase)
//{
//	return (phase == 0);
//}

template<class CONFIG>
bool ImplicitFlow<CONFIG>::BarrierStop()
{
	return true;
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::BarrierResume()
{
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::SetCurrentMask(std::bitset<CONFIG::WARP_SIZE> mask)
{
	replay_mask = current_mask = mask;
}

template<class CONFIG>
unsigned int ImplicitFlow<CONFIG>::AllPCs(SubWarp<CONFIG> subwarps[], unsigned int size)
{
	assert(size != 0);
	subwarps[0].pc = GetPC();
	subwarps[0].mask = GetCurrentMask();
	return 1;
}

template<class CONFIG>
void ImplicitFlow<CONFIG>::Issue(SubWarp<CONFIG> const & sw)
{
	assert(sw.pc == GetPC());
	assert(sw.mask == GetCurrentMask());
}



template<class CONFIG>
void ImplicitFlow<CONFIG>::Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask)
{
	if(cpu->TraceMask()) {
		cerr << " " << GetCurrentMask() << endl;
	}
	CheckJoin();
	if((current_mask & ~commit_mask).any()) {
		// Some work still to be done?
		// Replay
		replay_mask = current_mask & ~commit_mask;
	}
	else {
		SetPC(GetNPC());
		replay_mask = current_mask;
	}
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
