/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_OPERATION_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_OPERATION_TCC

#include <unisim/component/cxx/processor/tesla/tesla_operation.hh>
#include <unisim/component/cxx/processor/tesla/tesla_opcode.hh>
#include <unisim/component/cxx/processor/tesla/tesla_dest.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src1.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src2.hh>
#include <unisim/component/cxx/processor/tesla/tesla_src3.hh>
#include <unisim/component/cxx/processor/tesla/tesla_control.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template <class CONFIG>
tesla_isa::src1::Decoder<CONFIG> TeslaOperation<CONFIG>::src1_decoder;

template <class CONFIG>
tesla_isa::src2::Decoder<CONFIG> TeslaOperation<CONFIG>::src2_decoder;

template <class CONFIG>
tesla_isa::src3::Decoder<CONFIG> TeslaOperation<CONFIG>::src3_decoder;

template <class CONFIG>
tesla_isa::dest::Decoder<CONFIG> TeslaOperation<CONFIG>::dest_decoder;

template <class CONFIG>
tesla_isa::control::Decoder<CONFIG> TeslaOperation<CONFIG>::control_decoder;

template <class CONFIG>
TeslaOperation<CONFIG>::TeslaOperation(typename CONFIG::address_t addr, typename CONFIG::insn_t iw) :
	addr(addr), iw(iw)
{
	// Don't cache subfields. Scope is controlled by Operation.
	src1 = src1_decoder.NCDecode(addr, iw);
	src2 = src2_decoder.NCDecode(addr, iw);
	src3 = src3_decoder.NCDecode(addr, iw);
	dest = dest_decoder.NCDecode(addr, iw);
	control = control_decoder.NCDecode(addr, iw);
	
	// Too early! Operation is not decoded yet!
	//EnumerateOperands();
	inputCount = 0;
	outputCount = 0;
	loadCount = 0;
	fallthrough = true;
}

template <class CONFIG>
TeslaOperation<CONFIG>::~TeslaOperation()
{
	delete src1;
	delete src2;
	delete src3;
	delete dest;
	delete control;
}

template <class CONFIG>
void TeslaOperation<CONFIG>::initStats(typename CONFIG::operationstats_t * stats)
{
	classify(stats);
	if(op_type[OpDest] != DT_NONE) {
		dest->classify(*stats, op_type[OpDest]);
	}
	if(op_type[OpSrc1] != DT_NONE) {
		src1->classify(*stats, op_type[OpSrc1]);
	}
	if(op_type[OpSrc2] != DT_NONE) {
		src2->classify(*stats, op_type[OpSrc2]);
	}
	if(op_type[OpSrc3] != DT_NONE) {
		src3->classify(*stats, op_type[OpSrc3]);
	}
}


template <class CONFIG>
bool TeslaOperation<CONFIG>::OutputsPred()
{
	return dest->needWritePred;
}

template <class CONFIG>
void TeslaOperation<CONFIG>::EnumerateOperands()
{
	inputCount = 0;
	outputCount = 0;
	loadCount = 0;
	src1->enumerateOperands(*this);
	src2->enumerateOperands(*this);
	src3->enumerateOperands(*this);
	dest->enumerateOperands(*this);

	enumerateOperands(*this);
//	control->enumerateOperands(*this);	// After opcode operand enumeration: needs fallthrough
}

template <class CONFIG>
void TeslaOperation<CONFIG>::AddInput(OperandID const & op)
{
	assert(inputCount < MaxInputs);
	inputs[inputCount++] = op;
}

// Who wrote this and what is it used for? No idea. Likely dead code.
template <class CONFIG>
void TeslaOperation<CONFIG>::AddLoad(unsigned no)
{
	assert(loadCount < MaxLoads);
	loads[loadCount++] = no;
}


template <class CONFIG>
void TeslaOperation<CONFIG>::AddGPRInput(RegType rt, unsigned int id)
{
	if(id == 124) return;	// Zero register, no dependency
	switch(rt) {
	case RT_U16:
		// Only consider dependencies at 32-bit granularity:
		// Can cause false dependencies
		AddInput(OperandID(DomainGPR, id/2));
		break;
	case RT_U32:
		AddInput(OperandID(DomainGPR, id));
		break;
	case RT_U64:
		assert(!(id & 1));
		AddInput(OperandID(DomainGPR, id));
		AddInput(OperandID(DomainGPR, id+1));
		break;
	case RT_U128:
		assert(!(id & 3));
		AddInput(OperandID(DomainGPR, id));
		AddInput(OperandID(DomainGPR, id+1));
		AddInput(OperandID(DomainGPR, id+2));
		AddInput(OperandID(DomainGPR, id+3));
		break;
	default:
		assert(false);
	}
}

template <class CONFIG>
void TeslaOperation<CONFIG>::AddOutput(OperandID const & op)
{
	assert(outputCount < MaxOutputs);
	outputs[outputCount++] = op;
}

template <class CONFIG>
void TeslaOperation<CONFIG>::AddGPROutput(RegType rt, unsigned int id)
{
	switch(rt) {
	case RT_U16:
		// Only consider dependencies at 32-bit granularity:
		// Can cause false dependencies
		AddOutput(OperandID(DomainGPR, id/2));
		break;
	case RT_U32:
		AddOutput(OperandID(DomainGPR, id));
		break;
	case RT_U64:
		assert(!(id & 1));
		AddOutput(OperandID(DomainGPR, id));
		AddOutput(OperandID(DomainGPR, id+1));
		break;
	case RT_U128:
		assert(!(id & 3));
		AddOutput(OperandID(DomainGPR, id));
		AddOutput(OperandID(DomainGPR, id+1));
		AddOutput(OperandID(DomainGPR, id+2));
		AddOutput(OperandID(DomainGPR, id+3));
		break;
	default:
		assert(false);
	}
}



} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
