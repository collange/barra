/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATS_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATS_TCC


#include <unisim/component/cxx/processor/tesla/stats.hh>
#include <unisim/component/cxx/processor/tesla/register.hh>
#include <unisim/component/cxx/processor/tesla/instruction.hh>
#include <iomanip>
#include <iterator>
//#include <iostream>
#include <algorithm>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


using namespace std;

template<class T, class ACCUM>
void StatCounter<T, ACCUM>::Append(T v)
{

	// Don't consider NaNs when computing min and max
	if(v > max)
		max = v;
	if(v < min)
		min = v;

	// Knuth inline variance calculation
	++n;
	ACCUM delta = v - mean;
	mean += delta / n;
	m2 += delta * (v - mean);

}

template<class T, class ACCUM>
ACCUM StatCounter<T, ACCUM>::Avg() const
{
	return mean;
}

template<class T, class ACCUM>
ACCUM StatCounter<T, ACCUM>::Variance() const
{
	if(n < 2) {
		return 0;
	}
	return m2 / (n - 1);
}

template<class T, class ACCUM>
T StatCounter<T, ACCUM>::Min() const
{
	return min;
}

template<class T, class ACCUM>
T StatCounter<T, ACCUM>::Max() const
{
	return max;
}

template<class T, class ACCUM>
T StatCounter<T, ACCUM>::N() const
{
	return n;
}

template<class T, class ACCUM>
void StatCounter<T, ACCUM>::Merge(StatCounter<T, ACCUM> const & other)
{
	if(other.n > 0) {
		ACCUM delta = other.mean - mean;
		unsigned int nsum = n + other.n;
		mean += delta * other.n / nsum;
		m2 += other.m2 + delta * delta * (n * other.n) / nsum;
		n = nsum;
		min = std::min(min, other.min);
		max = std::max(max, other.max);
	}
}

template<class CONFIG>
SimilarityType SpaceSimilarityCounter<CONFIG>::Append(VectorRegister<CONFIG> const & reg,
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	typedef std::map<uint32_t, Family> buckets;
	typedef typename buckets::iterator it_buckets;
	buckets bins;
	int temp_cluster_number = 1;
	uint32_t last_prefix;
	bool first_time = true;
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(mask[i]) {
			uint32_t prefix = reg[i] & mask_bits;

			if (first_time) {
				last_prefix = prefix;
				first_time = false;
			}
			temp_cluster_number += last_prefix != prefix;
			last_prefix = prefix;

			bins[prefix].reg[i] = reg[i];
			bins[prefix].mask.set(i);
		}
	}
	AffineMetaData<CONFIG> amd;
	SimilarityType Return = OTHER;
	switch(bins.size()) {
	case 0:
		return OTHER;	// Ignore
	case 1:
		amd = reg.CheckStridedMMD(mask);
		bin1_strided += amd.strided;
		bin1_uniform += amd.IsUniform();
		++bin1;
		Return = SDMT;
		break;
	case 2:
		amd = reg.CheckStridedMMD(mask);
		bin2_totally_strided += amd.strided;

		it_buckets bins_it = bins.begin();
		VectorRegister<CONFIG> firstFamily = bins_it->second.reg;
		std::bitset<CONFIG::WARP_SIZE> firstMask = bins_it->second.mask;
		std::advance(bins_it, 1);
		VectorRegister<CONFIG> secondFamily = bins_it->second.reg;
		std::bitset<CONFIG::WARP_SIZE> secondMask = bins_it->second.mask;

		amd = firstFamily.CheckStridedMMD(firstMask);
		short incr_strided = amd.strided;
		short incr_uniform = amd.IsUniform();
		amd = secondFamily.CheckStridedMMD(secondMask);
		incr_strided += amd.strided;
		incr_uniform += amd.IsUniform();

		bin2_clusters_number += temp_cluster_number;
		bin2_used_registers += mask.count();
		bin2_strided += incr_strided;
		bin2_uniform += incr_uniform;
		++bin2;
		Return = DDMT;
		break;
	}
	++total;
	return Return;
}

template<class CONFIG>
void SpaceSimilarityCounter<CONFIG>::Merge(SpaceSimilarityCounter<CONFIG> const & other)
{
	assert(mask_bits == other.mask_bits);
	bin1 += other.bin1;
	bin2 += other.bin2;
	total += other.total;
	bin1_strided += other.bin1_strided;
	bin2_strided += other.bin2_strided;
	bin1_uniform += other.bin1_uniform;
	bin2_uniform += other.bin2_uniform;
	bin2_totally_strided += other.bin2_totally_strided;
	bin2_clusters_number += other.bin2_clusters_number;
	bin2_used_registers += other.bin2_used_registers;
}

template<class CONFIG>
bool TimeSimilarityCounter<CONFIG>::Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg)
{
	bool Return = false;
	if ( !Seen[reg] ) {
		uint32_t perTimeCounter = 0;
		for (unsigned int i  = 0; i != CONFIG::WARP_SIZE; ++i) {
			if (AlreadyUsed[reg] && mask[i] && LastMask[reg][i]) {
				bool incr = ((LastReg[reg][i] & MaskBits) == (regs[i] & MaskBits));
				counter += incr;
				perTimeCounter += incr;
				++total;
			}
			LastReg[reg][i] = regs[i];
			LastMask[reg][i] = mask[i];
		}
		if (mask.count() != 0 && AlreadyUsed[reg]) {
			if (perTimeCounter == mask.count()) {
				++vecCounter;
				Return = true;
			}
			++vecTotal;
		}
	}

	SetSeen(reg);
	AlreadyUsed[reg] = true;
	return Return;
}

template<class CONFIG>
void TimeSimilarityCounter<CONFIG>::Merge(TimeSimilarityCounter<CONFIG> const & other)
{
	assert(MaskBits == other.MaskBits);
	LastReg = other.LastReg;
	AlreadyUsed = other.AlreadyUsed;
	counter += other.counter;
	total += other.total;
	vecTotal += other.vecTotal;
	vecCounter += other.vecCounter;
}

template<class CONFIG>
void TimeSimilarityCounter<CONFIG>::SetSeen(int reg)
{
	Seen[reg] = true;
}

template<class CONFIG>
void TimeSimilarityCounter<CONFIG>::ResetSeen()
{
	for (std::map<int, bool>::iterator it_Seen = Seen.begin(); it_Seen != Seen.end(); it_Seen++)
		it_Seen->second = false;
}

template<class CONFIG>
void TimeSimilarityCounter<CONFIG>::Update(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg)
{
	for (unsigned int i  = 0; i != CONFIG::WARP_SIZE; ++i) {
		LastReg[reg][i] = regs[i];
		LastMask[reg][i] = mask[i];
	}
	AlreadyUsed[reg] = true;
}

template<class CONFIG>
SimilarityCounter<CONFIG>::SimilarityCounter() : width(32), stride(8)
{
	int length = (width / stride) + 1;
	uint32_t mask = 0;

	for (unsigned int i = 0; i < length; ++i) {
		space[i] = *(new SpaceSimilarityCounter<CONFIG>(mask));
		spaceInTime[i] = *(new SpaceSimilarityCounter<CONFIG>(mask));
		time[i] = *(new TimeSimilarityCounter<CONFIG>(mask));
		timeInSDMT[i] = *(new TimeSimilarityCounter<CONFIG>(mask));
		timeInDDMT[i] = *(new TimeSimilarityCounter<CONFIG>(mask));
		mask = (mask >> stride) | ((~0)<<(32 - stride));
	}
}

template<class CONFIG>
void SimilarityCounter<CONFIG>::Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg)
{
	for (unsigned int i = 0; i <= width/stride; ++i) {
		SimilarityType tmp = space[i].Append(regs, mask);
		switch(tmp) {
			case DDMT:
				timeInDDMT[i].Append(regs, mask, reg);
				timeInSDMT[i].Update(regs, mask, reg);
				break;
			case SDMT:
				timeInSDMT[i].Append(regs, mask, reg);
				timeInDDMT[i].Update(regs, mask, reg);
				break;
			default :
				timeInSDMT[i].Update(regs, mask, reg);
				timeInDDMT[i].Update(regs, mask, reg);
				break;
		}
		if ( time[i].Append(regs, mask, reg) )
			spaceInTime[i].Append(regs, mask);
	}
}

template<class CONFIG>
void SimilarityCounter<CONFIG>::Merge(SimilarityCounter<CONFIG> const & other)
{
	for (unsigned int i = 0; i <= width/stride; ++i) {
		space[i].Merge(other.space[i]);
		spaceInTime[i].Merge(other.spaceInTime[i]);
		time[i].Merge(other.time[i]);
		timeInDDMT[i].Merge(other.timeInDDMT[i]);
		timeInSDMT[i].Merge(other.timeInSDMT[i]);
	}
}

template<class CONFIG>
void SimilarityCounter<CONFIG>::ResetSeen()
{
	for (unsigned int i = 0; i <= width/stride; ++i) {
		time[i].ResetSeen();
		timeInSDMT[i].ResetSeen();
		timeInDDMT[i].ResetSeen();
	}
}

template<class CONFIG>
void DualRegisterStats<CONFIG>::Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, bool b16)
{
	if (b16) {
		Hits += regs.Hits16(mask, false);
		Flatten += regs.FlattenNb();
		ReadVectorAccess += (regs.Hits16(mask, false) != mask.count());
		WriteVectorAccess += (regs.Hits16(~mask, false) != 0 || !regs.tag.IsStrided16(false, mask));
		Total += mask.size();
	}
	else {
		Hits += regs.Hits(mask);
		Flatten += regs.FlattenNb();
		ReadVectorAccess += (regs.Hits(mask) != mask.count());
		WriteVectorAccess += (regs.Hits(~mask) != 0 || !regs.tag.IsStrided(mask));
		Total += mask.size();
	}
}

template<class CONFIG>
void DualRegisterStats<CONFIG>::Merge(DualRegisterStats<CONFIG> const & other)
{
	Hits += other.Hits;
	Flatten += other.Flatten;
	ReadVectorAccess += other.ReadVectorAccess;
	WriteVectorAccess += other.WriteVectorAccess;
	Total += other.Total;
}

template<class CONFIG>
void LocalCacheStats<CONFIG>::DumpCSV(std::ostream & os) const
{
	os << "," << Hits
	   << "," << Misses;
	os << "," << Fills
	   << "," << WBacks;
}

template<class CONFIG>
void AffineCacheStats<CONFIG>::DumpCSV(std::ostream & os) const
{
	os << "," << Hits
	   << "," << Misses_noroom
	   << "," << Misses_notaff;
	os << "," << Fills
	   << "," << WBacks;
}


template<class CONFIG>
void AffineCacheStats<CONFIG>::Merge(AffineCacheStats<CONFIG> const & other)
{
	Hits			+= other.Hits;
	Misses_noroom	+= other.Misses_noroom;
	Misses_notaff	+= other.Misses_notaff;

	Fills	+= other.Fills;
	WBacks	+= other.WBacks;
}

template<class CONFIG>
void LocalCacheStats<CONFIG>::Merge(LocalCacheStats<CONFIG> const & other)
{
	Hits	+= other.Hits;
	Misses	+= other.Misses;

	Fills	+= other.Fills;
	WBacks	+= other.WBacks;
}

template<class CONFIG>
void WordSectoredCacheStats<CONFIG>::DumpCSV(std::ostream & os) const
{
	os << "," << Hits
	   << "," << Misses
	   << "," << PartialMisses;
	os << "," << Fills
	   << "," << WBacks;
}


template<class CONFIG>
void WordSectoredCacheStats<CONFIG>::Merge(WordSectoredCacheStats<CONFIG> const & other)
{
	Hits			+= other.Hits;
	Misses	+= other.Misses;
	PartialMisses	+= other.PartialMisses;

	Fills	+= other.Fills;
	WBacks	+= other.WBacks;
	Pressures += other.Pressures;
}

template<class CONFIG>
void OperationStats<CONFIG>::Merge(OperationStats<CONFIG> const & other)
{
	name = other.name;
	integer = other.integer;
	mov = other.mov;
	fp = other.fp;
	flow = other.flow;
	memory = other.memory;
	shared = other.shared;
	constant = other.constant;
    aliveIn = other.aliveIn;
    aliveOut = other.aliveOut;
    aliveLoad = other.aliveLoad;
	for(int i = 0; i != 4; ++i)
		regnum[i] = other.regnum[i];

    nonstridedAliveOutTotal += other.nonstridedAliveOutTotal;
    nonstridedAliveOutMin = min(nonstridedAliveOutMin,other.nonstridedAliveOutMin);
    nonstridedAliveOutMax = max(nonstridedAliveOutMax,other.nonstridedAliveOutMax);

	count += other.count;
	scalarcount += other.scalarcount;
	scalarRegInputs += other.scalarRegInputs;
	stridedRegInputs += other.stridedRegInputs;
	scalarRegOutputs += other.scalarRegOutputs;
	stridedRegOutputs += other.stridedRegOutputs;

	scalarRegInputsCaught += other.scalarRegInputsCaught;
	stridedRegInputsCaught += other.stridedRegInputsCaught;
	scalarRegOutputsCaught += other.scalarRegOutputsCaught;
	stridedRegOutputsCaught += other.stridedRegOutputsCaught;

	llStridedRegs += other.llStridedRegs;
	slStridedRegs += other.slStridedRegs;

    L0Cache.Merge(other.L0Cache);
    L1Cache.Merge(other.L1Cache);

	branchTaken += other.branchTaken;
	branchNotTaken += other.branchNotTaken;
	branchDivergent += other.branchDivergent;
	jump += other.jump;

	gatherGlobal += other.gatherGlobal;
	loadGlobal += other.loadGlobal;
	scatterGlobal += other.scatterGlobal;
	storeGlobal += other.storeGlobal;

	gatherLocal += other.gatherLocal;
	loadLocal += other.loadLocal;
	scatterLocal += other.scatterLocal;
	storeLocal += other.storeLocal;

	load32 += other.load32;
	load64 += other.load64;
	load128 += other.load128;

	store32 += other.store32;
	store64 += other.store64;
	store128 += other.store128;

	scalarAddress += other.scalarAddress;
	stridedAddress += other.stridedAddress;

	time_spent += other.time_spent;
	fpValue.Merge(other.fpValue);

	steps += other.steps;

    localUniform += other.localUniform;
    localZero += other.localZero;
    localNonPow2 += other.localNonPow2;
    localSparseVector += other.localSparseVector;

	inputSimilar.Merge(other.inputSimilar);
	outputSimilar.Merge(other.outputSimilar);

	inputDualReg.Merge(other.inputDualReg);
	outputDualReg.Merge(other.outputDualReg);

	genericFlatten += other.genericFlatten;

	classStats.Merge(other.classStats);
}

template<class CONFIG>
void OperationStats<CONFIG>::SetRegNum(int16_t reg, DataType dt, Operand n)
{
	switch(dt) {
	case DT_U32:
	case DT_S32:
	case DT_F32:
		regnum[n] = reg;
		break;
	case DT_U16:
	case DT_S16:
		regnum[n] = reg / 2;
		break;
	case DT_U64:
	case DT_F64:
		regnum[n] = reg * 2;
		break;
	case DT_U128:
		regnum[n] = reg * 4;
		break;
	default:
		regnum[n] = reg;	// TODO: count multiple registers for DT_U64/DT_U128
		break;
	}
}

template<class CONFIG>
void OperationStats<CONFIG>::RegRead(VectorRegister<CONFIG> const * regs, DataType dt, std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, int reg)
{
	if(CONFIG::STAT_SCALAR_REG) {
		if(dt == DT_U16 || dt == DT_S16) {
			scalarRegInputs += regs->CheckScalar16(false);
			scalarRegInputsCaught += regs->IsScalar16(false);
		}
		else if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			scalarRegInputs += regs->CheckScalar();
			scalarRegInputsCaught += regs->IsScalar();
		}
	}
	if(CONFIG::STAT_STRIDED_REG) {
		if(dt == DT_U16 || dt == DT_S16) {
			stridedRegInputs += regs->CheckStrided16(false);
			stridedRegInputsCaught += regs->IsStrided16(false);
		}
		else if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			stridedRegInputs += regs->CheckStrided();
			stridedRegInputsCaught += regs->IsStrided();
		}
	}

	if(CONFIG::STAT_SIMILARITY) {
		if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			inputSimilar.Append(*regs, mask, reg);
		}
	}

	if(CONFIG::STAT_DUAL_REG) {
		if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			inputDualReg.Append(*regs, mask);
		}
		else if ( dt == DT_U16 || dt == DT_S16 ) {
			inputDualReg.Append(*regs, mask, 1);
		}
	}

/*    if (CONFIG::STAT_REGISTER_CACHE)
        cpu->GetRegisterCache().RegRead(regs, mask, reg, cpu->WarpId(), L0());*/
}

template<class CONFIG>
void OperationStats<CONFIG>::RegWrite(VectorRegister<CONFIG> const * regs, DataType dt,	std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, int reg)
{
	if(CONFIG::STAT_SCALAR_REG) {
		if(dt == DT_U16 || dt == DT_S16) {
			if(mask == ~std::bitset<CONFIG::WARP_SIZE>(0)) {
				scalarRegOutputs += regs->CheckScalar16(false);
				scalarRegOutputsCaught += regs[0].IsScalar16(false);
			}
		}
		else if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			if(mask == ~std::bitset<CONFIG::WARP_SIZE>(0)) {
				scalarRegOutputs += regs[0].CheckScalar();
				scalarRegOutputsCaught += regs[0].IsScalar();
			}
		}
	}
	if(CONFIG::STAT_STRIDED_REG) {
		if(dt == DT_U16 || dt == DT_S16) {
			if(mask == ~std::bitset<CONFIG::WARP_SIZE>(0)) {
				stridedRegOutputs += regs[0].CheckStrided16(false);
				stridedRegOutputsCaught += regs[0].IsStrided16(false);
			}
		}
		else if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			if(mask == ~std::bitset<CONFIG::WARP_SIZE>(0)) {
				stridedRegOutputs += regs[0].CheckStrided();
				stridedRegOutputsCaught += regs[0].IsStrided();
			}
		}
	}

	// TODO: should not be here
	if(CONFIG::DEBUG_NONAN && (dt == DT_F32)) {
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
		{
			if(mask[i])
				assert(!isnan(regs[0].ReadFloat(i)));
		}
	}

	if(CONFIG::STAT_FPDOMAIN && (dt == DT_F32)) {
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
		{
			if(mask[i])
				fpValue.Append(regs[0].ReadFloat(i));
		}
	}

	if(CONFIG::STAT_SIMILARITY) {
		if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			outputSimilar.Append(*regs, mask, reg);
		}
	}

	if(CONFIG::STAT_DUAL_REG) {
		if(dt == DT_U32 || dt == DT_S32 || dt == DT_F32) {
			outputDualReg.Append(*regs, mask);
		}
		if (dt == DT_U16 || dt == DT_S16) {
			//std::cerr << " r" << (reg>>1) << " : " << *regs << std::endl;
			outputDualReg.Append(*regs, mask, 1);
		}
	}
/*    if (CONFIG::STAT_REGISTER_CACHE)
        cpu->GetRegisterCache().RegWrite(regs, mask, reg, cpu->WarpId(), L0());*/
}

template<class CONFIG>
void OperationStats<CONFIG>::RegKill(std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, typename CONFIG::address_t addr)
{
/*    if (CONFIG::STAT_REGISTER_CACHE)
        cpu->GetRegisterCache().RegKill(cpu->GetLiveness().ins[addr-cpu->CodeBase()] & ~(cpu->GetLiveness().outs[addr-cpu->CodeBase()]),mask,cpu->WarpId(),L0());*/
}


#if defined(__GNUC__) && defined(__i386__)
inline uint64_t rdtsc()
{
	uint64_t clk;
	__asm__ volatile ("rdtsc" : "=A" (clk));
	return clk;
}
#elif defined(__GNUC__) && defined(__x86_64__)

inline uint64_t rdtsc()
{
	uint32_t hi, lo;
	__asm__ volatile ("rdtsc" : "=a"(lo), "=d"(hi));
	return lo | ((uint64_t)hi << 32);
}

#else
//#warning "No TSC on this platform"
inline uint64_t rdtsc()
{
	return 0ULL;
}
#endif


template<class CONFIG>
void OperationStats<CONFIG>::Begin()
{
	if(CONFIG::STAT_SIMTIME) {
		timestamp = rdtsc();
	}
}

template<class CONFIG>
void OperationStats<CONFIG>::End()
{
	if(CONFIG::STAT_SIMTIME) {
		time_spent += rdtsc() - timestamp;
	}
}

template<class CONFIG>
void OperationStats<CONFIG>::Step()
{
	if(CONFIG::STAT_NIMT) {
		++steps;
	}
}

// G80-class coalescing (+ < 32-bit loads/stores)
template<class CONFIG>
bool Coalesceable(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask)
{
	for(unsigned int j = 0; j != CONFIG::WARP_SIZE; j += CONFIG::TRANSACTION_SIZE)
	{
		unsigned int i = 0;
		for(;i != CONFIG::TRANSACTION_SIZE && !mask[j + i];++i) {}	// Skip inactive threads
		if(i != CONFIG::TRANSACTION_SIZE) {
			typename CONFIG::address_t base = addr[j + i];
			if((base & ((1 << logsize) * CONFIG::TRANSACTION_SIZE - 1)) != (i << logsize)) {
				return false;	// Unaligned
			}
			for(; i != CONFIG::TRANSACTION_SIZE; ++i)
			{
				if(mask[j + i]) {
					if(addr[j + i] != base + i * (1 << logsize)) {
						return false;
					}
				}
			}
		}
	}
	return true;
}

inline unsigned int NativeSegmentSize(unsigned int logsize)
{
	switch(logsize)
	{
	case 1:
		return 32;
	case 2:
		return 64;
	default:
		return 128;
	}
}

template<class ADDR>
ADDR GetBase(ADDR address, unsigned int segmentSize)
{
	return address & ~(segmentSize - 1);
}

template<class ADDR>
unsigned int GetOffset(ADDR address, unsigned int segmentSize)
{
	return address & (segmentSize - 1);
}

template<class ADDR>
bool GetOffsetHi(ADDR address, unsigned int segmentSize)
{
	return address & (segmentSize >> 1);	// Returns false when segmentSize = 1
}

inline void AccountTransaction(unsigned int segmentSize, bool full,
	unsigned int & trans32, unsigned int & trans64, unsigned int & trans128)
{
	if(!full && segmentSize != 32) {
		segmentSize /= 2;
	}
	switch(segmentSize)
	{
	case 32:
		++trans32;
		break;
	case 64:
		++trans64;
		break;
	case 128:
		++trans128;
		break;
	default:
		assert(false);
	}
}

// GT200-class coalescing
template<class CONFIG>
void SplitTransaction(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask,
	unsigned int & trans32, unsigned int & trans64, unsigned int & trans128)
{
	// From NVIDIA CUDA PG 2.3:
	//	The following protocol is used to issue a memory transaction for a
	//	half-warp:
	//	* Find the memory segment that contains the address requested by the lowest
	//	numbered active thread. Segment size is 32 bytes for 1-byte data, 64 bytes for
	//	2-byte data, 128 bytes for 4-, 8- and 16-bit data.
	//	* Find all other active threads whose requested address lies in the same segment.
	//	* Reduce the transaction size, if possible:
	//	   If the transaction size is 128 bytes and only the lower or upper half is used,
	//	   reduce the transaction size to 64 bytes;
	//	   If the transaction size is 64 bytes and only the lower or upper half is used,
	//	   reduce the transaction sizez to 32 bytes.
	//	* Carry out the transaction and mark the serviced threads as inactive.
	//	* Repeat until all threads in the half-warp are serviced.

	unsigned int segmentSize = NativeSegmentSize(logsize);
	for(unsigned int j = 0; j != CONFIG::WARP_SIZE; j += CONFIG::TRANSACTION_SIZE)
	{
		unsigned int i = 0;
		while(true) {
			for(;i != CONFIG::TRANSACTION_SIZE && !mask[j + i];++i) {}	// Skip inactive threads

		if(i == CONFIG::TRANSACTION_SIZE) break;

			typename CONFIG::address_t base = addr[j + i];
			typename CONFIG::address_t segmentBase = GetBase(base, segmentSize);
			bool offsetHiBase = GetOffsetHi(base, segmentSize);
			bool full = false;
			for(; i != CONFIG::TRANSACTION_SIZE; ++i)
			{
				if(mask[j + i]) {
					typename CONFIG::address_t segmentTrans = GetBase(addr[j + i], segmentSize);
					if(segmentTrans == segmentBase) {
						bool offsetHiTrans = GetOffsetHi(addr[j + i], segmentSize);
						full |= (offsetHiTrans != offsetHiBase);
						mask[j + i] = false;
					}
				}
			}
			AccountTransaction(segmentSize, full, trans32, trans64, trans128);
		}
	}
}


template<class CONFIG>
void OperationStats<CONFIG>::GatherGlobal(VectorAddress<CONFIG> const & addr, unsigned int logsize,
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(Coalesceable(addr, logsize, mask)) {
		LoadGlobal(logsize);
	}
	else {
		gatherGlobal += (1 << logsize);
	}

	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		SplitTransaction(addr, logsize, mask, load32, load64, load128);
	}
	Transaction(addr, logsize, mask);
}

template<class CONFIG>
void OperationStats<CONFIG>::ScatterGlobal(VectorAddress<CONFIG> const & addr, unsigned int logsize,
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(Coalesceable(addr, logsize, mask)) {
		StoreGlobal(logsize);
	}
	else {
		scatterGlobal += (1 << logsize);
	}
	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		SplitTransaction(addr, logsize, mask, store32, store64, store128);
	}
	Transaction(addr, logsize, mask);
}

template<class CONFIG>
void OperationStats<CONFIG>::GatherLocal(VectorAddress<CONFIG> const & addr, unsigned int logsize,
	std::bitset<CONFIG::WARP_SIZE> mask, typename OperationStats<CONFIG>::VecReg const output[])
{
	if(Coalesceable(addr, logsize, mask)) {
		LoadLocal(logsize, output, mask);
	}
	else {
		gatherLocal += (1 << logsize);
	}
	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		SplitTransaction(addr, logsize, mask, load32, load64, load128);
	}
	Transaction(addr, logsize, mask);
}

template<class CONFIG>
bool IsScalar(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// Only 1 bit set? (or zero)
	return mask.count() <= 1;
}

template<class CONFIG>
bool IsDense(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// At least one (2k, 2k+1) pair set
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; i += 2)
	{
		if(mask[i] && mask[i+1]) {
			return true;
		}
	}
	return false;
}

template<class CONFIG>
void OperationStats<CONFIG>::LoadLocal(unsigned int logsize, typename OperationStats<CONFIG>::VecReg const output[],
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	loadLocal += (1 << logsize);
	if((logsize==2) && output[0].CheckStridedMasked(mask)) {
		llStridedRegs++;
	}
	LoadStoreLocal(logsize, output, mask);
}

template<class CONFIG>
void OperationStats<CONFIG>::LoadStoreLocal(unsigned int logsize, typename OperationStats<CONFIG>::VecReg const output[],
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	AffineMetaData<CONFIG> amd = output[0].CheckStridedMMD(mask);
	if(amd.IsUniform()) {
		++localUniform;
		if(amd.IsZero()) {
			++localZero;
		}
	}
	if(amd.strided && !amd.IsStridePow2()) {
		++localNonPow2;
	}
	if(amd.strided && !IsScalar<CONFIG>(mask) && !IsDense<CONFIG>(mask)) {
		++localSparseVector;
	}
}


template<class CONFIG>
void OperationStats<CONFIG>::ScatterLocal(VectorAddress<CONFIG> const & addr, unsigned int logsize,
	std::bitset<CONFIG::WARP_SIZE> mask, typename OperationStats<CONFIG>::VecReg const output[])
{
	if(Coalesceable(addr, logsize, mask)) {
		StoreLocal(logsize, output, mask);
	}
	else {
		scatterLocal += (1 << logsize);
	}
	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		SplitTransaction(addr, logsize, mask, store32, store64, store128);
	}
	Transaction(addr, logsize, mask);
}

template<class CONFIG>
void OperationStats<CONFIG>::StoreLocal(unsigned int logsize, typename OperationStats<CONFIG>::VecReg const output[],
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	storeLocal += (1 << logsize);
	if((logsize==2) && output[0].CheckStridedMasked(mask)) {
		slStridedRegs++;
	}
	LoadStoreLocal(logsize, output, mask);
}


template<class CONFIG>
void OperationStats<CONFIG>::Transaction(VectorAddress<CONFIG> const & addr, unsigned int logsize,
	std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(CONFIG::STAT_SCALAR_REG) {
		scalarAddress += addr.IsScalar();
	}
	if(CONFIG::STAT_STRIDED_REG) {
		stridedAddress += addr.IsStrided();
	}
}

// Format:
// address,name,count,scalarcount,integer,fp,flow,memory
template<class CONFIG>
void OperationStats<CONFIG>::DumpCSV(std::ostream & os) const
{
	bool outputregs = (regnum[OpDest] != -1);
	int inputregs = 0;
	// Count unique registers only
	if(regnum[OpSrc1] != -1) { ++inputregs; }
	if(regnum[OpSrc2] != -1 && regnum[OpSrc2] != regnum[OpSrc1]) { ++inputregs; }
	if(regnum[OpSrc3] != -1 && regnum[OpSrc3] != regnum[OpSrc2]
		&& regnum[OpSrc3] != regnum[OpSrc2]) { ++inputregs; }

	os << "\"" << name << "\","
	   << count << ","
	   << scalarcount << ","
	   << integer << ","
	   << fp << ","
	   << flow << ","
	   << mov << ","
	   << memory << ","
	   << shared << ","
	   << constant << ","
	   << inputregs << ","
	   << outputregs;
	if(CONFIG::STAT_SCALAR_REG) {
		os << "," << scalarRegInputs
		   << "," << scalarRegOutputs;
		os << "," << scalarRegInputsCaught
		   << "," << scalarRegOutputsCaught;
	}
	if(CONFIG::STAT_STRIDED_REG) {
		os << "," << stridedRegInputs
		   << "," << stridedRegOutputs;
		os << "," << stridedRegInputsCaught
		   << "," << stridedRegOutputsCaught;
	}
	if(CONFIG::STAT_STRIDED_REG_LOCAL) {
		os << "," << llStridedRegs
		   << "," << slStridedRegs;
	}
	if(CONFIG::CACHE_ENABLED) {
        L1Cache.DumpCSV(os);
	}
	if(CONFIG::STAT_SIMTIME) {
		os << "," << time_spent;
	}
	os << "," << branchTaken
	   << "," << branchNotTaken
	   << "," << branchDivergent
	   << "," << jump;

	os << "," << gatherGlobal
	   << "," << loadGlobal
	   << "," << scatterGlobal
	   << "," << storeGlobal;

	os << "," << gatherLocal
	   << "," << loadLocal
	   << "," << scatterLocal
	   << "," << storeLocal;

	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		os << "," << load32
		   << "," << load64
		   << "," << load128;

		os << "," << store32
		   << "," << store64
		   << "," << store128;
	}

	if(CONFIG::STAT_FPDOMAIN) {
		if(fpValue.N() > 0) {
			os << "," << fpValue.Avg()
			   << "," << fpValue.Variance()
			   << "," << fpValue.Min()
			   << "," << fpValue.Max();
		}
		else {
			os << ",,,,";
		}
	}
	if(CONFIG::STAT_SCALAR_REG && CONFIG::STAT_STRIDED_REG) {
		os << "," << scalarAddress
		   << "," << stridedAddress;
	}
	if(CONFIG::STAT_NIMT) {
		os << "," << steps;
	}
    if(CONFIG::STAT_LIVENESS) {
        os << "," << aliveIn
		   << "," << aliveOut
		   << "," << ((count != 0)?double(nonstridedAliveOutTotal)/double(count):-1.)
		   << "," << nonstridedAliveOutMin
		   << "," << nonstridedAliveOutMax
		   << "," << aliveLoad;
    }
	if(CONFIG::STAT_REGISTER_CACHE) {
        L0Cache.DumpCSV(os);
	}
 	if(CONFIG::STAT_STRIDED_REG_LOCAL) {
 		os << "," << localUniform;
		os << "," << localZero;
		os << "," << localNonPow2;
		os << "," << localSparseVector;
	}

	if(CONFIG::STAT_SIMILARITY) {
		os << "," << inputSimilar.space[0].total;
		os << "," << inputSimilar.time[0].total;
		os << "," << inputSimilar.time[0].vecTotal;

		for (int i = 1; i < inputSimilar.width / inputSimilar.stride; ++i) {
			os << "," << inputSimilar.space[i].bin1;
			os << "," << inputSimilar.space[i].bin1_strided;
			os << "," << inputSimilar.space[i].bin1_uniform;
			os << "," << inputSimilar.timeInSDMT[i].counter;
			os << "," << inputSimilar.timeInSDMT[i].vecCounter;
			os << "," << inputSimilar.space[i].bin2;
			os << "," << inputSimilar.space[i].bin2_strided;
			os << "," << inputSimilar.space[i].bin2_totally_strided;
			os << "," << inputSimilar.space[i].bin2_uniform;
			os << "," << inputSimilar.space[i].bin2_clusters_number;
			os << "," << inputSimilar.space[i].bin2_used_registers;
			os << "," << inputSimilar.timeInDDMT[i].counter;
			os << "," << inputSimilar.timeInDDMT[i].vecCounter;
			os << "," << inputSimilar.time[i].counter;
			os << "," << inputSimilar.time[i].vecCounter;
			os << "," << inputSimilar.spaceInTime[i].bin1;
			os << "," << inputSimilar.spaceInTime[i].bin2;
		}

		os << "," << outputSimilar.space[0].total;
		os << "," << outputSimilar.time[0].total;
		os << "," << outputSimilar.time[0].vecTotal;

		for (int i = 1; i < outputSimilar.width / outputSimilar.stride; ++i) {
			os << "," << outputSimilar.space[i].bin1;
			os << "," << outputSimilar.space[i].bin1_strided;
			os << "," << outputSimilar.space[i].bin1_uniform;
			os << "," << outputSimilar.timeInSDMT[i].counter;
			os << "," << outputSimilar.timeInSDMT[i].vecCounter;
			os << "," << outputSimilar.space[i].bin2;
			os << "," << outputSimilar.space[i].bin2_strided;
			os << "," << outputSimilar.space[i].bin2_totally_strided;
			os << "," << outputSimilar.space[i].bin2_uniform;
			os << "," << outputSimilar.space[i].bin2_clusters_number;
			os << "," << outputSimilar.space[i].bin2_used_registers;
			os << "," << outputSimilar.timeInDDMT[i].counter;
			os << "," << outputSimilar.timeInDDMT[i].vecCounter;
			os << "," << outputSimilar.time[i].counter;
			os << "," << outputSimilar.time[i].vecCounter;
			os << "," << outputSimilar.spaceInTime[i].bin1;
			os << "," << outputSimilar.spaceInTime[i].bin2;
		}
	}

	if ( CONFIG::STAT_DUAL_REG ) {
		os << "," << inputDualReg.Hits;
		os << "," << inputDualReg.Total;
		os << "," << inputDualReg.Flatten;

		os << "," << outputDualReg.Hits;
		os << "," << outputDualReg.Total;
		os << "," << outputDualReg.Flatten;

		for (int i = 0; i != InstructionClassMax; ++i)
			os << "," << classStats.count[i];

		os << "," << inputDualReg.ReadVectorAccess;
		os << "," << outputDualReg.WriteVectorAccess;

		os << "," << genericFlatten;
	}

	os << endl;
}

template<class CONFIG>
void OperationStats<CONFIG>::Execute(std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, typename CONFIG::address_t addr)
{
	if(addr != 0) {
//		if (!mask.any()) {
		    // bar.sync, join have zero-predicate
		    //mask = cpu->GetCurrentMask();
		    // So what?
//		}

		unsigned nb_nonstrided = 0;

		for (unsigned i = 0; i < CONFIG::MAX_GPRS; i++) {
		    if (cpu->GetLiveness().outs[addr-cpu->CodeBase()][i] && !cpu->GetGPR(i).CheckStridedMasked(mask))
		        nb_nonstrided++;
		}

		nonstridedAliveOutTotal+=nb_nonstrided;
		nonstridedAliveOutMin = min(nonstridedAliveOutMin,nb_nonstrided);
		nonstridedAliveOutMax = max(nonstridedAliveOutMax,nb_nonstrided);

		scalarcount += mask.count();
	}

	inputSimilar.ResetSeen();
	outputSimilar.ResetSeen();
	++count;
}

template<class CONFIG>
void OperationStats<CONFIG>::Unexecute() {
	--count;
}


template<class CONFIG>
void OperationStats<CONFIG>::SetAliveIn(unsigned in)
{
    aliveIn = in;
}

template<class CONFIG>
void OperationStats<CONFIG>::SetAliveOut(unsigned out)
{
    aliveOut = out;
}

template<class CONFIG>
void OperationStats<CONFIG>::SetAliveLoad(unsigned load)
{
    aliveLoad = load;
}


template<class CONFIG>
void KernelStats<CONFIG>::Dump(std::ostream & os) const
{

	os << distance_generic.Avg()
		<< ", " << distance_generic.Min()
		<< ", " << distance_generic.Max();

	os	<< ", " << distance_affine.Avg()
	   	<< ", " << distance_affine.Min()
		<< ", " << distance_affine.Max();
	os << std::endl;
}


template<class CONFIG>
void KernelStats<CONFIG>::Merge(KernelStats<CONFIG> const & other)
{
	distance_generic.Merge(other.distance_generic);
	distance_affine.Merge(other.distance_affine);
}

template<class CONFIG>
void KernelStats<CONFIG>::RegDefUse(Instruction<CONFIG> const & instruction, CPU<CONFIG> & cpu)
{
	if(cpu.WarpId() == 0) {
		// Only consider 1st warp
		unsigned int const wid = 0;
		uint64_t progress = cpu.CurrentWarp().progress;

		// For each use (not def)
		for(unsigned int i = 0; i != instruction.InputCount(); ++i)
		{
			OperandID const & operand = instruction.Input(i);
			if(operand.domain == DomainGPR) {
				unsigned int r = operand.id;
				assert(progress >= last_use[r]);
				uint32_t distance = uint32_t(progress - last_use[r]);

				//  Check if affine
				//  Update stat counter
				VectorRegister<CONFIG> const & reg = cpu.GetGPR(wid, r);
				if(reg.CheckStrided()) {
					distance_affine.Append(distance);
				}
				else {
					distance_generic.Append(distance);
				}

			}
		}

		// For each def or use
		for(unsigned int i = 0; i != instruction.InputCount(); ++i)
		{
			OperandID const & operand = instruction.Input(i);
			if(operand.domain == DomainGPR) {
				last_use[operand.id] = progress;
			}
		}
		for(unsigned int i = 0; i != instruction.OutputCount(); ++i)
		{
			OperandID const & operand = instruction.Output(i);
			if(operand.domain == DomainGPR) {
				last_use[operand.id] = progress;	// Actually last def...
			}
		}

	}
}

template<class CONFIG>
void Stats<CONFIG>::Reset()
{
	stats.clear();
	kernel_stats.Reset();
}

template<class CONFIG>
void Stats<CONFIG>::DumpCSV(std::ostream & os) const
{
	os << "\"Address\","
	   << "\"Name\","
	   << "\"Executed\","
	   << "\"Exec. scalar\","
	   << "\"Integer\","
	   << "\"FP32\","
	   << "\"Flow\","
	   << "\"Mov\","
	   << "\"Memory\","
	   << "\"Shared\","
	   << "\"Constant\","
	   << "\"Input regs\","
	   << "\"Output regs\"";
	if(CONFIG::STAT_SCALAR_REG) {
		os << ",\"Scalar inputs\""
		   << ",\"Scalar outputs\"";
		os << ",\"Scalar inputs caught\""
		   << ",\"Scalar outputs caught\"";
	}
	if(CONFIG::STAT_STRIDED_REG) {
		os << ",\"Strided inputs\""
		   << ",\"Strided outputs\"";
		os << ",\"Strided inputs caught\""
		   << ",\"Strided outputs caught\"";
	}
	if(CONFIG::STAT_STRIDED_REG_LOCAL) {
		os << ",\"Strided regs in local loads\""
		   << ",\"Strided regs in local stores\"";
	}
	if(CONFIG::CACHE_ENABLED) {
		os << ",\"Local cache hits\""
		   << ",\"Local cache misses\"";
		os << ",\"Local cache fills\""
		   << ",\"Local cache WBacks\"";

		os << ",\"Affine cache hits\""
		   << ",\"Affine cache misses (no room)\""
		   << ",\"Affine cache misses (not aff)\"";
		os << ",\"Affine cache fills\""
		   << ",\"Affine cache WBacks\"";
	}
	if(CONFIG::STAT_SIMTIME) {
		os << ",\"Simulation time\"";
	}
	os << ",\"Branch taken\""
	   << ",\"Branch not taken\""
	   << ",\"Branch mixed\""
	   << ",\"Jump\"";

	os << ",\"Gather global\""
	   << ",\"Load global\""
	   << ",\"Scatter global\""
	   << ",\"Store global\"";

	os << ",\"Gather local\""
	   << ",\"Load local\""
	   << ",\"Scatter local\""
	   << ",\"Store local\"";

	if(CONFIG::STAT_TRANSACTIONWIDTH) {
		os << ",\"Load-32\""
		   << ",\"Load-64\""
		   << ",\"Load-128\"";

		os << ",\"Store-32\""
		   << ",\"Store-64\""
		   << ",\"Store-128\"";
	}

	if(CONFIG::STAT_FPDOMAIN) {
		os << ",\"fp-avg\""
		   << ",\"fp-var\""
		   << ",\"fp-min\""
		   << ",\"fp-max\"";
	}

	if(CONFIG::STAT_SCALAR_REG && CONFIG::STAT_STRIDED_REG) {
		os << ",\"Scalar@\""
		   << ",\"Strided@\"";
	}

	if(CONFIG::STAT_NIMT) {
		os << ",\"Steps\"";
	}

	if(CONFIG::STAT_LIVENESS) {
		os << ",\"AliveIn\""
		   << ",\"AliveOut\""
		   << ",\"Non Strided Alive : Average\""
		   << ",\"Non Strided Alive : Min\""
		   << ",\"Non Strided Alive : Max\""
		   << ",\"AliveLoad\"";
	}
    if(CONFIG::STAT_REGISTER_CACHE) {
		os << ",\"L0:Word sectored cache hits\""
		   << ",\"L0:Word sectored cache misses\""
		   << ",\"L0:Word sectored cache partial misses\"";
		os << ",\"L0:Word sectored cache fills\""
		   << ",\"L0:Word sectored cache WBacks\"";
	}
 	if(CONFIG::STAT_STRIDED_REG_LOCAL) {
 		os << "," << "\"localUniform\"";
		os << "," << "\"localZero\"";
		os << "," << "\"localNonPow2\"";
		os << "," << "\"localSparseVector\"";
	}

	if(CONFIG::STAT_SIMILARITY) {
		os << "," << "\"Input RegTotalNumber\"";
		os << "," << "\"Input RegTimeSimilarTotal\"";
		os << "," << "\"Input VecTimeSimilarTotal\"";

		for (int i = 8; i < 32; i += 8) {
			os << "," << "\"Input " << i << "-bit SDMT\"";
			os << "," << "\"Input " << i << "-bit SDMT strided\"";
			os << "," << "\"Input " << i << "-bit SDMT uniform\"";
			os << "," << "\"Input " << i << "-bit SDMT time similar register\"";
			os << "," << "\"Input " << i << "-bit SDMT time similar vector\"";
			os << "," << "\"Input " << i << "-bit DDMT\"";
			os << "," << "\"Input " << i << "-bit DDMT strided\"";
			os << "," << "\"Input " << i << "-bit DDMT totally strided\"";
			os << "," << "\"Input " << i << "-bit DDMT uniform\"";
			os << "," << "\"Input " << i << "-bit DDMT clusters number\"";
			os << "," << "\"Input " << i << "-bit DDMT used registers number\"";
			os << "," << "\"Input " << i << "-bit DDMT time similar register\"";
			os << "," << "\"Input " << i << "-bit DDMT time similar vector\"";
			os << "," << "\"Input " << i << "-bit time similar register\"";
			os << "," << "\"Input " << i << "-bit time similar vector\"";
			os << "," << "\"Input " << i << "-bit time similar SDMT\"";
			os << "," << "\"Input " << i << "-bit time similar DDMT\"";
		}

		os << "," << "\"Output RegTotalNumber\"";
		os << "," << "\"Output RegTimeSimilarTotal\"";
		os << "," << "\"Output VecTimeSimilarTotal\"";

		for (int i = 8; i < 32; i += 8) {
			os << "," << "\"Output " << i << "-bit SDMT\"";
			os << "," << "\"Output " << i << "-bit SDMT strided\"";
			os << "," << "\"Output " << i << "-bit SDMT uniform\"";
			os << "," << "\"Output " << i << "-bit SDMT time similar register\"";
			os << "," << "\"Output " << i << "-bit SDMT time similar vector\"";
			os << "," << "\"Output " << i << "-bit DDMT\"";
			os << "," << "\"Output " << i << "-bit DDMT strided\"";
			os << "," << "\"Output " << i << "-bit DDMT totally strided\"";
			os << "," << "\"Output " << i << "-bit DDMT uniform\"";
			os << "," << "\"Output " << i << "-bit DDMT clusters number\"";
			os << "," << "\"Output " << i << "-bit DDMT used registers number\"";
			os << "," << "\"Output " << i << "-bit DDMT time similar register\"";
			os << "," << "\"Output " << i << "-bit DDMT time similar vector\"";
			os << "," << "\"Output " << i << "-bit time similar register\"";
			os << "," << "\"Output " << i << "-bit time similar vector\"";
			os << "," << "\"Output " << i << "-bit time similar SDMT\"";
			os << "," << "\"Output " << i << "-bit time similar DDMT\"";
		}
	}

	if ( CONFIG::STAT_DUAL_REG ) {
		os << "," << "\"InputDualReg Hits\"";
		os << "," << "\"InputDualReg Total\"";
		os << "," << "\"InputDualReg Flatten\"";

		os << "," << "\"OutputDualReg Hits\"";
		os << "," << "\"OutputDualReg Total\"";
		os << "," << "\"OutputDualReg Flatten\"";

		os << "," << "\"ClassNOP\"";
		os << "," << "\"ClassMOV\"";
		os << "," << "\"ClassALU\"";
		os << "," << "\"ClassAGU\"";
		os << "," << "\"ClassXLU\"";
		os << "," << "\"ClassFMAD\"";
		os << "," << "\"ClassFMUL\"";
		os << "," << "\"ClassSFU\"";
		os << "," << "\"ClassDFMA\"";
		os << "," << "\"ClassBR\"";
		os << "," << "\"ClassMEM\"";
		os << "," << "\"ClassSCI\"";

		os << "," << "\"Input Vector Access\"";
		os << "," << "\"Output Vector Access\"";

		os << "," << "\"Read Vector Flatten\"";
	}

	os << endl;

	typedef typename stats_map::const_iterator it_t;
	for(it_t it = stats.begin(); it != stats.end(); ++it)
	{
		os << it->first << ",";
		it->second.DumpCSV(os);
	}
}

template<class CONFIG>
void Stats<CONFIG>::DumpGlobal(std::ostream & os, char const * kernelName) const
{
	//head
	os << "Kernels";
	os << "," << "\"dist_generic Avg\"";
	os << "," << "\"dist_generic Min\"";
	os << "," << "\"dist_generic Max\"";
	os << "," << "\"dist_affine Avg\"";
	os << "," << "\"dist_affine Min\"";
	os << "," << "\"dist_affine Max\"";
	os << endl;
	//data
	os << "\"" << kernelName << "\", ";
	kernel_stats.Dump(os);
}

template<class CONFIG>
void Stats<CONFIG>::Merge(Stats<CONFIG> const & other)
{
	typedef typename stats_map::const_iterator it_t;
	for(it_t it = other.stats.begin();
		it != other.stats.end();
		++it)
	{
		stats[it->first].Merge(it->second);
	}

	kernel_stats.Merge(other.kernel_stats);
}


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
