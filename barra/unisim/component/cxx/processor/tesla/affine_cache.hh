/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_AFFINE_CACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_AFFINE_CACHE_HH

#include <unisim/component/cxx/cache/cache.hh>
#include <unisim/component/cxx/processor/tesla/local_cache.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
class AffineCacheStats;


using unisim::component::cxx::cache::CacheBlock;
using unisim::component::cxx::cache::CacheLine;
using unisim::component::cxx::cache::CacheSet;


template<class CONFIG, class AFFINE_CACHE>
class AffineCache
{
	public:
		typedef	VectorRegister<CONFIG> VecReg;
		typedef typename AFFINE_CACHE::address_t address_t;
		typedef typename cxx::cache::Cache<AFFINE_CACHE> Cache;
		typedef std::bitset<CONFIG::WARP_SIZE> mask_t;
	
		void EvictFill(CacheAccess<AFFINE_CACHE>& affacess, AffineCacheStats<CONFIG> &stats, mask_t mask);

		mask_t LookupMasked(CacheAccess<AFFINE_CACHE>& access, AffineCacheStats<CONFIG> &stats);

		void ChooseLineToEvict(CacheAccess<AFFINE_CACHE>& access);
		void EmuEvict(CacheAccess<AFFINE_CACHE>& access, AffineCacheStats<CONFIG> &stats);
		void EmuFill(CacheAccess<AFFINE_CACHE>& access, mask_t mask, AffineCacheStats<CONFIG> &stats);
		void EmuWrite(CacheAccess<AFFINE_CACHE>& access, mask_t mask, AffineCacheStats<CONFIG> &stats);
		
		void UpdateReplacementPolicy(CacheAccess<AFFINE_CACHE>& access);
		void MRU(CacheAccess<AFFINE_CACHE>& access);

		void Invalidate();

		AffineCache(CPU<CONFIG>& my_cpu) : cpu(my_cpu) {  };

		void PartialInvalidate(CacheAccess<AFFINE_CACHE>& access, AffineCacheStats<CONFIG> &stats, mask_t mask);
        void Alloc(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask);
        void Fill(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask);
        void Read(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask);
        void Write(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask);

	private:
		Cache affine_cache;
		CPU<CONFIG>& cpu;
};






} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
