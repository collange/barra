/*
 *  Copyright (c) 2014,
 *  Inria,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_INSTRUCTION_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_INSTRUCTION_HH

//#include <unisim/component/cxx/processor/tesla/instruction.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template <class CONFIG>
struct InstructionBase;

template <class CONFIG>
struct TeslaInstruction : InstructionBase<CONFIG>
{
	typedef InstructionBase<CONFIG> Base;

	// External interface: with CPU
	// Loosely matches pipeline stages
	void Fetch(typename CONFIG::address_t addr);
	void Read();
	void Execute();
	void Write();
	void Disasm(std::ostream & os) const;
	void Abort(std::bitset<CONFIG::WARP_SIZE> abort_mask);

	VectorFlags<CONFIG> & Flags();

	void WriteFlags();
	void SetPredI32();
	void SetPredI16();
	void SetPredF32();
	

	// Deprecated interface
	typedef typename tesla_isa::opcode::Operation<CONFIG> OpCode;
	typedef tesla_isa::dest::Operation<CONFIG> OpDest;
	typedef tesla_isa::src1::Operation<CONFIG> OpSrc1;
	typedef tesla_isa::src2::Operation<CONFIG> OpSrc2;
	typedef tesla_isa::src3::Operation<CONFIG> OpSrc3;
	typedef tesla_isa::control::Operation<CONFIG> OpControl;

	void SetPred(VectorFlags<CONFIG> flags) const;

	
	void DisasmSrc1(std::ostream & os) const;
	void DisasmSrc2(std::ostream & os) const;
	void DisasmSrc3(std::ostream & os) const;
	void DisasmDest(std::ostream & os) const;
	void DisasmControl(std::ostream & os) const;
	
	bool IsLong() const;
	bool IsEnd() const;

	TeslaInstruction(CPU<CONFIG> * cpu, typename CONFIG::address_t addr);
	virtual ~TeslaInstruction();

	TeslaInstruction();
	
	RegType OperandRegType(Operand op) const;		// 
	DataType OperandDataType(Operand op) const;	// same value, different type (RegType more restricted)
	SMType	OperandSMType(Operand op) const;
	size_t OperandSize(Operand op) const;
	
	// Up-to-date interface (as of 2011...)
	void InitDependencies();	// Call BEFORE reading input/output/control dependencies
	unsigned int InputCount() const;  // Number of inputs dependencies
	OperandID const & Input(unsigned int i) const;  // Input dependency # i
	unsigned int OutputCount() const;
	OperandID const & Output(unsigned int i) const;
	unsigned int LoadCount() const;
	unsigned Load(unsigned int i) const;
    std::vector<Next<CONFIG> > findNexts (Context<CONFIG> &context);
	std::vector<Target<CONFIG> > Targets ();

	void InitStats();


private:
	VectorFlags<CONFIG> flags;
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
