/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULING_POLICIES_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULING_POLICIES_TCC


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// AgeSchedulingPolicy

template<class CONFIG>
uint64_t AgeSchedulingPolicy<CONFIG>::operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time)
{
	// Forbid (sub)warp splitting?
	if((!CONFIG::WARP_SPLIT_UNAVAIL && candidate.split_unavail)
		|| (!CONFIG::WARP_SPLIT_RESOURCES && candidate.split_resources)
		|| (CONFIG::SCHED_ONLY_PRIMARY && candidate.subwarpid != 0))
	{
		return 0;
	}
	// Give highest priority to oldest warp
	return time - last_issue[candidate.warpid] + 1;
}

template<class CONFIG>
void AgeSchedulingPolicy<CONFIG>::Reset()
{
	for(unsigned int i = 0; i != CONFIG::MAX_WARPS; ++i)
	{
		last_issue[i] = 0;
	}
}

template<class CONFIG>
void AgeSchedulingPolicy<CONFIG>::Update(unsigned int warpid, unsigned int subwarpid, Time time_issue)
{
	last_issue[warpid] = time_issue;
}

// SkewedSchedulingPolicy

template<class CONFIG>
uint64_t SkewedSchedulingPolicy<CONFIG>::operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time)
{
	// Forbid (sub)warp splitting?
	if((!CONFIG::WARP_SPLIT_UNAVAIL && candidate.split_unavail)
		|| (!CONFIG::WARP_SPLIT_RESOURCES && candidate.split_resources)
		|| (CONFIG::SCHED_ONLY_PRIMARY && candidate.subwarpid != 0))
	{
		return 0;
	}

	InstructionBufferEntry<CONFIG> & ibe = instruction_buffer.Read(candidate.warpid, candidate.subwarpid);
	typename CONFIG::address_t pc = ibe.subwarp.pc;
	typename CONFIG::address_t skewed_pc = pc + candidate.warpid * CONFIG::SCHED_SKEW_BYTES;

	// Give the highest priority to the lowest skewed PC
	return CONFIG::CODE_END - skewed_pc;
}

// BestfitSchedulingPolicy
template<class CONFIG, class SEC>
uint64_t BestfitSchedulingPolicy<CONFIG, SEC>::operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time)
{
	typedef typename CONFIG::address_t address_t;
	
	// Forbid (sub)warp splitting?
	if((!CONFIG::WARP_SPLIT_UNAVAIL && candidate.split_unavail)
		|| (!CONFIG::WARP_SPLIT_RESOURCES && candidate.split_resources)
		|| (CONFIG::SCHED_ONLY_PRIMARY && candidate.subwarpid != 0))
	{
		return 0;
	}
	
	if((candidate.warpid ^ candidate.previous_warpid) & CONFIG::SCHED_SET_BITS) {
		// Reject warps from different sets
		return 0;
	}

	// Check reconvergence constraints
	if(CONFIG::NIMT_CONSTRAINTS && candidate.subwarpid != 0) {
		InstructionBufferEntry<CONFIG> & main_ibe
			= instruction_buffer.Read(candidate.warpid, 0);
		InstructionBufferEntry<CONFIG> & ibe
			= instruction_buffer.Read(candidate.warpid, candidate.subwarpid);
		
		address_t divpt = this->sm.divergence_points[ibe.subwarp.pc - this->sm.CodeBase()];
		if(divpt != address_t(-1)
			&& main_ibe.valid
			&& (!CONFIG::NIMT_CONSTRAINTS_SELECTIVE 
			    || main_ibe.subwarp.pc > divpt + this->sm.CodeBase())
		)
		{
			return 0;
		}
	}

//	InstructionBufferEntry<CONFIG> & ibe = instruction_buffer.Read(candidate.warpid, candidate.subwarpid);
	
//	typename CONFIG::address_t pc = ibe.subwarp.pc;
//	typename CONFIG::address_t skewed_pc = pc + candidate.warpid * CONFIG::SCHED_SKEW_BYTES;
	uint64_t secondary_prio = secondary(candidate, time);

	// Give priority to subwarps that give higher occupancy
	uint64_t prio_idle = CONFIG::WARP_SIZE - candidate.idle;
	uint64_t prio_unavail = CONFIG::WARP_SIZE - candidate.split_unavail;
	uint64_t prio_resources = CONFIG::WARP_SIZE - candidate.split_resources;
	
	// Avoid subdivision whenever possible
	uint64_t prio = (prio_resources << CONFIG::LOG_WARP_SIZE | prio_unavail)
		<< CONFIG::LOG_WARP_SIZE | prio_idle;
	
	// Give priority to primary subwarps
	uint64_t swprio = 0;
	if(CONFIG::FAVOR_PRIMARY_SUBWARPS) {
		swprio = CONFIG::NIMT_PHASES - candidate.subwarpid - 1;
	}
	return (((prio << 2) | swprio) << 32) | secondary_prio;
}

// RandomSchedulingPolicy
template<class CONFIG>
uint64_t RandomSchedulingPolicy<CONFIG>::operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time)
{
	// Forbid (sub)warp splitting?
	if((!CONFIG::WARP_SPLIT_UNAVAIL && candidate.split_unavail)
		|| (!CONFIG::WARP_SPLIT_RESOURCES && candidate.split_resources)
		|| (CONFIG::SCHED_ONLY_PRIMARY && candidate.subwarpid != 0))
	{
		return 0;
	}

	return uint64_t(rand()) + 1;
}

// RoundrobinSchedulingPolicy
template<class CONFIG>
uint64_t RoundrobinSchedulingPolicy<CONFIG>::operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time)
{
	// Forbid (sub)warp splitting?
	if((!CONFIG::WARP_SPLIT_UNAVAIL && candidate.split_unavail)
		|| (!CONFIG::WARP_SPLIT_RESOURCES && candidate.split_resources)
		|| (CONFIG::SCHED_ONLY_PRIMARY && candidate.subwarpid != 0))
	{
		return 0;
	}

	// Nextwarp gets highest score. Nextwarp+1 second highest...
	// Nextwarp - 1 gets lowest
	unsigned int distance = (candidate.warpid - nextwarp) % CONFIG::MAX_WARPS;
	return uint64_t(CONFIG::MAX_WARPS - distance + 1);
}

template<class CONFIG>
void RoundrobinSchedulingPolicy<CONFIG>::Update(unsigned int warpid, unsigned int subwarpid, Time time_issue)
{
	nextwarp = (warpid + 1) % CONFIG::MAX_WARPS;
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
