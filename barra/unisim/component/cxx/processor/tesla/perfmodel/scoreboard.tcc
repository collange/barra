/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCOREBOARD_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCOREBOARD_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/scoreboard.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {


template<class CONFIG>
Scoreboard<CONFIG>::Scoreboard()
{
}

template<class CONFIG>
Scoreboard<CONFIG>::~Scoreboard()
{
}

template<class CONFIG>
void Scoreboard<CONFIG>::Reset()
{
	for(unsigned int j = 0; j != CONFIG::MAX_VGPR; ++j)
	{
			gpr[j] = 0;
	}
	for(unsigned int j = 0; j != CONFIG::MAX_WARPS; ++j)
	{
		for(unsigned int k = 0; k != CONFIG::MAX_ADDR_REGS; ++k)
		{
			ar[j][k] = 0;
		}
		
		for(unsigned int k = 0; k != CONFIG::MAX_PRED_REGS; ++k)
		{
			pred[j][k] = 0;
		}
		shmem[j] = 0;
	}
}

template<class CONFIG>
void Scoreboard<CONFIG>::Write(Warp<CONFIG> const & warp, OperandID const & operand, std::bitset<CONFIG::WARP_SIZE> mask, Time time)
{
	uint32_t rid;
	switch(operand.domain) {
	case DomainGPR:
		rid = warp.GetGPRAddress(operand.id);
		gpr[rid] = std::max(gpr[rid], time);
		break;
	case DomainAR:
		ar[warp.id][operand.id] = std::max(ar[warp.id][operand.id], time);
		break;
	case DomainPred:
		pred[warp.id][operand.id] = std::max(pred[warp.id][operand.id], time);
		break;
	case DomainShared:
		shmem[warp.id] = std::max(shmem[warp.id], time);
		break;
	default:
		// Just ignore
		break;
	}
	
}

template<class CONFIG>
void Scoreboard<CONFIG>::WriteScalar(Warp<CONFIG> const & warp,
	OperandID const & operand, unsigned int lane, Time time)
{
	Write(warp, operand,
		std::bitset<CONFIG::WARP_SIZE>().set(lane),
		time);
}

template<class CONFIG>
Time Scoreboard<CONFIG>::Query(Warp<CONFIG> const & warp,
	OperandID const & operand,
	std::bitset<CONFIG::WARP_SIZE> mask) const
{
	uint32_t rid;
	switch(operand.domain) {
	case DomainGPR:
		rid = warp.GetGPRAddress(operand.id);
		return gpr[rid];
		break;
	case DomainAR:
		return ar[warp.id][operand.id];
		break;
	case DomainPred:
		return pred[warp.id][operand.id];
		break;
	case DomainShared:
		return shmem[warp.id];
		break;
	default:
		// Always available
		return 0;
	}
}

template<class CONFIG>
std::bitset<CONFIG::WARP_SIZE> Scoreboard<CONFIG>::Available(Warp<CONFIG> const & warp,
		OperandID const & opid,
		std::bitset<CONFIG::WARP_SIZE> mask, Time time) const
{
	if(Query(warp, opid, mask) > time) {
		mask.reset();
	}
	return mask;
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
