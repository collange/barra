/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMETRACKER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMETRACKER_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/timetracker.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
TimeTracker<CONFIG>::TimeTracker()
{
	Reset();
}

template<class CONFIG>
TimeTracker<CONFIG>::~TimeTracker()
{
}

template<class CONFIG>
void TimeTracker<CONFIG>::WaitUntil(Time newTime, WaitReason reason)
{
	assert(reason < WaitReasonCount);
	Time delay = newTime - now;
	timeCategory[reason] += delay;
	now = newTime;
	UpdateWatchdog(reason, delay);
}

template<class CONFIG>
void TimeTracker<CONFIG>::WaitDelay(Time delay, WaitReason reason)
{
	assert(reason < WaitReasonCount);
	timeCategory[reason] += delay;
	now += delay;
	UpdateWatchdog(reason, delay);
}

// Very naive model
template<class CONFIG>
void TimeTracker<CONFIG>::SpendEnergy(double energy, EnergyReason reason)
{
	assert(reason < EnergyReasonCount);
	energyCategory[reason] += energy;
}

template<class CONFIG>
void TimeTracker<CONFIG>::UpdateWatchdog(WaitReason reason, Time delay)
{
	if(reason == WaitScheduler) {
		time_in_sched += delay;
		if(time_in_sched > 10000 * CONFIG::CYCLE_TIME) {
			cerr << "Deadlock in Scheduler" << endl;
			//this->sm.DumpState(cerr);
			assert(false);
		}
	}
	else {
		time_in_sched = 0;
	}
}

template<class CONFIG>
void TimeTracker<CONFIG>::Reset()
{
	now = 0;
	for(unsigned int i = 0; i != WaitReasonCount; ++i)
	{
		timeCategory[i] = 0;
	}
	instructionsIssued = 0;
	instructionsExecuted = 0;
	instructionsFetched = 0;
	scalarExecuted = 0;
	time_in_sched = 0;
	unitTimeScalar = 0;
	unitTimeTotal = 0;
	for(unsigned int i = 0; i != EnergyReasonCount; ++i)
	{
		energyCategory[i] = 0;
	}
}

template<class CONFIG>
void TimeTracker<CONFIG>::Issue(std::bitset<CONFIG::WARP_SIZE> mask)
{
	++instructionsIssued;
	instructionsExecuted += mask.count();
}

template<class CONFIG>
void TimeTracker<CONFIG>::Scalar(std::bitset<CONFIG::WARP_SIZE> mask)
{
	scalarExecuted += mask.count();
}

template<class CONFIG>
void TimeTracker<CONFIG>::Fetch()
{
	++instructionsFetched;
}

template<class CONFIG>
void TimeTracker<CONFIG>::DumpCSV(std::ostream & os) const
{
	os << instructionsIssued;
	os << ", " << instructionsExecuted;
	os << ", " << now / MICROSECOND;
	for(unsigned int i = 0; i != WaitReasonCount; ++i)
	{
		os << ", " << timeCategory[i] / MICROSECOND;
	}
	os << ", " << instructionsFetched;
	for(unsigned int i = 0; i != EnergyReasonCount; ++i)
	{
		os << ", " << energyCategory[i] / MICROJOULE;
	}

	os << "," << unitTimeScalar / MICROSECOND;
	os << "," << unitTimeTotal / MICROSECOND;
	os << "," << instructionsExecuted / unitTimeTotal;
	os << "," << scalarExecuted / unitTimeScalar;
	
	os << std::endl;
}

template<class CONFIG>
void TimeTracker<CONFIG>::DumpCSVHeader(std::ostream & os) const
{
	static char const * WaitReasonName[] = {
		"Scheduler",
		"Issue",
		"Memory"
	};
	
	static char const * EnergyReasonName[] = {
		"IFetch",
		"Scheduler",
		"Issue",
		"Execution",
		"DCache",
		"DRAM"
	};

	//os << "Kernel, ";
	os << "Issued, Executed";
	os << ", Total";
	for(unsigned int i = 0; i != WaitReasonCount; ++i)
	{
		os << ", " << WaitReasonName[i];
	}
	os << ", Fetched";
	for(unsigned int i = 0; i != EnergyReasonCount; ++i)
	{
		os << ", " << EnergyReasonName[i];
	}
	os << ", \"Scalar unit time\"";
	os << ", \"Unit time total\"";
	os << ", Throughput";
	os << ", \"Scalar throughput\"";

	os << std::endl;
}

template<class CONFIG>
void TimeTracker<CONFIG>::SpendTime(double timeScalar, double timeGlobal)
{
	unitTimeScalar += timeScalar;
	unitTimeTotal += timeGlobal;
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
