/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_INTERFACES_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_INTERFACES_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/units.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// Pseudo-TLM (loosely timed)
// Supports:
// * scatter-gather
// * word-enable masks
// * transaction nacking
// * register-ALU transfers (lightweight)
//
// Timing/Power-only interface. No functional payload.
// For loads, the 'data' field already contains the value
// that will be loaded in the future.

// TODO: split transactions into 2 classes: constant-time and variable-time

enum TransactionType
{
	TransactionLoad,
	TransactionStore,
	TransactionVectorLoad,
	TransactionVectorStore,
	TransactionGather,
	TransactionScatter,
};

template<class CONFIG>
struct MemoryTransaction
{
	TransactionType type;
	VectorAddress<CONFIG> const & address;
	VectorRegister<CONFIG> const & data;
	std::bitset<CONFIG::WARP_SIZE> mask;
	uint32_t logsize;
	uint32_t factor;
	typename CONFIG::address_t offset;
	Domain domain;
	Warp<CONFIG> const & initiator;
	OperandID const & destination;
	std::bitset<CONFIG::WARP_SIZE> nack;
	Time time_initiated;
	Time time_accepted;
	Time time_completed;	// May be partially completed before this time
	
	MemoryTransaction(TransactionType type,
		VectorAddress<CONFIG> const & address,
		VectorRegister<CONFIG> const & data,
		std::bitset<CONFIG::WARP_SIZE> mask,
		uint32_t logsize,
		uint32_t factor,
		typename CONFIG::address_t offset,
		Domain domain,
		Warp<CONFIG> const & initiator,
		OperandID const & destination,
		Time time_initiated) :
		type(type), address(address), data(data), mask(mask),
		logsize(logsize), factor(factor), offset(offset),
		domain(domain), initiator(initiator), destination(destination),
		nack(0), time_initiated(time_initiated),
		time_accepted(time_initiated),
		time_completed(time_initiated)
	{}
	
	MemoryTransaction(TransactionType type,
		VectorAddress<CONFIG> const & address,
		VectorRegister<CONFIG> const & data,
		std::bitset<CONFIG::WARP_SIZE> mask,
		uint32_t logsize,
		Domain domain,
		Warp<CONFIG> const & initiator,
		OperandID const & destination,
		Time time_initiated) :
		type(type), address(address), data(data),
		mask(mask), logsize(logsize), factor(1), offset(0),
		domain(domain), initiator(initiator), destination(destination),
		nack(0), time_initiated(time_initiated),
		time_accepted(time_initiated),
		time_completed(time_initiated)
	{}
};

template<class CONFIG>
struct TimedMemoryInterface
{
	virtual void Load(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed) = 0;
	virtual void Store(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted) = 0;
	//virtual Time Synchronize(Time t) = 0;	// TODO: generalize sync to all memories
};

template<class CONFIG>
struct TimedVectorMemoryInterface : TimedMemoryInterface<CONFIG>
{
	virtual void Transport(MemoryTransaction<CONFIG> & transaction) = 0;
};

template<class CONFIG>
struct TimedUnitInterface
{
	// TODO: payload?
	virtual void Execute(Time time, Time & time_accepted, Time & time_completed) = 0;
};


template<class CONFIG>
struct PerfectMemory : TimedVectorMemoryInterface<CONFIG>
{
	virtual void Load(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed) {
		time_accepted = time_completed = time;
	}
	virtual void Store(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted) {
		time_accepted = time;
	}
	virtual void Transport(MemoryTransaction<CONFIG> & transaction) {
		transaction.time_accepted = transaction.time_initiated;
		transaction.time_completed = transaction.time_initiated;
	}
	virtual Time Synchronize(Time t) {
		return t;
	}
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
