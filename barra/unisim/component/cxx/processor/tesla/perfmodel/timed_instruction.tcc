/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMED_INSTRUCTION_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMED_INSTRUCTION_TCC


#include "unisim/component/cxx/processor/tesla/perfmodel/timed_instruction.hh"


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
TimedInstruction<CONFIG>::TimedInstruction(TeslaSM<CONFIG> * cpu, typename CONFIG::address_t addr, typename CONFIG::insn_t iw) :
	Instruction<CONFIG>(cpu, addr, iw), Instruction<CONFIG>::scalarInsn(1)
{
}

template<class CONFIG>
TimedInstruction<CONFIG>::~TimedInstruction()
{
}

//template<class CONFIG>
//void TimedInstruction<CONFIG>::Writeback(std::bitset<CONFIG::WARP_SIZE> mask, Time t)
//{
//}

//template<class CONFIG>
//void TimedInstruction<CONFIG>::Writeback(unsigned int lane, Time t)
//{
//}

template<class CONFIG>
Time TimedInstruction<CONFIG>::PCAvail(Time time_issue, Time cycle_time)
{
	switch(this->Class())
	{
	case ClassBR:
		return time_issue + CONFIG::BRANCH_CYCLES * cycle_time;
	case ClassNOP:
	case ClassMOV:
	case ClassALU:
	case ClassAGU:
	case ClassXLU:
	case ClassFMAD:
	case ClassFMUL:
	case ClassSFU:
	case ClassDFMA:
	case ClassMEM:
	case ClassSCI:
		return time_issue + CONFIG::ISSUE_CYCLES * cycle_time;
	default:
		assert(false);
	}
}

template<class CONFIG>
Time TimedInstruction<CONFIG>::OutputAvail(Time time_issue, Time cycle_time)
{
	// TODO: framework for modeling heterogeneous execution units
	switch(this->Class())
	{
	case ClassNOP:
	case ClassBR:
		return time_issue;
	case ClassMOV:
	case ClassALU:
	case ClassAGU:
	case ClassXLU:
	case ClassFMAD:
	case ClassFMUL:
	case ClassSFU:
	case ClassDFMA:
	case ClassMEM:
		return time_issue + CONFIG::FU_CYCLES * cycle_time;
	case ClassSCI:
		return time_issue + CONFIG::SCALAR_CYCLES * cycle_time;
	default:
		assert(false);
	}
}

//template<class CONFIG>
//double TimedInstruction<CONFIG>::Energy() const
//{
//	switch(this->Class())
//	{
//	case ClassNOP:
//	case ClassBR:
//		return CONFIG::ENERGY_NOP;
//	case ClassMOV:
//	case ClassALU:
//	case ClassAGU:
//	case ClassXLU:
//	case ClassFMAD:
//	case ClassFMUL:
//	case ClassSFU:
//	case ClassDFMA:
//	case ClassMEM:
//		return CONFIG::ENERGY_MAD;
//	default:
//		assert(false);
//	}
//}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
