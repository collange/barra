/*
 *  Copyright (c) 2011,
 *  ENS Lyon
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TESLASM_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TESLASM_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/tesla_sm.hh>
#include <unisim/component/cxx/processor/tesla/enums.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/frontend.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
TeslaSM<CONFIG>::TeslaSM(char const * name, Object * parent, int coreid, int core_count) :
	Object(name, parent),
	CPU<CONFIG>(name, parent, coreid, core_count),
	param_trace_pipeline("trace-pipeline", this, trace_pipeline),
	param_trace_scheduler("trace-scheduler", this, trace_scheduler),
	trace_pipeline(CONFIG::TRACE_PIPELINE),
	trace_scheduler(CONFIG::TRACE_SCHEDULER),
	instruction_buffer(this),
	frontend(*this, instruction_buffer, scoreboard, execution_units),
	cycle_time_int(CONFIG::CYCLE_TIME),
	param_cycle_time("cycle-time", this, cycle_time_int),
	cycle_time(cycle_time_int),
	current_instruction(0),
	dram_power(tracker, dram),
	smem(cycle_time),
	smem_power(tracker, smem),
	dcache_power(tracker, local_cache),
	icache_power(tracker, dummy_memory),
	coalescer(dram_power, scoreboard),
	local_cache(*this, dram_power, cycle_time),
	l0i_cache(*this, icache_power, tracker),
	instruction_memory(&l0i_cache),
	const_memory(&dummy_memory),
	shared_memory(&smem_power),
//	shared_memory(&dummy_memory),
//	global_memory(&coalescer),
	global_memory(CONFIG::HAS_GLOBAL_CACHE ?
		(TimedVectorMemoryInterface<CONFIG> *) &local_cache 
		: (TimedVectorMemoryInterface<CONFIG> *) &coalescer),
//	global_memory(&dummy_memory),
//	local_memory(&coalescer)
	local_memory(&local_cache),
	divergence_points(this)
{
}

template<class CONFIG>
TeslaSM<CONFIG>::~TeslaSM() 
{
}

template<class CONFIG>
bool TeslaSM<CONFIG>::Setup() 
{
	cycle_time = double(cycle_time_int);	// in ps
	tracker.Reset();
	return CPU<CONFIG>::Setup();
}

template<class CONFIG>
void TeslaSM<CONFIG>::Stop(int ret) 
{
	CPU<CONFIG>::Stop(ret);
}


template<class CONFIG>
void TeslaSM<CONFIG>::Run()
{
	bool finished = false;

	local_cache.Invalidate();

	// TODO: Reset function
	scoreboard.Reset();
	frontend.Reset();
	
	divergence_points.Build(this->code_base, this->code_base + this->code_size);
//	divergence_points.Dump(std::cerr);
	//this->kernel_stats.Reset();	// hack
	this->stats->Init();

	FetchAll();

	CPU<CONFIG>::InitRegs();
	
	do
	{
		finished = Step();
	}
	while(!finished);

	if(this->TraceReset())
		cerr << "All warps finished\n";
	
	// Synch pending writes
	if(this->TracePipeline()) {
		cerr << "Sync. from " << (tracker.Now() / NANOSECOND);
	}
	tracker.WaitUntil(dram.Synchronize(tracker.Now()), WaitMemory);
	if(this->TracePipeline()) {
		cerr << " to " << (tracker.Now() / NANOSECOND) << std::endl;
	}
	
	// Hack. cout, not cerr.
	tracker.DumpCSVHeader(std::cout);
	std::cout << "BARRA_TIMING#";
	tracker.DumpCSV(std::cout);

	// TODO: kernel name
	//std::cout << "BARRA_KERNEL# ";
	//this->kernel_stats.Dump(std::cout);
}

// Fill instruction buffer
template<class CONFIG>
void TeslaSM<CONFIG>::FetchAll()
{
	for(unsigned int i = 0; i != this->WarpCount(); ++i)
	{
		if(this->GetWarp(i).state != Warp<CONFIG>::Finished) {
			SubWarp<CONFIG> subwarp_to_fetch[CONFIG::NIMT_PHASES];
			unsigned int fetchcount =
				this->GetWarp(i).flow.AllPCs(subwarp_to_fetch, CONFIG::NIMT_PHASES);
	
			for(unsigned int j = 0; j != fetchcount; ++j)
			{
				Fetch(i, j, subwarp_to_fetch[j]);
			}
		}
	}
}

template<class CONFIG>
bool TeslaSM<CONFIG>::Step()
{
	// Pipeline
	// Starts with instruction scheduling,
	// ends with fetching of next instruction
	// Centered on issue
	// In-order issue, out-of-order writeback
	
	//   |        |       |             |       |          |        |
	//   | Sched  | Issue | Memory(S/C) |     Exec         |		|
	//   |        |       | Branch/replay       |       Fetch       |
	//   |        |       |             |       |          |        |
	// tFetch  tSched  tIssue         tMem  tNPCAvail  tWriteback tFetch
	
	// Blocking stage: present
	Time time_schedule = tracker.Now();
	
	InstructionMetaData insn[CONFIG::ISSUE_WIDTH];
	
	// TODO: consider blocking front-end
	bool all_finished;
	bool idle = frontend.Phase(insn, tracker.Now(), all_finished);
	
	if(all_finished) {
		return true;
	}
	tracker.SpendEnergy(CONFIG::SCHED_ENERGY, EnergyScheduler);
	tracker.SpendEnergy(execution_units.Energy(tracker.Now()), EnergyExecution);

	// Synchronize at tIssue
	if(idle) {
		// TODO: track reason for waiting
		tracker.WaitDelay(cycle_time, WaitScheduler);
		// Shortcut
		return false;
	} else {
		tracker.WaitDelay(cycle_time, WaitIssue);	// issue 1 instruction / clock
	}
	
	// End of blocking stage
	Time time_issue = tracker.Now();

	// Issue and functionally simulate all subwarps
	for(unsigned int p = 0; p != CONFIG::ISSUE_WIDTH; ++p)
	{
		if(insn[p].valid) {
			Issue(insn[p]);
			tracker.SpendEnergy(CONFIG::ISSUE_ENERGY, EnergyIssue);
			InstructionBufferEntry<CONFIG> & ibe
				= instruction_buffer.Read(insn[p].warpid, insn[p].subwarpid);
			tracker.SpendTime(execution_units.ScalarDuration(time_issue), execution_units.Duration(time_issue));
//			tracker.SpendEnergy(execution_units.Energy(ibe.instruction, ibe.subwarp.mask, p),
//				EnergyExecution);
		}
	}

	// The following happens in the future:
	
	// Update instruction buffer times
	Time time_pc_avail = 0;
	for(unsigned int p = 0; p != CONFIG::ISSUE_WIDTH; ++p)
	{
		if(insn[p].valid) {
			InstructionBufferEntry<CONFIG> & ibe
				= instruction_buffer.Read(insn[p].warpid, insn[p].subwarpid);
			time_pc_avail = std::max(time_pc_avail,
				ibe.instruction.PCAvail(time_issue, cycle_time));
		}
	}

	for(unsigned int p = 0; p != CONFIG::ISSUE_WIDTH; ++p)
	{
		if(insn[p].valid) {
			instruction_buffer.UpdatePC(insn[p].warpid, insn[p].subwarpid, time_pc_avail);
	//		cerr << "P" << p << ": UpdatePC Warp" << insn[p].warpid << "." << insn[p].subwarpid << endl;
		}
	}
	
	// Synchronize at tPCAvail (in the future)
	
	for(unsigned int p = 0; p != CONFIG::ISSUE_WIDTH; ++p)
	{
		if(insn[p].valid) {
			Writeback(insn[p], time_schedule, time_issue, time_pc_avail);
		}
	}
	
	current_instruction = 0;
	
	// Fetch next instructions for all unique warps
	// Sub-warps can have diverged/reconverged here
	
	std::bitset<CONFIG::MAX_WARPS> warps_seen;
	for(unsigned int p = 0; p != CONFIG::ISSUE_WIDTH; ++p)
	{
		if(!insn[p].valid || warps_seen[insn[p].warpid]) {
			// Invalid or already fetched: skip
	continue;
		}
		warps_seen.set(insn[p].warpid);
	
		SubWarp<CONFIG> subwarp_to_fetch[CONFIG::NIMT_PHASES];
		unsigned int fetchcount = this->GetWarp(insn[p].warpid).flow.AllPCs(subwarp_to_fetch, CONFIG::NIMT_PHASES);
	
		for(unsigned int i = 0; i != fetchcount; ++i)
		{
			// Fetch next instructions for these warps
			// Assumption: no external event can affect the CF of this warp at this point.
			Fetch(insn[p].warpid, i, subwarp_to_fetch[i]);
		}
		// Invalidate other entries
		for(unsigned int i = fetchcount; i != CONFIG::NIMT_PHASES; ++i)
		{
			instruction_buffer.Invalidate(insn[p].warpid, i);
		}
	}
	
	return false;	// Still work to do
}

template<class CONFIG>
void TeslaSM<CONFIG>::Issue(InstructionMetaData & insn)
{
	// Functionally simulate instruction
	InstructionBufferEntry<CONFIG> & ibe
		= instruction_buffer.Read(insn.warpid, insn.subwarpid);

	current_instruction = &ibe.instruction;
	this->current_warpid = insn.warpid;
	this->CurrentWarp().flow.Issue(ibe.subwarp);
	FunctionalSimStep(insn.phase);
	ibe.subwarp.mask &= ~replay_threads;	// Do not commit replaying threads
	this->CurrentWarp().flow.Commit(ibe.subwarp.mask);

	// Do not count cancelled instructions
	if ( CONFIG::STAT_SCALAR_INSN && ibe.instruction.Class() == ClassSCI )
		tracker.Scalar(ibe.subwarp.mask);
	tracker.Issue(ibe.subwarp.mask);
}

template<class CONFIG>
void TeslaSM<CONFIG>::Writeback(InstructionMetaData & insn,
	Time time_schedule, Time time_issue, Time time_pc_avail)
{
	// Writeback timing updated from external events (memory...)
	InstructionBufferEntry<CONFIG> & ibe
		= instruction_buffer.Read(insn.warpid, insn.subwarpid);

	Time time_writeback = ibe.instruction.OutputAvail(time_issue, cycle_time);

	Time time_actual_writeback = time_writeback;

	if(ibe.subwarp.mask.any()) {
		// Update scoreboard times
		for(unsigned int i = 0; i != ibe.instruction.OutputCount(); ++i)
		{
			scoreboard.Write(this->GetWarp(insn.warpid),
				ibe.instruction.Output(i),
				ibe.subwarp.mask, time_writeback);
			time_actual_writeback = std::max(time_actual_writeback,
				scoreboard.Query(this->GetWarp(insn.warpid),
					ibe.instruction.Output(i),
					ibe.subwarp.mask));
		}
	}

	if(TracePipeline()) {
		cerr << "P" << insn.phase << ": "
			<< "F:" << instruction_buffer.Read(insn.warpid, insn.subwarpid).time_fetched / NANOSECOND
			<< " |S:" << time_schedule / NANOSECOND << " |I:" << time_issue / NANOSECOND
			<< " |B:" << (time_pc_avail / NANOSECOND)
			<< " |W1:" << (time_writeback / NANOSECOND)
			<< " |W:" << (time_actual_writeback / NANOSECOND);
		if(!ibe.subwarp.mask.any()) {
			cerr << " | [CANCELED]";
		}
		cerr << std::endl;
	}
}

template<class CONFIG>
void TeslaSM<CONFIG>::Reset() {
	CPU<CONFIG>::Reset();
}

template<class CONFIG>
void TeslaSM<CONFIG>::ComputeNPC()
{
	// TODO: Should not be here, should be in *Flow
	if(current_instruction->IsLong()) {
		this->SetNPC(this->GetPC() + 8);
	}
	else {
		this->SetNPC(this->GetPC() + 4);
	}
}

template<class CONFIG>
void TeslaSM<CONFIG>::FunctionalSimStep(unsigned int phase)
{
	replay_threads.reset();
	TimedInstruction<CONFIG> & insn = *current_instruction;
	insn.Stats()->Begin();
	if(phase == 0) {
		insn.Stats()->Step();
	}
	if(this->TraceInsn())
	{
		cerr << "[" << (tracker.Now() / NANOSECOND) << "]";
		cerr << this->coreid << "." << this->current_warpid << ": ";
		cerr << std::hex;
		cerr.fill('0'); cerr.width(6); 
		cerr << this->GetPC() - this->code_base << " ";
		cerr << std::dec;
		insn.Disasm(cerr);
		cerr << endl;
	}
	ComputeNPC();	// Not definitive NPC: can be overwritten by branch/replay
	
	typename CONFIG::operationstats_t SaveStats = *insn.Stats();
	insn.Read();	// Read operands

	if ( CONFIG::STAT_SCALAR_INSN && !CONFIG::VERTICAL_SIMT ) {
		if ( insn.Class() != ClassSCI && CONFIG::USE_FLATTEN && insn.Flatten() ) { // Check if scalar or vector instruction
			Replay(~std::bitset<CONFIG::WARP_SIZE>(0));
			*insn.Stats() = SaveStats;
			insn.Stats()->Flatten();
			insn.Stats()->End();
			return; 
		} 
	}
	insn.Execute(); // Execute

	if(replay_threads.any()) {
		insn.Abort(replay_threads);
	}
	insn.Write();	// Writeback
	// Join or take other branch?
	//this->CheckJoin();
	insn.Stats()->End();
	if(CONFIG::STAT_DEFUSEDISTANCE) {
		//this->kernel_stats.RegDefUse(insn, *this);
		this->stats->RegDefUse(insn, *this);
	}

	//this->CurrentWarp().flow.EndStep();

//	if(this->TraceInsn())
//	{
//		cerr << "\t-> " << std::hex;
//		cerr.fill('0'); cerr.width(6); 
//		cerr << this->GetPC() - this->code_base << " ";
//		cerr << std::dec;
//		cerr << endl;
//	}
}

template <class CONFIG>
void TeslaSM<CONFIG>::Fetch(uint32_t warpid, unsigned int subwarpid,
	SubWarp<CONFIG> const & subwarp)
{
	//address_t pc = this->GetWarp(warpid).flow.GetPC();
	Time time_accepted, time_completed;
	
	// Fetch starts as soon as next PC is available
	Time time_pc_avail = instruction_buffer.Read(warpid, subwarpid).time_pc_avail;
	
	// Fetch timing
	instruction_memory->Load(subwarp.pc, CONFIG::LOG_INSTRUCTIONLENGTH,
		time_pc_avail, time_accepted, time_completed);

	if(this->TraceScheduler()) {
		cerr << " Fetch " << warpid << "." << subwarpid
			<< " pc=" << hex << subwarp.pc << dec
			<<  " at " << (time_pc_avail / NANOSECOND)
			<< " completed " << (time_completed / NANOSECOND) << endl;
	}

	instruction_buffer.Fetch(warpid, subwarpid, subwarp, time_completed);
}

template <class CONFIG>
void TeslaSM<CONFIG>::ProcessTransaction(MemoryTransaction<CONFIG> & transaction)
{
	TimedVectorMemoryInterface<CONFIG> * mem_itf = MemoryInterface(transaction.domain);
	assert(mem_itf);
	//cerr << global_memory << ", " << mem_itf << endl;
	mem_itf->Transport(transaction);

	if(transaction.nack.any()) {
		Replay(transaction.nack);
	}
	
	// Update scoreboard
//	Time mem_ready = tracker.Now();
//	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
//	{
//		if(mask[i] && !transaction.nack[i]) {
//			current_instruction.Writeback(i, transaction.time_completed[i]);
//			mem_ready = std::max(mem_ready, transaction.time_accepted[i]);
//		}
//	}
	// TODO: this is broken, scoreboard is updated twice for
	// global/local gather.
	// Either way, execution takes place AFTER shared/const mem,
	// must consider exec latency.
	// Solution: separate global gather from other accesses
	Time mem_ready = transaction.time_accepted;
//	current_instruction->Writeback(transaction.mask & ~transaction.nack,
//		transaction.time_completed);
	for(unsigned int i = 0; i != current_instruction->OutputCount(); ++i)
	{
		scoreboard.Write(this->CurrentWarp(),
			current_instruction->Output(i),
			transaction.mask & ~transaction.nack,
			transaction.time_completed);
	}
	tracker.WaitUntil(mem_ready, WaitMemory);
}

template <class CONFIG>
void TeslaSM<CONFIG>::Replay(std::bitset<CONFIG::WARP_SIZE> nack)
{
	//assert((this->GetCurrentMask() & ~nack).any());	// If we replay all threads,
												// we have a problem
	// Actually, this is valid (with multiple issue competing for the same units)
	replay_threads |= nack;
}

template <class CONFIG>
void TeslaSM<CONFIG>::Gather(VecAddr const & addr, VecReg & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize,
	uint32_t factor, typename CONFIG::address_t offset, Domain domain)
{
	CPU<CONFIG>::Gather(addr, data, mask, logsize, factor, offset, domain);
	// Hack: assumes destination register is (offset/4)th operand.
	// Output operand is only needed for Gather from Global/Local.
	// Other memory spaces and transaction types return the completion time
	// instead of updating the scoreboard directly.
	
	// TODO: split memory interface into 2 distinct models!
	OperandID operand;
	if(domain == DomainGlobal || domain == DomainLocal) {
		operand = current_instruction->Output(offset/4);
	}
	
	MemoryTransaction<CONFIG> transaction(TransactionGather, addr, data, mask, logsize, factor,
		offset, domain, this->CurrentWarp(), operand, tracker.Now());
	ProcessTransaction(transaction);
}

template <class CONFIG>
void TeslaSM<CONFIG>::Scatter(VecAddr const & addr, VecReg const & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize,
	uint32_t factor, typename CONFIG::address_t offset, Domain domain)
{
	CPU<CONFIG>::Scatter(addr, data, mask, logsize, factor, offset, domain);

	MemoryTransaction<CONFIG> transaction(TransactionScatter, addr, data, mask, logsize, factor,
		offset, domain, this->CurrentWarp(), OperandID(), tracker.Now());
	ProcessTransaction(transaction);
}

template <class CONFIG>
void TeslaSM<CONFIG>::Broadcast(address_t addr, VecReg & data,
	uint32_t logsize,
	uint32_t factor, typename CONFIG::address_t offset, Domain domain)
{
	CPU<CONFIG>::Broadcast(addr, data, logsize, factor, offset, domain);

	VecAddr va(addr);
	MemoryTransaction<CONFIG> transaction(TransactionLoad, va, data,
		std::bitset<CONFIG::WARP_SIZE>().set(),
		logsize, factor, offset, domain, this->CurrentWarp(), OperandID(), tracker.Now());
	ProcessTransaction(transaction);
}

template <class CONFIG>
void TeslaSM<CONFIG>::VectorLoad(address_t addr, VecReg & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain)
{
	CPU<CONFIG>::VectorLoad(addr, data, mask, logsize, domain);

	VecAddr va(addr);
	MemoryTransaction<CONFIG> transaction(TransactionVectorLoad, va, data, mask, logsize,
		domain, this->CurrentWarp(), OperandID(), tracker.Now());
	ProcessTransaction(transaction);
}


template <class CONFIG>
void TeslaSM<CONFIG>::VectorStore(address_t addr, VecReg const & data,
	std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain)
{
	CPU<CONFIG>::VectorStore(addr, data, mask, logsize, domain);

	VecAddr va(addr);
	MemoryTransaction<CONFIG> transaction(TransactionVectorStore, va, data, mask, logsize,
		domain, this->CurrentWarp(), OperandID(), tracker.Now());
	ProcessTransaction(transaction);
}

template <class CONFIG>
TimedVectorMemoryInterface<CONFIG> * TeslaSM<CONFIG>::MemoryInterface(Domain domain)
{
	switch(domain) {
	case DomainConst:
		return const_memory;
	case DomainShared:
		return shared_memory;
	case DomainGlobal:
		return global_memory;
	case DomainLocal:
		return local_memory;
	default:
		assert(false);
	}
}


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
