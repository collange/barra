/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_FRONTEND_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_FRONTEND_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/config.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
struct TeslaSM;

template<class CONFIG>
struct InstructionBuffer;

template<class CONFIG>
struct Scoreboard;

struct InstructionMetaData {
	unsigned int warpid;
	unsigned int subwarpid;
	unsigned int phase;
	bool valid;
};

template<class CONFIG>
struct Frontend
{
	Frontend(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb,
		typename CONFIG::executionunits_t & eu)
		: sm(sm), instruction_buffer(ib), scoreboard(sb), execution_units(eu),
		primary_scheduler(sm, instruction_buffer, scoreboard, execution_units),
		secondary_scheduler(sm, instruction_buffer, scoreboard, execution_units)
	{}
	virtual ~Frontend() {}
	virtual void Reset();
	
	// Returns true if idle
	virtual bool Phase(InstructionMetaData imd[], Time time, bool & all_finished) = 0;
	
protected:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
	typename CONFIG::executionunits_t & execution_units;

	typename CONFIG::primary_scheduler_t primary_scheduler;
	typename CONFIG::secondary_scheduler_t secondary_scheduler;
};

// Front ends:
// - Simultaneous, nonblocking
// - Cascaded, nonblocking
// - Blocking, simultaneous


template<class CONFIG>
struct SimultaneousFrontend : Frontend<CONFIG>
{
	SimultaneousFrontend(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb,
		typename CONFIG::executionunits_t & eu)
		: Frontend<CONFIG>(sm, ib, sb, eu) {}
	virtual ~SimultaneousFrontend() {}
	virtual void Reset();
	
	virtual bool Phase(InstructionMetaData imd[], Time time, bool & all_finished);
	
protected:
};

template<class CONFIG>
struct CascadedFrontend : Frontend<CONFIG>
{
	CascadedFrontend(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb,
		typename CONFIG::executionunits_t & eu)
		: Frontend<CONFIG>(sm, ib, sb, eu) {}
	virtual ~CascadedFrontend() {}
	virtual void Reset();
	
	virtual bool Phase(InstructionMetaData imd[], Time time, bool & all_finished);
	
protected:
	InstructionMetaData delay_buffer;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
