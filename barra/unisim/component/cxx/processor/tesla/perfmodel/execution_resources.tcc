/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_EXECUTION_RESOURCES_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_EXECUTION_RESOURCES_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/execution_resources.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG, unsigned int W, uint64_t E>
void FunctionalUnit<CONFIG, W, E>::Reset()
{
	time_busy = 0;
	busy_mask.reset();
}

template<class CONFIG, unsigned int W, uint64_t E>
std::bitset<CONFIG::WARP_SIZE> FunctionalUnit<CONFIG, W, E>::Ready(Time time)
{
	// time < time_busy: busy
	// time == time_busy: mask
	// time_busy < time < time_busy + INV_THROUGHPUT * cycle_time: busy
	// time_busy + INV_THROUGHPUT * cycle_time <= time: available
	if(time < time_busy) {
		return std::bitset<CONFIG::WARP_SIZE>();
	}
	else if(time == time_busy) {
		return ~busy_mask;
	}
	else if(time < time_busy + INV_THROUGHPUT * CONFIG::CYCLE_TIME) {
		return std::bitset<CONFIG::WARP_SIZE>();
	}
	else {
		return std::bitset<CONFIG::WARP_SIZE>().set();
	}
}

template<class CONFIG, unsigned int W, uint64_t E>
void FunctionalUnit<CONFIG, W, E>::Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert(time >= time_busy);	// Attempt to schedule in the past?
	if(time == time_busy) {
		assert(!(mask & busy_mask).any());	// Attempt to oversubscribe?
		busy_mask |= mask;
	}
	else {
		time_busy = time;
		busy_mask = mask;
	}
}

///////

template<class CONFIG, unsigned int W, uint64_t E>
void NosplitFunctionalUnit<CONFIG, W, E>::Reset()
{
	time_busy = 0;
}

template<class CONFIG, unsigned int W, uint64_t E>
std::bitset<CONFIG::WARP_SIZE> NosplitFunctionalUnit<CONFIG, W, E>::Ready(Time time)
{
	if(time < time_busy) {
		return std::bitset<CONFIG::WARP_SIZE>();
	}
	else {
		return std::bitset<CONFIG::WARP_SIZE>().set();
	}
}

template<class CONFIG, unsigned int W, uint64_t E>
void NosplitFunctionalUnit<CONFIG, W, E>::Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert(time >= time_busy);	// Attempt to schedule in the past?
	time_busy = time + INV_THROUGHPUT * CONFIG::CYCLE_TIME;
}

//////

template<class CONFIG, uint64_t E>
void ScalarFunctionalUnit<CONFIG, E>::Reset()
{
	time_busy = 0;
}

template<class CONFIG, uint64_t E>
std::bitset<CONFIG::WARP_SIZE> ScalarFunctionalUnit<CONFIG, E>::Ready(Time time)
{
	if(time < time_busy) {
		return std::bitset<CONFIG::WARP_SIZE>();
	}
	else {
		return std::bitset<CONFIG::WARP_SIZE>().set();
	}
}

template<class CONFIG, uint64_t E>
void ScalarFunctionalUnit<CONFIG, E>::Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert(time >= time_busy);	// Attempt to schedule in the past?
	time_busy = time + CONFIG::CYCLE_TIME;
}

//////


template<class CONFIG>
std::bitset<CONFIG::WARP_SIZE> ExecutionResources<CONFIG>::Ready(TimedInstruction<CONFIG> const & insn,
	Time time, unsigned int port)
{
	InstructionClass cls = insn.Class();
	if(units[port][cls] == 0) {
		// Scheduling to /dev/null: always available
		assert(false);	// Actually, this is a Bad Idea
		                // would cause scheduler to issue same instruction on same cycle
		return std::bitset<CONFIG::WARP_SIZE>().set();
	}
	else {
		return units[port][cls]->Ready(time);
	}
}

template<class CONFIG>
void ExecutionResources<CONFIG>::Schedule(TimedInstruction<CONFIG> const & insn, Time time,
	std::bitset<CONFIG::WARP_SIZE> mask, unsigned int port)
{
	InstructionClass cls = insn.Class();
	assert(units[port][cls] != 0);
//	if(units[port][cls] != 0) {
		units[port][cls]->Schedule(time, mask);
//	}
}

template<class CONFIG>
double ExecutionResources<CONFIG>::Energy(Time time) const
{
//	InstructionClass cls = insn.Class();
//	assert(units[port][cls] != 0);
//	return units[port][cls]->Energy(mask);
	assert(false);
}

template<class CONFIG>
ExecutionResources<CONFIG>::ExecutionResources()
{
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		for(unsigned int j = 0; j != InstructionClassMax; ++j) {
			units[i][j] = 0;
		}
	}
}

template<class CONFIG>
NikoExecutionResources<CONFIG>::NikoExecutionResources() :
	mad("MAD"), lsu("LSU"), sfu("SFU"), scfu("ScFU"), misc("MISC")
{
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		for(unsigned int j = 0; j != InstructionClassMax; ++j) {
			this->units[i][j] = &misc;
		}
	}

	this->units[0][ClassMOV] = this->units[0][ClassALU] = this->units[0][ClassAGU]
		= this->units[0][ClassXLU] = this->units[0][ClassFMAD] = this->units[0][ClassFMUL]
		= &mad;
	this->units[0][ClassSFU] = &sfu;
	this->units[0][ClassMEM] = &lsu;
	this->units[0][ClassSCI] = &scfu;
	
	assert(CONFIG::ISSUE_WIDTH == 1);	
}

template<class CONFIG>
double NikoExecutionResources<CONFIG>::Energy(Time time) const
{
	return mad.Energy(time) + sfu.Energy(time) + lsu.Energy(time);
}

template<class CONFIG>
double NikoExecutionResources<CONFIG>::Duration(Time time) const
{
	return mad.Duration(time) + sfu.Duration(time) + lsu.Duration(time) + scfu.Duration(time);
}

template<class CONFIG>
double NikoExecutionResources<CONFIG>::ScalarDuration(Time time) const
{
	return scfu.Duration(time);
}

template<class CONFIG>
RicoExecutionResources<CONFIG>::RicoExecutionResources() :
	mad1("MAD1"), mad2("MAD2"), lsu("LSU"), sfu("SFU"), scfu1("ScFU1"), scfu2("ScFU2"), misc("MISC")
{
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		for(unsigned int j = 0; j != InstructionClassMax; ++j) {
			this->units[i][j] = &misc;
		}
	}

	this->units[0][ClassMOV] = this->units[0][ClassALU] = this->units[0][ClassAGU]
		= this->units[0][ClassXLU] = this->units[0][ClassFMAD] = this->units[0][ClassFMUL]
		= &mad1;
	this->units[0][ClassSFU] = &sfu;
	this->units[0][ClassMEM] = &lsu;
	this->units[0][ClassSCI] = &scfu1;
	
	if(CONFIG::ISSUE_WIDTH >= 2) {
		this->units[1][ClassMOV] = this->units[1][ClassALU] = this->units[1][ClassAGU]
			= this->units[1][ClassXLU] = this->units[1][ClassFMAD] = this->units[1][ClassFMUL]
			= &mad2;
		this->units[1][ClassSFU] = &sfu;
		this->units[1][ClassMEM] = &lsu;
		this->units[1][ClassSCI] = &scfu2;
	}
	
}

template<class CONFIG>
double RicoExecutionResources<CONFIG>::Energy(Time time) const
{
	return mad1.Energy(time) + mad2.Energy(time) + sfu.Energy(time) + lsu.Energy(time);
}

template<class CONFIG>
double RicoExecutionResources<CONFIG>::Duration(Time time) const
{
	return mad1.Duration(time) + mad2.Duration(time) + sfu.Duration(time) + lsu.Duration(time) + scfu1.Duration(time) + scfu2.Duration(time);
}

template<class CONFIG>
double RicoExecutionResources<CONFIG>::ScalarDuration(Time time) const
{
	return scfu1.Duration(time) + scfu2.Duration(time);
}

template<class CONFIG>
DimitriExecutionResources<CONFIG>::DimitriExecutionResources() :
	mad("MAD"), lsu("LSU"), sfu("SFU"), scfu("ScFU"), misc("MISC")
{
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		for(unsigned int j = 0; j != InstructionClassMax; ++j) {
			this->units[i][j] = &misc;
		}
	}

	this->units[0][ClassMOV] = this->units[0][ClassALU] = this->units[0][ClassAGU]
		= this->units[0][ClassXLU] = this->units[0][ClassFMAD] = this->units[0][ClassFMUL]
		= &mad;
	this->units[0][ClassSFU] = &sfu;
	this->units[0][ClassMEM] = &lsu;
	this->units[0][ClassSCI] = &scfu;
	
	if(CONFIG::ISSUE_WIDTH >= 2) {
		this->units[1][ClassMOV] = this->units[1][ClassALU] = this->units[1][ClassAGU]
			= this->units[1][ClassXLU] = this->units[1][ClassFMAD] = this->units[1][ClassFMUL]
			= &mad;
		this->units[1][ClassSFU] = &sfu;
		this->units[1][ClassMEM] = &lsu;
		this->units[1][ClassSCI] = &scfu;
	}
}
template<class CONFIG>
double DimitriExecutionResources<CONFIG>::Energy(Time time) const
{
	return mad.Energy(time) + sfu.Energy(time) + lsu.Energy(time);
}

template<class CONFIG>
double DimitriExecutionResources<CONFIG>::Duration(Time time) const
{
	return mad.Duration(time) + sfu.Duration(time) + lsu.Duration(time) + scfu.Duration(time);
}

template<class CONFIG>
double DimitriExecutionResources<CONFIG>::ScalarDuration(Time time) const
{
	return scfu.Duration(time);
}


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
