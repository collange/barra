/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_FRONTEND_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_FRONTEND_TCC

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
void Frontend<CONFIG>::Reset()
{
	instruction_buffer.Reset();
	primary_scheduler.Reset();
	secondary_scheduler.Reset();
	//execution_units.Reset();
}

template<class CONFIG>
void SimultaneousFrontend<CONFIG>::Reset()
{
}

template<class CONFIG>
bool SimultaneousFrontend<CONFIG>::Phase(InstructionMetaData insn[], Time time,
	bool & all_finished)
{
	Time time_schedule = time;
	
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		insn[i].valid = false;
		insn[i].phase = i;
		insn[i].warpid = 0;
		insn[i].subwarpid = 0;
	}
	
	unsigned int phase = 0;
	
	all_finished = true;
	bool idle = true;
	
	// Non-blocking
	if(this->primary_scheduler.Schedule(time,
		false, phase, all_finished, insn[phase].warpid,
		insn[phase].subwarpid, time_schedule)) {
		
		insn[phase].valid = true;
		idle = false;
	
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(insn[phase].warpid, insn[phase].subwarpid);
		if(this->sm.TraceScheduler()) {
			cerr << "Scheduled primary warp " << insn[phase].warpid << "." << insn[phase].subwarpid
				<< " pc=" << hex << ibe.subwarp.pc << dec
				<< " mask=" << ibe.subwarp.mask << " on ";
			this->execution_units.Describe(ibe.instruction.Class(), phase, cerr);
			cerr << endl;
		}
		this->execution_units.Schedule(ibe.instruction, time, ibe.subwarp.mask, phase);
	}
	

	// Cascaded scheduling: feed each scheduler with outcome of previous scheduling
	unsigned int warpid = insn[phase].warpid;
	unsigned int subwarpid = insn[phase].subwarpid;
	
	// Schedule other subwarps
	for(unsigned int p = 1; p < CONFIG::ISSUE_WIDTH; ++p)
	{
		insn[p].phase = p;
		if(this->secondary_scheduler.Schedule(time,
			false, p, all_finished,
			warpid, subwarpid, time_schedule)) {
			insn[p].warpid = warpid;
			insn[p].subwarpid = subwarpid;
			insn[p].valid = true;
			idle = false;
			
			InstructionBufferEntry<CONFIG> & ibe
				= this->instruction_buffer.Read(warpid, subwarpid);

			if(this->sm.TraceScheduler()) {
				cerr << "Scheduled secondary warp " << insn[p].warpid << "." << insn[p].subwarpid
					<< " pc=" << hex << ibe.subwarp.pc << dec
					<< " mask=" << ibe.subwarp.mask << " on ";
				this->execution_units.Describe(ibe.instruction.Class(), p, cerr);
				cerr << endl;
			}
			this->execution_units.Schedule(ibe.instruction, time, ibe.subwarp.mask, p);

			// Do not schedule same subwarp twice!
			assert(!insn[0].valid || insn[0].warpid != insn[1].warpid
				|| insn[0].subwarpid != insn[1].subwarpid);
		}
	}
	
	return idle;
}

// CascadedFrontend

template<class CONFIG>
void CascadedFrontend<CONFIG>::Reset()
{
	delay_buffer.phase = 0;
	delay_buffer.valid = false;
	delay_buffer.warpid = 0;
	delay_buffer.subwarpid = 0;
}

template<class CONFIG>
bool CascadedFrontend<CONFIG>::Phase(InstructionMetaData insn[], Time time,
	bool & all_finished)
{
	// TODO: all_finished is wrong. I think.
	assert(CONFIG::ISSUE_WIDTH <= 2);
	for(unsigned int i = 0; i != CONFIG::ISSUE_WIDTH; ++i) {
		insn[i].valid = false;
		insn[i].phase = i;
	}
	all_finished = true;
	Time time_schedule = time;
	
	// Start with secondary sched, time t
	
	// Use primary entry from last round
	insn[0] = delay_buffer;
	unsigned int warpid = insn[0].warpid;
	unsigned int subwarpid = insn[0].subwarpid;

	unsigned int p = 1;
	insn[p].phase = p;
	if(this->secondary_scheduler.Schedule(time,
		false, p, all_finished,
		warpid, subwarpid, time_schedule)) {
		insn[p].warpid = warpid;
		insn[p].subwarpid = subwarpid;
		insn[p].valid = true;
		
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(warpid, subwarpid);

		if(this->sm.TraceScheduler()) {
			cerr << "Scheduling secondary warp " << insn[p].warpid << "." << insn[p].subwarpid
				<< " pc=" << hex << ibe.subwarp.pc << dec
				<< " mask=" << ibe.subwarp.mask
				<< " at cycle " << time / this->sm.CycleTime()
				<< " on ";
			this->execution_units.Describe(ibe.instruction.Class(), p, cerr);
			cerr << endl;
		}
		this->execution_units.Schedule(ibe.instruction, time, ibe.subwarp.mask, p);

		// Do not schedule same subwarp twice!
		assert(!insn[0].valid || insn[0].warpid != insn[1].warpid
			|| insn[0].subwarpid != insn[1].subwarpid);
	}
	
	// Primary sched, time t+1
	time += this->sm.CycleTime();
	
	unsigned int phase = 0;
	delay_buffer.valid = false;
	if(this->primary_scheduler.Schedule(time,
		false, phase, all_finished, delay_buffer.warpid,
		delay_buffer.subwarpid, time_schedule)) {
		
		delay_buffer.valid = true;
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(delay_buffer.warpid, delay_buffer.subwarpid);
		assert(ibe.valid);
	}
	
	// Did we pick the same instruction twice?
	if(insn[1].valid && delay_buffer.valid
		&& insn[1].warpid == delay_buffer.warpid
		&& insn[1].subwarpid == delay_buffer.subwarpid) {
		if(this->sm.TraceScheduler()) {
			cerr << "Cascaded scheduling conflict. Aborting phase 0 of next cycle." << endl;
		}
		delay_buffer.valid = false;
	}

	if(delay_buffer.valid) {
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(delay_buffer.warpid, delay_buffer.subwarpid);
		if(this->sm.TraceScheduler()) {
			cerr << "Scheduling primary warp " << delay_buffer.warpid << "." << delay_buffer.subwarpid
				<< " pc=" << hex << ibe.subwarp.pc << dec
				<< " mask=" << ibe.subwarp.mask
				<< " at cycle " << time / this->sm.CycleTime()
				<< " on ";
			this->execution_units.Describe(ibe.instruction.Class(), phase, cerr);
			cerr << endl;
		}
		this->execution_units.Schedule(ibe.instruction, time, ibe.subwarp.mask, phase);
	
		// Make sure we do not pick the same next time
		this->instruction_buffer.Invalidate(delay_buffer.warpid, delay_buffer.subwarpid);
	}
	
	return !(insn[0].valid || insn[1].valid);	// Idle?
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
