/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BWCONSTRAINED_MEMORY_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BWCONSTRAINED_MEMORY_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
struct BaseMemConfig : CONFIG
{
	typedef CONFIG Base;
	typedef uint32_t address_t;

	static Time const LOAD_LATENCY = 330 * NANOSECOND;
	static unsigned int const LOG_TRANSACTION_WIDTH = 5;
	static unsigned int const TRANSACTION_WIDTH = 1 << LOG_TRANSACTION_WIDTH;	// Bytes
	static Time const ACCEPT_DELAY = (TRANSACTION_WIDTH / 10.) * NANOSECOND;	// 10 GB/s per SM
};

// Simple in-order, non-blocking, bandwidth-constrained memory with constant latency.
template<class MEMCONFIG>
struct BWConstrainedMemory : TimedMemoryInterface<typename MEMCONFIG::Base>
{
	virtual void Load(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed);
	virtual void Store(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted);
	virtual Time Synchronize(Time t) {
		return ready_time = std::max(ready_time, t);
	}

private:
	Time ready_time;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
