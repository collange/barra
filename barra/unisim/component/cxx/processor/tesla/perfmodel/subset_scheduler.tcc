/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SUBSET_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SUBSET_SCHEDULER_TCC

#include <limits>
#include <unisim/component/cxx/processor/tesla/perfmodel/subset_scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG, class POLICY>
SubsetScheduler<CONFIG, POLICY>::SubsetScheduler(TeslaSM<CONFIG> & sm,
	InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb, typename CONFIG::executionunits_t & eu) :
	Scheduler<CONFIG>(sm, ib, sb, eu),
	policy(sm, ib, sb)
{
}

template<class CONFIG, class POLICY>
SubsetScheduler<CONFIG, POLICY>::~SubsetScheduler()
{
}

// Return true if found
template<class CONFIG, class POLICY>
bool SubsetScheduler<CONFIG, POLICY>::Schedule(Time current_time, 	bool blocking, unsigned int round, bool & all_finished,
	unsigned int & warpid,
	unsigned int & subwarpid, Time & issue_time)
{
	assert(!blocking);
	// Ignore avail_mask

	unsigned int bin = round;
	unsigned int modulus = CONFIG::ISSUE_WIDTH;
	bool round_all_finished = true;
//	int alive = -1;
	for(unsigned int w = bin; w < this->sm.WarpCount(); w += modulus)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(w);
		if(warp.state != Warp<CONFIG>::Finished) {
			round_all_finished = false;
//			alive = w;
		}
	}
	if(round_all_finished) {
		if(this->sm.TraceScheduler()) {
			cerr << "SubsetScheduler " << round << ": all warps finished" << endl;
		}
		return false;
	}
	else {
//		if(this->sm.TraceScheduler()) {
//			cerr << "SubsetScheduler " << round << ": warp" << alive << " is alive" << endl;
//		}
		all_finished = false;
	}

	// For each feasible schedule, compute score. Select max non-zero score.
	// It is up to the scoring policy to apply the particular rules.
	
	return SchedulerRound(current_time, round, warpid,
		subwarpid, issue_time);
}

// TODO: clean up
// Returns true if found
template<class CONFIG, class POLICY>
bool SubsetScheduler<CONFIG, POLICY>::SchedulerRound(Time current_time,
	unsigned int round,
	unsigned int & warpid, unsigned int & subwarpid, Time & issue_time)
{
	unsigned int bin = round;
	unsigned int modulus = CONFIG::ISSUE_WIDTH;

	uint64_t max_score = 0;
	unsigned int best_warp = -1, best_subwarp = -1;
	mask_t best_mask;
	for(unsigned int w = bin; w < this->sm.WarpCount(); w += modulus)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(w);
		assert(warp.id == w);
		if(warp.state != Warp<CONFIG>::Active) {
	continue;
		}

		unsigned int phases = CONFIG::NIMT_PHASES;
		if(round == 0 && CONFIG::PRIMARY_SCHED_PRIMARY_SW) {
			// Forbid secondary subwarps on primary scheduling round
			phases = 1;
		}
		for(unsigned int sw = 0; sw != phases; ++sw)
		{
			InstructionBufferEntry<CONFIG> & ibe
				= this->instruction_buffer.Read(w, sw);
			if(!ibe.valid || !warp.flow.Ready(ibe.subwarp)) {
		continue;
			}

			std::bitset<CONFIG::WARP_SIZE> avail_mask
				= this->execution_units.Ready(ibe.instruction, current_time, round);

			// Occupancy at warp granularity
			if((~avail_mask).any()) {
		continue;
			}

			mask_t ready = this->Ready(warp, sw, current_time);
			if(!ready.any()) {
		continue;
			}
			SubWarp<CONFIG> const & subwarp = ibe.subwarp;
			
			// # threads left behind because their operands are not available
			unsigned int split_unavail = (subwarp.mask & ~ready).count();
			
			// # dedicated resources, do not split warps
			unsigned int split_resources = 0;
			
			unsigned int idle = (~ready).count();
			
			SchedulingCandidate<CONFIG> candidate(w, sw, split_unavail,
				split_resources, idle, warpid, subwarpid);

			// Feed all that to the arbitration black box
			uint64_t score = policy(candidate, current_time);
			
			if(score > max_score) {
				best_warp = w;
				best_subwarp = sw;
				best_mask = ready;
				max_score = score;
			}
		}
	}
	
	if(max_score != 0) {
		warpid = best_warp;
		assert(warpid % modulus == bin);
		subwarpid = best_subwarp;

		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(warpid, subwarpid);
		if(this->sm.TraceScheduler() && ibe.subwarp.mask != best_mask) {
			cerr << "SubsetScheduler: split round " << round
				<< " warp " << warpid << "." << subwarpid
				<< " old mask: " << ibe.subwarp.mask
				<< " new mask: " << best_mask << endl;
		}
		ibe.subwarp.mask = best_mask;
		policy.Update(warpid, subwarpid, current_time);
		return true;
	}
	else {
		return false;
	}
}



} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
