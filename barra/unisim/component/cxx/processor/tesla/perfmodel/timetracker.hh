/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMETRACKER_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TIMETRACKER_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/units.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

enum WaitReason
{
	WaitScheduler,
	WaitIssue,
	WaitMemory,
	// New states here
	
	WaitReasonCount
};

enum EnergyReason
{
	EnergyIFetch,
	EnergyScheduler,
	EnergyIssue,
	EnergyExecution,
	EnergyDCache,
	EnergyDRAM,
	// New states here
	
	EnergyReasonCount
};

// Tracks much more than time: instructions issued, executed, energy...
template<class CONFIG>
struct TimeTracker
{
	TimeTracker();
	virtual ~TimeTracker();
	
	void WaitUntil(Time newtime, WaitReason reason);
	void WaitDelay(Time delay, WaitReason reason);
	Time Now() { return now; }
	void Reset();
	void Issue(std::bitset<CONFIG::WARP_SIZE> mask);
	void Scalar(std::bitset<CONFIG::WARP_SIZE> mask);
	void Fetch();
	void DumpCSV(std::ostream & os) const;
	void DumpCSVHeader(std::ostream & os) const;
	
	void UpdateWatchdog(WaitReason reason, Time delay);
	
	void SpendEnergy(double energy, EnergyReason reason);
	
	void SpendTime(double timeScalar, double timeGlobal);
protected:
	Time now;
	
	Time timeCategory[WaitReasonCount];
	uint64_t instructionsIssued;
	uint64_t instructionsExecuted;
	uint64_t instructionsFetched;
	uint64_t scalarExecuted;
	Time time_in_sched;
	Time unitTimeScalar;
	Time unitTimeTotal;

	double energyCategory[EnergyReasonCount];
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
