/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CACHE_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {


template<class MEMCONFIG>
struct Cache :
	TimedVectorMemoryInterface<typename MEMCONFIG::Base>,
	MEMCONFIG::cache_t
{
	typedef typename MEMCONFIG::Base CONFIG;
	typedef typename MEMCONFIG::cache_t Base;
	Cache(TeslaSM<typename MEMCONFIG::Base> & sm, TimedMemoryInterface<CONFIG> & memory, Time cycle_time);
	virtual ~Cache();


	virtual void Load(typename MEMCONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed);
	virtual void Store(typename MEMCONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted);

	virtual void Transport(MemoryTransaction<CONFIG> & transaction);

	void LoadStore(MemoryTransaction<CONFIG> & transaction);
	void VectorLoadStore(MemoryTransaction<CONFIG> & transaction, bool is_load);
	void GatherScatter(MemoryTransaction<CONFIG> & transaction, bool is_load);


private:
	unsigned int ArbitrateBanks(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
		unsigned int round);
	TimedMemoryInterface<CONFIG> & memory;
	void SegmentLoadStore(MemoryTransaction<CONFIG> & transaction, bool is_load,
		typename CONFIG::address_t addr, Time & time_accepted,
	Time & time_completed);
	bool CheckMSHR(typename CONFIG::address_t addr, Time & time_completed);
	void SetMSHR(typename CONFIG::address_t addr, Time time_completed);
	void UpdateMSHR(Time current_time);

	TeslaSM<typename MEMCONFIG::Base> & sm;

	Time cycle_time;
	Time latency;
	Time ready_time;
	static unsigned int const ROUND_COUNT = (MEMCONFIG::WARP_SIZE * 4) / MEMCONFIG::CACHE_BLOCK_SIZE;
	static uint64_t const SLICE_WIDTH = MEMCONFIG::CACHE_BLOCK_SIZE / 4;
	
	typedef std::map<typename CONFIG::address_t, Time> mshr_t;
	mshr_t mshr;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
