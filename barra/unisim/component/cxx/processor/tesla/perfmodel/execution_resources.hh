/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_EXECUTION_RESOURCES_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_EXECUTION_RESOURCES_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/timed_instruction.hh>
#include <unisim/component/cxx/processor/tesla/stats.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
struct FunctionalUnitBase
{
	virtual void Reset() = 0;
	virtual std::bitset<CONFIG::WARP_SIZE> Ready(Time time) = 0;
	virtual void Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask) = 0;
	virtual void Describe(std::ostream & os) const = 0;
	virtual double Energy(Time time) const = 0;
	virtual double Duration(Time time) const = 0;
	virtual ~FunctionalUnitBase() {};
};

template<class CONFIG, unsigned int W, uint64_t E>
struct FunctionalUnit : FunctionalUnitBase<CONFIG>
{
	FunctionalUnit(char const * name) : name(name) { Reset(); }
	virtual void Reset();
	virtual std::bitset<CONFIG::WARP_SIZE> Ready(Time time);
	virtual void Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask);
	virtual ~FunctionalUnit() {}
	virtual void Describe(std::ostream & os) const {
		os << name << ", W=" << W << ", time=" << time_busy / NANOSECOND
		   << ", mask=" << busy_mask << std::endl;
	}
	virtual double Energy(Time time) const {
		return (time == time_busy && busy_mask.any()) ? E * CONFIG::WARP_SIZE : 0;
	}
	
private:
	Time time_busy;
	std::bitset<CONFIG::WARP_SIZE> busy_mask;
	static unsigned int const INV_THROUGHPUT = CONFIG::WARP_SIZE / W;
	char const * name;
};

template<class CONFIG, uint64_t E>
struct ScalarFunctionalUnit : FunctionalUnitBase<CONFIG>
{
	ScalarFunctionalUnit(char const * name) : name(name) { Reset(); }
	virtual void Reset();
	virtual std::bitset<CONFIG::WARP_SIZE> Ready(Time time);
	virtual void Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask);
	virtual ~ScalarFunctionalUnit() {}
	virtual void Describe(std::ostream & os) const {
		os << name << ", time=" << time_busy / NANOSECOND << std::endl;
	}
	virtual double Energy(Time time) const {
		return (time == time_busy) ? E : 0;
	}
	virtual double Duration(Time time) const {
		return (time == time_busy);
	}

private:
	Time time_busy;
	static unsigned int const INV_THROUGHPUT = 1;
	char const * name;
};

template<class CONFIG, unsigned int W, uint64_t E>
struct NosplitFunctionalUnit : FunctionalUnitBase<CONFIG>
{
	NosplitFunctionalUnit(char const * name) : name(name) { Reset(); }
	virtual void Reset();
	virtual std::bitset<CONFIG::WARP_SIZE> Ready(Time time);
	virtual void Schedule(Time time, std::bitset<CONFIG::WARP_SIZE> mask);
	virtual ~NosplitFunctionalUnit() {}
	virtual void Describe(std::ostream & os) const {
		os << name << ", W=" << W << ", time=" << time_busy / NANOSECOND << std::endl;
	}
	virtual double Energy(Time time) const {
		return (time == time_busy) ? E : 0;
	}
	virtual double Duration(Time time) const {
		return (time == time_busy) ? INV_THROUGHPUT : 0;
	}

private:
	Time time_busy;
	static unsigned int const INV_THROUGHPUT = CONFIG::WARP_SIZE / W;
	char const * name;
};


template<class CONFIG>
struct ExecutionResources
{
	virtual ~ExecutionResources() {}

	void Reset() {}
	std::bitset<CONFIG::WARP_SIZE> Ready(TimedInstruction<CONFIG> const & insn,
		Time time, unsigned int port);
	void Schedule(TimedInstruction<CONFIG> const & insn, Time time,
		std::bitset<CONFIG::WARP_SIZE> mask, unsigned int port);
	void Describe(InstructionClass cls, unsigned int port, std::ostream & os) const {
		assert(units[port][cls] != 0);
		units[port][cls]->Describe(os);
	}
	double Energy(Time time) const;
protected:
	ExecutionResources();

	FunctionalUnitBase<CONFIG> * units[CONFIG::ISSUE_WIDTH][InstructionClassMax];
};

// Niko is 16-lane single-issue
template<class CONFIG>
struct NikoExecutionResources : ExecutionResources<CONFIG>
{
	NikoExecutionResources();
	
	void Reset() {
		mad.Reset();
		lsu.Reset(); sfu.Reset(); misc.Reset(); scfu.Reset();
	}
	double Energy(Time time) const;
	double Duration(Time time) const;
	double ScalarDuration(Time time) const;
private:
	NosplitFunctionalUnit<CONFIG, 16, 2*1000/32> mad;
	NosplitFunctionalUnit<CONFIG, 16, 0> lsu;
	NosplitFunctionalUnit<CONFIG, 8, 4*1000/32> sfu;
	ScalarFunctionalUnit<CONFIG, 1> scfu;
	NosplitFunctionalUnit<CONFIG, 32, 0> misc;
};

// Rico is 32-lane dual-issue
template<class CONFIG>
struct RicoExecutionResources : ExecutionResources<CONFIG>
{
	RicoExecutionResources();
	
	void Reset() {
		mad1.Reset(); mad2.Reset();
		lsu.Reset(); sfu.Reset(); misc.Reset();
		scfu1.Reset(); scfu2.Reset();
	}
	double Energy(Time time) const;
	double Duration(Time time) const;
	double ScalarDuration(Time time) const;
private:
	NosplitFunctionalUnit<CONFIG, 32, 2*1000/32> mad1;
	NosplitFunctionalUnit<CONFIG, 32, 2*1000/32> mad2;
	NosplitFunctionalUnit<CONFIG, 32, 0> lsu;
	NosplitFunctionalUnit<CONFIG, 8, 4*1000/32> sfu;
	ScalarFunctionalUnit<CONFIG, 1> scfu1;
	ScalarFunctionalUnit<CONFIG, 1> scfu2;
	NosplitFunctionalUnit<CONFIG, 32, 0> misc;
};

// Dimitri is 64-lane DIMT
template<class CONFIG>
struct DimitriExecutionResources : ExecutionResources<CONFIG>
{
	DimitriExecutionResources();
	
	void Reset() {
		mad.Reset();
		lsu.Reset(); sfu.Reset(); misc.Reset();
		scfu.Reset();
	}
	double Energy(Time time) const;
	double Duration(Time time) const;
	double ScalarDuration(Time time) const;
private:
	FunctionalUnit<CONFIG, 64, 2*1000/32> mad;
	NosplitFunctionalUnit<CONFIG, 32, 0> lsu;
	FunctionalUnit<CONFIG, 8, 4*1000/32> sfu;
	ScalarFunctionalUnit<CONFIG, 1> scfu;
	NosplitFunctionalUnit<CONFIG, 64, 0> misc;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
