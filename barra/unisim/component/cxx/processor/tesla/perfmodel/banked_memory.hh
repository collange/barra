/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BANKED_MEMORY_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BANKED_MEMORY_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class PAYLOAD, unsigned int W, unsigned int D>
struct VectorPipelineState
{
	VectorPipelineState();
	void Reset();
	void Read(PAYLOAD row[], uint64_t cycle, unsigned int stage);
	PAYLOAD Read(uint64_t cycle, unsigned int stage, unsigned int lane);
	
	void Write(PAYLOAD const row[], uint64_t cycle, unsigned int stage);
	void Write(PAYLOAD s, uint64_t cycle, unsigned int stage, unsigned int lane);
private:
	void Forward(uint64_t cycle);
	void ClearAll();
	void ClearRow(unsigned int stage);

	PAYLOAD circular_buffer[D][W];
	uint64_t last_cycle;
	
	static PAYLOAD const NEUTRAL = 0;
};

template<class CONFIG>
struct BaseBankedMemConfig : CONFIG
{
	typedef CONFIG Base;
	//typedef uint32_t address_t;

	static unsigned int const LOG_BANKS = 5;
	static unsigned int const LOG_BANKWIDTH = 2;
	static unsigned int const LOG_TOTALWIDTH = LOG_BANKS + LOG_BANKWIDTH;

	static unsigned int const BANKS = 1 << LOG_BANKS;
	static unsigned int const BANKWIDTH = 1 << LOG_BANKWIDTH;
	static unsigned int const TOTALWIDTH = 1 << LOG_TOTALWIDTH;
	
	static unsigned int const STAGES = 3;
};

template<class MEMCONFIG>
struct BankedMemory : TimedVectorMemoryInterface<typename MEMCONFIG::Base>
{
	typedef typename MEMCONFIG::Base CONFIG;
	BankedMemory(Time cycle_time);
	virtual ~BankedMemory();


	virtual void Load(typename MEMCONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed);
	virtual void Store(typename MEMCONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted);
	virtual Time Synchronize(Time t) {
		// TODO: Forward(t / cycle_time);
		return t;
	}

	virtual void Transport(MemoryTransaction<CONFIG> & transaction);

	void LoadStore(MemoryTransaction<CONFIG> & transaction);
	void VectorLoadStore(MemoryTransaction<CONFIG> & transaction);
	void GatherScatter(MemoryTransaction<CONFIG> & transaction);

private:
	static unsigned int const ROUND_COUNT = (MEMCONFIG::WARP_SIZE * 4) / MEMCONFIG::TOTALWIDTH;
	Time cycle_time;
	Time latency;
	
	VectorPipelineState<typename MEMCONFIG::address_t, MEMCONFIG::BANKS, ROUND_COUNT> state;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
