/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_PERTHREAD_SCOREBOARD_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_PERTHREAD_SCOREBOARD_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/units.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// Instruction dependency scoreboard
// Naive implementation at thread granularity
template<class CONFIG>
struct PerthreadScoreboard
{
	PerthreadScoreboard();
	virtual ~PerthreadScoreboard();

	void Reset();

	void Write(Warp<CONFIG> const & warp, OperandID const & opid, std::bitset<CONFIG::WARP_SIZE> mask, Time time);
	
	Time Query(Warp<CONFIG> const & warp, OperandID const & opid,
		std::bitset<CONFIG::WARP_SIZE> mask) const;

	std::bitset<CONFIG::WARP_SIZE> Available(Warp<CONFIG> const & warp,
		OperandID const & opid,
		std::bitset<CONFIG::WARP_SIZE> mask, Time time) const;
	
private:
	void WriteTimeVector(Time * vector, Time newtime, std::bitset<CONFIG::WARP_SIZE> mask);
	Time ReadTimeVector(Time const * vector, std::bitset<CONFIG::WARP_SIZE> mask) const;
	std::bitset<CONFIG::WARP_SIZE> VectorLessThan(Time const * vector,
		std::bitset<CONFIG::WARP_SIZE> mask, Time time) const;

	Time gpr[CONFIG::MAX_VGPR][CONFIG::WARP_SIZE];
	Time ar[CONFIG::MAX_WARPS][CONFIG::MAX_ADDR_REGS][CONFIG::WARP_SIZE];
	Time pred[CONFIG::MAX_WARPS][CONFIG::MAX_PRED_REGS][CONFIG::WARP_SIZE];
	Time shmem[CONFIG::MAX_WARPS][CONFIG::WARP_SIZE];
	
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
