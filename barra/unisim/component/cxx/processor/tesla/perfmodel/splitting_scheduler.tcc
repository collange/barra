/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SPLITTING_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SPLITTING_SCHEDULER_TCC

#include <limits>
#include <unisim/component/cxx/processor/tesla/perfmodel/splitting_scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG, class POLICY>
SplittingScheduler<CONFIG, POLICY>::SplittingScheduler(TeslaSM<CONFIG> & sm,
	InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb, typename CONFIG::executionunits_t & eu) :
	Scheduler<CONFIG>(sm, ib, sb, eu),
	policy(sm, ib, sb)
{
}

template<class CONFIG, class POLICY>
SplittingScheduler<CONFIG, POLICY>::~SplittingScheduler()
{
}

// Return true if found
// all_finished should be initialized to true by caller
// (consecutive calls to Schedule compute an AND function)
template<class CONFIG, class POLICY>
bool SplittingScheduler<CONFIG, POLICY>::Schedule(Time current_time,
	bool blocking, unsigned int round, bool & all_finished,
	unsigned int & warpid,
	unsigned int & subwarpid, Time & issue_time)
{
	bool round_all_finished = true;
	for(unsigned int w = 0; w != this->sm.WarpCount(); ++w)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(w);
		if(warp.state != Warp<CONFIG>::Finished) {
			round_all_finished = false;
		}
	}
	if(round_all_finished) {
		return false;
	}
	else {
		all_finished = false;
	}

	// Look for any ready instruction from any subwarp from any warp.
	// Include partially-ready instructions.
	// Splitting subwarps is allowed.
	// Just filter out infeasible schedules at this point.
	
	// For each feasible schedule, compute score. Select max non-zero score.
	// It is up to the scoring policy to apply the particular rules.
	
	// Only wait if in blocking mode
	if(!blocking) {
		return SchedulerRound(current_time, round, warpid,
			subwarpid, issue_time);
	}
	else {
		unsigned timeout_counter = 0;
		while(!SchedulerRound(current_time, round, warpid,
			subwarpid, issue_time))
		{
			current_time += CONFIG::CYCLE_TIME;
			if(timeout_counter == 10000) {
				// Debug: if no warp was found at that time, assume we ran into a deadlock
				// Dump state and assert.
				cerr << "Deadlock in SplittingScheduler" << endl;
				this->sm.DumpState(cerr);
				assert(false);
			}
			++timeout_counter;
		}
		issue_time = current_time;
		return true;
	}
}

// Returns true if found
template<class CONFIG, class POLICY>
bool SplittingScheduler<CONFIG, POLICY>::SchedulerRound(Time current_time,
	unsigned int round,
	unsigned int & warpid, unsigned int & subwarpid, Time & issue_time)
{
	uint64_t max_score = 0;
	unsigned int best_warp = -1, best_subwarp = -1;
	mask_t best_mask;
	for(unsigned int w = 0; w != this->sm.WarpCount(); ++w)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(w);
		assert(warp.id == w);
		if(warp.state != Warp<CONFIG>::Active) {
	continue;
		}

		unsigned int phases = CONFIG::NIMT_PHASES;
		if(round == 0 && CONFIG::PRIMARY_SCHED_PRIMARY_SW) {
			// Forbid secondary subwarps on primary scheduling round
			phases = 1;
		}
		for(unsigned int sw = 0; sw != phases; ++sw)
		{
			InstructionBufferEntry<CONFIG> & ibe
				= this->instruction_buffer.Read(w, sw);
			if(!ibe.valid || !warp.flow.Ready(ibe.subwarp)) {
		continue;
			}

			std::bitset<CONFIG::WARP_SIZE> avail_mask
				= this->execution_units.Ready(ibe.instruction, current_time, round);
			
			mask_t ready = this->Ready(warp, sw, current_time);
			if(!(ready & avail_mask).any()) {
		continue;
			}
			SubWarp<CONFIG> const & subwarp = ibe.subwarp;
			
			// # threads left behind because their operands are not available
			unsigned int split_unavail = (subwarp.mask & ~ready).count();
			
			// # threads left behind because execution resources are not available
			unsigned int split_resources = (ready & ~avail_mask).count();
			
			unsigned int idle = (avail_mask & ~ready).count();

			SchedulingCandidate<CONFIG> candidate(w, sw, split_unavail,
				split_resources, idle, warpid, subwarpid);
			
			// Feed all that to the arbitration black box
			uint64_t score = policy(candidate, current_time);
			
			if(score > max_score) {
				best_warp = w;
				best_subwarp = sw;
				best_mask = ready & avail_mask;
				max_score = score;
			}
		}
	}
	
	if(max_score != 0) {
		warpid = best_warp;
		subwarpid = best_subwarp;
		// Split subwarp?
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(warpid, subwarpid);
		if(this->sm.TraceScheduler() && ibe.subwarp.mask != best_mask) {
			cerr << "Scheduler: split subwarp " << warpid << "." << subwarpid
				<< " old mask: " << ibe.subwarp.mask
				<< " new mask: " << best_mask << endl;
		}
		ibe.subwarp.mask = best_mask;
		policy.Update(warpid, subwarpid, current_time);
		return true;
	}
	else {
		return false;
	}
}



} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
