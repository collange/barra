/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_COALESCER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_COALESCER_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/coalescer.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
Coalescer<CONFIG>::Coalescer(TimedMemoryInterface<CONFIG> & memory, typename CONFIG::scoreboard_t & scoreboard) :
	memory(memory), scoreboard(scoreboard)
{
}

template<class CONFIG>
Coalescer<CONFIG>::~Coalescer()
{
}

template<class CONFIG>
void Coalescer<CONFIG>::Load(typename CONFIG::address_t addr,
	unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed)
{
	// Bypass
	memory.Load(addr, logwidth, time, time_accepted, time_completed);
	//time_completed += COALESCER_LATENCY;
}

template<class CONFIG>
void Coalescer<CONFIG>::Store(typename CONFIG::address_t addr,
	unsigned int logwidth, Time time, Time & time_accepted)
{
	// Bypass
	memory.Store(addr, logwidth, time, time_accepted);
}

template<class CONFIG>
void Coalescer<CONFIG>::Transport(MemoryTransaction<CONFIG> & transaction)
{
	switch(transaction.type) {
	case TransactionLoad:
		memory.Load(transaction.address[0],
			transaction.logsize,
			transaction.time_initiated,
			transaction.time_accepted,
			transaction.time_completed);
		break;
	case TransactionStore:
		memory.Store(transaction.address[0],
			transaction.logsize,
			transaction.time_initiated,
			transaction.time_accepted);
		break;
	case TransactionVectorLoad:
		memory.Load(transaction.address[0],
			transaction.logsize + CONFIG::LOG_WARP_SIZE,
			transaction.time_initiated,
			transaction.time_accepted,
			transaction.time_completed);
		break;
	case TransactionVectorStore:
		// Bypass
		memory.Store(transaction.address[0],
			transaction.logsize + CONFIG::LOG_WARP_SIZE,
			transaction.time_initiated,
			transaction.time_accepted);
		break;
	case TransactionGather:
	case TransactionScatter:
		SplitTransaction(transaction);
		break;
	default:
		assert(false);	// Transaction type not supported
	}
}

inline unsigned int NativeSegmentLogSize(unsigned int logsize)
{
	switch(logsize)
	{
	case 1:
		return 5;
	case 2:
		return 6;
	default:
		return 7;
	}
}

template<class ADDR>
ADDR GetBase(ADDR address, unsigned int segmentSize)
{
	return address & ~(segmentSize - 1);
}

template<class ADDR>
unsigned int GetOffset(ADDR address, unsigned int segmentSize)
{
	return address & (segmentSize - 1);
}

template<class ADDR>
bool GetOffsetHi(ADDR address, unsigned int segmentSize)
{
	return address & (segmentSize >> 1);	// Returns false when segmentSize = 1
}


template<class CONFIG>
void Coalescer<CONFIG>::SplitTransaction(MemoryTransaction<CONFIG> & transaction)
{
	// From NVIDIA CUDA PG 2.3:
	//	The following protocol is used to issue a memory transaction for a
	//	half-warp:
	//	* Find the memory segment that contains the address requested by the lowest
	//	numbered active thread. Segment size is 32 bytes for 1-byte data, 64 bytes for
	//	2-byte data, 128 bytes for 4-, 8- and 16-bit data.
	//	* Find all other active threads whose requested address lies in the same segment.
	//	* Reduce the transaction size, if possible:
	//	   If the transaction size is 128 bytes and only the lower or upper half is used,
	//	   reduce the transaction size to 64 bytes;
	//	   If the transaction size is 64 bytes and only the lower or upper half is used,
	//	   reduce the transaction sizez to 32 bytes.
	//	* Carry out the transaction and mark the serviced threads as inactive.
	//	* Repeat until all threads in the half-warp are serviced.

	unsigned int segmentLogSize = NativeSegmentLogSize(transaction.logsize);
	unsigned int segmentSize = 1 << segmentLogSize;
	
	std::bitset<CONFIG::WARP_SIZE> mask(transaction.mask);
	for(unsigned int j = 0; j != CONFIG::WARP_SIZE; j += CONFIG::TRANSACTION_SIZE)
	{
		unsigned int i = 0;
		while(i != CONFIG::TRANSACTION_SIZE) {
			if(!mask[j + i]) {
				++i;
				continue;	// Skip inactive threads
			}

			typename CONFIG::address_t base = transaction.address[j + i];
			typename CONFIG::address_t segmentBase = GetBase(base, segmentSize);
			bool offsetHiBase = GetOffsetHi(base, segmentSize);
			bool full = false;
			
			std::bitset<CONFIG::WARP_SIZE> participating_threads;
			participating_threads.set(j + i);
			
			// Check if remaining threads can piggy-back on this transaction
			for(; i != CONFIG::TRANSACTION_SIZE; ++i)
			{
				if(mask[j + i]) {
					typename CONFIG::address_t segmentTrans = GetBase(transaction.address[j + i], segmentSize);
					if(segmentTrans == segmentBase) {
						bool offsetHiTrans = GetOffsetHi(transaction.address[j + i], segmentSize);
						full |= (offsetHiTrans != offsetHiBase);
						mask[j + i] = false;
						participating_threads.set(j + i);
					}
				}
			}
			// TODO: adjust address with offsetHiBase if not full
			IssuePartialTransaction(transaction, segmentBase, participating_threads, segmentLogSize, full);
		}
	}
}

template<class CONFIG>
void Coalescer<CONFIG>::IssuePartialTransaction(MemoryTransaction<CONFIG> & transaction,
	typename CONFIG::address_t address, std::bitset<CONFIG::WARP_SIZE> mask,
	unsigned int segmentLogSize, bool full)
{
	if(!full && segmentLogSize > 5) {
		segmentLogSize--;
	}
	
	Time time_accepted, time_completed;
	if(transaction.type == TransactionGather) {
		memory.Load(address, segmentLogSize, transaction.time_initiated, time_accepted, time_completed);
		// Update scoreboard directly
		scoreboard.Write(transaction.initiator, transaction.destination, mask, time_completed);
	}
	else if(transaction.type == TransactionScatter) {
		memory.Store(address, segmentLogSize, transaction.time_initiated, time_accepted);
		time_completed = time_accepted;
	}
	else assert(false);
	
	transaction.time_accepted = std::max(transaction.time_accepted, time_accepted);
	transaction.time_completed = std::max(transaction.time_completed, time_completed);
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
