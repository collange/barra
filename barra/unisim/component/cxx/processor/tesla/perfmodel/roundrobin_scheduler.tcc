/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_ROUNDROBIN_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_ROUNDROBIN_SCHEDULER_TCC


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
RoundrobinScheduler<CONFIG>::RoundrobinScheduler(TeslaSM<CONFIG> & sm,
	InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb, typename CONFIG::executionunits_t & eu) :
	Scheduler<CONFIG>(sm, ib, sb, eu)
{
}

template<class CONFIG>
RoundrobinScheduler<CONFIG>::~RoundrobinScheduler()
{
}

template<class CONFIG>
bool RoundrobinScheduler<CONFIG>::Schedule(Time current_time,
	bool blocking, unsigned int round, bool & all_finished,
	unsigned int & warpid,
	unsigned int & subwarpid, Time & issue_time)
{
	// Round-robin
	// Cycle among all warps
	assert(!blocking);
	
	unsigned int warpcount = this->sm.WarpCount();
	int mywarp = -1;
	for(unsigned int w = nextwarp; w != (nextwarp-1) % warpcount; w = (w + 1) % warpcount)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(w);
		if(warp.state != Warp<CONFIG>::Finished) {
			all_finished = false;
			if(warp.state == Warp<CONFIG>::Active) {
				InstructionBufferEntry<CONFIG> & ibe
					= this->instruction_buffer.Read(w, 0);
				if(ibe.valid
					&& warp.flow.Ready(ibe.subwarp)
					&& !((~(this->execution_units.Ready(ibe.instruction, current_time, round))).any())
					&& this->Ready(warp, 0, current_time).any()
					) {
						mywarp = w;
	break;
				}
			}
		}
	}
	if(mywarp == -1) {
		return false;
	}
	warpid = mywarp;
	subwarpid = 0;
	nextwarp = (mywarp + 1) % warpcount;
	return true;
}


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
