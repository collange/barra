/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_L0ICACHE_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_L0ICACHE_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/l0icache.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
void L0ICache<CONFIG>::Load(typename CONFIG::address_t addr,
	unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed)
{
	typename CONFIG::address_t base_address = addr & ~(CONFIG::IFETCH_BLOCK_SIZE - 1);
	// Lookup in buffer first
	if(std::find(buffer.begin(), buffer.end(), base_address) != buffer.end()) {
		// Hit
		time_accepted = time_completed = time;
	}
	else {
		// Miss: fetch from L1 ICache
		//memory.Load(addr, logwidth, time, time_accepted, time_completed);
		memory.Load(base_address, CONFIG::IFETCH_BLOCK_LOGSIZE, time, time_accepted, time_completed);
		tracker.Fetch();
		buffer.push_back(base_address);
	}
}

template<class CONFIG>
void L0ICache<CONFIG>::Store(typename CONFIG::address_t addr,
	unsigned int logwidth, Time time, Time & time_accepted)
{
	assert(false);
}

template<class CONFIG>
void L0ICache<CONFIG>::Transport(MemoryTransaction<CONFIG> & transaction)
{
	assert(false);
}


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
