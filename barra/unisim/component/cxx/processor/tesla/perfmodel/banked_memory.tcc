/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BANKED_MEMORY_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_BANKED_MEMORY_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/banked_memory.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// VectorPipelineState

template<class PAYLOAD, unsigned int W, unsigned int D>
VectorPipelineState<PAYLOAD, W, D>::VectorPipelineState()
{
	Reset();
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::Reset()
{
	// Assumes PAYLOAD initialized to 0
	ClearAll();
	last_cycle = 0;
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::Read(PAYLOAD row[], uint64_t cycle, unsigned int stage)
{
	assert(cycle >= last_cycle);
	if(cycle + stage >= last_cycle + D) {
		// Beyond tracking window: Idle
		std::fill(&row[0], &row[W], NEUTRAL);
	}
	else {
		unsigned int head = ((cycle + stage) % D);
		std::copy(&circular_buffer[head][0], &circular_buffer[head][W], row);
	}
}

template<class PAYLOAD, unsigned int W, unsigned int D>
PAYLOAD VectorPipelineState<PAYLOAD, W, D>::Read(uint64_t cycle, unsigned int stage, unsigned int lane)
{
	assert(cycle >= last_cycle);
	if(cycle + stage >= last_cycle + D) {
		// Beyond tracking window: Idle
		return NEUTRAL;
	}
	else {
		unsigned int head = ((cycle + stage) % D);
		return circular_buffer[head][lane];
	}
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::Write(PAYLOAD const row[], uint64_t cycle, unsigned int stage)
{
	Forward(cycle);
	assert(cycle == last_cycle);
	unsigned int head = ((cycle + stage) % D);
	std::copy(&row[0], &row[W], circular_buffer[head]);
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::Forward(uint64_t cycle)
{
	assert(cycle >= last_cycle);
	if(cycle > last_cycle) {
		// Advance and clear
		if(cycle > last_cycle + D) {
			// Far in the future: clear all
			ClearAll();
			last_cycle = cycle;
			// Fast forward
		}
		else {
			// Clear buffer step-by-step
			for(; last_cycle < cycle; ++last_cycle) {
				unsigned int tail = last_cycle % D;
				ClearRow(tail);
			}
		}
	}
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::Write(PAYLOAD s, uint64_t cycle, unsigned int stage, unsigned int lane)
{
	Forward(cycle);
	assert(cycle == last_cycle);
	unsigned int head = ((cycle + stage) % D);
	assert(lane < W);
	assert(head < D);
	circular_buffer[head][lane] = s;
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::ClearAll()
{
	for(unsigned int i = 0; i != D; ++i) {
		ClearRow(i);
	}
}

template<class PAYLOAD, unsigned int W, unsigned int D>
void VectorPipelineState<PAYLOAD, W, D>::ClearRow(unsigned int stage)
{
	for(unsigned int i = 0; i != W; ++i) {
		circular_buffer[stage][i] = NEUTRAL;
	}
}

// BankedMemory

template<class MEMCONFIG>
BankedMemory<MEMCONFIG>::BankedMemory(Time cycle_time) :
	cycle_time(cycle_time),
	latency(MEMCONFIG::STAGES * cycle_time)
{
}

template<class MEMCONFIG>
BankedMemory<MEMCONFIG>::~BankedMemory()
{
}

template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::Load(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted, Time & time_completed)
{
	assert(false);
}

template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::Store(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted)
{
	assert(false);
}

template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::Transport(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	//ready_time = std::max(ready_time, transaction.time_initiated);
	//transaction.time_accepted = ready_time;
	transaction.time_accepted = transaction.time_initiated;	// Never block
	
	// Fixed latency
	transaction.time_completed = transaction.time_accepted + (ROUND_COUNT - 1) * cycle_time + latency;

	switch(transaction.type) {
	case TransactionLoad:
	case TransactionStore:
		LoadStore(transaction);
		break;
	case TransactionVectorLoad:
	case TransactionVectorStore:
		VectorLoadStore(transaction);
		break;
	case TransactionGather:
	case TransactionScatter:
		GatherScatter(transaction);
		break;
	default:
		assert(false);	// Transaction type not supported
	}
}

template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::LoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// Scalar transaction.
	uint64_t cycle = transaction.time_accepted / cycle_time;
	bool succeeded = false;
	for(unsigned int j = 0; j != ROUND_COUNT; ++j)
	{
		unsigned int bank_id = (transaction.address[0] >> MEMCONFIG::LOG_BANKWIDTH) & (MEMCONFIG::BANKS - 1);
		typename MEMCONFIG::address_t index = transaction.address[0] >> MEMCONFIG::LOG_TOTALWIDTH;
		//cerr << " Cycle " << cycle << ", stage " << j << ", bank " << bank_id;
		typename MEMCONFIG::address_t bin = state.Read(cycle, j, bank_id);
		//cerr << ": " << hex << bin << dec << endl;
		if(bin == 0 || bin == index) {
			// Assumes unlimited read/write combining
			state.Write(index, cycle, j, bank_id);
			succeeded = true;
			break;
		}
	}
	
	if(!succeeded) {
		// Nack all active lanes of the vector
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i) {
			transaction.nack[i] = transaction.mask[i];
		}
	}
}

template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::VectorLoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	assert(false);	// Is this function used at all? Let's see...
	
	// Assume aligned transaction
	assert((transaction.address[0] & (MEMCONFIG::TOTALWIDTH - 1)) == 0);
	assert(transaction.logsize == 2);	// Assume 32-bit words

	//transaction.time_completed = transaction.time_accepted + (ROUND_COUNT - 1) * cycle_time + latency;
	//ready_time += ROUND_COUNT * cycle_time;
}

// Check for conflicts and nack conflicting transactions.
// Runs in constant simulated time.
template<class MEMCONFIG>
void BankedMemory<MEMCONFIG>::GatherScatter(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// |------------|-------|----|
	// |    index   |bank_id|byte|
	uint64_t cycle = transaction.time_accepted / cycle_time;
	
	for(unsigned int j = 0; j != ROUND_COUNT; ++j)
	{
		typename MEMCONFIG::address_t bins[MEMCONFIG::BANKS] = { 0 };
		state.Read(bins, cycle, j);
		
		for(unsigned int i = j * MEMCONFIG::BANKS; i != (j + 1) * MEMCONFIG::BANKS; ++i)
		{
			if(transaction.mask[i]){
				unsigned int bank_id = (transaction.address[i] >> MEMCONFIG::LOG_BANKWIDTH) & (MEMCONFIG::BANKS - 1);
				typename MEMCONFIG::address_t index = transaction.address[i] >> MEMCONFIG::LOG_TOTALWIDTH;

				if(bins[bank_id] == 0 || bins[bank_id] == index) {
					// Assumes unlimited read/write combining
					// TODO: model Tesla behavior
					// Tesla Gather: 1 broadcast word, Tesla Scatter: sequential w/ replay
					bins[bank_id] = index;
				}
				else {
					// Conflict: nack request
					transaction.nack[i] = true;
				}
			}
		}
		state.Write(bins, cycle, j);
	}
}	

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
