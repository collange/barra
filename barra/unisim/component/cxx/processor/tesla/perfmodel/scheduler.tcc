/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULER_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
Time Scheduler<CONFIG>::EarliestSchedule(Warp<CONFIG> const & warp,
	unsigned int subwarpid, Time current_time)
{
	// Decoded instruction
	InstructionBufferEntry<CONFIG> const & ibe = instruction_buffer.Read(warp.id, subwarpid);
	assert(ibe.valid);
	Time time_last = std::max(ibe.time_fetched, current_time);
	
	TimedInstruction<CONFIG> const & insn = ibe.instruction;
	SubWarp<CONFIG> const & subwarp = ibe.subwarp;
	
	
	// Check for RAW and WAW dependencies in scoreboard
	// Why WAW? Don't know, Tesla does that.
	for(unsigned int i = 0; i != insn.InputCount(); ++i)
	{
		time_last = std::max(time_last,
			this->scoreboard.Query(warp, insn.Input(i), subwarp.mask));
	}
	
	for(unsigned int i = 0; i != insn.OutputCount(); ++i)
	{
		time_last = std::max(time_last,
			this->scoreboard.Query(warp, insn.Output(i), subwarp.mask));
	}
	
	return time_last;
}

template<class CONFIG>
std::bitset<CONFIG::WARP_SIZE> Scheduler<CONFIG>::Ready(Warp<CONFIG> const & warp,
	unsigned int subwarpid, Time current_time)
{
	InstructionBufferEntry<CONFIG> const & ibe = instruction_buffer.Read(warp.id, subwarpid);
	assert(ibe.valid);
	if(ibe.time_fetched > current_time) {
		return std::bitset<CONFIG::WARP_SIZE>();
	}

	TimedInstruction<CONFIG> const & insn = ibe.instruction;
	SubWarp<CONFIG> const & subwarp = ibe.subwarp;
	std::bitset<CONFIG::WARP_SIZE> ready = subwarp.mask;
	
	// Check for RAW and WAW dependencies in scoreboard
	// Why WAW? Don't know, Tesla does that.
	for(unsigned int i = 0; i != insn.InputCount(); ++i)
	{
		ready &= scoreboard.Available(warp, insn.Input(i),
			subwarp.mask, current_time);
	}
	
	for(unsigned int i = 0; i != insn.OutputCount(); ++i)
	{
		ready &= scoreboard.Available(warp, insn.Output(i),
			subwarp.mask, current_time);
	}
	
	return ready;
}

template<class CONFIG>
void Scheduler<CONFIG>::Reset()
{
}


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
