/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULING_POLICIES_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_SCHEDULING_POLICIES_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
struct SchedulingCandidate
{
	unsigned int warpid;
	unsigned int subwarpid;
	unsigned int split_unavail;
	unsigned int split_resources;
	unsigned int idle;
	unsigned int previous_warpid;
	unsigned int previous_subwarpid;
	
	SchedulingCandidate(unsigned int warpid, unsigned int subwarpid,
		unsigned int split_unavail, unsigned int split_resources,
		unsigned int idle, unsigned int prev_wid, unsigned int prev_swid) :
		warpid(warpid), subwarpid(subwarpid), split_unavail(split_unavail),
		split_resources(split_resources), idle(idle),
		previous_warpid(prev_wid), previous_subwarpid(prev_swid) {}
};

template<class CONFIG>
struct AgeSchedulingPolicy
{
	AgeSchedulingPolicy(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb) :
		sm(sm), instruction_buffer(ib),
		scoreboard(sb) {
		Reset();
	}
	
	uint64_t operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time);
	
	void Reset();
	void Update(unsigned int warpid, unsigned int subwarpid, Time time_issue);
private:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
	Time last_issue[CONFIG::MAX_WARPS];
};

template<class CONFIG>
struct SkewedSchedulingPolicy
{
	SkewedSchedulingPolicy(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb) :
		sm(sm), instruction_buffer(ib),
		scoreboard(sb) {}
	
	uint64_t operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time);
	void Reset() {}
	void Update(unsigned int warpid, unsigned int subwarpid, Time time_issue) {}
	
private:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
};

template<class CONFIG, class SEC>
struct BestfitSchedulingPolicy
{
	BestfitSchedulingPolicy(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb) :
		sm(sm), instruction_buffer(ib),
		scoreboard(sb),
		secondary(sm, ib, sb) {}
	
	uint64_t operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time);
	void Reset() { secondary.Reset(); }
	void Update(unsigned int warpid, unsigned int subwarpid, Time time_issue) {
		secondary.Update(warpid, subwarpid, time_issue);
	}
	
private:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
	SEC secondary;
};

// My personal favorite
template<class CONFIG>
struct RandomSchedulingPolicy
{
	RandomSchedulingPolicy(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb) :
		sm(sm), instruction_buffer(ib),
		scoreboard(sb) {}
	
	uint64_t operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time);
	void Reset() {}
	void Update(unsigned int warpid, unsigned int subwarpid, Time time_issue) {}
	
private:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
};

template<class CONFIG>
struct RoundrobinSchedulingPolicy
{
	RoundrobinSchedulingPolicy(TeslaSM<CONFIG> & sm, InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb) :
		sm(sm), instruction_buffer(ib),
		scoreboard(sb) {}
	
	uint64_t operator() (SchedulingCandidate<CONFIG> const & candidate,
		Time time);
	void Reset() { nextwarp = 0; }
	void Update(unsigned int warpid, unsigned int subwarpid, Time time_issue);
	
private:
	TeslaSM<CONFIG> & sm;
	InstructionBuffer<CONFIG> & instruction_buffer;
	typename CONFIG::scoreboard_t & scoreboard;
	unsigned int nextwarp;
};


} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
