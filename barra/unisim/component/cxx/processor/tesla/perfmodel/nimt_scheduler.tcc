/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_NIMT_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_NIMT_SCHEDULER_TCC

#include <limits>
#include <unisim/component/cxx/processor/tesla/perfmodel/nimt_scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
NimtScheduler<CONFIG>::NimtScheduler(TeslaSM<CONFIG> & sm,
	InstructionBuffer<CONFIG> & ib,
		typename CONFIG::scoreboard_t & sb, typename CONFIG::executionunits_t & eu) :
	Scheduler<CONFIG>(sm, ib, sb, eu)
{
}

template<class CONFIG>
NimtScheduler<CONFIG>::~NimtScheduler()
{
}

template<class CONFIG>
bool NimtScheduler<CONFIG>::Schedule(Time current_time,
	bool blocking, unsigned int round, bool & all_finished,
	unsigned int & warpid,
	unsigned int & subwarpid, Time & issue_time)
{
	typedef typename CONFIG::address_t address_t;
	assert(!blocking);
	// Look for other ready instructions (subwarpid) from the same warp (warpid)
//	if(!avail_mask.any()) {
//		return false;	// Optimization: shortcut
//	}
	InstructionBufferEntry<CONFIG> & prev_ibe
		= this->instruction_buffer.Read(warpid, subwarpid);
	
	for(unsigned int sw = subwarpid + 1; sw != CONFIG::NIMT_PHASES; ++sw)
	{
		InstructionBufferEntry<CONFIG> & ibe
			= this->instruction_buffer.Read(warpid, sw);
		Warp<CONFIG> const & warp = this->sm.GetWarp(warpid);
		
		if(!ibe.valid) {
	continue;
		}

		std::bitset<CONFIG::WARP_SIZE> avail_mask
			= this->execution_units.Ready(ibe.instruction, current_time, round);

		// Check reconvergence constraints
		if(CONFIG::NIMT_CONSTRAINTS) {
			address_t divpt = this->sm.divergence_points[ibe.subwarp.pc - this->sm.CodeBase()];
			if(divpt != address_t(-1)
				&& prev_ibe.valid
				&& (!CONFIG::NIMT_CONSTRAINTS_SELECTIVE
				    || prev_ibe.subwarp.pc > divpt + this->sm.CodeBase())
			)
			{
				if(this->sm.TraceScheduler()) {
					cerr << "NimtSched: subwarp " << warpid << ":" << sw << " blocked by sync constraint" << endl;
				}
	continue;
			}
		}

		// Exclude invalid, suspended or conflicting subwarps
		// Conflicting subwarps are caused by reconvergence
		if(!(ibe.subwarp.mask & ~avail_mask).any() &&
			warp.flow.Ready(ibe.subwarp)) {
			
			
			Time issue_wi = this->EarliestSchedule(warp, sw, current_time);

			if(issue_wi <= issue_time) {
				// Possible schedule
				// Greedy: select first one
				subwarpid = sw;
				return true;
			}
		}
	}
	// Do not modify all_finished.
	
	return false;	// No match found
}



} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
