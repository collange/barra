/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#include <unisim/component/cxx/processor/tesla/perfmodel/config.hh>

// Include all perfmodel/.tcc here
#include <unisim/component/cxx/processor/tesla/cpu.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/tesla_sm.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/timed_instruction.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/age_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/minpc_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/random_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/skewed_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/nimt_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/splitting_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/subset_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/roundrobin_scheduler.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/timetracker.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/instruction_buffer.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/scoreboard.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/perthread_scoreboard.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/coalescer.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/bwconstrained_memory.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/banked_memory.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/cache.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/affine_vector_cache.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/scheduling_policies.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/execution_resources.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/frontend.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/l0icache.tcc>
#include <unisim/component/cxx/processor/tesla/perfmodel/memory_power.tcc>

#include <unisim/component/cxx/processor/tesla/static/divergence_points.tcc>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template class TeslaSM<TimedConfig>;

} // end of namespace perfmodel

using namespace unisim::component::cxx::processor::tesla::perfmodel;
template class CPU<TimedConfig>;
template class Stats<TimedConfig>;
template class Liveness<TimedConfig>;

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

