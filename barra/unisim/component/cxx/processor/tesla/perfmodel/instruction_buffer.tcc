/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_INSTRUCTION_BUFFER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_INSTRUCTION_BUFFER_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/instruction_buffer.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
InstructionBuffer<CONFIG>::InstructionBuffer(TeslaSM<CONFIG> * cpu)
{
	for(unsigned int i = 0; i != CONFIG::MAX_WARPS; ++i)
	{
		for(unsigned int j = 0; j != CONFIG::NIMT_PHASES; ++j)
		{
			buffer[i][j].instruction.SetCPU(cpu);
			buffer[i][j].wid = i;
		}
	}
	Reset();
}

template<class CONFIG>
void InstructionBuffer<CONFIG>::Reset()
{
	for(unsigned int i = 0; i != CONFIG::MAX_WARPS; ++i)
	{
		for(unsigned int j = 0; j != CONFIG::NIMT_PHASES; ++j)
		{
			buffer[i][j].valid = false;
			buffer[i][j].time_fetched = 0;
			buffer[i][j].time_pc_avail = 0;
		}
	}
}


template<class CONFIG>
InstructionBuffer<CONFIG>::~InstructionBuffer()
{
}

template<class CONFIG>
void InstructionBuffer<CONFIG>::Fetch(unsigned int warpid, unsigned int subwarpid,
	SubWarp<CONFIG> const & subwarp, Time time_fetched)
{
	buffer[warpid][subwarpid].instruction.Fetch(subwarp.pc);
	buffer[warpid][subwarpid].time_fetched = time_fetched;
	buffer[warpid][subwarpid].valid = true;
	buffer[warpid][subwarpid].subwarp = subwarp;
//	buffer[warpid][subwarpid].wid = warpid;
}

template<class CONFIG>
InstructionBufferEntry<CONFIG> & InstructionBuffer<CONFIG>::Read(unsigned int warpid,
	unsigned int subwarpid)
{
	return buffer[warpid][subwarpid];
}

template<class CONFIG>
void InstructionBuffer<CONFIG>::UpdatePC(unsigned int warpid, unsigned int subwarpid, Time time_pc_avail)
{
	buffer[warpid][subwarpid].time_pc_avail = time_pc_avail;
	buffer[warpid][subwarpid].valid = false;	// Not fetched yet (is this needed?)
}

template<class CONFIG>
void InstructionBuffer<CONFIG>::Invalidate(unsigned int warpid, unsigned int subwarpid)
{
	buffer[warpid][subwarpid].valid = false;
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
