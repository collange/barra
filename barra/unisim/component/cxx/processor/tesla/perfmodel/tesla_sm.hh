/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TESLASM_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_TESLASM_HH

#include <unisim/component/cxx/processor/tesla/cpu.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/timed_instruction.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/instruction_buffer.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/timetracker.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/coalescer.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/bwconstrained_memory.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/banked_memory.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/affine_vector_cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/l0icache.hh>
#include <unisim/component/cxx/processor/tesla/static/divergence_points.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

using namespace unisim::component::cxx::processor::tesla;

template <class CONFIG>
struct TeslaSM : CPU<CONFIG>
{
	typedef typename CONFIG::address_t address_t;
	typedef VectorRegister<CONFIG> VecReg;
	typedef VectorFlags<CONFIG> VecFlags;
	typedef VectorAddress<CONFIG> VecAddr;

	TeslaSM(char const * name, Object * parent = 0, int coreid = 0, int core_count = 1);
	virtual ~TeslaSM();

	virtual bool Setup();
	virtual void Reset();
	virtual void Stop(int ret);
	virtual void Run();
	bool Step();

	void Fetch(uint32_t warpid, unsigned int subwarpid, SubWarp<CONFIG> const & subwarp);
	void UpdatePC();
	void FunctionalSimStep(unsigned int phase);
	void Writeback();
	void ComputeNPC();
	void Replay(std::bitset<CONFIG::WARP_SIZE> nack);
	
	void FetchAll();

	// Memory access interface
	virtual void Gather(VecAddr const & addr, VecReg & data, std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, uint32_t factor, typename CONFIG::address_t offset, Domain domain);
	virtual void Scatter(VecAddr const & addr, VecReg const & data,
		std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, uint32_t factor, typename CONFIG::address_t offset, Domain domain);
	virtual void Broadcast(address_t addr, VecReg & data, uint32_t logsize, uint32_t factor, typename CONFIG::address_t offset, Domain domain);
	virtual void VectorLoad(address_t addr, VecReg & data, std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain);
	virtual void VectorStore(address_t addr, VecReg const & data,
		std::bitset<CONFIG::WARP_SIZE> mask, uint32_t logsize, Domain domain);


	Parameter<bool> param_trace_pipeline;
	Parameter<bool> param_trace_scheduler;
	bool trace_pipeline;
	bool TracePipeline() { return trace_pipeline && this->TraceEnabled(); }
	bool trace_scheduler;
	bool TraceScheduler() { return trace_scheduler && this->TraceEnabled(); }
	Time CycleTime() const { return cycle_time; }

private:
	TimedVectorMemoryInterface<CONFIG> * MemoryInterface(Domain domain);
	void ProcessTransaction(MemoryTransaction<CONFIG> & transaction);
	
	void Issue(InstructionMetaData & insn);
	void Writeback(InstructionMetaData & insn,
		Time time_schedule, Time time_issue, Time time_pc_avail);

	typename CONFIG::executionunits_t execution_units;
	
	typename CONFIG::scoreboard_t scoreboard;
	InstructionBuffer<CONFIG> instruction_buffer;
	
	typename CONFIG::frontend_t frontend;
	
	uint64_t cycle_time_int;
	Parameter<uint64_t> param_cycle_time;
	
	Time cycle_time;
	TimeTracker<CONFIG> tracker;
	
	TimedInstruction<CONFIG> * current_instruction;
	
	PerfectMemory<CONFIG> dummy_memory;
	BWConstrainedMemory<BaseMemConfig<CONFIG> > dram;
	MemoryPower<typename CONFIG::DRAMPower> dram_power;
	BankedMemory<BaseBankedMemConfig<CONFIG> > smem;
	VectorMemoryPower<typename CONFIG::SMemPower> smem_power;
	VectorMemoryPower<typename CONFIG::DCachePower> dcache_power;
	MemoryPower<typename CONFIG::IFetchPower> icache_power;

	Coalescer<CONFIG> coalescer;
	//AffineVectorCache<L1Config<CONFIG> > local_cache;
	//Cache<LocalCacheConfig<CONFIG> > local_cache;
	typename CONFIG::l1_cache_t local_cache;
	L0ICache<CONFIG> l0i_cache;
	
	TimedVectorMemoryInterface<CONFIG> * instruction_memory;
	TimedVectorMemoryInterface<CONFIG> * const_memory;
	TimedVectorMemoryInterface<CONFIG> * shared_memory;
	TimedVectorMemoryInterface<CONFIG> * const global_memory;
	TimedVectorMemoryInterface<CONFIG> * local_memory;
	
	std::bitset<CONFIG::WARP_SIZE> replay_threads;

public:
	DivergencePoints<CONFIG> divergence_points;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
