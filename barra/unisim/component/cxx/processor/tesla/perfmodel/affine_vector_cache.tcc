/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_AFFINE_VECTOR_CACHE_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_AFFINE_VECTOR_CACHE_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/affine_vector_cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/tesla_sm.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class MEMCONFIG>
AffineVectorCache<MEMCONFIG>::AffineVectorCache(TeslaSM<typename MEMCONFIG::Base> & sm,
	TimedMemoryInterface<typename MEMCONFIG::Base> & memory, Time cycle_time) :
//	Base(sm),
	memory(memory),
	cpu(sm),
	local_cache(sm),
	affine_cache(sm),
	cycle_time(cycle_time),
	latency(MEMCONFIG::STAGES * cycle_time),
	ready_time(0)
{
}

template<class MEMCONFIG>
AffineVectorCache<MEMCONFIG>::~AffineVectorCache()
{
}

template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::Load(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted, Time & time_completed)
{
	// TODO?
	assert(false);
}

template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::Store(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted)
{
	// TODO?
	assert(false);
}

template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::Transport(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	ready_time = std::max(ready_time, transaction.time_initiated);
	transaction.time_accepted = ready_time;

	switch(transaction.type) {
	case TransactionLoad:
	case TransactionStore:
		LoadStore(transaction);
		break;
	case TransactionVectorLoad:
	case TransactionVectorStore:
		VectorLoadStore(transaction);
		break;
	case TransactionGather:
	case TransactionScatter:
		GatherScatter(transaction);
		break;
	default:
		assert(false);	// Transaction type not supported
	}
}

template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::LoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// TODO?
	assert(false);

	// Scalar transaction, no conflict
	// 1 time slot
	transaction.time_completed = transaction.time_accepted + latency;
	ready_time += cycle_time;
}

template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::VectorLoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// Assume aligned transaction
	//assert((transaction.address[0] & (MEMCONFIG::TOTALWIDTH - 1)) == 0);
	assert(transaction.logsize == 2);	// Assume 32-bit words

	typename MEMCONFIG::CacheType t;
	typename MEMCONFIG::stats_t & stats = cpu.GetOpStats().CacheStats(t);

	bool hit;
	if(transaction.type == TransactionVectorLoad) {
		if(AFFINE::ENABLED) {
			hit = SplitLoad(transaction, stats);
			if(hit) {
				transaction.time_completed = transaction.time_accepted + latency;
			}
		}
		else {
			hit = local_cache.Load(transaction.data, transaction.address[0],
				transaction.mask, stats.Local());
			if(hit) {
				transaction.time_completed = transaction.time_accepted + latency;
			}
			else {
				// Forward transaction to memory. Add cache latency.
				memory.Load(transaction.address[0], CONFIG::LOG_WARP_SIZE + 2,
					transaction.time_initiated, transaction.time_accepted,
					transaction.time_completed);
				transaction.time_completed += latency;
			}
		}

	}
	else {	// Store
		if(AFFINE::ENABLED) {
			hit = SplitStore(transaction, stats);
		}
		else {
			hit = local_cache.Store(transaction.data, transaction.address[0],
				transaction.mask, stats.Local());
			if(hit) {
				transaction.time_completed = transaction.time_accepted + latency;
			}
			else {
				// Forward transaction to memory. Add cache latency.
				// TODO: this is wrong. Fix it.
				memory.Store(transaction.address[0], CONFIG::LOG_WARP_SIZE + 2,
					transaction.time_initiated, transaction.time_accepted);
				transaction.time_completed = transaction.time_accepted;
			}
		}
	}

	ready_time += cycle_time;
}

// Check for conflicts and nack conflicting transactions.
// Runs in constant simulated time.
template<class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::GatherScatter(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// TODO
	assert(false);
	transaction.time_completed = transaction.time_accepted + latency;
	ready_time += cycle_time;
}

template <class MEMCONFIG>
bool AffineVectorCache<MEMCONFIG>::SplitLoad(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
	MetaCacheStats<CONFIG> &stats)
{
	mask_t mask = transaction.mask;
	address_t addr = transaction.address[0];
	VectorRegister<CONFIG> const & output = transaction.data;
	bool is_affine = output.CheckStridedMasked(mask);
	typedef MEMCONFIG L;
	bool hit;
	if(cpu.TraceCache())
	{
		cerr << dec;
		cerr << "Load for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << " ; Mask: " << mask << endl;
	}
	CacheAccess<typename L::AFFINE> affaccess;
	affaccess.addr = addr;
	mask_t afflookup = affine_cache.LookupMasked(affaccess, stats.Affine());
	mask_t access_mask = mask & afflookup ;
	if( access_mask.any() ) // hit in A$
	{
		hit = true;
		stats.Affine().Hit();
		if(cpu.TraceCache())
		{
			cerr << dec << "    Affine hit on way " << affaccess.way << " on set " << affaccess.index << 
						" with vmask: " << afflookup << " Affine? " << is_affine << endl;
		}
		affine_cache.UpdateReplacementPolicy(affaccess);
		access_mask = mask & ~afflookup;
		if (access_mask.any())	// not every element was present in A$
		{
//			hit = Load(output, addr, access_mask, stats);	// replay
			// Nack conflicting lanes
			transaction.nack = access_mask;
			// And the scheduler will take care of everything else (or livelock).
		}
	}
	else	// miss in A$
	{
		if(cpu.TraceCache())
		{
			cerr << "    Affine miss with vmask: " << afflookup << " Affine? " << is_affine << endl;
		}
		if(is_affine)
		{
			stats.Affine().MissNoRoom();
			affine_cache.UpdateReplacementPolicy(affaccess);
		}
		else
		{
			stats.Affine().MissNotAff();
		}
		CacheAccess<typename L::LOCAL> locaccess;
		locaccess.addr = addr;
		if(local_cache.Lookup(locaccess, stats.Local()))	// hit in G$
		{
			hit = true;
			if(cpu.TraceCache())
			{
				cerr << "    Generic hit on way " << locaccess.way << " hit " << endl;
			}
			// great
		}
		else	// miss in G$
		{
			hit = false;
			if(cpu.TraceCache())
			{
				cerr << "    Generic miss " << endl;
			}

			if(is_affine)	// affine vector
			{
				if (!affaccess.line_hit) // line miss in A$
				{
					affine_cache.ChooseLineToEvict(affaccess);
					affine_cache.EmuEvict(affaccess, stats.Affine());
				}
				affine_cache.EmuFill(affaccess, mask, stats.Affine());
				affine_cache.UpdateReplacementPolicy(affaccess);
				if(cpu.TraceCache())
				{
					cerr << "    Affine fill on way " << affaccess.way << " on set " << affaccess.index << endl;
				}
			}
			else	// generic vector
			{
				local_cache.EvictFill(locaccess, stats.Local()); // eviction from and fill to G$
			}
			// Fill timing
			memory.Load(addr, AFFINE::CACHE_BLOCK_SIZE,
				transaction.time_accepted, transaction.time_accepted,
				transaction.time_completed);
		}
	}
	return hit;
}


template <class MEMCONFIG>
bool AffineVectorCache<MEMCONFIG>::SplitStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
	MetaCacheStats<CONFIG> &stats)
{
	mask_t mask = transaction.mask;
	address_t addr = transaction.address[0];
	VectorRegister<CONFIG> const & output = transaction.data;
	typedef MEMCONFIG L;

	bool hit;
	bool isStrided;
	if ( CONFIG::STAT_DUAL_REG )
		isStrided = output.tag.IsStrided(mask);
	else
		isStrided = output.CheckStridedMasked(mask);
	CacheAccess<typename L::AFFINE> affaccess;
	affaccess.addr = addr;
	mask_t afflookup = affine_cache.LookupMasked(affaccess, stats.Affine());
	if(cpu.TraceCache())
	{
		cerr << "Store for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << " ; Mask: " << mask << " Affine? " << isStrided << endl;
	}
	if(isStrided) // affine vector
	{
		mask_t access_mask = mask & afflookup ;
		if( affaccess.line_hit ) // line hit in A$
		{
			hit = true;
			stats.Affine().Hit();
			if(cpu.TraceCache())
			{
				cerr << dec << "    Affine hit on way " << affaccess.way << " on set " << affaccess.index << " with vmask: " << afflookup << endl;
			}
			if( All(mask | ~affaccess.block->status.vmask) )	// every element is masked or invalid
			{
				affaccess.block->status.vmask = mask;		// replacing the element
			}
			else
			{
				// check data
				VecReg temp;
				// Note: this is absolutely ugly
				cpu.ReadMemory(addr, &temp.v[0], CONFIG::WARP_SIZE * 4);
				if( output.CheckStridedMMD(mask) == temp.CheckStridedMMD(mask) ) // same data
				{
					affaccess.block->status.vmask = (affaccess.block->status.vmask & mask);
				}
				else // different data
				{
					hit = false;
					stats.Affine().WBack();
					if(cpu.TraceCache())
					{
						cerr << dec << "    Affine WBack from way " << affaccess.way << " from set " << affaccess.index << endl;
					}
					affaccess.block->status.vmask = mask;
					memory.Store(addr, AFFINE::CACHE_BLOCK_SIZE,
						transaction.time_accepted, transaction.time_accepted);
				}
			}
		}
		else		// line miss in A$
		{
			hit = false;
			stats.Affine().MissNoRoom();
			affine_cache.ChooseLineToEvict(affaccess);
			// Write back all dirty blocks in the cache line
			LineWriteback(transaction, affaccess);
			affine_cache.EmuEvict(affaccess, stats.Affine());
			affine_cache.EmuWrite(affaccess, mask, stats.Affine());
			if(cpu.TraceCache())
			{
				cerr << dec << "    Affine miss on way " << affaccess.way << " on set " << affaccess.index << " with vmask: " << afflookup << endl;
			}
		}
		affaccess.block->status.dirty = true ;
		affine_cache.UpdateReplacementPolicy(affaccess);
	}
	else	// not affine
	{
		mask_t access_mask = mask & afflookup ;
		if( access_mask.any() ) // hit in A$
		{
			stats.Affine().Hit();
			affine_cache.PartialInvalidate(affaccess, stats.Affine(), mask);
		}
		if(cpu.TraceCache())
		{
			cerr << "    Forward to generic store" << endl;
		}
		hit = local_cache.Store(output, addr, mask, stats.Local());	// just forward the store to G$

		if(!hit) {
			// Forward transaction to memory. Add cache latency.
			// TODO: this is wrong. Fix it.
			memory.Store(transaction.address[0], AFFINE::CACHE_BLOCK_SIZE,
				transaction.time_initiated, transaction.time_accepted);
			transaction.time_completed = transaction.time_accepted;
		}
		
	}
	return hit;
}

template <class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::LineWriteback(MemoryTransaction<CONFIG> & transaction,
	CacheAccess<typename MEMCONFIG::AFFINE> & access)
{
	//typedef typename MEMCONFIG::AFFINE AFFINE;
	// Write back all dirty blocks in the cache line
	for(unsigned int i = 0; i != AFFINE::CACHE_BLOCKS_PER_LINE; ++i)
	{
		CacheBlock<AFFINE> & block = (*access.line_to_evict)[i];
		if(block.status.dirty) {
			memory.Store(block.GetBaseAddr(),
				AFFINE::CACHE_BLOCK_SIZE,
				transaction.time_accepted, transaction.time_accepted);
		}
	}
}

template <class MEMCONFIG>
void AffineVectorCache<MEMCONFIG>::Invalidate()
{ 
	local_cache.Invalidate() ; 
	if(AFFINE::ENABLED)
		affine_cache.Invalidate();
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
