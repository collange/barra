/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CONFIG_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CONFIG_HH

#include <unisim/component/cxx/processor/tesla/config.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/units.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/age_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/minpc_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/random_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/skewed_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/nimt_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/perthread_scoreboard.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/splitting_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/scheduling_policies.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/subset_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/roundrobin_scheduler.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/execution_resources.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/frontend.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/affine_vector_cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/memory_power.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
struct BaseCacheConfig : CONFIG
{
	typedef CONFIG Base;
	//typedef uint32_t address_t;
	typedef uint32_t address_t;
	typedef uint32_t ADDRESS;

	struct CACHE_STATUS
	{
	};
	struct SET_STATUS
	{
		uint32_t plru_bits;
	};
	struct LINE_STATUS
	{
		bool valid;
	};
	struct BLOCK_STATUS
	{
		bool valid;
		bool dirty;
	};

	static const uint32_t CACHE_SIZE = 32 * 1024;
	static const uint32_t CACHE_LOG_BLOCK_SIZE = 7;
	static const uint32_t CACHE_BLOCK_SIZE = 1 << CACHE_LOG_BLOCK_SIZE;   // 128 bytes
	static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
	static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY;
	static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 0; // 1 blocks per line
	static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 1 blocks per line

	static unsigned int const STAGES = 3;
	static unsigned int const LOG_BANKS = 5;
	static unsigned int const LOG_BANKWIDTH = 2;
	static unsigned int const LOG_TOTALWIDTH = LOG_BANKS + LOG_BANKWIDTH;

	static unsigned int const BANKS = 1 << LOG_BANKS;
	static unsigned int const BANKWIDTH = 1 << LOG_BANKWIDTH;
	static unsigned int const TOTALWIDTH = 1 << LOG_TOTALWIDTH;
};

template<class CONFIG>
struct LocalCacheConfig : BaseCacheConfig<CONFIG>
{
	typedef CONFIG Base;
	typedef LocalCache<CONFIG, LocalCacheConfig> cache_t;
	typedef LocalCacheStats<CONFIG> stats_t;
	typedef CacheTypeL1G CacheType;
	//typedef uint32_t address_t;
	typedef uint32_t address_t;
	typedef uint32_t ADDRESS;

	static const uint32_t CACHE_SIZE = 48 * 1024;
	static const uint32_t CACHE_LOG_BLOCK_SIZE = 7;
	static const uint32_t CACHE_BLOCK_SIZE = 1 << CACHE_LOG_BLOCK_SIZE;   // 128 bytes
	static const uint32_t CACHE_LOG_ASSOCIATIVITY = 3; // 6-way set associative
	static const uint32_t CACHE_ASSOCIATIVITY = 6; //1 << CACHE_LOG_ASSOCIATIVITY;
	static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 0; // 1 blocks per line
	static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 1 blocks per line

	static unsigned int const STAGES = 3;

};
template<class CONFIG>
struct L1Config : CONFIG
{
	typedef CONFIG Base;
	//typedef MetaCache<CONFIG, L1Config> cache_t;
	typedef MetaCacheStats<CONFIG> stats_t;
	typedef CacheTypeL1 CacheType;

	static unsigned int const STAGES = 3;

	struct AFFINE
	{
		typedef typename CONFIG::address_t address_t;
		typedef typename CONFIG::address_t ADDRESS;

		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			std::bitset<CONFIG::WARP_SIZE> vmask;
			bool dirty;
		};

		static const bool ENABLED = true;
		static const uint32_t DECOMPRESSION_FACTOR = 8;
		static const uint32_t PHYSICAL_SIZE = 16;
		static const uint32_t CACHE_SIZE = DECOMPRESSION_FACTOR * PHYSICAL_SIZE * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 16 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 2; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 4; //1 << CACHE_LOG_ASSOCIATIVITY; // 4-way set associative
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 3; // 8 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 8 blocks per line
	};

	struct LOCAL
	{
		typedef typename CONFIG::address_t address_t;
		typedef typename CONFIG::address_t ADDRESS;

		struct CACHE_STATUS
		{
		};
		struct SET_STATUS
		{
			uint32_t plru_bits;
		};
		struct LINE_STATUS
		{
			bool valid;
		};
		struct BLOCK_STATUS
		{
			bool valid;
			bool dirty;
		};

		static const uint32_t CACHE_SIZE = 48 * 1024;
		static const uint32_t CACHE_BLOCK_SIZE = 128;   // 128 bytes
		static const uint32_t CACHE_LOG_ASSOCIATIVITY = 3; // 4-way set associative
		static const uint32_t CACHE_ASSOCIATIVITY = 6; //1 << CACHE_LOG_ASSOCIATIVITY;
		static const uint32_t CACHE_LOG_BLOCKS_PER_LINE = 0; // 1 blocks per line
		static const uint32_t CACHE_BLOCKS_PER_LINE = 1 <<  CACHE_LOG_BLOCKS_PER_LINE; // 1 blocks per line
	};
};

template<class CONFIG>
struct DRAMPowerConfig : CONFIG
{
	typedef CONFIG Base;
	static double const TRANSACTION_ENERGY = 100 * NANOJOULE;
	static EnergyReason const REASON = EnergyDRAM;
};

template<class CONFIG>
struct SMemPowerConfig : CONFIG
{
	typedef CONFIG Base;
	static double const TRANSACTION_ENERGY = 5 * NANOJOULE;
	static EnergyReason const REASON = EnergyDCache;
};

template<class CONFIG>
struct DCachePowerConfig : CONFIG
{
	typedef CONFIG Base;
	static double const TRANSACTION_ENERGY = 5 * NANOJOULE;
	static EnergyReason const REASON = EnergyDCache;
};

template<class CONFIG>
struct IFetchPowerConfig : CONFIG
{
	typedef CONFIG Base;
	static double const TRANSACTION_ENERGY = 1 * NANOJOULE;
	static EnergyReason const REASON = EnergyIFetch;
};

struct BaseTimedConfig : BaseConfig
{
	typedef BaseTimedConfig Self;
	
	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
//	typedef ImplicitFlow<Self> control_flow_t;
//	typedef TeslaFlow<Self> control_flow_t;
//	typedef NimtFlow<Self> control_flow_t;
	typedef Tesla2Flow<Self> control_flow_t;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

//	typedef AgeScheduler<Self> primary_scheduler_t;
//	typedef MinPCScheduler<Self> primary_scheduler_t;
//	typedef RandomScheduler<Self> primary_scheduler_t;
//	typedef SkewedScheduler<Self> primary_scheduler_t;
//	typedef SplittingScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SplittingScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SubsetScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;

//	typedef NimtScheduler<Self> secondary_scheduler_t;
	//typedef SplittingScheduler<Self, SkewedSchedulingPolicy<Self> > secondary_scheduler_t;
	//typedef SplittingScheduler<Self, AgeSchedulingPolicy<Self> > secondary_scheduler_t;
//	typedef SplittingScheduler<Self, BestfitSchedulingPolicy<Self> > secondary_scheduler_t;
//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > secondary_scheduler_t;
	typedef SubsetScheduler<Self, RandomSchedulingPolicy<Self> > secondary_scheduler_t;
	
	typedef SimultaneousFrontend<Self> frontend_t;

	static uint32_t const NIMT_PHASES = 1;
	static unsigned int const ISSUE_WIDTH = 2;
	
	typedef PerthreadScoreboard<Self> scoreboard_t;
	
	typedef RicoExecutionResources<Self> executionunits_t;
//	typedef DimitriExecutionResources<Self> executionunits_t;

	static bool const WARP_SPLIT_UNAVAIL = false;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;
	static bool const FAVOR_PRIMARY_SUBWARPS = false;
	static bool const NIMT_CONSTRAINTS = true;
	static bool const NIMT_CONSTRAINTS_SELECTIVE = true;
	static bool const DELAY_PRIMARY_SCHED = false;

	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 2;
	static unsigned int const FU_CYCLES = 8;
	static unsigned int const SCALAR_CYCLES = 1;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = 4;
	static unsigned int const L0ISIZE = 2;
	static unsigned int const IFETCH_BLOCK_LOGSIZE = 6;	// 64 bytes
	static unsigned int const IFETCH_BLOCK_SIZE = 1 << IFETCH_BLOCK_LOGSIZE;
	static bool const HAS_GLOBAL_CACHE = true;
	static double const SCHED_ENERGY = 1. * NANOJOULE;
	static double const ISSUE_ENERGY = 1. * NANOJOULE;
	
	static unsigned int const LOG_INSTRUCTIONLENGTH = 3;	// 8 bytes
	//static uint32_t const NOC_BURST_SIZE = 128;
	//static bool const DEBUG_ENABLE = true;
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
//	static unsigned int const SCHED_SET_BITS = 0x03;
	static unsigned int const SCHED_SET_BITS = 0x0;
	static bool const TRACE_PIPELINE = false;
	static bool const TRACE_SCHEDULER = false;
};

// Niko is single-issue, 32-wide warp
// with MADx32, SFUx8, LSUx32
struct NikoConfig : BaseTimedConfig
{
	typedef NikoConfig Self;

	typedef Tesla2Flow<Self> control_flow_t;

//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SubsetScheduler<Self, AgeSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef RoundrobinScheduler<Self> primary_scheduler_t;
	
	// We need a secondary scheduler, even though it is single-issue
//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > secondary_scheduler_t;
	typedef SubsetScheduler<Self, AgeSchedulingPolicy<Self> > secondary_scheduler_t;

	static uint32_t const NIMT_PHASES = 1;
	static unsigned int const ISSUE_WIDTH = 1;
	
	typedef Scoreboard<Self> scoreboard_t;
	typedef NikoExecutionResources<Self> executionunits_t;
	typedef SimultaneousFrontend<Self> frontend_t;

	static bool const WARP_SPLIT_UNAVAIL = false;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;

	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 2;
	static unsigned int const FU_CYCLES = 8;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = 4;
	
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
	static LaneMapping const LANE_MAPPING = LaneMappingLinear;

	static uint32_t const LOG_WARP_SIZE = 5;
	static uint32_t const MAX_VGPR = 512;
	static uint32_t const MAX_CTAS = 8;
	static uint32_t const SHARED_SIZE = 16 * 1024;
	static AddressMapping const LOCAL_ADDRESS_MAPPING = AddressMappingSparse;
	
	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;
	typedef AffineVectorCache<L1Config<Self> > l1_cache_t;
	//typedef Cache<LocalCacheConfig<Self> > l1_cache_t;
	static bool const HAS_GLOBAL_CACHE = false;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

	// Constants that depend on warp size
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const LOG_MAX_WARPS = 10 - LOG_WARP_SIZE;	// 1024 threads
	static uint32_t const MAX_WARPS_PER_BLOCK = (16 * 32) / WARP_SIZE;
	static uint32_t const MAX_WARPS = 1 << LOG_MAX_WARPS; //(32 * 32) / WARP_SIZE;		// Fermi
	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;
};

// Rico is 2-issue, 32-wide warp
// with 2xMADx32, SFUx8, LSUx32
struct RicoConfig : BaseTimedConfig
{
	typedef RicoConfig Self;

	typedef Tesla2Flow<Self> control_flow_t;
//	typedef NimtFlow<Self> control_flow_t;

//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SubsetScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SubsetScheduler<Self, AgeSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SubsetScheduler<Self, RoundrobinSchedulingPolicy<Self> > primary_scheduler_t;

//	typedef SubsetScheduler<Self, SkewedSchedulingPolicy<Self> > secondary_scheduler_t;
//	typedef SubsetScheduler<Self, RandomSchedulingPolicy<Self> > secondary_scheduler_t;
	typedef SubsetScheduler<Self, AgeSchedulingPolicy<Self> > secondary_scheduler_t;
//	typedef SubsetScheduler<Self, RoundrobinSchedulingPolicy<Self> > secondary_scheduler_t;

	static uint32_t const NIMT_PHASES = 1;
	static unsigned int const ISSUE_WIDTH = 2;
	
	//typedef PerthreadScoreboard<Self> scoreboard_t;
	typedef Scoreboard<Self> scoreboard_t;
	typedef RicoExecutionResources<Self> executionunits_t;
	typedef SimultaneousFrontend<Self> frontend_t;

	static bool const WARP_SPLIT_UNAVAIL = false;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;
	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 3;
	static unsigned int const FU_CYCLES = 8;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = 6;
	//static unsigned int const L0ISIZE = 4;
	
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
	static LaneMapping const LANE_MAPPING = LaneMappingLinear;

	static uint32_t const LOG_WARP_SIZE = 5;
	static uint32_t const LOG_MAX_WARPS = 11 - LOG_WARP_SIZE;
	static uint32_t const MAX_WARPS = (48 * 32) / WARP_SIZE;	// 1536 threads
	
	static uint32_t const MAX_VGPR = 1024;
	static uint32_t const MAX_CTAS = 8;
	static uint32_t const SHARED_SIZE = 16 * 1024;
	static AddressMapping const LOCAL_ADDRESS_MAPPING = AddressMappingSkewedXorRev;
	
	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;
	typedef AffineVectorCache<L1Config<Self> > l1_cache_t;
	//typedef Cache<LocalCacheConfig<Self> > l1_cache_t;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

	// Constants that depend on warp size
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const MAX_WARPS_PER_BLOCK = (32 * 32) / WARP_SIZE;
	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;
};

// Dimitri is 2-issue DIMT with 64-wide warps
// with MADx64, SFUx8, LSUx32
struct DimitriConfig : BaseTimedConfig
{
	typedef DimitriConfig Self;
	

	typedef NimtFlow<Self> control_flow_t;

//	typedef SplittingScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SplittingScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SplittingScheduler<Self, AgeSchedulingPolicy<Self> > primary_scheduler_t;

//	typedef NimtScheduler<Self> secondary_scheduler_t;
	typedef SplittingScheduler<Self, BestfitSchedulingPolicy<Self, RandomSchedulingPolicy<Self> > > secondary_scheduler_t;

	static uint32_t const NIMT_PHASES = 1;
	static bool const NIMT_CONSTRAINTS = false;
	static unsigned int const ISSUE_WIDTH = 2;
	
	typedef PerthreadScoreboard<Self> scoreboard_t;
	
	typedef DimitriExecutionResources<Self> executionunits_t;
	typedef SimultaneousFrontend<Self> frontend_t;
//	typedef CascadedFrontend<Self> frontend_t;

	static bool const WARP_SPLIT_UNAVAIL = true;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;
	static bool const DELAY_PRIMARY_SCHED = false;

	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 4;
	static unsigned int const FU_CYCLES = 8;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = ISSUE_CYCLES + 3+1;
	//static unsigned int const L0ISIZE = 4;
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
//	static unsigned int const SCHED_SET_BITS = 0x1b;
//	static unsigned int const SCHED_SET_BITS = 0x13;
//	static unsigned int const SCHED_SET_BITS = 0x00;
	static unsigned int const SCHED_SET_BITS = 0x1f;

	static LaneMapping const LANE_MAPPING = LaneMappingXorRev;
//	static LaneMapping const LANE_MAPPING = LaneMappingReverseHalf;

	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;
	typedef Cache<LocalCacheConfig<Self> > l1_cache_t;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

	// Constants that depend on warp size
	static uint32_t const LOG_WARP_SIZE = 6;
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const LOG_MAX_WARPS = 11 - LOG_WARP_SIZE;
	static uint32_t const MAX_WARPS_PER_BLOCK = (24 * 32) / WARP_SIZE;	// 1536 threads
	static uint32_t const MAX_WARPS = 1 << LOG_MAX_WARPS; //(32 * 32) / WARP_SIZE;		// Fermi
	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;

	static uint32_t const MAX_VGPR = 1024 * 32 / WARP_SIZE;
	static uint32_t const MAX_CTAS = 8;
	static uint32_t const SHARED_SIZE = 16 * 1024;
};

struct SbiConfig : BaseTimedConfig
{
	typedef SbiConfig Self;
	

	typedef NimtFlow<Self> control_flow_t;

//	typedef SplittingScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
//	typedef SplittingScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SplittingScheduler<Self, AgeSchedulingPolicy<Self> > primary_scheduler_t;

	typedef NimtScheduler<Self> secondary_scheduler_t;

	static uint32_t const NIMT_PHASES = 2;
	static unsigned int const ISSUE_WIDTH = 2;
	
	typedef PerthreadScoreboard<Self> scoreboard_t;
	
	typedef DimitriExecutionResources<Self> executionunits_t;
	typedef SimultaneousFrontend<Self> frontend_t;

	static bool const WARP_SPLIT_UNAVAIL = false;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;
	static bool const DELAY_PRIMARY_SCHED = false;

	static bool const NIMT_CONSTRAINTS = false;
	static bool const NIMT_CONSTRAINTS_SELECTIVE = true;

	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 3;
	static unsigned int const FU_CYCLES = 8;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = ISSUE_CYCLES + 3+1;
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
//	static unsigned int const SCHED_SET_BITS = 0x0b;
	static unsigned int const SCHED_SET_BITS = 0x0;
	static unsigned int const SCHED_FLIP_BITS = 0x0;

	static LaneMapping const LANE_MAPPING = LaneMappingLinear;

	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;
	typedef Cache<LocalCacheConfig<Self> > l1_cache_t;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

	// Constants that depend on warp size
	static uint32_t const LOG_WARP_SIZE = 6;
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const LOG_MAX_WARPS = 11 - LOG_WARP_SIZE;
	static uint32_t const MAX_WARPS_PER_BLOCK = (24 * 32) / WARP_SIZE;	// 1536 threads
	static uint32_t const MAX_WARPS = 1 << LOG_MAX_WARPS; //(32 * 32) / WARP_SIZE;		// Fermi
	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;
	static uint32_t const MAX_VGPR = 1024 * 32 / WARP_SIZE;
	static uint32_t const MAX_CTAS = 8;
	static uint32_t const SHARED_SIZE = 16 * 1024;
};


struct ScratchConfig : BaseTimedConfig
{
	typedef ScratchConfig Self;
	

	typedef NimtFlow<Self> control_flow_t;

//	typedef SplittingScheduler<Self, SkewedSchedulingPolicy<Self> > primary_scheduler_t;
	typedef SplittingScheduler<Self, RandomSchedulingPolicy<Self> > primary_scheduler_t;

//	typedef NimtScheduler<Self> secondary_scheduler_t;
	typedef SplittingScheduler<Self, BestfitSchedulingPolicy<Self, SkewedSchedulingPolicy<Self> > > secondary_scheduler_t;

	static uint32_t const NIMT_PHASES = 2;
	static unsigned int const ISSUE_WIDTH = 2;
	
	typedef PerthreadScoreboard<Self> scoreboard_t;
	
	typedef DimitriExecutionResources<Self> executionunits_t;
	typedef SimultaneousFrontend<Self> frontend_t;

	static bool const WARP_SPLIT_UNAVAIL = true;
	static bool const WARP_SPLIT_RESOURCES = false;
	static bool const SCHED_ONLY_PRIMARY = false;
	static bool const PRIMARY_SCHED_PRIMARY_SW = true;

	static Time const CYCLE_TIME = 1 * NANOSECOND;
	static unsigned int const ISSUE_CYCLES = 2;
	static unsigned int const FU_CYCLES = 8;
	static Time const COALESCER_LATENCY = 0;
	static unsigned int const BRANCH_CYCLES = 4;
	
//	static unsigned int const SCHED_SKEW_BYTES = 128;
	static unsigned int const SCHED_SKEW_BYTES = 0;
//	static unsigned int const SCHED_SET_BITS = 0x0b;
	static unsigned int const SCHED_SET_BITS = 0x0;

	static LaneMapping const LANE_MAPPING = LaneMappingXorRev;

	// Types that depend on Config
	typedef VectorFP32<Self> vfp32;
	typedef Stats<Self> stats_t;
	typedef OperationStats<Self> operationstats_t;
	typedef AVCL0<Self> L0;
	typedef AVCL1<Self> L1;
	typedef Cache<LocalCacheConfig<Self> > l1_cache_t;
	typedef DRAMPowerConfig<Self> DRAMPower;
	typedef SMemPowerConfig<Self> SMemPower;
	typedef DCachePowerConfig<Self> DCachePower;
	typedef IFetchPowerConfig<Self> IFetchPower;

	// Constants that depend on warp size
	static uint32_t const LOG_WARP_SIZE = 6;
	static uint32_t const WARP_SIZE = 1 << LOG_WARP_SIZE;
	static uint32_t const LOG_MAX_WARPS = 10 - LOG_WARP_SIZE;	// 1024 threads
	static uint32_t const MAX_WARPS_PER_BLOCK = (16 * 32) / WARP_SIZE;
	static uint32_t const MAX_WARPS = 1 << LOG_MAX_WARPS; //(32 * 32) / WARP_SIZE;		// Fermi
	static uint32_t const MAX_THREADS = MAX_WARPS * WARP_SIZE;
	static uint32_t const TRANSACTIONS_PER_WARP = WARP_SIZE / TRANSACTION_SIZE;
	static uint32_t const STRIDED_SEGS_PER_WARP = WARP_SIZE / STRIDED_SEG_SIZE;
};

//typedef BaseTimedConfig TimedConfig;
typedef NikoConfig TimedConfig;
//typedef RicoConfig TimedConfig;
//typedef DimitriConfig TimedConfig;
//typedef SbiConfig TimedConfig;




} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
