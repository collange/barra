/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CACHE_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_CACHE_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/cache.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/tesla_sm.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class MEMCONFIG>
Cache<MEMCONFIG>::Cache(TeslaSM<typename MEMCONFIG::Base> & sm,
	TimedMemoryInterface<typename MEMCONFIG::Base> & memory, Time cycle_time) :
	Base(sm),
	memory(memory),
	sm(sm),
	cycle_time(cycle_time),
	latency(MEMCONFIG::STAGES * cycle_time),
	ready_time(0)
{
}

template<class MEMCONFIG>
Cache<MEMCONFIG>::~Cache()
{
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::Load(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted, Time & time_completed)
{
	// TODO?
	assert(false);
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::Store(typename MEMCONFIG::address_t addr,
	unsigned int logwidth, Time time,
	Time & time_accepted)
{
	// TODO?
	assert(false);
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::Transport(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	ready_time = std::max(ready_time, transaction.time_initiated);
	transaction.time_accepted = ready_time;
	transaction.time_completed = ready_time;

	switch(transaction.type) {
	case TransactionLoad:
	case TransactionStore:
		LoadStore(transaction);
		break;
	case TransactionVectorLoad:
		VectorLoadStore(transaction, true);
		break;
	case TransactionVectorStore:
		VectorLoadStore(transaction, false);
		break;
	case TransactionGather:
		GatherScatter(transaction, true);
		break;
	case TransactionScatter:
		GatherScatter(transaction, false);
		break;
	default:
		assert(false);	// Transaction type not supported
	}
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::LoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction)
{
	// TODO?
	assert(false);

	// Scalar transaction, no conflict
	// 1 time slot
	transaction.time_completed = transaction.time_accepted + latency;
	ready_time += cycle_time;
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::VectorLoadStore(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
	bool is_load)
{
	// Assume aligned transaction
	//assert((transaction.address[0] & (MEMCONFIG::TOTALWIDTH - 1)) == 0);
	//assert(transaction.logsize == 2);	// Assume 32-bit words
	typename MEMCONFIG::address_t base_address
		= transaction.address[0] & ~(MEMCONFIG::CACHE_BLOCK_SIZE - 1);
	
	for(unsigned int i = 0; i != ROUND_COUNT; ++i) {
		typename MEMCONFIG::address_t row_address = base_address + i * MEMCONFIG::CACHE_BLOCK_SIZE;
		
		std::bitset<CONFIG::WARP_SIZE> slice_mask = ((1ull << SLICE_WIDTH)-1) << i * SLICE_WIDTH;
		
		Time time_accepted = transaction.time_accepted;
		Time time_completed = transaction.time_completed;
		
		if((transaction.mask & slice_mask).any()) {
			SegmentLoadStore(transaction, is_load, row_address, time_accepted,
				time_completed);
			transaction.time_accepted = std::max(transaction.time_accepted, time_accepted);
			transaction.time_completed = std::max(transaction.time_completed, time_completed);

			ready_time += cycle_time;
		}
	}
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::SegmentLoadStore(MemoryTransaction<CONFIG> & transaction, bool is_load,
	typename CONFIG::address_t base_address, Time & time_accepted,
	Time & time_completed)
{
	typename MEMCONFIG::CacheType t;
	typename MEMCONFIG::stats_t & stats = sm.GetOpStats().CacheStats(t);
	bool hit;
	if(is_load) {
		// Lookup MSHRs first
		UpdateMSHR(time_accepted);
		if(!CheckMSHR(base_address, time_completed)) {
			// No hit under miss?
			
			hit = Base::Load(transaction.data, base_address,
				transaction.mask, stats);
			if(hit) {
				time_completed = time_accepted + latency;
			}
			else {
				// Forward transaction to memory. Add cache latency.
				// TODO: take into account time skew between cached/uncached transactions
				memory.Load(base_address, MEMCONFIG::CACHE_LOG_BLOCK_SIZE,
					transaction.time_initiated, time_accepted,
					time_completed);
				time_completed += latency;
				SetMSHR(base_address, time_completed);
			}
		}
	}
	else {	// Store
		hit = Base::Store(transaction.data, base_address,
			transaction.mask, stats);

		if(hit) {
			time_completed = time_accepted + latency;
		}
		else {
			// Forward transaction to memory. Add cache latency.
			// TODO: this is wrong. Fix it.
			memory.Store(base_address, MEMCONFIG::CACHE_LOG_BLOCK_SIZE,
				transaction.time_initiated, time_accepted);
			time_completed = time_accepted;
		}
	}

}

// Check for conflicts and nack conflicting transactions.
// Runs in constant simulated time.
template<class MEMCONFIG>
void Cache<MEMCONFIG>::GatherScatter(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
	bool is_load)
{
	if(!transaction.mask.any()) {
		ready_time += cycle_time;
		return;
	}
	
	// Works even for larger transaction sizes
	//assert(transaction.logsize <= 2);
	for(unsigned int i = 0; i != ROUND_COUNT; ++i)
	{
		std::bitset<CONFIG::WARP_SIZE> slice_mask = ((1ull << SLICE_WIDTH)-1) << i * SLICE_WIDTH;
		if((transaction.mask & slice_mask).any()) {
			unsigned int master_lane = ArbitrateBanks(transaction, i);
	
			typename MEMCONFIG::address_t base_address
				= transaction.address[master_lane] & ~(MEMCONFIG::CACHE_BLOCK_SIZE - 1);

			SegmentLoadStore(transaction, is_load, base_address, transaction.time_accepted,
				transaction.time_completed);
			ready_time += cycle_time;
		}
	}
}

template<class MEMCONFIG>
unsigned int Cache<MEMCONFIG>::ArbitrateBanks(MemoryTransaction<typename MEMCONFIG::Base> & transaction,
	unsigned int round)
{
	// Find first set bit in mask: enabled transaction
	std::bitset<CONFIG::WARP_SIZE> slice_mask = ((1ull << SLICE_WIDTH)-1) << round * SLICE_WIDTH;
	assert((transaction.mask & slice_mask).any());
	
	unsigned int slice_begin = round * SLICE_WIDTH;
	unsigned int master_lane = -1;
	for(unsigned int i = 0; i != SLICE_WIDTH; ++i) {
		if(transaction.mask[i + slice_begin]) {
			master_lane = i + slice_begin;
			break;
		}
	}

	typename MEMCONFIG::address_t base_address
		= transaction.address[master_lane] & ~(MEMCONFIG::CACHE_BLOCK_SIZE - 1);

	// Nack transactions that conflict with it
	// Do not bother to track all requests, we only simulate timing
	for(unsigned int i = 0; i != SLICE_WIDTH; ++i)
	{
		if(transaction.mask[i + slice_begin]){
			if((transaction.address[i + slice_begin] & ~(MEMCONFIG::CACHE_BLOCK_SIZE - 1))
				!= base_address) {
				// Conflict: nack request
				transaction.nack[i + slice_begin] = true;
			}
		}
	}
	return master_lane;
}

template<class MEMCONFIG>
bool Cache<MEMCONFIG>::CheckMSHR(typename CONFIG::address_t addr, Time & time_completed)
{
	// Match second (non-key) value of map
	// Likely suboptimal data-structure
	for(typename mshr_t::iterator it = mshr.begin(); it != mshr.end(); ++it)
	{
		if(it->second == addr) {
			time_completed = it->first;
			return true;
		}
	}
	return false;
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::SetMSHR(typename CONFIG::address_t addr, Time time_completed)
{
	mshr[time_completed] = addr;
}

template<class MEMCONFIG>
void Cache<MEMCONFIG>::UpdateMSHR(Time current_time)
{
	// Flush all events from the past
	typename mshr_t::iterator it = mshr.lower_bound(current_time - cycle_time);
	if(it != mshr.end()) {
		mshr.erase(mshr.begin(), it);
	}
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
