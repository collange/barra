/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_L0ICACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_L0ICACHE_HH

#include <boost/circular_buffer.hpp>
#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// Fully-associative small instruction cache, FIFO replacement policy (loop buffer)
template<class CONFIG>
struct L0ICache :
	TimedVectorMemoryInterface<CONFIG>
{
	L0ICache(TeslaSM<CONFIG> & sm, TimedMemoryInterface<CONFIG> & memory,
		TimeTracker<CONFIG> & tracker) :
		sm(sm), memory(memory), tracker(tracker), buffer(CONFIG::L0ISIZE) {}
	virtual ~L0ICache() {}

	virtual void Load(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed);
	virtual void Store(typename CONFIG::address_t addr,
		unsigned int logwidth, Time time, Time & time_accepted);

	virtual void Transport(MemoryTransaction<CONFIG> & transaction);

private:
	TeslaSM<CONFIG> & sm;
	TimedMemoryInterface<CONFIG> & memory;
	TimeTracker<CONFIG> & tracker;
	
	typedef boost::circular_buffer<typename CONFIG::address_t> buffer_t;
	buffer_t buffer;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
