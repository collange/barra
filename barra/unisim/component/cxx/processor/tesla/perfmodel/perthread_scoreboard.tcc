/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_PERTHREAD_SCOREBOARD_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_PERTHREAD_SCOREBOARD_TCC

#include <unisim/component/cxx/processor/tesla/perfmodel/perthread_scoreboard.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {


template<class CONFIG>
PerthreadScoreboard<CONFIG>::PerthreadScoreboard()
{
}

template<class CONFIG>
PerthreadScoreboard<CONFIG>::~PerthreadScoreboard()
{
}

template<class CONFIG>
void PerthreadScoreboard<CONFIG>::Reset()
{
	for(unsigned int j = 0; j != CONFIG::MAX_VGPR; ++j)
	{
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
		{
			gpr[j][i] = 0;
		}
	}
	for(unsigned int j = 0; j != CONFIG::MAX_WARPS; ++j)
	{
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
		{
			for(unsigned int k = 0; k != CONFIG::MAX_ADDR_REGS; ++k)
			{
				ar[j][k][i] = 0;
			}
			
			for(unsigned int k = 0; k != CONFIG::MAX_PRED_REGS; ++k)
			{
				pred[j][k][i] = 0;
			}
			shmem[j][i] = 0;
		}
	}
}

template<class CONFIG>
void PerthreadScoreboard<CONFIG>::Write(Warp<CONFIG> const & warp, OperandID const & operand, std::bitset<CONFIG::WARP_SIZE> mask, Time time)
{
	//assert(mask.any());
	if(!mask.any()) {
		// This is valid, e.g. if explicit predicate evaluates to zero
		return;
	}
	uint32_t rid;
	switch(operand.domain) {
	case DomainGPR:
		rid = warp.GetGPRAddress(operand.id);
		WriteTimeVector(gpr[rid], time, mask);
		break;
	case DomainAR:
		WriteTimeVector(ar[warp.id][operand.id], time, mask);
		break;
	case DomainPred:
		WriteTimeVector(pred[warp.id][operand.id], time, mask);
		break;
	case DomainShared:
		WriteTimeVector(shmem[warp.id], time, mask);
		break;
	default:
		// Just ignore
		break;
	}
	
}

template<class CONFIG>
void PerthreadScoreboard<CONFIG>::WriteTimeVector(Time * vector, Time newtime, std::bitset<CONFIG::WARP_SIZE> mask)
{
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(mask[i]) {
			vector[i] = std::max(vector[i], newtime);
		}
	}
}

template<class CONFIG>
Time PerthreadScoreboard<CONFIG>::ReadTimeVector(Time const * vector, std::bitset<CONFIG::WARP_SIZE> mask) const
{
	Time maxtime = 0;
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(mask[i]) {
			maxtime = std::max(maxtime, vector[i]);
		}
	}
	return maxtime;
}

template<class CONFIG>
Time PerthreadScoreboard<CONFIG>::Query(Warp<CONFIG> const & warp,
	OperandID const & operand,
	std::bitset<CONFIG::WARP_SIZE> mask) const
{
	assert(mask.any());
	uint32_t rid;
	switch(operand.domain) {
	case DomainGPR:
		rid = warp.GetGPRAddress(operand.id);
		return ReadTimeVector(gpr[rid], mask);
		break;
	case DomainAR:
		return ReadTimeVector(ar[warp.id][operand.id], mask);
		break;
	case DomainPred:
		return ReadTimeVector(pred[warp.id][operand.id], mask);
		break;
	case DomainShared:
		return ReadTimeVector(shmem[warp.id], mask);
		break;
	default:
		// Always available
		return 0;
	}
}

template<class CONFIG>
std::bitset<CONFIG::WARP_SIZE> PerthreadScoreboard<CONFIG>::Available(Warp<CONFIG> const & warp,
		OperandID const & operand,
		std::bitset<CONFIG::WARP_SIZE> mask, Time time) const
{
	assert(mask.any());
	uint32_t rid;
	switch(operand.domain) {
	case DomainGPR:
		rid = warp.GetGPRAddress(operand.id);
		return VectorLessThan(gpr[rid], mask, time);
		break;
	case DomainAR:
		return VectorLessThan(ar[warp.id][operand.id], mask, time);
		break;
	case DomainPred:
		return VectorLessThan(pred[warp.id][operand.id], mask, time);
		break;
	case DomainShared:
		return VectorLessThan(shmem[warp.id], mask, time);
		break;
	default:
		// Always available
		return std::bitset<CONFIG::WARP_SIZE>().set();
	}
}

template<class CONFIG>
std::bitset<CONFIG::WARP_SIZE> PerthreadScoreboard<CONFIG>::VectorLessThan(Time const * vector,
	std::bitset<CONFIG::WARP_SIZE> mask, Time time) const
{
	std::bitset<CONFIG::WARP_SIZE> less;
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(mask[i] && vector[i] <= time) {
			less.set(i);
		}
	}
	return less;
}

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
