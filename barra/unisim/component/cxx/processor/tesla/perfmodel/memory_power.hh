/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_MEMORY_POWER_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_MEMORY_POWER_HH

#include <unisim/component/cxx/processor/tesla/perfmodel/interfaces.hh>
#include <unisim/component/cxx/processor/tesla/perfmodel/timetracker.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

// Very naive model, fixed transaction cost
template<class MEMCONFIG>
struct MemoryPower : TimedVectorMemoryInterface<typename MEMCONFIG::Base>
{
	MemoryPower(TimeTracker<typename MEMCONFIG::Base> & tracker, TimedMemoryInterface<typename MEMCONFIG::Base> & memory) :
		tracker(tracker), memory(memory) {}

	virtual void Load(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed);
	virtual void Store(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted);
	virtual Time Synchronize(Time t) {
		return t;
	}
	virtual void Transport(MemoryTransaction<typename MEMCONFIG::Base> & transaction);

private:
	void Transaction();
	TimeTracker<typename MEMCONFIG::Base> & tracker;
	TimedMemoryInterface<typename MEMCONFIG::Base> & memory;
};

template<class MEMCONFIG>
struct VectorMemoryPower : TimedVectorMemoryInterface<typename MEMCONFIG::Base>
{
	VectorMemoryPower(TimeTracker<typename MEMCONFIG::Base> & tracker,
		TimedVectorMemoryInterface<typename MEMCONFIG::Base> & memory) :
		tracker(tracker), memory(memory) {}

	virtual void Load(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted, Time & time_completed) {
		Transaction();
		memory.Load(addr, logwidth, time, time_accepted, time_completed);
	}

	virtual void Store(typename MEMCONFIG::address_t addr, unsigned int logwidth, Time time, Time & time_accepted) {
		Transaction();
		memory.Store(addr, logwidth, time, time_accepted);
	}
	virtual Time Synchronize(Time t) {
		return t;
	}
	virtual void Transport(MemoryTransaction<typename MEMCONFIG::Base> & transaction) {
		Transaction();
		memory.Transport(transaction);
	}

private:
	void Transaction() {
		tracker.SpendEnergy(MEMCONFIG::TRANSACTION_ENERGY, MEMCONFIG::REASON);
	}
	TimeTracker<typename MEMCONFIG::Base> & tracker;
	TimedVectorMemoryInterface<typename MEMCONFIG::Base> & memory;
};

} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
