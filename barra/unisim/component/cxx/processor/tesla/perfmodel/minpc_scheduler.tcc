/*
 *  Copyright (c) 2011,
 *  ENS Lyon,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENSL nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

// This file is obsolete, don't use it

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_MINPC_SCHEDULER_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_PERFMODEL_MINPC_SCHEDULER_TCC

#include <limits>
#include <unisim/component/cxx/processor/tesla/perfmodel/minpc_scheduler.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
namespace perfmodel {

template<class CONFIG>
MinPCScheduler<CONFIG>::MinPCScheduler(TeslaSM<CONFIG> & sm,
	InstructionBuffer<CONFIG> & ib,
		Scoreboard<CONFIG> & sb) :
	Scheduler<CONFIG>(sm, ib, sb)
{
}

template<class CONFIG>
MinPCScheduler<CONFIG>::~MinPCScheduler()
{
}

template<class CONFIG>
bool MinPCScheduler<CONFIG>::Schedule(Time current_time, unsigned int & warpid, Time & issue_time)
{
	// Select warp with smallest PC among those that can be scheduled first
	// TODO: consider SP as well
	bool all_finished = true;
	Time earliest_future_issue = std::numeric_limits<Time>::max();
	typename CONFIG::address_t minpc = std::numeric_limits<typename CONFIG::address_t>::max();
	int candidate = -1;
	for(unsigned int i = 0; i != this->sm.WarpCount(); ++i)
	{
		Warp<CONFIG> const & warp = this->sm.GetWarp(i);
		if(warp.state != Warp<CONFIG>::Finished) {
			all_finished = false;
		}
		if(warp.state == Warp<CONFIG>::Active) {
			Time issue_wi = this->EarliestSchedule(warp, current_time);

			if(issue_wi < earliest_future_issue) {
				// Can be scheduler earlier, reset PC
				candidate = i;
				minpc = warp.flow.GetPC();
				earliest_future_issue = issue_wi;
			}
			else if(issue_wi == earliest_future_issue) {
				typename CONFIG::address_t pc = warp.flow.GetPC();
				if(pc < minpc) {
					// As ready as other warps and older: select
					candidate = i;
					minpc = pc;
				}
				earliest_future_issue = issue_wi;
			}
		}
	}
	assert(all_finished || candidate != -1);
	warpid = candidate;
	issue_time = earliest_future_issue;
	return all_finished;
}



} // end of namespace perfmodel
} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
