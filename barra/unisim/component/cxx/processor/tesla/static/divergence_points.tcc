/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_DIVERGENCE_POINTS_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_DIVERGENCE_POINTS_TCC

#include <unisim/component/cxx/processor/tesla/static/divergence_points.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
void DivergencePoints<CONFIG>::Build(typename CONFIG::address_t start, typename CONFIG::address_t end)
{
//	cerr << "Building divpt map from " << start << " to " << end << endl;
	
	typedef typename CONFIG::address_t address_t;
	// Reverse mapping given by SSY and PBK instructions
	divpoints.clear();
	address_t pc = start;
	while(pc < end) {
		Instruction<CONFIG> instruction(cpu, pc);
		
		typedef std::vector<Target<CONFIG> > myvector;
		myvector targets = instruction.Targets();
		for(typename myvector::iterator it = targets.begin();
			it != targets.end();
			++it) {
			if(it->type == TargetSSY || it->type == TargetPBK) {
				divpoints[it->pc] = pc - start;
			}
		}
		pc += instruction.IsLong() ? 8 : 4;
	}
}

template<class CONFIG>
void DivergencePoints<CONFIG>::Dump(std::ostream & os) const
{
	for(typename divpoints_t::const_iterator it = divpoints.begin(); it != divpoints.end(); ++it) {
		os << hex << it->first << " -> " << it->second << dec << endl;
	}
}

template<class CONFIG>
typename CONFIG::address_t DivergencePoints<CONFIG>::operator[](typename CONFIG::address_t pc) const
{
	typename divpoints_t::const_iterator it = divpoints.find(pc);
	if(it == divpoints.end()) {
		return -1;
	}
	else {
		return it->second;
	}
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
