/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_CONTROL_FLOW_GRAPH_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_CONTROL_FLOW_GRAPH_HH

#include <unisim/component/cxx/processor/tesla/forward.hh>
#include <unisim/component/cxx/processor/tesla/liveness.hh>	// Hack. Should be the other way around


#include <map>
#include <vector>
#include <bitset>
#include <iomanip>
#include <stack>
#include <set>
namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

using std::map;
using std::vector;
using std::set;
using std::stack;

enum TargetType
{
	TargetFallthrough,
	TargetUncondJump,
	TargetCondJump,
	TargetSSY,
	TargetPBK,
	TargetBreak,	// No PC in this case
	TargetCall,
	TargetReturn,	// No PC
};

template<class CONFIG>
struct Target
{
	TargetType type;
	typename CONFIG::address_t pc;
	
	Target(TargetType type, typename CONFIG::address_t pc)
		: type(type), pc(pc) {}
	Target() {}
};

template<class CONFIG>
struct BasicBlock
{
	typename CONFIG::address_t first;
	typename CONFIG::address_t last;
	
	std::set<BasicBlock*> successors;
	std::set<BasicBlock*> predecessors;
	
	void Split(BasicBlock<CONFIG> & newbb, typename CONFIG::address_t split_label);
};

template<class CONFIG>
struct CallGraph;

template<class CONFIG>
struct ControlFlowGraph
{
	ControlFlowGraph(CPU<CONFIG> * cpu,
		CallGraph<CONFIG> * call_graph,
		typename CONFIG::address_t entry_point);
	
	void Build(typename CONFIG::address_t entry_point);
	
private:
	void AddBasicBlock(typename CONFIG::address_t pc);
	void BuildBasicBlock(typename CONFIG::address_t pc);
    CPU<CONFIG> * cpu;
    CallGraph<CONFIG> * call_graph;
    typedef std::map<typename CONFIG::address_t, BasicBlock<CONFIG> > bbmap_t;
	bbmap_t basic_blocks;
	BasicBlock<CONFIG> * head;
	BasicBlock<CONFIG> * tail;	// Tail BB has address -1 (0xfff...f)

	std::set<ControlFlowGraph*> callees;
	std::set<ControlFlowGraph*> callers;
};

template<class CONFIG>
struct CallGraph
{
	CallGraph(CPU<CONFIG> * cpu) : cpu(cpu) {}
	void Build();

private:
	CPU<CONFIG> * cpu;
	std::map<typename CONFIG::address_t, ControlFlowGraph<CONFIG> > functions;
	
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
