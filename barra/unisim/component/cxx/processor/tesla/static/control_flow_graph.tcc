/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_CONTROL_FLOW_GRAPH_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATIC_CONTROL_FLOW_GRAPH_TCC

#include <unisim/component/cxx/processor/tesla/static/control_flow_graph.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
void BasicBlock<CONFIG>::Split(BasicBlock<CONFIG> & newbb, typename CONFIG::address_t split_label)
{
}

template<class CONFIG>
ControlFlowGraph<CONFIG>::ControlFlowGraph(CPU<CONFIG> * cpu,
	CallGraph<CONFIG> * call_graph,
	typename CONFIG::address_t entry_point)
	: cpu(cpu), call_graph(call_graph)
{
	Build(entry_point);
}

template<class CONFIG>
void ControlFlowGraph<CONFIG>::Build(typename CONFIG::address_t entry_point)
{
	typedef typename CONFIG::address_t address_t;
	// Breadth-first graph traversal
	std::queue<address_t> worklist;
	std::set<address_t> visited;
	worklist.push(entry_point);
	
	while(!worklist.empty())
	{
		address_t pc = worklist.front();
		worklist.pop();
		
		// Not already traversed
		if(visited.find(pc) == visited.end())
		{
			AddBasicBlock(pc);
		}
	}
}

template<class CONFIG>
void ControlFlowGraph<CONFIG>::AddBasicBlock(typename CONFIG::address_t pc)
{
	if(previousbb->first == pc) {
		// Already added
		return;
	}
	
	// Build basic block
	
	
	
	bbmap_t::iterator previousbb = basic_blocks.lower_bound(pc);
	if(previousbb == basic_blocks.end()
		|| previousbb->last < pc) {
		// No overlap, create new
	}
	
	if(previousbb->last >= pc) {
		// Split
	}
}

template<class CONFIG>
void ControlFlowGraph<CONFIG>::BuildBasicBlock(typename CONFIG::address_t startpc)
{
	BasicBlock<CONFIG> newbb;
	newbb.first = startpc;
	// Add pcs until non-fallthrough edge found or ran into another existing bb
	typename CONFIG::address_t pc = startpc;
	std::vector<Target<CONFIG> > targets;
	while(true)
	{
		Instruction<CONFIG> instruction(cpu, pc + CONFIG::CODE_START);
		targets = instruction.Targets();
		if(targets.size() != 1
			|| targets[0].type != TargetFallthrough
			|| basic_blocks.find(targets[0].pc) != basic_blocks.end()) {
	break;
		}
		pc = targets[0].pc;
	}
	
	newbb.last = pc;
	
	// Add successors from targets of last instruction
	for(auto it = targets.begin(); it != targets.end(); ++it) {
		switch(it->type) {
		case TargetFallthrough:
		case TargetUncondJump:
		case TargetCondJump:
			AddBasicBlock(it->pc);
			newbb.successors.push_back(&basic_blocks[it->pc]);
			break;
		case TargetSSY:
		case TargetPBK:
		case TargetBreak:	// No PC in this case
		case TargetCall:
		case TargetReturn:	// No PC
		default:
			assert(false);
		}
	}
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
