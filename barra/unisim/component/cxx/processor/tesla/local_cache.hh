/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LOCAL_CACHE_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LOCAL_CACHE_HH

#include <unisim/component/cxx/cache/cache.hh>
#include <bitset>
#include <unisim/component/cxx/processor/tesla/register.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {



template<class CONFIG>
class LocalCacheStats;

template<class CONFIG>
class CPU;


using unisim::component::cxx::cache::CacheBlock;
using unisim::component::cxx::cache::CacheLine;
using unisim::component::cxx::cache::CacheSet;

template <class LOC>
class CacheAccess;

template <class LOC>
class CacheAccess
{
	public:
		typename LOC::ADDRESS addr;
		typename LOC::ADDRESS line_base_addr;
		typename LOC::ADDRESS block_base_addr;
		uint32_t index;
		uint32_t way;
		uint32_t sector;
		uint32_t offset;
		uint32_t size_to_block_boundary;
		CacheSet<LOC> *set;
		CacheLine<LOC> *line;
		CacheLine<LOC> *line_to_evict;
		CacheBlock<LOC> *block;
		bool line_hit;
};

template<class CONFIG,class LOC>
class LocalCache
{
	public:
		typedef	VectorRegister<CONFIG> VecReg;
		typedef typename LOC::ADDRESS address_t;
		typedef typename cxx::cache::Cache<LOC> Cache;
		typedef std::bitset<CONFIG::WARP_SIZE> mask_t;
	
		bool Load(VecReg const & output, address_t addr, mask_t mask, LocalCacheStats<CONFIG> &stats);
		void EvictFill(CacheAccess<LOC>& locacess, LocalCacheStats<CONFIG> &stats);

		bool Store(VecReg const & output, address_t addr, std::bitset<CONFIG::WARP_SIZE> mask, LocalCacheStats<CONFIG> &stats);
		bool Lookup(CacheAccess<LOC>& access, LocalCacheStats<CONFIG> &stats);
		void ChooseLineToEvict(CacheAccess<LOC>& access);
		void EmuEvict(CacheAccess<LOC>& access, LocalCacheStats<CONFIG> &stats);
		void EmuFill(CacheAccess<LOC>& access);
		void UpdateReplacementPolicy(CacheAccess<LOC>& access);
		void PartialInvalidate(CacheAccess<LOC>& access, mask_t mask);		
		void Invalidate();
		
		LocalCache(CPU<CONFIG>& my_cpu) : cpu(my_cpu) {  }

	private:
		Cache local_cache;
		CPU<CONFIG>& cpu;
};







} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
