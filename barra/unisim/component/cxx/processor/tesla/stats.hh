/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATS_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_STATS_HH

#include <string>
#include <algorithm>
#include <limits>
#include <unisim/component/cxx/processor/tesla/tesla_operation.hh>
#include <unisim/component/cxx/processor/tesla/register.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class T, class ACCUM>
struct StatCounter
{
	StatCounter();
	void Append(T v);
	ACCUM Avg() const;
	ACCUM Variance() const;
	T Min() const;
	T Max() const;
	T N() const;
	void Merge(StatCounter<T, ACCUM> const & other);

private:
	unsigned int n;
	ACCUM mean;
	ACCUM m2;
	T min;
	T max;
};

enum SimilarityType {
	SDMT,
	DDMT,
	OTHER 
};

template<class CONFIG>
struct SpaceSimilarityCounter
{
private :
	struct Family {
		VectorRegister<CONFIG> reg;
		std::bitset<CONFIG::WARP_SIZE> mask;

		Family() : reg(0), mask() {}
	};
public :
	SpaceSimilarityCounter(uint32_t mask_bits = 0) :
		bin1(0), bin1_uniform(0), bin1_strided(0),
		bin2(0), bin2_uniform(0), bin2_strided(0), bin2_totally_strided(0),
		total(0), bin2_clusters_number(0), bin2_used_registers(0), mask_bits(mask_bits) {}
	SimilarityType Append(VectorRegister<CONFIG> const & reg, std::bitset<CONFIG::WARP_SIZE> mask);
	void Merge(SpaceSimilarityCounter<CONFIG> const & other);
	
	uint64_t bin1;
	uint64_t bin1_uniform;
	uint64_t bin1_strided;
	uint64_t bin2;
	uint64_t bin2_uniform;
	uint64_t bin2_strided;
	uint64_t bin2_totally_strided;
	uint64_t total;
	uint64_t bin2_clusters_number;
	uint64_t bin2_used_registers;

	uint32_t mask_bits;
};

template<class CONFIG>
struct TimeSimilarityCounter
{
private :
public :
	TimeSimilarityCounter(uint32_t mask_bits = 0) : Seen(), LastReg(), LastMask(), AlreadyUsed(), MaskBits(mask_bits), counter(0), total(0), vecCounter(0), vecTotal(0) {}
	bool Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg);
	void Merge(TimeSimilarityCounter<CONFIG> const & other);

	std::map<int, VectorRegister<CONFIG> > LastReg;
	std::map<int, std::bitset<CONFIG::WARP_SIZE> > LastMask;
	std::map<int, bool> AlreadyUsed;
	std::map<int, bool> Seen;

	uint32_t MaskBits;
	uint64_t counter;
	uint64_t total;
	uint64_t vecCounter;
	uint64_t vecTotal;
	void SetSeen(int reg);
	void ResetSeen();
	void Update(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg);
};

template<class CONFIG>
struct SimilarityCounter
{
public :
	uint32_t stride;
	uint32_t width;
	// TODO : Change hardcoded length by a parameter
	SpaceSimilarityCounter<CONFIG> space[5];
	SpaceSimilarityCounter<CONFIG> spaceInTime[5];
	TimeSimilarityCounter<CONFIG> time[5];
	TimeSimilarityCounter<CONFIG> timeInSDMT[5];
	TimeSimilarityCounter<CONFIG> timeInDDMT[5];

	SimilarityCounter();
	void Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, int reg);
	void Merge(SimilarityCounter<CONFIG> const & other);
	void ResetSeen();
};

template<class CONFIG>
struct DualRegisterStats
{
public :
	uint32_t Hits;
	uint32_t Flatten;
	uint32_t ReadVectorAccess;
	uint32_t WriteVectorAccess;
	uint32_t Total;

	DualRegisterStats() : Hits(0), Flatten(0), ReadVectorAccess(0), WriteVectorAccess(0), Total(0) {}
	void Append(VectorRegister<CONFIG> const & regs, std::bitset<CONFIG::WARP_SIZE> mask, bool b16 = 0);
	void Merge(DualRegisterStats<CONFIG> const & other);
};

template<class CONFIG>
struct ClassStats
{
public:
	ClassStats<CONFIG> () { std::fill(count, count + InstructionClassMax, 0); }
	void Merge(ClassStats<CONFIG> const & other)
	{ 
		for (int i = 0; i != InstructionClassMax; ++i)
			count[i] += other.count[i];
	}
	uint64_t count[InstructionClassMax];
};

template<class CONFIG>
class Instruction;

template<class CONFIG>
class CPU;

template<class CONFIG>
struct WordSectoredCacheStats
{
private:
	unsigned int Hits;
	unsigned int Misses;
	unsigned int PartialMisses;

	unsigned int Fills;
	unsigned int WBacks;

    unsigned int Pressures;

public:

    WordSectoredCacheStats():
		Hits(0), Misses(0), PartialMisses(0), Fills(0),
		WBacks(0), Pressures(0)
    {}

    void Merge(const WordSectoredCacheStats <CONFIG> & other);

	void Hit() { ++Hits; }
	void Miss() { ++Misses; }
	void PartialMiss() { ++PartialMisses; }
    void Pressure(int p) { Pressures+=p;}

	void Fill() { ++Fills; }
	void WBack() { ++WBacks; }
	void DumpCSV(std::ostream & os) const;
};

template<class CONFIG>
struct AffineCacheStats
{
private:
	unsigned int Hits;
	unsigned int Misses_noroom;
	unsigned int Misses_notaff;

	unsigned int Fills;
	unsigned int WBacks;

public:

    AffineCacheStats():
		Hits(0), Misses_noroom(0), Misses_notaff(0),
		Fills(0), WBacks(0)
    {};

    void Merge(const AffineCacheStats <CONFIG> & other);

	void Hit() { ++Hits; }
	void MissNoRoom() { ++Misses_noroom; }
	void MissNotAff() { ++Misses_notaff; }

	void Fill() { ++Fills; }
	void WBack() { ++WBacks; }
	void DumpCSV(std::ostream & os) const;
};


template<class CONFIG>
struct LocalCacheStats
{
private:
	unsigned int Hits;
	unsigned int Misses;

	unsigned int Fills;
	unsigned int WBacks;

public:

    LocalCacheStats():
        Hits(0), Misses(0),
		Fills(0), WBacks(0)
    {};

    void Merge(const LocalCacheStats <CONFIG> & other);
	void Hit() { ++Hits; }
	void Miss() { ++Misses; }

	void Fill() { ++Fills; }
	void WBack() { ++WBacks; }

	void DumpCSV(std::ostream & os) const;
};	

template<class CONFIG>
struct MetaCacheStats
{
private:
    AffineCacheStats<CONFIG> affine;
    LocalCacheStats<CONFIG> local;

public:
    void Merge(const MetaCacheStats <CONFIG> & other) {local.Merge(other.local); affine.Merge(other.affine);}
    AffineCacheStats<CONFIG>& Affine(){return affine;}
    LocalCacheStats<CONFIG>& Local(){return local;}
	void DumpCSV(std::ostream & os) const {local.DumpCSV(os); affine.DumpCSV(os);}
};

// Only used for function overloading
struct CacheTypeL0 {};
struct CacheTypeL1G {};
struct CacheTypeL1A {};
struct CacheTypeL1 {};

template<class CONFIG>
struct OperationStats
{
private:
	std::string name;
	unsigned int count;
	unsigned int scalarcount;
	bool integer;
	bool mov;
	bool fp;
	bool flow;
	bool memory;
	bool shared;
	bool constant;
	int16_t regnum[4];
	unsigned int scalarRegInputs;
	unsigned int stridedRegInputs;
	unsigned int scalarRegOutputs;
	unsigned int stridedRegOutputs;

	unsigned int scalarRegInputsCaught;
	unsigned int stridedRegInputsCaught;
	unsigned int scalarRegOutputsCaught;
	unsigned int stridedRegOutputsCaught;

	unsigned int llStridedRegs;     // strided registers from local loads
	unsigned int slStridedRegs;		// strided registers from local stores

	unsigned int branchTaken, branchNotTaken, branchDivergent;
	unsigned int jump;
	
	unsigned int gatherGlobal, loadGlobal, scatterGlobal, storeGlobal;
	unsigned int gatherLocal, loadLocal, scatterLocal, storeLocal;
	
	unsigned int load32, load64, load128;
	unsigned int store32, store64, store128;
	
	unsigned int scalarAddress, stridedAddress;
	
	uint64_t time_spent;
	uint64_t timestamp;
	
	unsigned int steps;	// NIMT rounds
	
	StatCounter<float, double> fpValue;

    unsigned int aliveIn;
    unsigned int aliveOut;
    unsigned int aliveLoad;
    unsigned int nonstridedAliveOutTotal;
    unsigned int nonstridedAliveOutMin;
    unsigned int nonstridedAliveOutMax;

    WordSectoredCacheStats<CONFIG> L0Cache;
    MetaCacheStats<CONFIG> L1Cache;
    
    unsigned int localUniform;
    unsigned int localZero;
    unsigned int localNonPow2;
    unsigned int localSparseVector;
    
    SimilarityCounter<CONFIG> inputSimilar;
	SimilarityCounter<CONFIG> outputSimilar;

	DualRegisterStats<CONFIG> inputDualReg;
	DualRegisterStats<CONFIG> outputDualReg;

	unsigned int genericFlatten;

	ClassStats<CONFIG> classStats;
public:
	typedef	VectorRegister<CONFIG> VecReg;
	OperationStats() :
		count(0), scalarcount(0), integer(false), mov(false),
		fp(false), flow(false), memory(false),
		shared(false), constant(false),
		scalarRegInputs(0), stridedRegInputs(0),
		scalarRegOutputs(0), stridedRegOutputs(0),
		scalarRegInputsCaught(0), stridedRegInputsCaught(0),
		scalarRegOutputsCaught(0), stridedRegOutputsCaught(0),
		llStridedRegs(0), slStridedRegs(0),
		branchTaken(0), branchNotTaken(0), branchDivergent(0),
		jump(0),
		gatherGlobal(0), loadGlobal(0), scatterGlobal(0), storeGlobal(0),
		gatherLocal(0), loadLocal(0), scatterLocal(0), storeLocal(0),
		load32(0), load64(0), load128(0), store32(0), store64(0), store128(0),
		scalarAddress(0), stridedAddress(0),
		time_spent(0), steps(0), aliveIn(0), aliveOut(0),nonstridedAliveOutTotal(0),nonstridedAliveOutMin(CONFIG::MAX_GPRS+1),nonstridedAliveOutMax(0),
		localUniform(0), localZero(0), localNonPow2(0), localSparseVector(0),
		inputSimilar(), outputSimilar(),
		inputDualReg(), outputDualReg(),
		genericFlatten(0),
		classStats()
	{
		std::fill(regnum, regnum + 4, -1);
	}
	
	void Merge(OperationStats<CONFIG> const & other);

	void SetName(char const * insnname) {
		name = std::string(insnname);
	}
	
	void SetInteger() { integer = true;	}
	void SetFP() { fp = true; }
	void SetFP64() { fp = true; }

	void SetTransc() { fp = true; }
	void SetIMul() { integer = true; }
	void SetFMul() { fp = true; }

	void SetFlow() { flow = true; }
	void SetMov() { mov = true;	}
	void SetGather() { memory = true;	}
	void SetScatter() { memory = true;	}
	void SetInputConst() { constant = true; }
	void SetInputShared() { shared = true; }
	void SetOutputShared() { shared = true; }
 
	void AddClass(InstructionClass c) { ++classStats.count[(int) c]; }

	void SetRegNum(int16_t reg, DataType dt, Operand n);

	bool IsInteger() const { return integer; }

	void Flatten() { ++genericFlatten; }
	
	void RegRead(VectorRegister<CONFIG> const * regs, DataType dt, std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, int noReg);
	void RegWrite(VectorRegister<CONFIG> const * regs, DataType dt, std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, int noReg);
	void RegKill(std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, typename CONFIG::address_t addr);

	void Execute(std::bitset<CONFIG::WARP_SIZE> mask, CPU<CONFIG> *cpu, typename CONFIG::address_t addr);
	void Unexecute();

	void Begin();
	void End();
	
	void Step();
	
	// Branch instruction
	void BranchUniTaken() { ++branchTaken; }
	void BranchUniNotTaken() { ++branchNotTaken; }
	void BranchDivergent() { ++branchDivergent; }

	// Effective jump, by branch, call, return, join...
	void Jump() { ++jump; }
	//void Gather(VectorAddress<CONFIG> const & addr, std::bitset<CONFIG::WARP_SIZE> mask);

	void GatherGlobal(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask);
	void LoadGlobal(unsigned int logsize) { loadGlobal += (1 << logsize); }
	void ScatterGlobal(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask);
	void StoreGlobal(unsigned int logsize) { storeGlobal += (1 << logsize); }
	void GatherLocal(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask, VecReg const output[]);
	void LoadLocal(unsigned int logsize, VecReg const output[], std::bitset<CONFIG::WARP_SIZE> mask) ;
	void ScatterLocal(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask, VecReg const output[]);
	void StoreLocal(unsigned int logsize, VecReg const output[], std::bitset<CONFIG::WARP_SIZE> mask) ;
	void Transaction(VectorAddress<CONFIG> const & addr, unsigned int logsize, std::bitset<CONFIG::WARP_SIZE> mask);
	void LoadStoreLocal(unsigned int logsize, VecReg const output[], std::bitset<CONFIG::WARP_SIZE> mask);

	void DumpCSV(std::ostream & os) const;

    void SetAliveIn(unsigned in);
    void SetAliveOut(unsigned out);
    void SetAliveLoad(unsigned load);

    WordSectoredCacheStats<CONFIG> &L0(){return L0Cache;}
    MetaCacheStats<CONFIG> &L1(){return L1Cache;}
    
    WordSectoredCacheStats<CONFIG> & CacheStats(CacheTypeL0 t) { return L0Cache; }
    LocalCacheStats<CONFIG> & CacheStats(CacheTypeL1G t) { return L1Cache.Local(); }
    AffineCacheStats<CONFIG> & CacheStats(CacheTypeL1A t) { return L1Cache.Affine(); }
    MetaCacheStats<CONFIG> & CacheStats(CacheTypeL1 t) { return L1Cache; }
};


template<class CONFIG>
struct KernelStats
{
	KernelStats() { Reset(); }

	void Dump(std::ostream & os) const;
	void Reset();
	void Merge(KernelStats<CONFIG> const & other);
	void Init();
	
	void RegDefUse(Instruction<CONFIG> const & insn, CPU<CONFIG> & cpu);
	
private:
	StatCounter<uint32_t, double> distance_generic;
	StatCounter<uint32_t, double> distance_affine;
	uint64_t last_use[CONFIG::MAX_GPRS];
};

template<class CONFIG>
struct Stats
{
private:
	typedef std::map<typename CONFIG::address_t, OperationStats<CONFIG> > stats_map;
	stats_map stats;
	KernelStats<CONFIG> kernel_stats;
	
public:
	OperationStats<CONFIG> & operator[] (typename CONFIG::address_t addr) {
		return stats[addr];
	}
	
	void DumpCSV(std::ostream & os) const;
	void DumpGlobal(std::ostream & os, char const * kernelName) const;
	void Reset();	// Never called?
	void Merge(Stats<CONFIG> const & other);
	void Init() { kernel_stats.Init(); }

	void RegDefUse(Instruction<CONFIG> const & insn, CPU<CONFIG> & cpu) { kernel_stats.RegDefUse(insn, cpu); }
};

// Hack: inlined to avoid #include hell (Stats ctor called from main)
template<class CONFIG>
void KernelStats<CONFIG>::Init()
{
	std::fill(last_use, last_use + CONFIG::MAX_GPRS, 0);
}


template<class CONFIG>
void KernelStats<CONFIG>::Reset()
{
	distance_generic = StatCounter<uint32_t, double>();
	distance_affine = StatCounter<uint32_t, double>();
	Init();
}

template<class T, class ACCUM>
StatCounter<T, ACCUM>::StatCounter() :
	n(0), mean(0), m2(0),
	min(std::numeric_limits<T>::has_infinity ? +std::numeric_limits<T>::infinity() : std::numeric_limits<T>::max()),
	max(std::numeric_limits<T>::has_infinity ? -std::numeric_limits<T>::infinity() : std::numeric_limits<T>::min())
{
}




} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
