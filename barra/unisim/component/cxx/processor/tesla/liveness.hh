/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: 
 *     Elie Gédéon (elie.gedeon@ens-lyon.fr)
 *
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LIVENESS_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_LIVENESS_HH

#include <unisim/component/cxx/processor/tesla/forward.hh>
//#include <unisim/component/cxx/processor/tesla/instruction.hh>

#include <map>
#include <vector>
#include <bitset>
#include <iomanip>
#include <stack>
#include <set>
namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {
//using namespace std;

using std::set;
using std::map;
using std::bitset;
using std::stack;
using std::vector;
using std::pair;

template<class CONFIG>
struct Context
{
    stack <typename CONFIG::address_t> returns;
    stack <typename CONFIG::address_t> prebreak;
    typename CONFIG::address_t code_base;

    typename CONFIG::address_t CallSite()
    {
    	if (returns.empty())
    		return CONFIG::CODE_END - CONFIG::CODE_START;
        return returns.top();
    }

    Context() : code_base(CONFIG::CODE_START)
    {
    }
    
    Context(typename CONFIG::address_t base) : code_base(base) {}
};

template<class CONFIG>
struct Next
{
    Context<CONFIG> context;
    typename CONFIG::address_t addr;
    Next(Context <CONFIG> &ctx, typename CONFIG::address_t a) : context(ctx), addr(a) {};
};

template <class CONFIG>
class Liveness{
	// This is the abstract state for an instruction
    struct InOut
    {
        InOut (Instruction<CONFIG> &ins, vector<Next<CONFIG> > &Context);
        InOut ();
        void operator|=(InOut &io);
        bitset<CONFIG::MAX_GPRS> in;	// Uses
        bitset<CONFIG::MAX_GPRS> out;	// Defs
        bitset<CONFIG::MAX_GPRS> load;	// Defs from load instructions.
                                        // Does not work for 64-bit and 128-bit loads

		// For some reason, it also contain the CFG
        set<typename CONFIG::address_t> next;
        set<typename CONFIG::address_t> previous;
        int nb_registres;
    };
public:
    struct Load
    {
        public:
            int lru[CONFIG::MAX_GPRS];
            bitset<CONFIG::MAX_GPRS> pending;

        public:
            Load();
            int &operator[](unsigned);
            void operator|=(Load&);
            bool operator!=(Load&);
    };

public:
    Liveness(CPU<CONFIG> *cpu);
    void compute();
    void export_stats();
private:
	void BuildCFG();

    CPU<CONFIG> *cpu;
    map<typename CONFIG::address_t, InOut> inOuts;        
    bool ready;

public:
	typedef map<typename CONFIG::address_t, bitset<CONFIG::MAX_GPRS> > RegSetMap;
    int nb_registres;
    RegSetMap ins;
    RegSetMap outs;
    map<typename CONFIG::address_t, Load > loads;
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
