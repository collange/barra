/*
 *  Copyright (c) 2014,
 *  Inria,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_INSTRUCTION_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_INSTRUCTION_TCC

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

//////////////////////////////////////////////////
// Tesla Instruction

template <class CONFIG>
TeslaInstruction<CONFIG>::TeslaInstruction(CPU<CONFIG> * cpu, typename CONFIG::address_t addr) :
	Base(cpu, addr)
{
	Fetch(addr);
}

template <class CONFIG>
TeslaInstruction<CONFIG>::TeslaInstruction() :
	Base()
{
	// I hate containers
}

template <class CONFIG>
TeslaInstruction<CONFIG>::~TeslaInstruction()
{
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::Fetch(typename CONFIG::address_t fetchaddr)
{
	this->addr = fetchaddr;
	this->cpu->Fetch(this->iw, this->addr);
//	if(!ReadMemory(addr, &iw, sizeof(typename CONFIG::insn_t))) {
//		throw MemoryAccessException<CONFIG>();
//	}
	this->operation = this->op_decoder.Decode(this->addr, this->iw);
	if(this->operation->OutputsPred()) {
		flags.Reset();
	}
	if(this->cpu->stats != 0) {
		this->stats = &this->cpu->GetIStats(this->addr);
	}	
}


template <class CONFIG>
void TeslaInstruction<CONFIG>::Read()
{
	// mask = current warp mask & predicate mask
	this->ScalarInsn() = 1; // Initialize insn scalar

	this->mask = this->cpu->GetCurrentMask();
	this->mask &= this->operation->dest->predMask(this->cpu);

	if(this->operation->control->is_join)
	{
		if(this->cpu->Join()) {
			// Mask may be altered by Join
			this->mask = this->cpu->GetCurrentMask();
		}
		else {
			// Do not execute if Join returns "not synched"
			this->mask = 0;
		}
	}
	
	//if(mask != 0)
	{
		this->operation->read(this->cpu, this);
	}
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::Execute()
{
	//if(mask != 0) {
	// Execute even when mask = 0 ! Esp. branches, sync...
	this->operation->execute(this->cpu, this);
	std::bitset<CONFIG::WARP_SIZE> execmask = this->Mask();

	// bar.sync, join have zero-predicate
	// Change mask? What should the mask be?
	
	this->stats->Execute(execmask, this->cpu, this->addr);
	if(this->cpu->TraceMask()) {
		cerr << "XMask: " << this->Mask() << endl;
	}
	//}
	if(this->operation->control->is_end) {
		this->cpu->End();
	}
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::Write()
{
	this->operation->write(this->cpu, this);
	if(this->operation->OutputsPred()) {
		this->operation->writePred(this->cpu, this);
	}
	this->stats->AddClass(this->Class());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::Disasm(std::ostream & os) const
{
	this->operation->dest->disasmPred(this->cpu, this, os);

	this->operation->disasm(this->cpu, this, os);
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::Abort(std::bitset<CONFIG::WARP_SIZE> abort_mask)
{
	// Cancel writeback for selected lanes
	this->mask &= ~abort_mask;
}



template <class CONFIG>
void TeslaInstruction<CONFIG>::InitStats()
{
	ostringstream sstr;
	Disasm(sstr);
	this->stats->SetName(sstr.str().c_str());
	this->operation->initStats(this->stats);
}

template <class CONFIG>
VectorFlags<CONFIG> & TeslaInstruction<CONFIG>::Flags()
{
	return flags;
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::SetPredI32()
{
	VectorFlags<CONFIG> myflags = ComputePredI32(this->temp[this->TempDest], flags);
	this->operation->dest->writePred(this->cpu, myflags, this->Mask());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::SetPredI16()
{
	VectorFlags<CONFIG> myflags = ComputePredI16(this->temp[this->TempDest], flags);
	this->operation->dest->writePred(this->cpu, myflags, this->Mask());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::SetPredF32()
{
	VectorFlags<CONFIG> myflags = CONFIG::vfp32::ComputePredFP32(this->temp[this->TempDest], this->Mask());
	this->operation->dest->writePred(this->cpu, myflags, this->Mask());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::WriteFlags()
{
	this->operation->dest->writePred(this->cpu, flags, this->Mask());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::SetPred(VectorFlags<CONFIG> flags) const
{
	this->operation->dest->writePred(this->cpu, flags, this->Mask());
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::DisasmSrc1(std::ostream & os) const
{
	this->operation->src1->disasm(this->cpu, this, os);
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::DisasmSrc2(std::ostream & os) const
{
	this->operation->src2->disasm(this->cpu, this, os);
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::DisasmSrc3(std::ostream & os) const
{
	this->operation->src3->disasm(this->cpu, this, os);
}


template <class CONFIG>
void TeslaInstruction<CONFIG>::DisasmDest(std::ostream & os) const
{
	this->operation->dest->disasm(this->cpu, this, os);
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::DisasmControl(std::ostream & os) const
{
	this->operation->control->disasm(os);
}

template <class CONFIG>
bool TeslaInstruction<CONFIG>::IsLong() const
{
	return this->operation->control->is_long;
}

template <class CONFIG>
bool TeslaInstruction<CONFIG>::IsEnd() const
{
	return this->operation->control->is_end;
}

template <class CONFIG>
RegType TeslaInstruction<CONFIG>::OperandRegType(Operand op) const
{
	DataType dt = OperandDataType(op);
	return DataTypeToRegType(dt);
}

template <class CONFIG>
SMType TeslaInstruction<CONFIG>::OperandSMType(Operand op) const
{
	DataType dt = OperandDataType(op);
	return DataTypeToSMType(dt);
}

template <class CONFIG>
size_t TeslaInstruction<CONFIG>::OperandSize(Operand op) const
{
	DataType dt = OperandDataType(op);
	return DataTypeSize(dt);
}

template <class CONFIG>
DataType TeslaInstruction<CONFIG>::OperandDataType(Operand op) const
{
	// NOT actual type, can be overriden by operand
	// TODO: fix with override
	return this->operation->op_type[op];
}

template <class CONFIG>
void TeslaInstruction<CONFIG>::InitDependencies()
{
	this->operation->EnumerateOperands();
}

template <class CONFIG>
unsigned int TeslaInstruction<CONFIG>::InputCount() const
{
	return this->operation->inputCount;
}

template <class CONFIG>
OperandID const & TeslaInstruction<CONFIG>::Input(unsigned int i) const
{
	assert(i < this->operation->inputCount);
	return this->operation->inputs[i];
}

template <class CONFIG>
unsigned int TeslaInstruction<CONFIG>::LoadCount() const
{
	return this->operation->loadCount;
}

template <class CONFIG>
unsigned TeslaInstruction<CONFIG>::Load(unsigned int i) const
{
	assert(i < this->operation->loadCount);
	return this->operation->loads[i];
}

template <class CONFIG>
unsigned int TeslaInstruction<CONFIG>::OutputCount() const
{
	return this->operation->outputCount;
}

template <class CONFIG>
OperandID const & TeslaInstruction<CONFIG>::Output(unsigned int i) const
{
	assert(i < this->operation->outputCount);
	return this->operation->outputs[i];
}

template <class CONFIG>
std::vector<Next<CONFIG> > TeslaInstruction<CONFIG>::findNexts (Context<CONFIG> &context)
{
    return this->operation->findNexts(context);
}

template <class CONFIG>
std::vector<Target<CONFIG> > TeslaInstruction<CONFIG>::Targets()
{
	return this->operation->targets(this->cpu->CodeBase());
}


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
