/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

// Deprecated, removed from build. See perfmodel/affine_vector_cache.tcc

//#include <unisim/component/cxx/processor/tesla/register.tcc>
#include <unisim/component/cxx/processor/tesla/meta_cache.hh>
#include <iostream>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


using namespace std;


template <class CONFIG, class L>
bool MetaCache<CONFIG,L>::Load(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats)
{
	if(mask.any()) {
		if(CONFIG::AFFINE_CACHE_ENABLED)
			SplitLoad(output, addr, mask, stats);
		else
			local_cache.Load(output, addr, stats);
	}
}


template <class CONFIG, class L>
bool MetaCache<CONFIG,L>::Store(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats)
{
	if(mask.any()) {
		if(CONFIG::AFFINE_CACHE_ENABLED)
			SplitStore(output, addr, mask, stats);
		else
			local_cache.Store(output, addr, mask, stats);
	}
}


template <class CONFIG, class L>
bool MetaCache<CONFIG,L>::SplitLoad(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats)
{
	bool hit;
	if(cpu.TraceCache())
	{
		cerr << dec;
		cerr << "Load for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << " ; Mask: " << mask << endl;
	}
	CacheAccess<typename L::AFFINE> affaccess;
	affaccess.addr = addr;
	mask_t afflookup = affine_cache.LookupMasked(affaccess, stats.Affine()) ;
	mask_t access_mask = mask & afflookup ;
	if( access_mask.any() ) // hit in A$
	{
		hit = true;
		stats.Affine().Hit();
		if(cpu.TraceCache())
		{
			cerr << dec << "    Affine hit on way " << affaccess.way << " on set " << affaccess.index << 
						" with vmask: " << afflookup << " Affine? " << output.CheckStridedMasked(mask) << endl;
		}
		affine_cache.UpdateReplacementPolicy(affaccess);
		access_mask = mask & ~afflookup;
		if ( access_mask.any() )	// not every element was present in A$
		{
			hit = Load(output, addr, access_mask, stats);	// replay
			// TODO: actual replay, timing, crack MetaCache class
		}
	}
	else	// miss in A$
	{
		bool is_affine = output.CheckStridedMasked(mask);
		if(cpu.TraceCache())
		{
			cerr << "    Affine miss with vmask: " << afflookup << " Affine? " << is_affine << endl;
		}
		if(is_affine)
		{
			stats.Affine().MissNoRoom();
			affine_cache.UpdateReplacementPolicy(affaccess);
		}
		else
		{
			stats.Affine().MissNotAff();
		}
		CacheAccess<typename L::LOCAL> locaccess;
		locaccess.addr = addr;
		if(local_cache.Lookup(locaccess, stats.Local()))	// hit in G$
		{
			hit = true;
			if(cpu.TraceCache())
			{
				cerr << "    Generic hit on way " << locaccess.way << " hit " << endl;
			}
			// great
		}
		else	// miss in G$
		{
			hit = false;
			if(cpu.TraceCache())
			{
				cerr << "    Generic miss " << endl;
			}

			if(is_affine)	// affine vector
			{
				if (!affaccess.line_hit) // line miss in A$
				{
					affine_cache.ChooseLineToEvict(affaccess);
					affine_cache.EmuEvict(affaccess, stats.Affine());
				}
				affine_cache.EmuFill(affaccess, mask, stats.Affine());
				affine_cache.UpdateReplacementPolicy(affaccess);
				if(cpu.TraceCache())
				{
					cerr << "    Affine fill on way " << affaccess.way << " on set " << affaccess.index << endl;
				}
			}
			else	// generic vector
			{
				local_cache.EvictFill(locaccess, stats.Local()); // eviction from and fill to G$
			}
		}
	}
	return hit;
}



template <class CONFIG, class L>
bool MetaCache<CONFIG,L>::SplitStore(VecReg const & output, address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats)
{
	bool hit;
	CacheAccess<typename L::AFFINE> affaccess;
	affaccess.addr = addr;
	mask_t afflookup = affine_cache.LookupMasked(affaccess, stats.Affine());
	if(cpu.TraceCache())
	{
		cerr << "Store for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << " ; Mask: " << mask << " Affine? " << output.CheckStridedMasked(mask) << endl;
	}
	if(output.CheckStridedMasked(mask)) // affine vector
	{
		mask_t access_mask = mask & afflookup ;
		if( affaccess.line_hit ) // line hit in A$
		{
			hit = true;
			stats.Affine().Hit();
			if(cpu.TraceCache())
			{
				cerr << dec << "    Affine hit on way " << affaccess.way << " on set " << affaccess.index << " with vmask: " << afflookup << endl;
			}
			if( All(mask | ~affaccess.block->status.vmask) )	// every element is masked or invalid
			{
				affaccess.block->status.vmask = mask;		// replacing the element
			}
			else
			{
				// check data
				VecReg temp;
				cpu.ReadMemory(addr, &temp.v[0], CONFIG::WARP_SIZE * 4);
				if( output.CheckStridedMMD(mask) == temp.CheckStridedMMD(mask) ) // same data
				{
					affaccess.block->status.vmask = (affaccess.block->status.vmask | mask);
				}
				else // different data
				{
					hit = false;
					stats.Affine().WBack();
					if(cpu.TraceCache())
					{
						cerr << dec << "    Affine WBack from way " << affaccess.way << " from set " << affaccess.index << endl;
					}
					affaccess.block->status.vmask = mask;
				}
			}
		}
		else		// line miss in A$
		{
			hit = false;
			stats.Affine().MissNoRoom();
			affine_cache.ChooseLineToEvict(affaccess);
			affine_cache.EmuEvict(affaccess, stats.Affine());
			affine_cache.EmuWrite(affaccess, mask, stats.Affine());
			if(cpu.TraceCache())
			{
				cerr << dec << "    Affine miss on way " << affaccess.way << " on set " << affaccess.index << " with vmask: " << afflookup << endl;
			}
		}
		affaccess.block->status.dirty = true ;
		affine_cache.UpdateReplacementPolicy(affaccess);
	}
	else	// not affine
	{
		mask_t access_mask = mask & afflookup ;
		if( access_mask.any() ) // hit in A$
		{
			stats.Affine().Hit();
			affine_cache.PartialInvalidate(affaccess, stats.Affine(), mask);
		}
		if(cpu.TraceCache())
		{
			cerr << "    Forward to generic store" << endl;
		}
		hit = local_cache.Store(output, addr, mask, stats.Local());	// just forward the store to G$
	}
	return hit;
}

template <class CONFIG, class L>
void MetaCache<CONFIG,L>::PartialInvalidate(address_t addr, mask_t mask, MetaCacheStats<CONFIG> &stats)
{
	if(cpu.TraceCache())
	{
		cerr << dec;
		cerr << "Invalidate for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << " ; Mask: " << mask << endl;
	}
	CacheAccess<typename L::AFFINE> affaccess;
	affaccess.addr = addr;
	mask_t afflookup = affine_cache.LookupMasked(affaccess, stats.Affine()) ;
	mask_t access_mask = mask & afflookup ;
	if( access_mask.any() ) // hit in A$
	{
		affine_cache.PartialInvalidate(affaccess, mask);
        if (!affaccess.block->status.vmask.any())
        {
            affaccess.line_to_evict = affaccess.line;
	        affine_cache.EmuEvict(affaccess, stats.Affine());
	        affine_cache.UpdateReplacementPolicy(affaccess);
        }
	}
	else	// miss in A$
	{
		CacheAccess<typename L::LOCAL> locaccess;
		locaccess.addr = addr;
		if(local_cache.Lookup(locaccess, stats.Local()))
		{
/*			local_cache.PartialInvalidate(locaccess, mask);
            if (!locaccess.block->status.vmask.any())
        	    local_cache.EmuEvict(locaccess, stats.Local());*/
		}
		else
		{
			cerr << "inconsistency in meta_cache.tcc?" << endl;
			assert(false);
		}
	}
}


template <class CONFIG, class L>
void MetaCache<CONFIG,L>::Invalidate()
{ 
	local_cache.Invalidate() ; 
	if(L::AFFINE::ENABLED)
		affine_cache.Invalidate();
}














} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim
