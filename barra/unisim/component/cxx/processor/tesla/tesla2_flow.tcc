/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA2_FLOW_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA2_FLOW_TCC

#include <unisim/component/cxx/processor/tesla/tesla_flow.hh>
#include <unisim/component/cxx/processor/tesla/cpu.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
Tesla2Flow<CONFIG>::Tesla2Flow(Warp<CONFIG> & warp) :
	warp(warp)
{
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
	typename CONFIG::address_t code_base)
{
	this->cpu = cpu;
	replay_mask = active_mask = initial_mask = mask;
	previous_mask = mask;
	this->code_base = code_base;
	disable_break_mask.reset();
	disable_call_mask.reset();
	stack = stack_t();
	sync_token_popped_pre = sync_token_popped_post = false;
	SetPC(code_base);
	SetNPC(code_base);
	//stack.push(StackToken(mask, ID_BOTTOM, 0));
}

template<class CONFIG>
bool Tesla2Flow<CONFIG>::UnwindStack()
{
	do {
		if(stack.empty()) {
			// Kill warp
			assert(!disable_break_mask.any());
			warp.state = Warp<CONFIG>::Finished;
			if(cpu->TraceBranch()) {
				cerr << " Empty stack while unwinding. Killing warp." << endl;
			}
			return false;
		}
		StackToken token = stack.top();
		stack.pop();
		if(cpu->TraceBranch()) {
			cerr << " Pop. ";
		}
		if(token.id == ID_CALL) {
			if(cpu->TraceBranch()) {
				cerr << "CALL token. ";
			}
			disable_call_mask &= ~token.mask;
		}
		else if(token.id == ID_BREAK) {
			if(cpu->TraceBranch()) {
				cerr << "BREAK token. ";
			}
			disable_break_mask &= ~token.mask;
		}
		else if(token.id == ID_SYNC) {
			sync_token_popped_pre = true;
			if(cpu->TraceBranch()) {
					cerr << "SYNC token. ";
			}
		}
		else if(token.id == ID_DIV && cpu->TraceBranch()) {
				cerr << "DIV token. ";
		}

		active_mask = token.mask & ~disable_call_mask & ~disable_break_mask;

		if(cpu->TraceBranch()) {
			cerr << "PC=" << hex << token.address - code_base << dec
				<< " mask=" << token.mask << " newmask=" << active_mask << endl;
		}
		
		SetNPC(token.address);
	} while(!active_mask.any());
	
	return false; // do not execute instruction
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Branch(address_t target_addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert((target_addr & 3) == 0);	// 64-bit-aligned targets
	typename CONFIG::address_t adjpc = GetRPC();

	// Check mask
	// Mask == none -> nop
	// Mask == all  -> jump
	// Mask == other -> argh
	
	typename CONFIG::address_t abs_target = target_addr + code_base;
	if(cpu->TraceBranch()) {
		cerr << "  Branch mask = " << mask << endl;
	}
	
	assert((mask & ~(active_mask)).none());	// No zombies allowed

	if((mask).none())
	{
		// Uniformly not taken
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, not taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniNotTaken();
		// Nop, just increment PC as usual
	}
	else if(mask == GetCurrentMask())	// Same number of enabled threads as before
	{
		// Uniformly taken
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniTaken();
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(abs_target);
		// current_mask = mask already
	}
	else
	{
		(*cpu->stats)[GetRPC()].BranchDivergent();
		(*cpu->stats)[GetRPC()].Jump();
		// Start by *taking* the branch ??
	
		// Mixed
		// Push divergence token
		stack.push(StackToken(active_mask & ~mask, ID_DIV, GetNPC()));
		if(cpu->TraceBranch()) {
			cerr << hex;
			cerr << " Conditional non-coherent jump from "
				<< adjpc << " to " << target_addr << endl;
			cerr << dec;
			cerr << " Push DIVERGE." << endl;
		}
		SetNPC(abs_target);
		SetCurrentMask(mask);

		if(cpu->TraceBranch()) {
			cerr << "  New mask = " << GetCurrentMask() << endl;
		}
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Meet(address_t addr)
{
	// Addr relative address
	// SSY instruction
	stack.push(StackToken(active_mask, ID_SYNC, addr + code_base));	// Does addr mean anything here??
	// Yes it does
	if(cpu->TraceBranch()) {
		cerr << " Push SYNC." << endl;
		cerr << "  Mask = " << GetCurrentMask() << endl;
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::PreBreak(address_t addr)
{
	stack.push(StackToken(active_mask, ID_BREAK, addr + code_base));
	if(cpu->TraceBranch()) {
		cerr << " Push BREAK." << endl;
		cerr << "  Mask = " << GetCurrentMask() << endl;
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

// No PRET instruction?

template<class CONFIG>
bool Tesla2Flow<CONFIG>::Join()
{
	// .S modifier
	if(sync_token_popped_post) {
		if(cpu->TraceBranch()) {
			cerr << " Already unwound. Ignore." << endl;
		}
		return true;	// Execute instruction
	}
	// Unwind the stack
	return UnwindStack();
}

template<class CONFIG>
bool Tesla2Flow<CONFIG>::End()
{
	// Implicit sync
	if(!stack.empty()) {
		// Unwind stack
		// Is this correct?
		if(cpu->TraceBranch()) {
			cerr << " End reached, stack not empty." << endl;
		}
		UnwindStack();
//		StackToken token = stack.top();
//		stack.pop();
//		if(/*cpu->TraceBranch() && */token.id != ID_DIVERGE) {
//			cerr << " Warning: Unexpected " << token.id << " token remaining." << endl;
//		}
//		// Re-branch to address in token
//		(*cpu->stats)[GetRPC()].Jump();
//		SetNPC(token.address);
//		SetCurrentMask(token.mask);
		return false;
	}
	if(cpu->TraceBranch()) {
		cerr << " End reached, stack empty. Killing warp." << endl;
	}
	return true;
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Return(std::bitset<CONFIG::WARP_SIZE> mask)
{
	if((mask).none()) {
		(*cpu->stats)[GetRPC()].BranchUniNotTaken();
	}
	else if(mask == GetCurrentMask()) {
		(*cpu->stats)[GetRPC()].BranchUniTaken();
	}
	else {
		(*cpu->stats)[GetRPC()].BranchDivergent();
	}
	
	if(mask == GetCurrentMask()) {
		// All threads take the return
		if(cpu->TraceBranch()) {
			cerr << " Coherent return." << endl;
		}
		// Unwind stack
		UnwindStack();
	}
	else {
		disable_call_mask |= mask;
		active_mask &= ~mask;
	}
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Break(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// Deviation from 7877585
	disable_break_mask |= mask;
	active_mask &= ~mask;
	if(mask == GetCurrentMask()) {
		// All threads take a break
		if(cpu->TraceBranch()) {
			cerr << " Coherent break." << endl;
		}
		// Unwind stack
		UnwindStack();
	}
	else if(cpu->TraceBranch()) {
			cerr << " Divergent break." << endl;
	}
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Kill(std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert(false);
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Call(address_t target_addr)
{
	stack.push(StackToken(active_mask, ID_CALL, GetNPC()));
	(*cpu->stats)[GetRPC()].Jump();
	SetNPC(target_addr + code_base);
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> Tesla2Flow<CONFIG>::GetCurrentMask() const
{
	//return current_mask;
	return replay_mask;
}

template<class CONFIG>
bool Tesla2Flow<CONFIG>::BarrierStop()
{
	return true;
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::BarrierResume()
{
}

template<class CONFIG>
unsigned int Tesla2Flow<CONFIG>::AllPCs(SubWarp<CONFIG> subwarps[], unsigned int size)
{
	assert(size != 0);
	subwarps[0].pc = GetPC();
	subwarps[0].mask = GetCurrentMask();
	return 1;
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Issue(SubWarp<CONFIG> const & sw)
{
	assert(sw.pc == GetPC());
	assert(sw.mask == GetCurrentMask());
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask)
{
	// TODO: replay only non-branch instructions
	mask_t replay = previous_mask & ~commit_mask;
	if(replay.any()) {
		// Some work still to be done?
		// Replay
		replay_mask = replay;
		if(cpu->TraceBranch()) {
			cerr << " Replaying threads " << replay << endl;
		}
		// Ignore sync token next time
		sync_token_popped_post = true;
	}
	else {
		SetPC(GetNPC());
		replay_mask = active_mask;
		sync_token_popped_post = sync_token_popped_pre;
		sync_token_popped_pre = false;
	}
	if(cpu->TraceMask()) {
		cerr << " " << GetCurrentMask() << endl;
	}
	previous_mask = GetCurrentMask();
}

template<class CONFIG>
bool Tesla2Flow<CONFIG>::Ready(SubWarp<CONFIG> const & subwarp) const
{
	// Is this wrong?
	return warp.state == Warp<CONFIG>::Active;
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::SetCurrentMask(std::bitset<CONFIG::WARP_SIZE> mask)
{
	replay_mask = active_mask = mask;
}

template<class CONFIG>
void Tesla2Flow<CONFIG>::WriteBaseMask(unsigned int lane, bool b)
{
	initial_mask[lane] = b;
	active_mask[lane] = b;
	replay_mask[lane] = b;
	previous_mask[lane] = b;
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
