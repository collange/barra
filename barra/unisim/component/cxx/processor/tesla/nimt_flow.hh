/*
 *  Copyright (c) 2011,
 *  Ecole normale superieure de Lyon (ENS Lyon),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENS Lyon nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_NIMT_FLOW_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_NIMT_FLOW_HH

#include <unisim/component/cxx/processor/tesla/forward.hh>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
struct NimtFlow
{
	typedef typename CONFIG::address_t address_t;
	typedef std::bitset<CONFIG::WARP_SIZE> mask_t;
	
	NimtFlow(Warp<CONFIG> & warp);
	
	void Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
		typename CONFIG::address_t code_base);
	void Branch(address_t target, std::bitset<CONFIG::WARP_SIZE> mask);
	void Meet(address_t addr);
	bool Join();
	bool End();
	void Return(std::bitset<CONFIG::WARP_SIZE> mask);
	void Kill(std::bitset<CONFIG::WARP_SIZE> mask);
	std::bitset<CONFIG::WARP_SIZE> GetCurrentMask() const;
	void PreBreak(address_t addr);
	void Break(std::bitset<CONFIG::WARP_SIZE> mask);
	void Call(address_t addr);
	
	void SetNPC(address_t npc);
	address_t GetPC() const;
	address_t GetRPC() const { return GetPC() - code_base; }	// Relative PC
	
	bool BarrierStop();
	void BarrierResume();
	
	// List sub-warps: instructions to fetch next
	// Includes non-ready subwarps
	// Returns number of valid subwarps
	unsigned int AllPCs(SubWarp<CONFIG> list[], unsigned int size);
	
	// Start execution. Update current PC and current Mask.
	void Issue(SubWarp<CONFIG> const & sw);

	// If conflict encountered, instruction needs to be played back in
	// the future for remaining threads
	void Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask);

	// Used for barriers
	bool Ready(SubWarp<CONFIG> const & subwarp) const;
	
	std::bitset<CONFIG::WARP_SIZE> BaseMask() const { return base_mask; }
	void WriteBaseMask(unsigned int lane, bool b);
	
	
private:
	unsigned int CallDepth(unsigned int i) const;
	
	// Return true if valid
	bool AssembleSubWarp(std::bitset<CONFIG::WARP_SIZE> remaining_mask,
		unsigned int phase, SubWarp<CONFIG> & subwarp);

	void SetPC(address_t pc) { mpc = pc; }

	CPU<CONFIG> * cpu;
	Warp<CONFIG> & warp;

	bitset<CONFIG::WARP_SIZE> base_mask;
	bitset<CONFIG::WARP_SIZE> step_mask;
	bitset<CONFIG::WARP_SIZE> barrier_mask;	// Threads waiting at barrier
	
	address_t breakaddr;	// Breaks don't nest, do they?
	address_t syncaddr;
	
	address_t pc[CONFIG::WARP_SIZE];
	address_t npc[CONFIG::WARP_SIZE];
	
	std::stack<address_t> call_stack[CONFIG::WARP_SIZE];
	// Depth[i] is call_stack[i].size().
	
	address_t mpc;
	address_t code_base;
};

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
