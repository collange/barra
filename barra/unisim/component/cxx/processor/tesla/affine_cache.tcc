/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#include <unisim/component/cxx/processor/tesla/affine_cache.hh>
#include <unisim/component/cxx/cache/cache.tcc>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


using namespace std;


template<class CONFIG, class AFFINE_CACHE>
std::bitset<CONFIG::WARP_SIZE> AffineCache<CONFIG, AFFINE_CACHE>::LookupMasked(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats)
{
	typename AFFINE_CACHE::ADDRESS addr = affaccess.addr;
	typename AFFINE_CACHE::ADDRESS line_base_addr;
	typename AFFINE_CACHE::ADDRESS block_base_addr;
	uint32_t index;
	uint32_t sector;
	uint32_t offset;
	uint32_t size_to_block_boundary;
	uint32_t way;
	CacheSet<AFFINE_CACHE> *set;
	CacheLine<AFFINE_CACHE> *line;
	CacheBlock<AFFINE_CACHE> *block;
	
	// Decode the address
	Cache::DecodeAddress(addr, line_base_addr, block_base_addr, index, sector, offset, size_to_block_boundary);

	affaccess.line_base_addr = line_base_addr;
	affaccess.block_base_addr = block_base_addr;
	affaccess.index = index;
	affaccess.sector = sector;
	affaccess.offset = offset;
	affaccess.size_to_block_boundary = size_to_block_boundary;
	affaccess.set = set = &affine_cache[index];
	affaccess.line_hit = false;

	// Associative search
	for(way = 0; way < AFFINE_CACHE::CACHE_ASSOCIATIVITY; way++)
	{
		line = &(*set)[way];
		if(line->status.valid && line->GetBaseAddr() == line_base_addr)
		{
			// line hit: block may need a fill if not yet present in the line
			affaccess.line_hit = true;
			affaccess.line = line;
			affaccess.line_to_evict = 0;
			affaccess.way = way;
			block = &(*line)[sector];
			affaccess.block = block;


			return affaccess.block->status.vmask;
		}
	}

	// line miss
	affaccess.line = 0;
	affaccess.block = 0;
	mask_t result;
	result.reset();
	return result;
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::ChooseLineToEvict(CacheAccess<AFFINE_CACHE>& affaccess)
{
	uint32_t way;
	uint32_t i;
	uint32_t n;
	uint32_t plru_bits = affaccess.set->status.plru_bits;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
		
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
		
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(i = 0, way = 0, n = 0; n < AFFINE_CACHE::CACHE_LOG_ASSOCIATIVITY; n++)
	{
		uint32_t bi = (plru_bits >> i) & 1;
		way = (way << 1) | bi;
		i = (i << 1) + 1 + bi;
	}
	affaccess.way = way;
	affaccess.line_to_evict = &(*affaccess.set)[way];
	assert(way < AFFINE_CACHE::CACHE_ASSOCIATIVITY);
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::EmuEvict(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats)
{
	if(affaccess.line_to_evict->status.valid)
	{
		uint32_t sector;
		for(sector = 0; sector < CacheLine<AFFINE_CACHE>::BLOCKS_PER_LINE; sector++)
		{
			CacheBlock<AFFINE_CACHE>& block_to_evict = (*affaccess.line_to_evict)[sector];
			if(block_to_evict.status.vmask.any() && block_to_evict.status.dirty)
			{
				stats.WBack();
				if(cpu.TraceCache())
				{
					cerr << dec << "    Affine WBack from way " << affaccess.way << " from set " << affaccess.index << endl;
				}
			}
			block_to_evict.status.vmask.reset();
			block_to_evict.status.dirty = false;
		}
	
		affaccess.line_to_evict->status.valid = false;
	}
	affaccess.line = affaccess.line_to_evict;
	affaccess.line_to_evict = 0;
}



template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::EmuFill(CacheAccess<AFFINE_CACHE>& affaccess, mask_t mask, AffineCacheStats<CONFIG> &stats)
{
	affaccess.block = &(*affaccess.line)[affaccess.sector];
	affaccess.line->status.valid = true;
	affaccess.line->SetBaseAddr(affaccess.line_base_addr);
	affaccess.block->status.vmask = mask;
	affaccess.block->status.dirty = false;
	stats.Fill();
}

template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::EmuWrite(CacheAccess<AFFINE_CACHE>& affaccess, mask_t mask, AffineCacheStats<CONFIG> &stats)
{
	affaccess.block = &(*affaccess.line)[affaccess.sector];
	affaccess.line->status.valid = true;
	affaccess.line->SetBaseAddr(affaccess.line_base_addr);
	affaccess.block->status.vmask = mask;
	affaccess.block->status.dirty = false;
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::UpdateReplacementPolicy(CacheAccess<AFFINE_CACHE>& affaccess)
{
	uint32_t i;
	uint32_t n;
	uint32_t way = affaccess.way;
	uint32_t plru_bits = affaccess.set->status.plru_bits;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(n = AFFINE_CACHE::CACHE_LOG_ASSOCIATIVITY, i = 0; n != 0; n--)
	{
		uint32_t bi = (way >> (n - 1)) & 1;
		plru_bits = (plru_bits & ~(1 << i)) | ((1 ^ bi) << i);
		i = (i << 1) + 1 + bi;
	}

	affaccess.set->status.plru_bits = plru_bits;
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::MRU(CacheAccess<AFFINE_CACHE>& affaccess)
{
	uint32_t i;
	uint32_t n;
	uint32_t way = affaccess.way;
	uint32_t plru_bits = affaccess.set->status.plru_bits;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
	
	if(AFFINE_CACHE::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(n = AFFINE_CACHE::CACHE_LOG_ASSOCIATIVITY, i = 0; n != 0; n--)
	{
		uint32_t bi = (way >> (n - 1)) & 1;
		plru_bits = (plru_bits & ~(1 << i)) | (bi << i);
		i = (i << 1) + 1 + bi;
	}

	affaccess.set->status.plru_bits = plru_bits;
}



template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::EvictFill(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
	ChooseLineToEvict(affaccess);
	EmuEvict(affaccess, stats);
	EmuFill(affaccess, mask);
	UpdateReplacementPolicy(affaccess);
}



template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::Invalidate()
{
	uint32_t index;
	for(index = 0; index < Cache::NUM_SETS; index++)
	{
		uint32_t way;
		CacheSet<AFFINE_CACHE>& set = affine_cache[index];
		set.status.plru_bits = 0;
		for(way = 0; way < CacheSet<AFFINE_CACHE>::ASSOCIATIVITY; way++)
		{
			CacheLine<AFFINE_CACHE>& line = set[way];
			uint32_t sector;
			for(sector = 0; sector < CacheLine<AFFINE_CACHE>::BLOCKS_PER_LINE; sector++)
			{
				CacheBlock<AFFINE_CACHE>& block = line[sector];
				block.status.vmask.reset();
				block.status.dirty = false;
			}
			line.status.valid = false;
			line.SetBaseAddr(0);
		}
	}
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::PartialInvalidate(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
	affaccess.block->status.vmask = affaccess.block->status.vmask & ~mask;
    if (!affaccess.block->status.vmask.any())
    {
        MRU(affaccess);
    }
}


template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::Alloc(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
	ChooseLineToEvict(affaccess);
	EmuEvict(affaccess, stats);
	UpdateReplacementPolicy(affaccess);
}

template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::Fill(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
    if (!(affaccess->vmask.any()))
    {
    	ChooseLineToEvict(affaccess);
	    EmuEvict(affaccess, stats);
    }
	EmuFill(affaccess, mask);
	UpdateReplacementPolicy(affaccess);
}

template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::Read(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
    assert(!(mask & ~affaccess.vmask).any());
	UpdateReplacementPolicy(affaccess);
}

template<class CONFIG, class AFFINE_CACHE>
void AffineCache<CONFIG,AFFINE_CACHE>::Write(CacheAccess<AFFINE_CACHE>& affaccess, AffineCacheStats<CONFIG> &stats, mask_t mask)
{
    affaccess.vmask |= mask;
	UpdateReplacementPolicy(affaccess);
}


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim
