/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_FLOW_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_TESLA_FLOW_TCC

#include <unisim/component/cxx/processor/tesla/tesla_flow.hh>
#include <unisim/component/cxx/processor/tesla/cpu.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
TeslaFlow<CONFIG>::TeslaFlow(Warp<CONFIG> & warp) :
	warp(warp)
{
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
	typename CONFIG::address_t code_base)
{
	this->cpu = cpu;
	replay_mask = current_mask = initial_mask = mask;
	this->code_base = code_base;
	stack = stack_t();
	SetPC(code_base);
	SetNPC(code_base);
	//stack.push(StackToken(mask, ID_BOTTOM, 0));
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Branch(address_t target_addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert((target_addr & 3) == 0);	// 64-bit-aligned targets
	typename CONFIG::address_t adjpc = GetRPC();

	// Check mask
	// Mask == none -> nop
	// Mask == all  -> jump
	// Mask == other -> argh
	
	typename CONFIG::address_t abs_target = target_addr + code_base;
	if(cpu->TraceBranch()) {
		cerr << "  Branch mask = " << mask << endl;
	}
	
	assert((mask & ~(current_mask)).none());	// No zombies allowed

	if((mask).none())
	{
		// Uniformally not taken
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, not taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniNotTaken();
		// Nop, just increment PC as usual
	}
	else if(mask == GetCurrentMask())	// Same number of enabled threads as before
	{
		// Uniformally taken
		if(cpu->TraceBranch()) {
			cerr << "  Coherent, taken." << endl;
		}
		(*cpu->stats)[GetRPC()].BranchUniTaken();
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(abs_target);
		// current_mask = mask already
	}
	else
	{
		(*cpu->stats)[GetRPC()].BranchDivergent();
		(*cpu->stats)[GetRPC()].Jump();
		// Start by *taking* the branch ??
	
		// Mixed
		// Push divergence token
		stack.push(StackToken(current_mask & ~mask, ID_DIVERGE, GetNPC()));
		if(cpu->TraceBranch()) {
			cerr << hex;
			cerr << " Conditional non-coherent jump from "
				<< adjpc << " to " << target_addr << endl;
			cerr << dec;
			cerr << " Push DIVERGE." << endl;
		}
		SetNPC(abs_target);
		SetCurrentMask(mask);

		if(cpu->TraceBranch()) {
			cerr << "  New mask = " << GetCurrentMask() << endl;
		}
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Meet(address_t addr)
{
	// Addr relative address
	// SYNC instruction
	stack.push(StackToken(current_mask, ID_SYNC, addr + code_base));	// Does addr mean anything here??
	if(cpu->TraceBranch()) {
		cerr << " Push SYNC." << endl;
		cerr << "  Mask = " << GetCurrentMask() << endl;
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template<class CONFIG>
void TeslaFlow<CONFIG>::PreBreak(address_t addr)
{
	stack.push(StackToken(current_mask, ID_BREAK, addr + code_base));
	if(cpu->TraceBranch()) {
		cerr << " Push BREAK." << endl;
		cerr << "  Mask = " << GetCurrentMask() << endl;
	}
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template<class CONFIG>
bool TeslaFlow<CONFIG>::Join()
{
	if(stack.empty()) {
		if(cpu->TraceBranch()) {
			cerr << " Reached bottom of stack, restoring initial mask." << endl;
		}
		// implicit SYNC token at the bottom of stack?
		SetCurrentMask(initial_mask);
		if(cpu->TraceBranch()) {
			cerr << "  New mask = " << GetCurrentMask() << endl;
		}
		return true;
	}
	StackToken token = stack.top();
	stack.pop();
	
	SetCurrentMask(token.mask);
	if(cpu->TraceBranch()) {
		cerr << "  New mask = " << GetCurrentMask() << endl;
	}
	if(token.id != ID_SYNC)
	{
		if(cpu->TraceBranch()) {
			cerr << " Pop. Not a SYNC token. Go back." << endl;
		}
		// Re-branch to address in token
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(token.address);
		
		// Do NOT execute current instruction
		return false;
	}
	else if(cpu->TraceBranch()) {
		cerr << " Pop. SYNC token." << endl;
	}
	return true;
}

template<class CONFIG>
bool TeslaFlow<CONFIG>::End()
{
	// Implicit sync
	if(!stack.empty()) {
		if(cpu->TraceBranch()) {
			cerr << " End reached, stack not empty." << endl;
		}
		StackToken token = stack.top();
		stack.pop();
		if(/*cpu->TraceBranch() && */token.id != ID_DIVERGE) {
			cerr << " Warning: Unexpected " << token.id << " token remaining." << endl;
		}
		// Re-branch to address in token
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(token.address);
		SetCurrentMask(token.mask);
		return false;
	}
	return true;
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Return(std::bitset<CONFIG::WARP_SIZE> mask)
{
	if((mask).none()) {
		(*cpu->stats)[GetRPC()].BranchUniNotTaken();
	}
	else if(mask == GetCurrentMask()) {
		(*cpu->stats)[GetRPC()].BranchUniTaken();
	}
	else {
		(*cpu->stats)[GetRPC()].BranchDivergent();
	}
	// mask : threads to return
	if(stack.empty()) {
		if(cpu->TraceBranch()) {
			cerr << " Reached bottom of stack, killing thread." << endl;
		}
		SetCurrentMask(GetCurrentMask() & ~mask);
		initial_mask = current_mask;
		if(initial_mask.none()) {
			// Kill warp
			warp.state = Warp<CONFIG>::Finished;
		}
	}
	else
	{
		if(cpu->TraceBranch()) {
			cerr << " Pop." << endl;
		}
		StackToken token = stack.top();
		assert(token.id == ID_CALL);
		stack.pop();
		SetCurrentMask(token.mask);
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(token.address);	// always go back?
		// what if some threads are still enabled?
		// Patent: "no thread diverge when a call/return branch is encountered"
	}
	if(cpu->TraceBranch()) {
		cerr << "  New mask = " << GetCurrentMask() << endl;
	}
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Break(std::bitset<CONFIG::WARP_SIZE> mask)
{
	if(stack.empty()) {
		if(cpu->TraceBranch()) {
			cerr << " Reached bottom of stack, killing threads." << endl;
		}
		SetCurrentMask(GetCurrentMask() & ~mask);
		initial_mask = current_mask;
	}
	else if(mask == GetCurrentMask())
	{
		// All threads take a break
		if(cpu->TraceBranch()) {
			cerr << " Coherent break." << endl;
			cerr << " Pop." << endl;
		}
		StackToken token = stack.top();
		//assert(token.id == ID_BREAK || token.id == ID_SYNC || token.id == ID_CALL);
		stack.pop();
		SetCurrentMask(token.mask);
		(*cpu->stats)[GetRPC()].Jump();
		SetNPC(token.address);	// always go back?
		// what if some threads are still enabled?
		// handled by software?
		// Patent: ???
	}
	else
	{
		// Just disable breaking threads
		SetCurrentMask(GetCurrentMask() & ~mask);
	}
	if(cpu->TraceBranch()) {
		cerr << "  New mask = " << GetCurrentMask() << endl;
	}
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Kill(std::bitset<CONFIG::WARP_SIZE> mask)
{
	assert(false);
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Call(address_t target_addr)
{
	stack.push(StackToken(current_mask, ID_CALL, GetNPC()));
	(*cpu->stats)[GetRPC()].Jump();
	SetNPC(target_addr + code_base);
	assert(stack.size() < CONFIG::STACK_DEPTH);
}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> TeslaFlow<CONFIG>::GetCurrentMask() const
{
	//return current_mask;
	return replay_mask;
}

template<class CONFIG>
bool TeslaFlow<CONFIG>::BarrierStop()
{
	return true;
}

template<class CONFIG>
void TeslaFlow<CONFIG>::BarrierResume()
{
}

template<class CONFIG>
unsigned int TeslaFlow<CONFIG>::AllPCs(SubWarp<CONFIG> subwarps[], unsigned int size)
{
	assert(size != 0);
	subwarps[0].pc = GetPC();
	subwarps[0].mask = GetCurrentMask();
	return 1;
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Issue(SubWarp<CONFIG> const & sw)
{
	assert(sw.pc == GetPC());
	assert(sw.mask == GetCurrentMask());
}

template<class CONFIG>
void TeslaFlow<CONFIG>::Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask)
{
//	mask_t replay = current_mask & ~commit_mask;
//	if(replay.any()) {
//		// Some work still to be done?
//		// Replay
//		replay_mask = replay;
//	}
//	else {
		SetPC(GetNPC());
		replay_mask = current_mask;
//	}
	if(cpu->TraceMask()) {
		cerr << " " << GetCurrentMask() << endl;
	}
}

template<class CONFIG>
bool TeslaFlow<CONFIG>::Ready(SubWarp<CONFIG> const & subwarp) const
{
	// TODO: is this wrong?
	return warp.state == Warp<CONFIG>::Active;
}

template<class CONFIG>
void TeslaFlow<CONFIG>::SetCurrentMask(std::bitset<CONFIG::WARP_SIZE> mask)
{
	replay_mask = current_mask = mask;
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
