/*
 *  Copyright (c) 2011,
 *  Ecole normale superieure de Lyon (ENS Lyon),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of ENS Lyon nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */

#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_NIMT_FLOW_TCC
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_NIMT_FLOW_TCC

#include <unisim/component/cxx/processor/tesla/nimt_flow.hh>
#include <unisim/component/cxx/processor/tesla/cpu.hh>
#include <limits>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template<class CONFIG>
NimtFlow<CONFIG>::NimtFlow(Warp<CONFIG> & warp) :
	warp(warp),
	breakaddr(0)
{
}

template<class CONFIG>
void NimtFlow<CONFIG>::Reset(CPU<CONFIG> * cpu, std::bitset<CONFIG::WARP_SIZE> mask,
	typename CONFIG::address_t code_base)
{
	this->cpu = cpu;
	base_mask = mask;
	step_mask = mask;
	this->code_base = code_base;
	barrier_mask.reset();
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		pc[i] = npc[i] = code_base;
	}
}

template<class CONFIG>
void NimtFlow<CONFIG>::Branch(address_t target_addr, std::bitset<CONFIG::WARP_SIZE> mask)
{
	// target_addr is RELATIVE to CODE_START!
	assert((target_addr & 3) == 0);	// 64-bit-aligned targets
	typename CONFIG::address_t abs_target = target_addr + code_base;

	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i) {
		if(mask[i]) {
			npc[i] = abs_target;
		}
	}
	if(cpu->TraceBranch()) {
		cerr << " Branch. New NPCs are:" << endl;
		
		cerr << std::hex << "  ";
		for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i) {
			cerr << npc[i] - code_base << " ";
		}
		cerr << std::endl << std::dec;
	}
}

template<class CONFIG>
void NimtFlow<CONFIG>::Meet(address_t addr)
{
	// Ignore instruction
	(*cpu->GetStats())[GetRPC()].Unexecute();
	syncaddr = addr;
}

template<class CONFIG>
void NimtFlow<CONFIG>::PreBreak(address_t addr)
{
	// Ignore instruction
	(*cpu->GetStats())[GetRPC()].Unexecute();
	breakaddr = addr;
}


template<class CONFIG>
bool NimtFlow<CONFIG>::Join()
{
	return true;
}

template<class CONFIG>
bool NimtFlow<CONFIG>::End()
{
	//return true;
	Kill(GetCurrentMask());
	return (warp.state == Warp<CONFIG>::Finished);
}

template<class CONFIG>
void NimtFlow<CONFIG>::Return(std::bitset<CONFIG::WARP_SIZE> mask)
{
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(mask[i])
		{
			if(call_stack[i].empty()) {
				// BOS reached, kill thread
				if(cpu->TraceBranch()) {
					cerr << " Thread " << i << " killed." << endl;
				}
				base_mask[i] = false;
				
			}
			else {
				address_t abs_target = call_stack[i].top();
				if(cpu->TraceBranch()) {
					cerr << " Thread " << i << " returned to " << abs_target - code_base << endl;
				}
				npc[i] = abs_target;
				call_stack[i].pop();
			}
		}
	}

	if(base_mask.none())
	{
		warp.state = Warp<CONFIG>::Finished;
		if(cpu->TraceBranch()) {
			cerr << "  Warp killed" << endl;
		}
	}
	
}

template<class CONFIG>
void NimtFlow<CONFIG>::Break(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// Treat like a regular branch instruction
	//assert(breakaddr != 0);
	// Destination address is *usually* the operand of the last PreBreak
	// instruction encountered.
	// However, ptxas seems to nest loops controlled by SetSync inside
	// loops controlled by PreBreak. So we select whatever target is closer
	// and > PC.
	address_t target = 0;
	if(breakaddr > GetRPC()) {
		target = breakaddr;
	}

	// Actually, let's ignore sync for now
//	if(syncaddr > GetRPC()
//		&& (target == 0 || syncaddr < target)) {
//		target = syncaddr;
//	}
	assert(target != 0);
	
	Branch(target, mask);
}

template<class CONFIG>
void NimtFlow<CONFIG>::Kill(std::bitset<CONFIG::WARP_SIZE> mask)
{
	// mask : threads to kill
	// GetCurrentMask() & ~mask : threads alive
	base_mask &= ~mask;
	if(cpu->TraceBranch()) {
		cerr << " Kill, mask=" << mask << endl;
	}
	if(base_mask.none())
	{
		warp.state = Warp<CONFIG>::Finished;
		if(cpu->TraceBranch()) {
			cerr << "  Warp killed" << endl;
		}
	}
	else if(!mask.none())
	{
		if(cpu->TraceBranch()) {
			cerr << "  Inactive threads killed. Mask="
				<< base_mask << endl;
		}
	}
	else if(cpu->TraceBranch()) {
		cerr << "  No thread killed. Mask="
			<< base_mask << endl;
	}
}

template <class CONFIG>
std::bitset<CONFIG::WARP_SIZE> NimtFlow<CONFIG>::GetCurrentMask() const
{
	return step_mask;
}

template <class CONFIG>
typename CONFIG::address_t NimtFlow<CONFIG>::GetPC() const
{
	return mpc;
}

template<class CONFIG>
void NimtFlow<CONFIG>::Call(address_t target_addr)
{
	if(cpu->TraceBranch()) {
		cerr << " Call to " << target_addr << ", mask=" << step_mask << endl;
	}
	address_t abs_target = target_addr + code_base;
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(step_mask[i])
		{
			call_stack[i].push(npc[i]);
			npc[i] = abs_target;
		}
	}
}

template<class CONFIG>
unsigned int NimtFlow<CONFIG>::AllPCs(SubWarp<CONFIG> subwarps[], unsigned int size)
{
	mask_t remaining_mask = base_mask;
	unsigned int phase;
	for(phase = 0; phase != size; ++phase)
	{
		if(AssembleSubWarp(remaining_mask, phase, subwarps[phase])) {
			remaining_mask &= ~subwarps[phase].mask;	// Exclude from subsequent phases
		}
		else {
	break;
		}
	}
	return phase;
}

template<class CONFIG>
bool NimtFlow<CONFIG>::AssembleSubWarp(std::bitset<CONFIG::WARP_SIZE> remaining_mask,
	unsigned int phase, SubWarp<CONFIG> & subwarp)
{
	// Select one PC among active threads (remaining_mask & ~barrier_mask set)
	// Policy: Deepest call depth first, then min PC first
	address_t common_pc = std::numeric_limits<address_t>::max();
	unsigned int deepest_calldepth = 0;
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(remaining_mask[i]
			&& (phase != 0 || !barrier_mask[i])	// Make sure subwarp 0 is not blocked. Other subwarps are okay.
			&& CallDepth(i) >= deepest_calldepth
			&& pc[i] < common_pc) {
			deepest_calldepth = CallDepth(i);
			common_pc = pc[i];
		}
	}
	if(common_pc == std::numeric_limits<address_t>::max())
	{
		// No assert, this is valid.
#if 0
		// No instruction to execute on this phase
		if(phase == 0) {
			// TEMP: debug
			cerr << " Stall. base_mask=" << base_mask
				<< "\n  barrier_mask=" << barrier_mask << "\n";
			for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i) {
				cerr << "  PC[" << i << "]=" << pc[i] - code_base << "\n";
			}
			assert(false);
		}
#endif

		if(phase == 0) {
			// Attempt to find a blocked subwarp
			for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
			{
				if(remaining_mask[i]
					&& pc[i] < common_pc) {
					common_pc = pc[i];
				}
			}
			if(common_pc == std::numeric_limits<address_t>::max()) {
				return false;
			}
			else {
				if(cpu->TraceBranch()) {
					cerr << "Found no active subwarp. Returning inactive sw." << endl;
				}
			}
		}
		else {
			return false;
		}
	}

	subwarp.pc = common_pc;
	subwarp.mask.reset();
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(remaining_mask[i] && pc[i] == common_pc) {
			// Select for execution during this phase
			subwarp.mask[i] = true;
		}
	}
	return true;
}

template<class CONFIG>
void NimtFlow<CONFIG>::Issue(SubWarp<CONFIG> const & sw)
{
	// Update current MPC and current mask
	SetPC(sw.pc);
	step_mask = sw.mask;
	if(cpu->TraceMask()) {
		cerr << " " << GetCurrentMask() << endl;
	}
}

template<class CONFIG>
void NimtFlow<CONFIG>::SetNPC(address_t newnpc)
{
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		if(step_mask[i]) {
			npc[i] = newnpc;
		}
	}
}

template<class CONFIG>
bool NimtFlow<CONFIG>::BarrierStop()
{
	barrier_mask |= step_mask;
	if(cpu->TraceBranch()) {
		cerr << "BarrierStop. Barrier mask: " << barrier_mask << endl;
	}
	return (barrier_mask == base_mask);	// All threads have reached the barrier
}

template<class CONFIG>
void NimtFlow<CONFIG>::BarrierResume()
{
	barrier_mask.reset();
}

template<class CONFIG>
unsigned int NimtFlow<CONFIG>::CallDepth(unsigned int i) const
{
	return call_stack[i].size();
}

template<class CONFIG>
void NimtFlow<CONFIG>::Commit(std::bitset<CONFIG::WARP_SIZE> commit_mask)
{
	for(unsigned int i = 0; i != CONFIG::WARP_SIZE; ++i)
	{
		// Warp may have been just killed: do not go to (invalid) NPC in that case
		if(commit_mask[i] && warp.state != Warp<CONFIG>::Finished) {
			// Commit instruction for this thread, advance state to next PC
			pc[i] = npc[i];
		}
		// PC of other threads do not move
	}
	mask_t replay = step_mask & ~commit_mask;
	if(cpu->TraceBranch() && replay.any()) {
		std::cerr << "Replay " << replay << std::endl;
	}
}

template<class CONFIG>
bool NimtFlow<CONFIG>::Ready(SubWarp<CONFIG> const & subwarp) const
{
	return !(subwarp.mask & barrier_mask).any();
}

template<class CONFIG>
void NimtFlow<CONFIG>::WriteBaseMask(unsigned int lane, bool b)
{
	base_mask[lane] = b;
	step_mask[lane] = b;
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim


#endif
