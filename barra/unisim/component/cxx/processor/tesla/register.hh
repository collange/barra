/*
 *  Copyright (c) 2009,
 *  University of Perpignan (UPVD),
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of UPVD nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Caroline Collange (caroline.collange@inria.fr)
 */
 
#ifndef UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_REGISTER_HH
#define UNISIM_COMPONENT_CXX_PROCESSOR_TESLA_REGISTER_HH

#include <bitset>
#include <iosfwd>

#include <unisim/component/cxx/processor/tesla/forward.hh>
#include <unisim/component/cxx/processor/tesla/enums.hh>
#include <unisim/component/cxx/processor/tesla/strided_register.hh>
#include <unisim/component/cxx/processor/tesla/dual_register.hh>
#include <unisim/util/debug/register.hh>

namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {

template <class CONFIG>
struct BaseVectorAddress;

template <class CONFIG>
struct DualTag;

template <class CONFIG>
struct AffineMetaData
{
	struct st_values
	{
		int32_t b;		// base
		int32_t s;		// stride
	} recap[CONFIG::WARP_SIZE / CONFIG::STRIDED_SEG_SIZE] ;
	bool strided;
public:
	bool operator==(AffineMetaData<CONFIG> const rhs);
	
	bool IsUniform() const;
	//{ return strided && s == 0; }
	bool IsZero() const;
	//{ return strided && s == 0 && b == 0; }
	bool IsStridePow2() const;
	//{ return strided && !(s & (s-1)); }
	friend struct DualTag<CONFIG>;
};


// Inherit from valarray<CONFIG::reg_t>??
template <class CONFIG>
struct BaseVectorRegister
{
	static unsigned int const WARP_SIZE = CONFIG::WARP_SIZE;
	typedef typename CONFIG::reg_t reg_t;
	BaseVectorRegister();
	BaseVectorRegister(uint32_t val);
	BaseVectorRegister(BaseVectorRegister<CONFIG> const & other);
	BaseVectorRegister(BaseVectorAddress<CONFIG> const & addr);
	void Write(BaseVectorRegister<CONFIG> const & vec, std::bitset<CONFIG::WARP_SIZE> mask);
	void Write16(BaseVectorRegister<CONFIG> const & vec, std::bitset<CONFIG::WARP_SIZE> mask, int hi);
	
	void WriteLane(uint32_t val, unsigned int lane);
	uint32_t ReadLane(unsigned int lane) const;
	void WriteFloat(float val, unsigned int lane);
	float ReadFloat(unsigned int lane) const;
	void WriteSimfloat(typename CONFIG::float_t val, unsigned int lane);
	typename CONFIG::float_t ReadSimfloat(unsigned int lane) const;
	BaseVectorRegister<CONFIG> Split(int hilo) const;
	void DumpFloat(std::ostream & os);
	
	uint32_t operator[] (unsigned int lane) const;
	uint32_t & operator[] (unsigned int lane);
	
	uint16_t Read16(unsigned int lane, bool hi) const;

	DualTag<CONFIG> tag;



	bool CheckScalar() const;
	bool CheckStrided() const;
	bool CheckStridedMasked(std::bitset<CONFIG::WARP_SIZE> mask) const;
	AffineMetaData<CONFIG> CheckStridedMMD(std::bitset<CONFIG::WARP_SIZE> mask = ~std::bitset<CONFIG::WARP_SIZE>(0)) const;
	bool CheckScalar16(bool hi) const;
	bool CheckStrided16(bool hi) const;

	reg_t v[WARP_SIZE];
	bool scalar;
	bool strided;
	bool scalar16[2];
	bool strided16[2];
	
	int FlattenNb() const { return 0; }
	bool IsScalar() const { return false; }
	bool IsStrided() const { return false; }
	bool IsScalar16(bool hi) const { return false; }
	bool IsStrided16(bool hi) const { return false; }
	bool Flatten(std::bitset<CONFIG::WARP_SIZE> mask) {}

	uint32_t Hits(std::bitset<CONFIG::WARP_SIZE> mask) const { return 0; }
	uint32_t Hits16(std::bitset<CONFIG::WARP_SIZE> mask, bool hilo = false) const { return 0; }

	void UpdateRead(std::bitset<CONFIG::WARP_SIZE> mask = ~std::bitset<CONFIG::WARP_SIZE>(0), bool b16 = false, bool hilo = false) {};
	void UpdateWrite(std::bitset<CONFIG::WARP_SIZE> mask = ~std::bitset<CONFIG::WARP_SIZE>(0), bool b16 = false, bool hilo = false) {};
	void GetStridedValue(std::bitset<CONFIG::WARP_SIZE> mask = ~std::bitset<CONFIG::WARP_SIZE>(0)) {}

	void SetScalar(bool s = true) {	}
	void SetStrided(bool s = true) { }
	void SetScalar16(bool hi, bool s = true) { }
	void SetStrided16(bool hi, bool s = true) { }
};

template <class CONFIG>
bool Compare(VectorRegister<CONFIG> const & vec0, VectorRegister<CONFIG> const & vec1, std::bitset<CONFIG::WARP_SIZE> const mask);

template <class CONFIG>
std::ostream & operator << (std::ostream & os, BaseVectorRegister<CONFIG> const & r);

template <class CONFIG>
struct BaseVectorAddress
{
	static unsigned int const WARP_SIZE = CONFIG::WARP_SIZE;
	typedef typename CONFIG::address_t address_t;
	
	BaseVectorAddress();
	BaseVectorAddress(address_t addr);
	BaseVectorAddress(BaseVectorAddress<CONFIG> const & other);
	BaseVectorAddress(BaseVectorRegister<CONFIG> const & vr);

	void Write(BaseVectorAddress<CONFIG> const & vec, std::bitset<CONFIG::WARP_SIZE> mask);
	
	void Reset();
	address_t operator[] (unsigned int lane) const;
	address_t & operator[] (unsigned int lane);
	
	BaseVectorAddress<CONFIG> & operator+=(BaseVectorAddress<CONFIG> const & other);
	
	void Increment(DataType dt, size_t imm, std::bitset<CONFIG::WARP_SIZE> mask);
	DualTag<CONFIG> tag;
	
	address_t v[WARP_SIZE];
	bool IsScalar() const { return false; }
	bool IsStrided() const { return false; }
	void SetScalar(bool s = true) { }
	void SetStrided(bool s = true) { }
};

template<class CONFIG>
BaseVectorAddress<CONFIG> operator+(BaseVectorAddress<CONFIG> const & a, BaseVectorAddress<CONFIG> const & b);

template<class CONFIG>
BaseVectorAddress<CONFIG> operator*(unsigned int factor, BaseVectorAddress<CONFIG> const & addr);

template <class CONFIG>
std::ostream & operator << (std::ostream & os, BaseVectorAddress<CONFIG> const & r);



enum RegisterType {
	Base = 0,
	Strided = 1,
	Dual = 2
};

template <RegisterType SEL, class B, class S, class  D>
struct SelectRegisterType
{
};

template <class B, class S, class D>
struct SelectRegisterType<Base, B, S, D>
{
	typedef B t;
};

template <class B, class S, class D>
struct SelectRegisterType<Strided, B, S, D>
{
	typedef S t;
};

template <class B, class S, class D>
struct SelectRegisterType<Dual, B, S, D>
{
	typedef D t;
};

template <class CONFIG>
struct VectorRegister :
/*	SelectType<CONFIG::STAT_SCALAR_REG | CONFIG::STAT_STRIDED_REG,
	           StridedTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> >,
	           BaseVectorRegister<CONFIG> >::t */
	SelectRegisterType<(RegisterType) ((CONFIG::STAT_DUAL_REG * 2) | ((CONFIG::STAT_STRIDED_REG | CONFIG::STAT_SCALAR_REG) & ~CONFIG::STAT_DUAL_REG)) ,
					   BaseVectorRegister<CONFIG>,
					   StridedTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> >,
					   DualTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> > >::t
{
	/* typedef typename SelectType<CONFIG::STAT_SCALAR_REG | CONFIG::STAT_STRIDED_REG,
	           StridedTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> >,
	           BaseVectorRegister<CONFIG> >::t Base; */
	typedef typename SelectRegisterType<(RegisterType) ((CONFIG::STAT_DUAL_REG * 2) | ((CONFIG::STAT_STRIDED_REG | CONFIG::STAT_SCALAR_REG) & ~CONFIG::STAT_DUAL_REG)) ,
					   BaseVectorRegister<CONFIG>,
					   StridedTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> >,
					   DualTagVectorRegister<CONFIG, BaseVectorRegister<CONFIG> > >::t Base;
	VectorRegister() : Base() {}
	VectorRegister(Base const & other) : Base(other) {}
	VectorRegister(uint32_t val) : Base(val) {}
	VectorRegister(VectorRegister<CONFIG> const & other) : Base(other) {}
	VectorRegister(VectorAddress<CONFIG> const & addr) : Base(addr) {}
};

template <class CONFIG>
struct VectorAddress :
	/*SelectType<CONFIG::STAT_SCALAR_REG | CONFIG::STAT_STRIDED_REG,
	           StridedTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> >,
	           BaseVectorAddress<CONFIG> >::t*/
	SelectRegisterType<(RegisterType) ((CONFIG::STAT_DUAL_REG * 2) | ((CONFIG::STAT_STRIDED_REG | CONFIG::STAT_SCALAR_REG) & ~CONFIG::STAT_DUAL_REG)) ,
					   BaseVectorAddress<CONFIG>,
					   StridedTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> >,
					   DualTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> > >::t
{
	/* typedef typename SelectType<CONFIG::STAT_SCALAR_REG | CONFIG::STAT_STRIDED_REG,
	           StridedTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> >,
	           BaseVectorAddress<CONFIG> >::t Base; */
	typedef typename SelectRegisterType<(RegisterType) ((CONFIG::STAT_DUAL_REG * 2) | ((CONFIG::STAT_STRIDED_REG | CONFIG::STAT_SCALAR_REG) & ~CONFIG::STAT_DUAL_REG)) ,
					   BaseVectorAddress<CONFIG>,
					   StridedTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> >,
					   DualTagVectorAddress<CONFIG, BaseVectorAddress<CONFIG> > >::t Base;

	VectorAddress() : Base() {}
	VectorAddress(Base const & other) : Base(other) {}
	VectorAddress(typename CONFIG::address_t addr) : Base(addr) {}
	VectorAddress(VectorAddress<CONFIG> const & other) : Base(other) {}
	VectorAddress(VectorRegister<CONFIG> const & vr) : Base(vr) {}
};

template<class CONFIG>
VectorAddress<CONFIG> operator+(VectorAddress<CONFIG> const & a, VectorAddress<CONFIG> const & b)
{
	return VectorAddress<CONFIG>(operator+((typename VectorAddress<CONFIG>::Base)(a), (typename VectorAddress<CONFIG>::Base)(b)));
}

template<class CONFIG>
VectorAddress<CONFIG> operator*(unsigned int factor, VectorAddress<CONFIG> const & addr)
{
	return VectorAddress<CONFIG>(operator*(factor, (typename VectorAddress<CONFIG>::Base)(addr)));
}

} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim

#endif
