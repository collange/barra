/*
 *  Copyright (c) 2011,
 *  Institut National de Recherche en Informatique et en Automatique (INRIA)
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: 
 *     Alexandre Kouyoumdjian (lexoka@gmail.com)
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#include <unisim/component/cxx/processor/tesla/local_cache.hh>
#include <unisim/component/cxx/cache/cache.tcc>


namespace unisim {
namespace component {
namespace cxx {
namespace processor {
namespace tesla {


using namespace std;


template<class CONFIG, class LOC>
bool LocalCache<CONFIG,LOC>::Lookup(CacheAccess<LOC>& locaccess, LocalCacheStats<CONFIG> &stats)
{
	typename LOC::ADDRESS addr = locaccess.addr;
	typename LOC::ADDRESS line_base_addr;
	typename LOC::ADDRESS block_base_addr;
	uint32_t index;
	uint32_t sector;
	uint32_t offset;
	uint32_t size_to_block_boundary;
	uint32_t way;
	CacheSet<LOC> *set;
	CacheLine<LOC> *line;
	CacheBlock<LOC> *block;
	
	// Decode the address
	Cache::DecodeAddress(addr, line_base_addr, block_base_addr, index, sector, offset, size_to_block_boundary);

	locaccess.line_base_addr = line_base_addr;
	locaccess.block_base_addr = block_base_addr;
	locaccess.index = index;
	locaccess.sector = sector;
	locaccess.offset = offset;
	locaccess.size_to_block_boundary = size_to_block_boundary;
	locaccess.set = set = &local_cache[index];

	// Associative search
	for(way = 0; way < LOC::CACHE_ASSOCIATIVITY; way++)
	{
		line = &(*set)[way];
		if(line->status.valid && line->GetBaseAddr() == line_base_addr)
		{
			// line hit: block may need a fill if not yet present in the line
			stats.Hit();
			locaccess.line = line;
			locaccess.line_to_evict = 0;
			locaccess.way = way;
			block = &(*line)[sector];
			locaccess.block = block->status.valid ? block : 0;
			if(cpu.TraceCache())
			{
				cerr << "    Hit on way " << way << " and on set " << set << endl;
			}

			return true;
		}
	}

	// line miss
	locaccess.line = 0;
	locaccess.block = 0;
	stats.Miss();
	if(cpu.TraceCache())
	{
		cerr << "    Miss on set " << set << endl;
	}
	return false;
}


template<class CONFIG, class LOC>
inline void LocalCache<CONFIG,LOC>::ChooseLineToEvict(CacheAccess<LOC>& locaccess)
{
	uint32_t way;
	uint32_t i;
	uint32_t n;
	uint32_t plru_bits = locaccess.set->status.plru_bits;
	
	if(LOC::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
		
	if(LOC::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
				
	if(LOC::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;
	
	for(i = 0, way = 0, n = 0; n < LOC::CACHE_LOG_ASSOCIATIVITY; n++)
	{
		uint32_t bi = (plru_bits >> i) & 1;
		way = (way << 1) | bi;
		i = (i << 1) + 1 + bi;
	}
	locaccess.way = way;
	locaccess.line_to_evict = &(*locaccess.set)[way];
	assert(way < LOC::CACHE_ASSOCIATIVITY);
}


template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::EmuEvict(CacheAccess<LOC>& locaccess, LocalCacheStats<CONFIG> &stats)
{
	if(locaccess.line_to_evict->status.valid)
	{
		uint32_t sector;
		for(sector = 0; sector < CacheLine<LOC>::BLOCKS_PER_LINE; sector++)
		{
			CacheBlock<LOC>& block_to_evict = (*locaccess.line_to_evict)[sector];
			if(block_to_evict.status.valid && block_to_evict.status.dirty)
			{
				stats.WBack();
				if(cpu.TraceCache())
				{
					cerr << "    WBack from way " << locaccess.way << " from set " << locaccess.index << endl;
				}
			}
			block_to_evict.status.valid = false;
			block_to_evict.status.dirty = false;
		}
	
		locaccess.line_to_evict->status.valid = false;
	}
	locaccess.line = locaccess.line_to_evict;
	locaccess.line_to_evict = 0;
}



template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::EmuFill(CacheAccess<LOC>& locaccess)
{
	locaccess.block = &(*locaccess.line)[locaccess.sector];
	locaccess.line->status.valid = true;
	locaccess.line->SetBaseAddr(locaccess.line_base_addr);
	locaccess.block->status.valid = true;
	locaccess.block->status.dirty = false;
}

template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::UpdateReplacementPolicy(CacheAccess<LOC>& locaccess)
{
	uint32_t i;
	uint32_t n;
	uint32_t way = locaccess.way;
	uint32_t plru_bits = locaccess.set->status.plru_bits;
	
	if(LOC::CACHE_ASSOCIATIVITY % 3 == 0)
		plru_bits &= ~4;
		
	if(LOC::CACHE_ASSOCIATIVITY % 5 == 0)
		plru_bits &= ~36;
		
	if(LOC::CACHE_ASSOCIATIVITY % 7 == 0)
		plru_bits &= ~64;

	for(n = LOC::CACHE_LOG_ASSOCIATIVITY, i = 0; n != 0; n--)
	{
		uint32_t bi = (way >> (n - 1)) & 1;
		plru_bits = (plru_bits & ~(1 << i)) | ((1 ^ bi) << i);
		i = (i << 1) + 1 + bi;
	}

	locaccess.set->status.plru_bits = plru_bits;
}

template<class CONFIG, class LOC>
bool LocalCache<CONFIG,LOC>::Load(VecReg const & output, address_t addr,
	mask_t mask, LocalCacheStats<CONFIG> &stats)
{
	bool hit;
	if(cpu.TraceCache())
	{
		cerr << "Generic load for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << endl;
	}
	CacheAccess<LOC> locaccess;
	locaccess.addr = addr;
	if(Lookup(locaccess, stats))
	{
		// great
		hit = true;
	}
	else
	{
		hit = false;
		ChooseLineToEvict(locaccess);
		EmuEvict(locaccess, stats);
		EmuFill(locaccess);
		if(cpu.TraceCache())
		{
			cerr << "    fill on way " << locaccess.way << " on set " << locaccess.index << endl;
		}
		stats.Fill();
	}
	UpdateReplacementPolicy(locaccess);
	return hit;
}



template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::EvictFill(CacheAccess<LOC>& locaccess, LocalCacheStats<CONFIG>& stats)
{
	ChooseLineToEvict(locaccess);
	EmuEvict(locaccess, stats);
	EmuFill(locaccess);
	if(cpu.TraceCache())
	{
		cerr << "    fill on way " << locaccess.way << " on set " << locaccess.index << endl;
	}
	stats.Fill();
	UpdateReplacementPolicy(locaccess);
}


template<class CONFIG, class LOC>
bool LocalCache<CONFIG,LOC>::Store(VecReg const & output, address_t addr, std::bitset<CONFIG::WARP_SIZE> mask, LocalCacheStats<CONFIG>& stats)
{
	bool hit;
	if(cpu.TraceCache())
	{
		cerr << "Generic store for WARP # " << cpu.WarpId() << " at: " << hex << addr << dec << endl;
	}
	CacheAccess<LOC> locaccess;
	locaccess.addr = addr;
	if(Lookup(locaccess, stats))
	{
		// great
		hit = true;
	}
	else
	{
		hit = false;
		ChooseLineToEvict(locaccess);
		EmuEvict(locaccess, stats);
		EmuFill(locaccess);
		if (mask.count() < CONFIG::WARP_SIZE)
			stats.Fill();
	}
	UpdateReplacementPolicy(locaccess);
	locaccess.block->status.dirty = true ;
	return hit;
}

template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::Invalidate()
{
	uint32_t index;
	for(index = 0; index < Cache::NUM_SETS; index++)
	{
		uint32_t way;
		CacheSet<LOC>& set = local_cache[index];
		set.status.plru_bits = 0;
		for(way = 0; way < CacheSet<LOC>::ASSOCIATIVITY; way++)
		{
			CacheLine<LOC>& line = set[way];
			uint32_t sector;
			for(sector = 0; sector < CacheLine<LOC>::BLOCKS_PER_LINE; sector++)
			{
				CacheBlock<LOC>& block = line[sector];
				block.status.valid = false;
				block.status.dirty = false;
			}
			line.status.valid = false;
			line.SetBaseAddr(0);
		}
	}
}

/*
template<class CONFIG, class LOC>
void LocalCache<CONFIG,LOC>::PartialInvalidate(CacheAccess<LOC>& locacess, mask_t mask)
{
	locacess.block->status.vmask = locacess.block->status.vmask & ~mask;
}*/


} // end of namespace tesla
} // end of namespace processor
} // end of namespace cxx
} // end of namespace component
} // end of namespace unisim
