#ifndef GDEV_INTERFACE_INCLUDED
#define GDEV_INTERFACE_INCLUDED

// Copy of the subset of Gdev headers we need
#include <sys/sem.h> /* struct sembuf */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Gdev types: they are not exposed to end users.
 */
typedef struct gdev_vas gdev_vas_t;
typedef struct gdev_ctx gdev_ctx_t;
typedef struct gdev_mem gdev_mem_t;

/**
 * OS and user-space private types.
 */
typedef struct gdev_lock gdev_lock_t;
typedef struct gdev_mutex gdev_mutex_t;

struct gdev_lock {
	int semid;
	struct sembuf sembuf;
};

struct gdev_mutex {
	int semid;
	struct sembuf sembuf;
};

struct gdev_msg_struct {
    long mtype;
    char mtext;
};


/* a list structure: we could use Linux's list_head, but it's not available
   in user-space - hence use our own list structure. */
struct gdev_list {
    struct gdev_list *next;
    struct gdev_list *prev;
	void *container;
};

/* compatible with struct timeval. */
struct gdev_time {
	uint64_t sec;
	uint64_t usec;
	int neg;
};

/**
 * Gdev device struct:
 */
struct gdev_device {
	int id; /* device ID */
	int users; /* the number of threads/processes using the device */
	int accessed; /* indicate if any process is on the device */
	int blocked; /* incidate if a process is allowed to access the device */
	uint32_t chipset;
	uint64_t mem_size;
	uint64_t mem_used;
	uint64_t dma_mem_size;
	uint64_t dma_mem_used;
	uint32_t com_bw; /* available compute bandwidth */
	uint32_t mem_bw; /* available memory bandwidth */
	uint32_t mem_sh; /* available memory space share */
	uint32_t period; /* minimum inter-arrival time (us) of replenishment. */
	uint32_t com_bw_used; /* used compute bandwidth */
	uint32_t mem_bw_used; /* used memory bandwidth */
	uint32_t com_time; /* cumulative computation time. */
	uint32_t mem_time; /* cumulative memory transfer time. */
	struct gdev_time credit_com; /* credit of compute execution */
	struct gdev_time credit_mem; /* credit of memory transfer */
	void *priv; /* private device object */
	void *compute; /* private set of compute functions */
	void *sched_com_thread; /* compute scheduler thread */
	void *sched_mem_thread; /* memory scheduler thread */
	void *credit_com_thread; /* compute credit thread */
	void *credit_mem_thread; /* memory credit thread */
	void *current_com; /* current compute execution entity */
	void *current_mem; /* current memory transfer entity */
	struct gdev_device *parent; /* only for virtual devices */
	struct gdev_list list_entry_com; /* entry to active compute list */
	struct gdev_list list_entry_mem; /* entry to active memory list */
	struct gdev_list sched_com_list; /* wait list for compute scheduling */
	struct gdev_list sched_mem_list; /* wait list for memory scheduling */
	struct gdev_list vas_list; /* list of VASes allocated to this device */
	struct gdev_list shm_list; /* list of shm users allocated to this device */
	gdev_lock_t sched_com_lock;
	gdev_lock_t sched_mem_lock;
	gdev_lock_t vas_lock;
	gdev_lock_t global_lock;
	gdev_mutex_t shm_mutex;
	gdev_mem_t *swap; /* reserved swap memory space */
};

/**
 * Gdev shared memory information:
 */
struct gdev_shm {
	struct gdev_mem *holder; /* current memory holder */
	struct gdev_list mem_list; /* list of memory objects attached */
	struct gdev_list list_entry; /* entry to the list of shared memory */
	gdev_mutex_t mutex;
	uint64_t size;
	int prio; /* highest prio among users (effective only for master) */
	int users; /* number of users (effective only for master) */
	int key; /* key value of this shared memory */
	int id; /* indentifier of this shared memory */
	int implicit; /* true if created due to a lack of memory space. */
	void *bo; /* private buffer object */
};

/**
 * virtual address space (VAS) object struct:
 *
 * NVIDIA GPUs support virtual memory (VM) with 40 bits addressing.
 * VAS hence ranges in [0:1<<40]. In particular, the pscnv bo function will
 * allocate [0x20000000:1<<40] to any buffers in so called global memory,
 * local memory, and constant memory. the rest of VAS is used for different
 * purposes, e.g., for shared memory.
 * CUDA programs access these memory spaces as follows:
 * g[$reg] redirects to one of g[$reg], l[$reg-$lbase], and s[$reg-$sbase],
 * depending on how local memory and shared memory are set up.
 * in other words, g[$reg] may reference global memory, local memory, and
 * shared memory.
 * $lbase and $sbase are respectively local memory and shared memory base
 * addresses, which are configured when GPU kernels are launched.
 * l[0] and g[$lbase] reference the same address, so do s[0] and g[$sbase].
 * constant memory, c[], is another type of memory space that is often used
 * to store GPU kernels' parameters (arguments).
 * global memory, local memory, and constant memory are usually mapped on
 * device memory, a.k.a., video RAM (VRAM), though they could also be mapped
 * on host memory, a.k.a., system RAM (SysRAM), while shared memory is always
 * mapped on SRAM present in each MP.
 */
struct gdev_vas {
	int vid; /* vritual address space ID. */
	void *handle; /* Gdev API handle. */
	void *pvas; /* driver private object. */
	struct gdev_device *gdev; /* vas is associated with a specific device. */
	struct gdev_list mem_list; /* list of device memory spaces. */
	struct gdev_list dma_mem_list; /* list of host dma memory spaces. */
	struct gdev_list list_entry; /* entry to the vas list. */
	gdev_lock_t lock;
	int prio;
};

/**
 * GPU context object struct:
 */
struct gdev_ctx {
	int cid; /* context ID. */
	void *pctx; /* driver private object. */
	struct gdev_vas *vas; /* chan is associated with a specific vas object. */
	struct gdev_fifo {
		volatile uint32_t *regs; /* channel control registers. */
		void *ib_bo; /* driver private object. */
		uint32_t *ib_map;
		uint32_t ib_order;
		uint64_t ib_base;
		uint32_t ib_mask;
		uint32_t ib_put;
		uint32_t ib_get;
		void *pb_bo; /* driver private object. */
		uint32_t *pb_map;
		uint32_t pb_order;
		uint64_t pb_base;
		uint32_t pb_mask;
		uint32_t pb_size;
		uint32_t pb_pos;
		uint32_t pb_put;
		uint32_t pb_get;
		void (*space)(struct gdev_ctx *, uint32_t);
		void (*push)(struct gdev_ctx *, uint64_t, uint32_t, int);
		void (*kick)(struct gdev_ctx *);
		void (*update_get)(struct gdev_ctx *);
	} fifo; /* command FIFO queue struct. */
	struct gdev_fence { /* fence objects (for compute and dma). */
		void *bo; /* driver private object. */
		uint32_t *map;
		uint64_t addr;
		uint32_t seq;
	} fence;
	struct gdev_intr { /* notifier objects (for compute and dma). */
		void *bo; /* driver private object. */
		uint64_t addr;
	} notify;
	uint32_t dummy;
	void *pdata; /* arch-specific private data object. */
	struct gdev_desc {
	    void *bo;
	    uint32_t *map;
	    uint32_t addr;
	} desc; /* compute desc struct. But really, need to structure for each launch grid */
};

/**
 * device/host memory object struct:
 */
struct gdev_mem {
	void *bo; /* driver private object */
	struct gdev_vas *vas; /* mem is associated with a specific vas object */
	struct gdev_list list_entry_heap; /* entry to heap list */
	struct gdev_list list_entry_shm; /* entry to shared memory list */
	struct gdev_shm *shm; /* shared memory information */
	struct gdev_mem *swap_mem; /* device memory for temporal swap */
	void *swap_buf; /* host buffer for swap */
	int evicted; /* 1 if evicted, 0 otherwise */
	uint64_t size; /* memory size */
	uint64_t addr; /* virtual memory address */
	int type; /* device or host dma? */
	void *map; /* memory-mapped buffer */
	int map_users; /* # of users referencing the map */
	void *pdata; /* arch-specific private data object. */
};

/**
 * private compute functions.
 */
struct gdev_compute {
	int (*launch)(struct gdev_ctx *, struct gdev_kernel *);
	uint32_t (*fence_read)(struct gdev_ctx *, uint32_t);
	void (*fence_write)(struct gdev_ctx *, int, uint32_t);
	void (*fence_reset)(struct gdev_ctx *, uint32_t);
	void (*memcpy)(struct gdev_ctx *, uint64_t, uint64_t, uint32_t);
	void (*memcpy_async)(struct gdev_ctx *, uint64_t, uint64_t, uint32_t);
	void (*membar)(struct gdev_ctx *);
	void (*notify_intr)(struct gdev_ctx *);
	void (*init)(struct gdev_ctx *);
};

/**
 * static numbers for nvidia GPUs.
 */
#define GDEV_NVIDIA_CONST_SEGMENT_MAX_COUNT 32 /* by definition? */

/**
 * GPGPU kernel object struct:
 * we use the same kernel struct between user-space and kernel-space.
 *
 * if you want to know how NVIDIA GPGPU kernels work, please reference
 * the NVIDIA docs and/or the PSCNV wiki available at:
 * https://github.com/pathscale/pscnv/wiki/Nvidia_Compute
 */
struct gdev_kernel {
	uint64_t code_addr; /* code address in VAS */
	uint32_t code_size; /* code size */
	uint32_t code_pc; /* initial program counter */
	struct gdev_cmem {
		uint64_t addr; /* constant memory address in VAS */
		uint32_t size; /* constant memory size */
		uint32_t offset; /* offset in constant memory */
	} cmem[GDEV_NVIDIA_CONST_SEGMENT_MAX_COUNT];
	uint32_t cmem_count; /* constant memory count */
	uint32_t param_size; /* kernel parameter size */
	uint32_t *param_buf; /* kernel parameter buffer */
	uint64_t lmem_addr; /* local memory address in VAS */
	uint64_t lmem_size_total; /* local memory size for all threads */
	uint32_t lmem_size; /* local memory size per thread (l[positive]) */
	uint32_t lmem_size_neg; /* data stack size per thread (l[negaive]) */
	uint32_t lmem_base; /* $lbase */
	uint32_t smem_size_func; /* shared memory size specified statically in func bin */
	uint32_t smem_size; /* shared memory size used in real kernel execution */
	uint32_t smem_base; /* $sbase */
	uint32_t warp_stack_size; /* warp stack size (call stack) */
	uint32_t warp_lmem_size; /* total warp memory size */
	uint32_t reg_count; /* register count */
	uint32_t bar_count; /* barrier count */
	uint32_t grid_id; /* grid ID */
	uint32_t grid_x; /* grid dimension X */
	uint32_t grid_y; /* grid dimension Y */
	uint32_t grid_z; /* grid dimension Z */
	uint32_t block_x; /* block dimension X */
	uint32_t block_y; /* block dimension Y */
	uint32_t block_z; /* block dimension Z */
	//barra_hack
	char* name;
};

/**
 * chipset specific functions.
 */
void barra_compute_setup(struct gdev_device *gdev);

/**
 * CUDA types
 */
typedef struct CUctx_st *CUcontext;     ///< CUDA context
typedef struct CUmod_st *CUmodule;      ///< CUDA module
typedef struct CUfunc_st *CUfunction;   ///< CUDA function


void barra_dev_open();
void barra_dev_close();
int barra_get_attribute(unsigned int attrib, int dev);
CUcontext barra_ctx_new(int dev);
void barra_ctx_free(CUcontext ctx);
int64_t barra_mem_alloc(unsigned int bytesize);
void barra_mem_free(int64_t addr);
int barra_read(void *buf, uint64_t addr, uint32_t size);
int barra_write(uint64_t addr, const void *buf, uint32_t size);
int barra_ib_bo_alloc();

#ifdef __cplusplus
}
#endif

#endif
