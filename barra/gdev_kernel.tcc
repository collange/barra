/*
 *  Copyright (c) 2014,
 *  INRIA,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors:
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#include "gdev_kernel.hh"

template<class CONFIG>
GdevKernel<CONFIG>::GdevKernel(gdev_kernel & k) : k(k), name(string(k.name))
{
}


template<class CONFIG>
int GdevKernel<CONFIG>::BlockX() const
{
	return k.block_x;
}

template<class CONFIG>
int GdevKernel<CONFIG>::BlockY() const
{
	return k.block_y;
}

template<class CONFIG>
int GdevKernel<CONFIG>::BlockZ() const
{
	return k.block_z;
}

template<class CONFIG>
int GdevKernel<CONFIG>::GridX() const
{
	return k.grid_x;
}

template<class CONFIG>
int GdevKernel<CONFIG>::GridY() const
{
	return k.grid_y;
}

template<class CONFIG>
int GdevKernel<CONFIG>::GPRs() const
{
	return k.reg_count;
}

template<class CONFIG>
uint32_t GdevKernel<CONFIG>::SharedTotal() const
{
	// Align to 256-byte boundary
	return (16 + k.param_size + k.smem_size_func + k.smem_size + 255) & (~255);
}

template<class CONFIG>
uint32_t GdevKernel<CONFIG>::LocalTotal() const
{
	return k.lmem_size;	// TODO +lmem_size_neg ?
}


template<class CONFIG>
uint32_t GdevKernel<CONFIG>::CodeSize() const
{
	return k.code_size;
}

template<class CONFIG>
typename CONFIG::address_t GdevKernel<CONFIG>::CodeBase() const
{
	assert(k.code_pc == 0);	// Code base is also starting PC
	return k.code_addr;
}

template<class CONFIG>
typename CONFIG::address_t GdevKernel<CONFIG>::ConstBase(int segment) const
{
	assert(segment < k.cmem_count);
	return k.cmem[segment].addr + k.cmem[segment].offset;
}

template<class CONFIG>
uint8_t const * GdevKernel<CONFIG>::GetParameters() const
{
	return (uint8_t*)k.param_buf;
}

template<class CONFIG>
uint32_t GdevKernel<CONFIG>::ParametersSize() const
{
	return k.param_size;
}

template<class CONFIG>
unsigned int GdevKernel<CONFIG>::SamplersSize() const
{
	return 0;	// TODO: Add sampler support in GDev
}

template<class CONFIG>
SamplerBase<typename CONFIG::address_t> const * GdevKernel<CONFIG>::GetSampler(unsigned int i) const
{
	return 0;
}


template<class CONFIG>
Stats<CONFIG> & GdevKernel<CONFIG>::GetStats()
{
    return stats;
}

//barra_hack
template<class CONFIG>
std::string GdevKernel <CONFIG>::GetName(){
	return name;
}
