/*
 *  Copyright (c) 2014,
 *  INRIA,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors:
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

#ifndef SIMULATOR_CXX_TESLA_GDEV_KERNEL_HH
#define SIMULATOR_CXX_TESLA_GDEV_KERNEL_HH

#include "gdev_interface.h"
#include <unisim/component/cxx/processor/tesla/interfaces.hh>
#include <unisim/component/cxx/processor/tesla/stats.hh>
#include <unisim/component/cxx/scheduler/cuda_scheduler/cuda_scheduler.hh>

using unisim::component::cxx::scheduler::cuda_scheduler::CUDAGrid;

template<class CONFIG>
struct GdevKernel : CUDAGrid<CONFIG>
{
	GdevKernel(gdev_kernel & k);

	virtual int BlockX() const;
	virtual int BlockY() const;
	virtual int BlockZ() const;
	virtual int GridX() const;
	virtual int GridY() const;
	virtual int GPRs() const;
	virtual uint32_t SharedTotal() const;
	virtual uint32_t LocalTotal() const;

	virtual uint8_t const * GetParameters() const;
	virtual uint32_t ParametersSize() const;
	// Samplers
	virtual unsigned int SamplersSize() const;
	virtual SamplerBase<typename CONFIG::address_t> const * GetSampler(unsigned int i) const;
	// Stats
	// depends on config...
	virtual Stats<CONFIG> & GetStats();
	virtual uint32_t CodeSize() const;
	virtual typename CONFIG::address_t CodeBase() const;
	virtual typename CONFIG::address_t ConstBase(int segment) const;
	//barra_hack
	virtual std::string GetName();

private:
	Stats<CONFIG> stats;
	gdev_kernel & k;
	std::string name;
};

#endif
