/*
 *  Copyright (c) 2014,
 *  INRIA,
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification,
 *  are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   - Neither the name of INRIA nor the names of its contributors may be used to
 *     endorse or promote products derived from this software without specific prior
 *     written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors:
 *     Caroline Collange (caroline.collange@inria.fr)
 *
 */

// Gdev interface to Barra

#include <unisim/component/cxx/processor/tesla/config.hh>
#include "system.hh"
#include "driver.hh"

#include "module.hh"
#include "device.hh"
#include "event.hh"
#include "exception.hh"

#include "driver.tcc"
#include "module.tcc"
#include "device.tcc"
#include "kernelmanager.tcc"
#include <unisim/component/cxx/command_processor/tesla/stream.hh>
#include "event.tcc"
#include <unisim/component/cxx/processor/tesla/interfaces.hh>

#include "gdev_interface.h"

#include "gdev_kernel.tcc"

using std::cerr;
using std::endl;
using boost::shared_ptr;
using unisim::component::cxx::processor::tesla::SamplerBase;
using unisim::component::cxx::processor::tesla::ArrayFormat;
using unisim::component::cxx::processor::tesla::AddressMode;
using unisim::component::cxx::processor::tesla::FilterMode;
using unisim::component::cxx::processor::tesla::TextureFlags;
using unisim::component::cxx::command_processor::tesla::Stream;


typedef TimedConfig MyConfig;

extern Driver<MyConfig> * driver;
extern System mysystem;

// For now, abort when check fails
#define CHECK_PTR(p) \
	{ if(!p) { \
		cerr << "Error: null pointer " #p " at " << __FILE__ << ":" << __LINE__ << endl; \
		assert(false); } }

#define CHECK_DRIVER \
	{ if(!driver) { \
		cerr << "Error: driver not initialized. At " << __FILE__ << ":" << __LINE__ << endl; \
		assert(false); } }


bool const verbose = true;



// Barra compute functions
// TODO: move to gdev
static int barra_launch(struct gdev_ctx *ctx, struct gdev_kernel *k)
{
 	if(verbose) cerr << "barra_launch()" << endl;
 	CHECK_DRIVER;
 	GdevKernel<MyConfig>* kernel = driver->KernelsManager().getKernel(k);
	driver->Launch(*kernel);
}

static uint32_t barra_fence_read(struct gdev_ctx *ctx, uint32_t sequence)
{
	return ctx->fence.map[sequence];
}

static void barra_fence_write(struct gdev_ctx *ctx, int subch, uint32_t sequence)
{
	if(verbose) cerr << "barra_fence_write(..., " << subch << ", " << sequence << ")" << endl;
	CHECK_DRIVER;
	Stream<MyConfig>* str = driver->CurrentDevice().getStream();
	uint32_t *array = ctx->fence.map;
	str->pushSync(array, sequence);
}

static void barra_fence_reset(struct gdev_ctx *ctx, uint32_t sequence)
{
	if(verbose) cerr << "barra_fence_reset(..., " << sequence << ")" << endl;
	ctx->fence.map[sequence] = ~0;



}

static void barra_memcpy(struct gdev_ctx *ctx, uint64_t dst_addr, uint64_t src_addr, uint32_t size)
{
	if(verbose) cerr << "barra_memcpy(..., " << std::hex << dst_addr << ", " << src_addr << std::dec << ", " << size << ")" << endl;
	CHECK_DRIVER;
	Stream<MyConfig>* str = driver->CurrentDevice().getStream();
	try
	{
		if(dst_addr & 0x8000000000000000ull) {
			if(!(src_addr & 0x8000000000000000ull)) {
				// Device to Host
				driver->CopyDtoH((void*)(dst_addr & 0x7fffffffffffffff), src_addr, size);
			}
			else {
				// Host to Host?
				cerr << "barra_memcpy: attempt to memcpy from Host to Host?" << endl;
			}

		}
		else {
			if((src_addr & 0x8000000000000000ull)) {
				// Host to device
				driver->CopyHtoD(dst_addr, (void*)(src_addr & 0x7fffffffffffffff), size);
			}
			else {
				// Device to Device
				driver->CopyDtoD(dst_addr, src_addr, size);
			}
		}
	}
	catch(CudaException e) {
		cerr << "barra_memcpy failed: " << e.code << endl;
	}
}

static void barra_memcpy_async(struct gdev_ctx *ctx, uint64_t dst_addr, uint64_t src_addr, uint32_t size)
{
	if(verbose) cerr << "barra_memcpy_async(..., " << std::hex << dst_addr << ", " << src_addr << std::dec << ", " << size << ")" << endl;
}

static void barra_membar(struct gdev_ctx *ctx)
{
 	if(verbose) cerr << "barra_membar()" << endl;
}

static void barra_notify_intr(struct gdev_ctx *ctx)
{
 	if(verbose) cerr << "barra_notify_intr()" << endl;
}

static void barra_init(struct gdev_ctx *ctx)
{
	if(verbose) cerr << "barra_init()" << endl;


}

static struct gdev_compute gdev_compute_barra = {
	.launch = barra_launch,
	.fence_read = barra_fence_read,
	.fence_write = barra_fence_write,
	.fence_reset = barra_fence_reset,
	.memcpy = barra_memcpy,
	.memcpy_async = barra_memcpy_async,
	.membar = barra_membar,
	.notify_intr = barra_notify_intr,
	.init = barra_init,
};

// Exported
void barra_compute_setup(struct gdev_device *gdev)
{
	gdev->compute = &gdev_compute_barra;


}


// Preliminary API, *not stable*

void barra_dev_open()
{
	mysystem.Build();
	driver = mysystem.GetDriver();
}

void barra_dev_close(){
	cerr << "Barra close Device" << endl;
}


int barra_get_attribute(unsigned int attrib, int dev)
{
	if(verbose) cerr << "barra_get_attribute(" << attrib << ", " << dev << ")" << endl;
	CHECK_DRIVER;
 	try {
		return driver->Dev(dev).Attribute(attrib);
	}
	catch(CudaException e) {
		cerr << "barra_get_attribute failed: " << e.code << endl;
		return -EINVAL;
	}
}

CUcontext barra_ctx_new(int dev)
{
	CUcontext ctx;
	if(verbose) cerr << "barra_ctx_new(" << dev << ")" << endl;
	CHECK_DRIVER;
	if(driver->cuCtxCreate(&ctx, 0, dev)) {
		return NULL;
	}



	return ctx;
}

void barra_ctx_free(CUcontext ctx)
{
	if(verbose) cerr << "barra_ctx_free(" << ctx << ")" << endl;
	CHECK_DRIVER;
	std::map<uint64_t, GdevKernel<MyConfig>*> map = driver->KernelsManager().getKernelsMap();
	for(std::map<uint64_t, GdevKernel<MyConfig>*>::iterator it = map.begin(); it!=map.end(); ++it ){
		cerr<< "dump kernel stat" <<endl;
		driver->CurrentDevice().ExportStats(*(it->second));
	}
	driver->cuCtxDestroy(ctx);
}

int64_t barra_mem_alloc(unsigned int bytesize)
{
	if(verbose) cerr << "barra_mem_alloc(..." << bytesize << ")" << endl;
	CHECK_DRIVER;
	try
	{
		MyConfig::address_t addr = driver->MAlloc(bytesize);
		return addr;
	}
	catch(CudaException e) {
		cerr << "barra_mem_alloc failed: " << e.code << endl;
		return 0;
	}
}

void barra_mem_free(int64_t addr)
{
	if(verbose) cerr << "barra_mem_free()" << endl;
	CHECK_DRIVER;
	CHECK_PTR(addr);
	try
	{
		driver->Free(addr);
	}
	catch(CudaException e) {
		cerr << "barra_mem_free failed: " << e.code << endl;
		assert(false);
	}
}

int barra_read(void *buf, uint64_t addr, uint32_t size)
{
	if(verbose) cerr << "barra_read(" << buf << ", " << addr << ", " << size << ")" << endl;
	CHECK_DRIVER;
	try
	{
		driver->CopyDtoH(buf, addr, size);
		return 0;
	}
	catch(CudaException e) {
		cerr << "barra_read failed: " << e.code << endl;
		return -EINVAL;
	}
}

int barra_write(uint64_t addr, const void *buf, uint32_t size)
{
	if(verbose) cerr << "barra_write(" << addr << ", " << buf << ", " << size << ")" << endl;
	CHECK_DRIVER;
	try
	{
		driver->CopyHtoD(addr, buf, size);
		return 0;
	}
	catch(CudaException e) {
		cerr << "barra_write failed: " << e.code << endl;
		return -EINVAL;
	}
}
