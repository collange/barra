/***************************************************************************
                                   generator.hh
                             -------------------
    begin                : Thu May 25 2003
    copyright            : (C) 2003-2007 CEA and Universite Paris Sud
    authors              : Gilles Mouchard, Yves Lhuillier
    email                : gilles.mouchard@cea.fr, yves.lhuillier@cea.fr
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2        *
 *   as published by the Free Software Foundation.                         *
 *                                                                         *
 ***************************************************************************/

#ifndef __GENERATOR_HH__
#define __GENERATOR_HH__

#include <fwd.hh>
#include <conststr.hh>

struct Generator {
  enum Exception_t { GenerationError };
  
  Isa*                                m_isa;
  unsigned int                        m_minwordsize;
  unsigned int                        m_insn_maxsize;
  unsigned int                        m_insn_minsize;
  
  Generator();
  virtual ~Generator() {};
  
  Generator&                          init( Isa& _isa );
  virtual void                        finalize() = 0;

  void                                iss( Product_t& _product ) const;
  /* header file */
  void                                decoder_decl( Product_t& _product ) const;
  void                                operation_decl( Product_t& _product ) const;
  /* source file */
  void                                decoder_impl( Product_t& _product ) const;
  void                                operation_impl( Product_t& _product ) const;
  void                                isa_operations_decl( Product_t& _product ) const;
  void                                isa_operations_ctors( Product_t& _product ) const;
  void                                isa_operations_methods( Product_t& _product ) const;
  
  Isa const&                          isa() const { return *m_isa; }
  Isa&                                isa() { return *m_isa; }
  
  virtual void                        codetype_decl( Product_t& _product ) const = 0;
  virtual void                        codetype_impl( Product_t& _product ) const = 0;
  virtual ConstStr_t                  codetype_name() const = 0;
  virtual ConstStr_t                  codetype_ref() const = 0;
  virtual ConstStr_t                  codetype_constref() const = 0;
  virtual void                        insn_bits_code( Product_t& _product, Operation_t const& _op ) const = 0;
  virtual void                        insn_mask_code( Product_t& _product, Operation_t const& _op ) const = 0;
  virtual ConstStr_t                  insn_id_expr( char const* _addrname ) const = 0;
  virtual void                        insn_match_ifexpr( Product_t& _product, char const* _code, char const* _mask, char const* _bits ) const = 0;
  virtual void                        insn_unchanged_expr( Product_t& _product, char const* _old, char const* _new ) const = 0;
  virtual void                        insn_decode_impl( Product_t& _product, Operation_t const& _op, char const* _codename, char const* _addrname ) const = 0;
  virtual void                        additional_impl_includes( Product_t& _product ) const = 0;
  virtual void                        additional_decl_includes( Product_t& _product ) const = 0;
  
  virtual void                        subdecoder_bounds( Product_t& _product ) const = 0;
  virtual void                        insn_destructor_decl( Product_t& _product, Operation_t const& _op ) const = 0;
  virtual void                        insn_destructor_impl( Product_t& _product, Operation_t const& _op ) const = 0;
  virtual void                        op_getlen_decl( Product_t& _product ) const = 0;
  virtual void                        insn_getlen_decl( Product_t& _product, Operation_t const& _op ) const = 0;
  
  static unsigned int                 least_ctype_size( unsigned int bits );

};

struct FieldIterator {
  Vect_t<BitField_t> const& m_bitfields;
  unsigned int              m_idx;
  unsigned int              m_ref;
  unsigned int              m_pos, m_size;
  unsigned int              m_chkpt_pos, m_chkpt_size;
  
  FieldIterator( bool little_endian, Vect_t<BitField_t> const& bitfields, unsigned int maxsize );
  
  unsigned int      pos() { return m_pos; }
  unsigned int      insn_size() { return (m_ref == 0) ? m_pos + m_size : m_ref - m_pos; }
  BitField_t const& item();
  bool              next();
};

#endif // __GENERATOR_HH__
