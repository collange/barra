/***************************************************************************
                                    fw.hh
                             -------------------
    begin                : Thu May 25 2003
    copyright            : (C) 2003-2007 CEA and Universite Paris Sud
    authors              : Gilles Mouchard, Yves Lhuillier
    email                : gilles.mouchard@cea.fr, yves.lhuillier@cea.fr
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2        *
 *   as published by the Free Software Foundation.                         *
 *                                                                         *
 ***************************************************************************/

#ifndef __FWD_HH__
#define __FWD_HH__

struct SourceCode_t;
struct Operation_t;
struct ActionProto_t;
struct Comment_t;
struct CodePair_t;
struct Action_t;
struct Group_t;
struct Variable_t;
struct Inheritance_t;
struct BitField_t;
struct Isa;
struct Product_t;
struct ConstStr_t;
struct Generator;
struct Product_t;
struct SDClass_t;
struct SDInstance_t;
struct Specialization_t;
struct Constraint_t;
template <class Type_t> struct Vect_t;
struct StringVect_t;
namespace Str { struct Buf; }

#endif // __FWD_HH__
