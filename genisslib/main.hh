/***************************************************************************
                                 main.hh
                             -------------------
    begin                : Thu May 25 2003
    copyright            : (C) 2003-2007 CEA and Universite Paris Sud
    authors              : Gilles Mouchard, Yves Lhuillier
    email                : gilles.mouchard@cea.fr, yves.lhuillier@cea.fr
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License version 2        *
 *   as published by the Free Software Foundation.                         *
 *                                                                         *
 ***************************************************************************/

#ifndef __MAIN_HH__
#define __MAIN_HH__

#define GENISSLIB "GenISSLib"
#define GENISSLIB_VERSION "2.1"
#define AUTHORS "Gilles Mouchard, Yves Lhuillier"
#define EMAILS "gilles.mouchard@cea.fr, yves.lhuillier@cea.fr"
#define COPYRIGHT "Copyright (c) 2003-2007 CEA and Universite Paris Sud"

struct Opts {
  Opts();
  
  char const*      outputprefix;
  char const*      expandname;
  char const*      inputname;
  char const*      depfilename;
  unsigned int     minwordsize;
  bool             sourcelines;
  bool             specialization;

  static Opts&     shared();
  
  static Opts*     s_shared;
};

#endif // __MAIN_HH__
